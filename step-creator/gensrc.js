let util = require("util")
let fs = require("fs")
let allData = require("./content.js") //JSON.parse(fs.readFileSync(__dirname + "/content.json"))
let path = require("path")
let projdir = path.normalize(__dirname + "/../frontend/src/step-narrations")
let imports = []
let str = ""
var execute = function (command, callback) {
  require("child_process").exec(command, { maxBuffer: 1024 * 500 }, function (error, stdout, stderr) {
    callback(error, stdout)
  })
}
const exec = util.promisify(execute)
let run = async () => {
  for (let i = 0; i < allData.length; i++) {
    let name = allData[i].name
    let text = allData[i].text
    imports.push(`import ${name}_snd_ from "./${name}.mp3"`)
    str += `export const ${name} = {name: "${name}", text:"${text}", sndFile: ${name}_snd_}\n`
    await exec(`sox ${projdir}/${name}.wav  ${projdir}/${name}.mp3`)
  }
  let result = imports.join("\n") + "\n" + str

  fs.writeFileSync(projdir + "/step-by-step-data.js", result)
}
run()
