module.exports = [
  {
    name: "NONLOCAL_IN_CONTEXT",
    text: "We mark the variable as nonlocal in the context",
  },
  {
    name: "GLOBAL_IN_CONTEXT",
    text: "We mark the variable as global in the context",
  },
  {
    name: "LOOKUP_VAR_INTRO",
    text: "We need to evaluate this variable.",
  },
  {
    name: "VAR_LOOKUP_HIGHLIGHT",
    text: "The variable is in the frame.",
  },
  {
    name: "LOOKUP_PARENT_FRAME",
    text: "The variable is not in that frame, so we look in the parent frame.",
  },
  {
    name: "LOOKUP_CURRENT_FRAME",
    text: "We first try to find the variable in the current frame.",
  },
  {
    name: "HIGHLIGHT_NONLOCAL",
    text: "It is a nonlocal variable, so we look for it in an ancestor of the current frame.",
  },
  {
    name: "EMPHASIZE_CLASSFRAME",
    text:
      "Since the current frame is a class frame, it cannot be in the environment of a function object. Therefore, we take the current frame's parent as the environment of the function object.",
  },
  {
    name: "ADD_TO_FRAMEHEAVEN",
    text:
      "We cannot destroy the frame. This is because the frame is in the environment of some function object. When that function gets called, its body may refer to variables in that frame, so we place the frame in frame heaven instead of destroying it.",
  },
  {
    name: "HIGHLIGHT_FUNCTION_CALL",
    text:
      "We have to find the function object because it contains two important pieces of information: the function definition line and the environment.",
  },

  {
    name: "PUSH_EXCEPT_CONTEXT",
    text:
      "The interpreter needs to keep track of what block to interpret if it encounters an exception while interpreting the try block, so it places the except marker in the context.",
  },
  {
    name: "EXCEPT_POP",
    text:
      "We have finished interpreting the try except statement, so we remove the except marker from the context.",
  },

  {
    name: "ADD_EXCEPT_MARKER",
    text:
      "We mark the beginning of the except block because this is where the interpreter will jump to if it encounters an exception while interpreting the try block.",
  },
  {
    name: "WHILE_ITERATE",
    text:
      "We finished interpreting the statement block and there is a while loop marker at the bottom of our context so the interpreter jumps back to the marker location.",
  },
  {
    name: "PUSH_WHILE_IN_CONTEXT",
    text:
      "The interpreter needs to keep track that it is currently interpreting a while statement so we place the while statement marker in the context.",
  },
  {
    name: "WHILE_LOOP_POP",
    text:
      "Since the condition expression evaluated to falsy, we have finished interpreting the while statement and we remove it from our context.",
  },

  {
    name: "ADD_WHILE_MARKER",
    text:
      "We mark the beginning of the while statement because the interpreter will jump back to this line after the statement block has finished being interpreted.",
  },

  {
    name: "RESTRICTION_VIOLATED",
    text: "You used something you're not allowed to use. Refer to the box on the top left for more details.",
  },
  {
    name: "TOKEN_QUOTA",
    text: "There are more tokens than are allowed. Refer to the box on the top left for more details.",
  },
  {
    name: "INTERPRETATION_STARTS",
    text: "The interpreter starts at the first line.",
  },
  {
    name: "HIDE_EXTERNAL",
    text: "We hide the external variables in the global frame to save space in the interface.",
  },
  {
    name: "WHILE_COND_RESET",
    text: "The condition expression is reset to its original state.",
  },
  {
    name: "UPDATE_NONLOCAL_ROW",
    text: "We update the value of the variable with the value on the right hand side of the equal symbol.",
  },
  {
    name: "TURTLE_CIRCLE",
    text: "The turtle draws an arc",
  },
  {
    name: "SUPERCLASS_LIST_RESET",
    text: "The superclass expressions are reset to their original states.",
  },

  {
    name: "RUNTIME_ERROR",
    text:
      "A runtime error has occurred. A description of the error is displayed on the top left corner of the screen.",
  },
  {
    name: "RESTORED_ORIGINAL_STATE_EXCEPT",
    text: "We clear the contexts that were created after the try block started being interpreted.",
  },
  {
    name: "RESET_FOR_LOOP",
    text: "The expression before the colon is reset to its original state.",
  },
  {
    name: "RESET_ELIF",
    text: "The condition expression is reset to its original state.",
  },

  {
    name: "REINTEGRATE_EXPRESSION",
    text: "We replace the line with the computed value of the expression.",
  },

  {
    name: "PUSHED_CONTEXT",
    text: "We add a new context which contains a frame to store our local variables and their values.",
  },
  {
    name: "POPULATE_EXTERNAL",
    text: "We add the variables of the package to the global frame.",
  },
  {
    name: "POPPED_CONTEXT_NO_HEAVEN",
    text:
      "The body of the function has finished being interpreted so the local variables are no longer accessible so we destroy the current context.",
  },

  {
    name: "POPPED_CONTEXT",
    text: "The body of the function has finished being interpreted, so we remove the context.",
  },
  {
    name: "PENDOWN",
    text: "The pen is placed on the paper. The turtle is drawing again.",
  },

  {
    name: "OR_REDUCTION",
    text:
      "The expression on the left of the or operator evaluated to falsy, so the expression is replaced with what is on the right hand side of the or expression",
  },
  {
    name: "NO_RETURN",
    text:
      "The function did not return, so the return value is None. The hole symbol is replaced with this value.",
  },

  {
    name: "NEW_DICT",
    text: "A new dictionary is created in the heap.",
  },

  {
    name: "METHODS_CREATED",
    text: "For every attribute that contains a function reference, we create a method object.",
  },
  {
    name: "LIST_REF",
    text: "The list literal expression is replaced with a reference to the list in the heap.",
  },

  {
    name: "LINEMARKER_ELIF",
    text: "We move the line marker.",
  },

  {
    name: "INSTANCE_CREATED",
    text: "An instance object is created in the heap.",
  },

  {
    name: "HIGHLIGHT_LIST_LOOKUP",
    text: "Using the reference, we find the object in the heap. It is a list",
  },
  {
    name: "HIGHLIGHT_GLOBAL_ROW",
    text: "We find the variable.",
  },

  {
    name: "FOR_NEXT_ELEM",
    text: "The variable moves on to the next element in the list.",
  },
  {
    name: "DICT_REF",
    text: "The dictionary literal is replaced with a reference to the dictionary object in the heap.",
  },

  {
    name: "LIST_APPEND",
    text: "We append an element to a list object and thereby increase the list's size.",
  },

  {
    name: "LIST_POP_NOARG",
    text: "We pop the last element of the list.",
  },

  {
    name: "LIST_POP_ARG",
    text: "We pop an element at a specific index of the list.",
  },

  {
    name: "RANGE_LIST_OBJECT_CREATED",
    text: "range creates a list and returns a reference to that list.",
  },

  {
    name: "DREW_TOO_MUCH",
    text: "Too much was drawn.",
  },

  {
    name: "DREW_TOO_LITTLE",
    text: "You're missing elements from your drawing.",
  },

  {
    name: "INCORRECT_DRAWING",
    text: "Your drawing is incorrect.",
  },

  {
    name: "MY_BAD",
    text: "Jacques made a mistake. Please contact Jacques to tell him.",
  },

  {
    name: "TURTLE_FORWARD",
    text: "The turtle moves forward.",
  },

  {
    name: "TURTLE_RIGHT",
    text: "The turtle turns right.",
  },

  {
    name: "TURTLE_LEFT",
    text: "The turtle turns left.",
  },

  {
    name: "PENUP",
    text: "The pen is lifted. The turtle is no longer drawing.",
  },

  {
    name: "GOBACK_MARKER_GUTTER",
    text:
      "We mark the line that we are isolating with an identifier so that we can jump back to it once the expression is evaluated.",
  },

  {
    name: "HIGHLIGHT_RETURN",
    text: "We look for the identifier that corresponds to our line.",
  },

  {
    name: "HIGHLIGHT_LIST_OBJECT",
    text: "We look in the heap for the object at the reference's address. It is a list object.",
  },

  {
    name: "HIGHLIGHT_LIST_INDEX",
    text: "We look for the index indicated by the value inside the brackets.",
  },

  {
    name: "HIGHLIGHT_DICT_OBJECT",
    text: "We look in the heap for the object at the reference's address. It is a dictionary object.",
  },

  {
    name: "HIGHLIGHT_DICT_KEY",
    text: "We look for the key equal to the value inside the brackets.",
  },

  {
    name: "HIGHLIGHT_VAR_IN_FRAME",
    text: "We look for the variable in the frame.",
  },

  {
    name: "REPLACE_VAR",
    text:
      "We replace the value of the variable in the frame with the value on the right hand side of the equal sign in our statement.",
  },

  {
    name: "PLACE_VAR",
    text: "We add the variable to the current frame.",
  },

  {
    name: "NEXT_LINE",
    text: "The next line is interpreted",
  },

  {
    name: "FILL_HOLE_NONE",
    text:
      "Since the return statement does not have an expression, it fills the hole that was previously created with the value None.",
  },

  {
    name: "FILL_HOLE_EXP",
    text:
      "The hole that was previously created is replaced with the value to the right of the return keyword.",
  },

  {
    name: "HIGHLIGHT_NONLOCAL_ROW",
    text: "We find the variable.",
  },

  {
    name: "HIGHLIGHT_GLOBAL",
    text: "It is a global variable, so we look for it in the global frame.",
  },

  {
    name: "UPDATE_GLOBAL_ROW",
    text: "We update the value of the variable with the value on the right hand side of the equal symbol.",
  },

  {
    name: "LIST_UPDATED",
    text: "The list has been updated.",
  },

  {
    name: "RESET_STATEMENT",
    text: "The statement is reset to its original state.",
  },

  {
    name: "ISOLATED_SO_RESET_EXPR",
    text: "The expression was isolated so it is reset in the sourcecode.",
  },

  {
    name: "ATTRIBUTE_UPDATED",
    text: "The value of the attribute is updated.",
  },

  {
    name: "CREATE_CLASSFRAME",
    text:
      "We create a frame. In the future, the variables in this frame will become attributes of a class object in the heap.",
  },

  {
    name: "CLASS_IN_HEAP_CREATED",
    text: "We create a class object in the heap.",
  },

  {
    name: "CLASS_CONTEXT_POPPED",
    text: "The class context is no longer needed, so it is destroyed.",
  },

  {
    name: "CLASSDEF_REF_FRAME",
    text:
      "A variable is added to the current frame. The value of that variable is a reference to the class object.",
  },

  {
    name: "ADD_FUNCDEF_MARKER",
    text:
      "A marker is added to the function definition line. That marker will be used to associate function objects with the function definition.",
  },

  {
    name: "CREATE_FUNCTION",
    text: "A function object is created.",
  },

  {
    name: "FUNCDEF_POPULATE_FRAME",
    text:
      "A variable with the funtion's name is added to the current frame. The value of that variable is a reference to the function object.",
  },

  {
    name: "FOR_ITERATOR_CONTEXT",
    text:
      "The list and the variable are copied into the current context to keep track of the value of the variable.",
  },

  {
    name: "FOR_LOOP_POP",
    text: "The for loop is done so we no longer need the list in the context.",
  },

  {
    name: "EXTERNAL_FUNC_FINISHED",
    text: "We replace the function call with the return value.",
  },

  {
    name: "DETACH_EXPR",
    text: "We will move the expression to another part of our interface so that we can focus on it.",
  },

  {
    name: "HIGHLIGHT_METHOD_CALL",
    text: "We're calling a method, so there is an implicit first argument to the function call.",
  },

  {
    name: "HIGHLIGHT_FUNCALL_LINE",
    text:
      "According to our function object, this is the function definition line, which determines the parameters and the statements to be interpreted.",
  },

  {
    name: "MAKE_HOLE",
    text: "This newly created hole symbol will be replaced with the function call return value.",
  },

  {
    name: "BINARY_DONE",
    text: "The binary expression is evaluated",
  },

  {
    name: "AND_REDUCTION",
    text:
      "The expression on the left of the and operator evaluated to truthy, so the expression is replaced with what is on the right hand side of the or expression",
  },

  {
    name: "OBJECT_REF",
    text: "The expression is replaced by a reference to the object in the heap.",
  },

  {
    name: "POPULATE_ONE_LEVEL",
    text: "The superclass attributes are copied to the new object.",
  },

  {
    name: "METHODS_REFS_CREATED",
    text:
      "For every attribute that contains a function reference, we replace its value with the corresponding method reference.",
  },

  {
    name: "NEW_LIST",
    text: "A list object is created in the heap.",
  },

  {
    name: "HIGHLIGHT_LIST_LOOKUP_INNER",
    text: "Using the index inside the brackets, we locate the element of the list that we're retrieving.",
  },

  {
    name: "BRACKET_LOOKUP_DONE",
    text: "We replace the bracket expression with the value in the list object.",
  },

  {
    name: "HIGHLIGHT_DICT_LOOKUP",
    text: "Using the reference, we find the object in the heap. It is a dictionary object",
  },
  {
    name: "HIGHLIGHT_DICT_LOOKUP_INNER",
    text:
      "Using the value inside the brackets, we locate the element of the dictionary that we're retrieving.",
  },

  {
    name: "DICT_BRACKET_LOOKUP_DONE",
    text: "We replace the bracket expression with the value in the dictionary object.",
  },

  {
    name: "HIGHLIGHT_ATTRIBUTE_LOOKUP",
    text: "Using the reference, we find the object in the heap.",
  },

  {
    name: "HIGHLIGHT_ATTRIBUTE_LOOKUP_NAME",
    text: "Using the identifier beside the dot, we locate the attribute of the object that we're retrieving.",
  },

  {
    name: "ATTRIBUTE_LOOKUP_DONE",
    text: "We replace the bracket expression with the value in the instance object.",
  },

  {
    name: "TERNARY_COND",
    text: "We evaluate the condition expression of the ternary operator.",
  },

  {
    name: "TERNARY_TRUE",
    text: "The condition expression evaluated to truthy, so we only evaluate the middle expression.",
  },

  {
    name: "TERNARY_FALSE",
    text: "The condition expression evaluated to false, so we only evaluate the last expression.",
  },

  {
    name: "VAR_LOOKUP",
    text: "We replace the variable with its associated value in the frame.",
  },

  {
    name: "VAR_LOOKUP_EXTERNAL",
    text: "We replace the variable with its associated value.",
  },

  {
    name: "PAREN",
    text: "The value inside the parentheses is the value that we're computing.",
  },

  {
    name: "NOT_EXPR",
    text: "We apply the not operator, which evaluates falsy values to true and truthy values to false.",
  },
]
