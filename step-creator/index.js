let fs = require("fs")
const ffprobe = require("ffprobe")
let ffprobeStatic = require("ffprobe-static")
let path = require("path")
let projdir = path.normalize(__dirname + "/../frontend/src/step-narrations")

const player = require("node-wav-player")
var keypress = require("keypress")
let util = require("util")

let durationToTimestamp = (duration) => {
  let minutes = 0
  while (duration > 60) {
    minutes++
    duration -= 60
  }
  minutes = ("" + minutes).padStart(2, "0")
  return `00:${minutes}:${duration.toFixed(3)}`
}

let audioFileDuration = async (path) => {
  let ret = await new Promise((res, rej) =>
    ffprobe(path, { path: ffprobeStatic.path }, function (err, info) {
      if (err) {
        rej(err)
        return
      }
      res(parseFloat(info.streams[0].duration))
    })
  )
  if (!ret || isNaN(ret)) {
    console.log("no duration")
    throw new Error("unexpected duration")
  }
  return ret
}

var execute = function (command, callback) {
  require("child_process").exec(command, { maxBuffer: 1024 * 500 }, function (error, stdout, stderr) {
    callback(error, stdout)
  })
}
const exec = util.promisify(execute)
keypress(process.stdin)

let allData = require("./content.js") //JSON.parse(fs.readFileSync(__dirname + "/content.json"))
let soundlist = allData
// Import module.
const AudioRecorder = require("node-audiorecorder")
let audioRecorder = undefined
let playing = false
let wavplaying = false
let out = undefined
let card = undefined
let device = undefined
// Options is an optional parameter for the constructor call.
// If an option is not given the default value, as seen below, will be used.
let applyCmd = async (fileName, f) => {
  let root = fileName.split(".")[0]
  let ext = fileName.split(".")[1]
  let tmp = root + "____tmp." + ext
  await exec(f(fileName, tmp))
  await exec(`mv -f ${tmp} ${fileName}`)
}

let options = () => {
  return {
    program: `arecord`, // Which program to use, either `arecord`, `rec`, or `sox`.
    device: `hw:${card},${device}`, // Recording device to use. (only for `arecord`)
    bits: 16, // Sample size. (only for `rec` and `sox`)
    channels: 2, // Channel count.
    encoding: `signed-integer`, // Encoding type. (only for `rec` and `sox`)
    format: `S16_LE`, // Encoding type. (only for `arecord`)
    // rate: 16000,        // Sample rate.
    rate: 44100,
    type: "wav",
    //type: `wav`, // Format type.

    // Following options only available when using `rec` or `sox`.
    silence: 2, // Duration of silence in seconds before it stops recording.
    thresholdStart: 0.5, // Silence threshold to start recording.
    thresholdStop: 0.5, // Silence threshold to stop recording.
    keepSilence: false, // Keep the silence in the recording.
  }
}

let recordBackground = () => {
  const logger = console
  let fileName = `${__dirname}/tmp/background.wav`
  out = fs.createWriteStream(fileName)

  out.on("finish", async () => {
    let endTime = await audioFileDuration(fileName)
    if (endTime > 1) endTime -= 0.3
    //endTime = Math.max(0, endTime)
    let endTimestamp = durationToTimestamp(endTime)
    await applyCmd(
      fileName,
      (pathIn, pathOut) => `ffmpeg -y -i ${pathIn} -ss 00:00:00 -to ${endTimestamp} -c:a copy ${pathOut}`
    )
    await exec(`sox ${fileName} -n noiseprof ${__dirname}/tmp/noise.prof`)
    console.log(`${fileName} written`)
  })

  // Create an instance.
  audioRecorder = new AudioRecorder(options(), logger)
  audioRecorder.start()
  audioRecorder.stream().pipe(out)
  audioRecorder.stream().on("close", () => {
    out.end()
  })
  playing = true
}

let record = (fileRoot, highpitch) => {
  const logger = console
  let fileName = fileRoot + ".wav"
  out = fs.createWriteStream(fileName)

  out.on("finish", async () => {
    let endTime = await audioFileDuration(fileName)
    if (endTime > 1) endTime -= 0.6
    //endTime = Math.max(0, endTime)
    let endTimestamp = durationToTimestamp(endTime)
    await applyCmd(
      fileName,
      (pathIn, pathOut) => `ffmpeg -y -i ${pathIn} -ss 00:00:00 -to ${endTimestamp} -c:a copy ${pathOut}`
    )
    await applyCmd(
      fileName,
      (pathIn, pathOut) => `ffmpeg -y -i ${pathIn} -af "lowpass=10000,highpass=50" ${pathOut}`
    )

    await applyCmd(
      fileName,
      (pathIn, pathOut) => `sox ${pathIn} ${pathOut} noisered ${__dirname}/tmp/noise.prof 0.21`
    )

    // await applyCmd(fileName,
    //     (pathIn, pathOut) =>
    //         `ffmpeg -y -i ${pathIn} -af "firequalizer=gain_entry='entry(136,0);entry(141, -5);entry(146, 0);entry(390,0);entry(400,-5);entry(405,0)'" ${pathOut}`)
    // `ffmpeg -y -i ${pathIn} -af "firequalizer=gain_entry='entry(110,0);entry(120, -5);entry(130, 0);entry(280,0);entry(290,-5);entry(295,0);entry(4900, 0);entry(4950, -5);entry(5000, 0)'" ${pathOut}`)

    await applyCmd(
      fileName,
      (pathIn, pathOut) =>
        `ffmpeg -y -i ${pathIn} -af "compand=attacks=0:points=-60/-55|-18/-10|0/-5" ${pathOut}`
    )

    // await applyCmd(fileName,
    //     (pathIn, pathOut) =>
    //         `ffmpeg -y -i ${pathIn} -af "firequalizer=gain_entry='entry(30,2);entry(50,4);entry(70,6);entry(100,4);entry(200,2);entry(400, 0);entry(4000,-1);entry(5000, -2);entry(5500, -3);entry(6000, -2);entry(7000, 0)'" ${pathOut}`)

    // await applyCmd(fileName,
    //     (pathIn, pathOut) =>
    //         `sox ${pathIn} ${pathOut} gain -n 0`)

    // await applyCmd(fileName,
    //     (pathIn, pathOut) =>
    //         `ffmpeg -y -i ${pathIn} -af "loudnorm=I=-24:TP=0:LRA=7" ${pathOut}`)

    //        let cmd = `ffmpeg -y -i ${tmpFileName} -ss 00:00:00 -to ${endTimestamp} -c:a copy ${finalFilename}`
    // ffmpeg -y -i compressed.wav -af "lowpass=10000,highpass=50" eqcompressed.wav
    //
    // sox noise-audio.wav -n noiseprof noise.prof
    // sox audio.wav audio-clean.wav noisered noise.prof 0.21
    //let cmd2 = `ffmpeg -y -i ${tmpFileName2} -af "compand=attacks=0:points=-80/-80|-60/-45|-30/-10|0/0" ${finalFilename}`

    //      await exec(cmd)
    //await exec(dm2)
    //    console.log(`written: ${finalFilename} `)
    //  await exec(`rm ${tmpFileName}`)
    console.log(`${fileName} written`)
  })

  // Create an instance.
  audioRecorder = new AudioRecorder(options(), logger)
  audioRecorder.start()
  audioRecorder.stream().pipe(out)
  audioRecorder.stream().on("close", () => {
    out.end()
  })
  playing = true
}
let stop = () => {
  playing = false

  audioRecorder.stop()
}
let index = -1
process.stdin.on("keypress", function (ch, key) {
  if (key && key.ctrl && key.name == "c") {
    process.exit(0)
  }
  if (key && key.name == "left" && !playing) {
    index--
    if (index < -1) index = -1
    if (index === -1) console.log("***background")
    else console.log(soundlist[index].text)
  }
  if (key && key.name == "right" && !playing) {
    index++
    if (index >= soundlist.length) index = soundlist.length - 1
    if (index === -1) console.log("***background")
    else console.log(soundlist[index].text)
  }
  if (key && key.name == "s") {
    setTimeout(() => {
      if (!playing) {
        if (index === -1) recordBackground()
        else record(projdir + "/" + soundlist[index].name)
      } else {
        stop()
      }
    }, 250)
  }

  if (key && key.name == "a") {
    setTimeout(() => {
      if (!playing) {
        if (index === -1) recordBackground()
        else record(projdir + "/" + soundlist[index].name, true)
      } else {
        stop()
      }
    }, 250)
  }

  if (key && key.name == "space") {
    if (index === -1) return
    if (wavplaying) {
      player.stop()
      wavplaying = false
      return
    }
    wavplaying = true

    player
      .play({
        path: projdir + "/" + soundlist[index].name + ".wav",
        sync: true,
      })
      .then(() => {
        wavplaying = false
      })
  }
})
console.log(index === -1 ? "***background" : soundlist[index].text)

process.stdin.setRawMode(true)
process.stdin.resume()

let setCardDevice = async () => {
  let foo = await exec("arecord -l")
  let lines = foo.split("\n")
  lines = lines.filter((x) => x.includes("Yeti Stereo Microphone"))
  if (lines.length !== 1) throw new Error("huh")
  let line = lines[0]
  let parts = line.split(" ")
  card = parseInt(parts[1])
  device = parseInt(parts[7])
  if (isNaN(card) || isNaN(device)) throw new Error("whaaat")
}
setCardDevice()
