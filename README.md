# Summary

Code for https://foundationsofpython.com

# Directories of interest

- [frontend](frontend)
    - All the frontend code, including the Python interpreter
- [step-creator](step-creator)
    - Audio recording tool to record audio for every action that an interpreter can take
- [upload-server](upload-server)
    - HTTP server so I can upload images when I'm creating the tutorials