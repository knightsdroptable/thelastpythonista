import { store } from "./store.js"
import { useSelector } from "react-redux"
export let colorModalBlanket = "rgba(250,250,250,0.8)"
export let colorDarkened = "#aaa"
export let useColorBackground = () => {
  let tutorialFocus = useSelector((s) => s.tutorialFocus)
  let currentView = useSelector((s) => s.currentView)
  let tutorialDescriptions = useSelector((s) => s.content.tutorialDescriptions)
  let state = store.getState()
  let getDarkened = () => {
    //    if (!tutorialDescriptions?.[currentView.exercise]) debugger
    return (
      tutorialFocus?.type === "segment" &&
      currentView?.exercise &&
      state.content.segmentDescriptions[tutorialFocus.id]?.darken
    )
  }
  let darkened = getDarkened()

  return darkened ? colorDarkened : "#fafafa"
}
export let colorBlack = "black"
export let colorError = "#F08080"
export let colorSelectedInMenu = "#89cff0"
export let colorBreakpoint = "rgba(255,0,0,0.5)"
export let colorEditCode = "rgba(255,182,193,0.5)"
export let colorStartingCode = "rgba(175, 238, 238,0.5)"
