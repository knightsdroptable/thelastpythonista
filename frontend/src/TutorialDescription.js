import tooltipAttributes from "./tooltip-attributes"

import { adminTutorialSeekToRow } from "./tutorial-events"
import * as R from "ramda"
import React, { useState } from "react"
import produce from "immer"
import { useSelector } from "react-redux"
import { dispatch } from "./store"
import { MicSelect, recordAudio } from "./record-audio"
import { drive } from "./drive"
import { hibernate, wakeup } from "./hibernate"
import {
  tutorialEventsData,
  adminPlayTutorialFromBeginningButton,
  adminPlayTutorialFromSelectedButton,
  stopTutorialButton,
} from "./tutorial-events"

let cellStyle = {
  height: "30px",
  minHeight: "30px",
  minWidth: "30px",
}

function ScriptImportWindow() {
  const [state, setState] = useState({ scriptEntered: "" })

  return (
    <div
      style={{
        position: "fixed",
        top: "0px",
        left: "0px",
        bottom: "0px",
        right: "0px",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <div style={{ height: "500px", width: "500px", backgroundColor: "white" }}>
        <textarea
          onChange={(evt) => {
            let txt = evt.target.value
            setState((state) =>
              produce(state, (state) => {
                state.scriptEntered = txt
              })
            )
          }}
          cols={50}
          rows={50}
        />
        <button
          onClick={() => {
            dispatch({ type: "populate-script", text: state.scriptEntered })
            dispatch({ type: "set-state-prop", name: "scriptImportWindow", value: false })
          }}
        >
          submit
        </button>
        <button
          onClick={() => {
            dispatch({ type: "set-state-prop", name: "scriptImportWindow", value: false })
          }}
        >
          close
        </button>
      </div>
    </div>
  )
}

function Choices({ value, choices, onSelect }) {
  return (
    <select
      style={{ border: "none" }}
      value={value}
      onChange={(x) => {
        onSelect(x.target.value)
      }}
    >
      {choices.map((c) => {
        return (
          <option value={c.id}>{c.label}</option>

          // <div>
          //   {c.label}
          //   <input
          //     type="radio"
          //     onChange={() => {}}
          //     checked={c.id === value}
          //     onClick={() => {
          //       onSelect(c.id)
          //     }}
          //   />
          // </div>
        )
      })}
    </select>
  )
}

let selectEventType = (eventDetails) => {
  let detailsOfType = (type) => {
    let d = tutorialEventsData.find((x) => x.id === type)

    if (!d) throw new Error("unable to find data for " + type)
    if (d.inputView) return d.inputView(eventDetails)
    else return d.name
  }
  return (
    <>
      <Choices
        choices={tutorialEventsData.map((x) => ({ label: x.name, id: x.id }))}
        value={eventDetails.type}
        onSelect={(choice) => {
          dispatch({ type: "change-event-type", id: eventDetails.id, choice })
        }}
      />
      {/* {detailsOfType(eventDetails.type)} */}
    </>
  )
}

function ViewOfEventAtIndex({ i, eventId }) {
  let event = useSelector((s) => s.content.eventDescriptions[eventId])
  const currentBeingDraggedOver = useSelector((s) => s.draggingOverEventId === eventId)
  const stateDraggingEvent = useSelector((s) => s.draggingEvent)
  let focused = useSelector((s) => s.tutorialFocus?.type === "event" && s.tutorialFocus?.id === eventId)
  if (!event) {
    return (
      <td
        onClick={() => {
          dispatch({ type: "add-empty-event", row: i })
        }}
        style={cellStyle}
      >
        nothing
      </td>
    )
  }
  let d = tutorialEventsData?.find((x) => {
    if (!event) debugger
    return x.id === event.type
  })
  let onDragStart = () => {
    dispatch({ type: "move-event-drag-start", id: event.id })
  }
  let onDrop = () => {
    dispatch({ type: "event-drop", row: i })
  }
  //let focused = R.equals(focus, { type: "event", id: event.id })

  let header = (
    <div
      style={{ display: "inline-block", flex: 1, display: "flex" }}
      onDragOver={(e) => {
        if (stateDraggingEvent) {
          dispatch({ type: "dragging-over-event", eventId: event.id })
        }
        e.preventDefault()
      }}
      onDrop={onDrop}
    >
      <div
        style={{ display: "inline-block", width: "40px", backgroundColor: "#eee" }}
        draggable
        onDragStart={onDragStart}
      >
        ⋮
      </div>
      {selectEventType(event)}

      <div style={{ flex: 1 }}>{d.inputView ? d.inputView(event) : d.name}</div>
      {/* <span
        // onClick={() => {
        //   // view event details
        //   adminTutorialSeekToRow(i)

        //   dispatch({ type: "tutorial-focus-click", goal: { type: "event", id: eventId } })
        // }}
        ></span> */}
    </div>
  )
  //let focused = R.equals(focus, { type: "segment", id: eventId })

  return (
    <>
      <td
        style={{
          width: "100%",
          backgroundColor: focused ? "#ccc" : currentBeingDraggedOver ? "blue" : undefined,
        }}
        onClick={() => {
          dispatch({ type: "tutorial-focus-click", goal: { type: "event", id: eventId } })
          adminTutorialSeekToRow(i)
        }}
      >
        <div style={{ display: "flex" }}>{header}</div>
        {/* {selectEventType(event)} */}
      </td>
      <td>
        <div style={{ display: "flex" }}>
          <button
            onClick={() => {
              dispatch({ type: "del-event", row: i })
            }}
          >
            🗑
          </button>
          <button
            {...tooltipAttributes("add before", true)}
            onClick={() => {
              dispatch({ type: "add-event-before", row: i })
            }}
          >
            Add
          </button>
        </div>
      </td>
    </>
  )
}

let MemoViewOfEventAtIndex = React.memo(ViewOfEventAtIndex)

function GetToolsButtons() {
  const tutorialFocus = useSelector((s) => s.tutorialFocus)
  const segmentDescriptions = useSelector((s) => {
    return s.content.segmentDescriptions
  })
  const stateSegmentTool = useSelector((s) => s.segmentTool)
  if (tutorialFocus?.type !== "segment") return null
  if (!tutorialFocus.id) throw new Error("what")
  let segmentDetails = segmentDescriptions[tutorialFocus.id]
  let darkenBackground = segmentDetails?.darken
  return (
    <>
      <button
        {...tooltipAttributes("toggle darken", true)}
        style={{
          border: "1px solid",
          width: "30px",
          height: "30px",
        }}
        onClick={() => {
          dispatch({ type: "set-segment-darken", value: !darkenBackground, segmentId: tutorialFocus.id })
        }}
      >
        {darkenBackground ? "W" : "B"}
      </button>

      <button
        {...tooltipAttributes("fill orange", true)}
        style={{ backgroundColor: "orange", width: "30px", height: "30px" }}
        onClick={() => {
          dispatch({ type: "segment-tool-click", tool: "fill-orange" })
        }}
      >
        {stateSegmentTool === "fill-orange" ? "S" : " "}
      </button>
      <button
        {...tooltipAttributes("fill white", true)}
        style={{ backgroundColor: "white", width: "30px", height: "30px" }}
        onClick={() => {
          dispatch({ type: "segment-tool-click", tool: "fill-white" })
        }}
      >
        {stateSegmentTool === "fill-white" ? "S" : " "}
      </button>

      <button
        {...tooltipAttributes("add pointer", true)}
        style={{ width: "30px", height: "30px", backgroundColor: stateSegmentTool === "point" && "#bbb" }}
        onClick={() => {
          dispatch({ type: "segment-tool-click", tool: "point" })
        }}
      >
        👉
      </button>
      <button
        {...tooltipAttributes("set picture", true)}
        style={{ width: "30px", height: "30px" }}
        onClick={() => dispatch({ type: "picture-view-shown", shown: true })}
      >
        🖼️
      </button>
      <button
        {...tooltipAttributes("clear decorations", true)}
        style={{ width: "30px", height: "30px", backgroundColor: stateSegmentTool === "del" && "#bbb" }}
        onClick={() => {
          dispatch({ type: "clear-decorations", segmentId: tutorialFocus.id })
          //dispatch({ type: "segment-tool-click", tool: "clear-decorations", segmentId: tutorialFocus.id })
        }}
      >
        🌬
      </button>
      <button
        {...tooltipAttributes("delete segment", true)}
        style={{ width: "30px", height: "30px" }}
        onClick={() => {
          dispatch({ type: "del-segment", segmentId: tutorialFocus.id })
        }}
      >
        🗑
      </button>
    </>
  )
}

let MemoGetToolsButtons = React.memo(GetToolsButtons)

function TutorialDescriptionNeedsMemo() {
  const stateDraggingOverSegmentRow = useSelector((s) => s.draggingOverSegmentRow)
  const stateDraggingSegment = useSelector((s) => s.draggingSegment)
  const stateScriptImportWindow = useSelector((s) => s.scriptImportWindow)
  const pictureViewShown = useSelector((s) => s.pictureViewShown)
  const micSelectShown = useSelector((s) => s.micSelectShown)
  const tdesc = useSelector(
    (s) => s.currentView?.exercise && s.content.tutorialDescriptions?.[s.currentView.exercise]
  )
  let stateAdminMode = useSelector((s) => s.adminMode)
  let stateHideTutorialEditor = useSelector((s) => s.hideTutorialEditor)
  // const tutorialFocus = useSelector((s) => s.tutorialFocus)

  let focus = useSelector((s) => s.tutorialFocus)

  let focusedSegmentDetails =
    useSelector((s) => focus?.type === "segment" && s.content.segmentDescriptions[focus.id]) || {}
  let segmentFocused = useSelector((s) => s.tutorialFocus?.type === "segment")

  if (!tdesc) {
    if (!stateAdminMode) return null
    return (
      <button
        onClick={() => {
          dispatch({ type: "initialize-tutorial-description" })
        }}
      >
        Add tutorial description
      </button>
    )
  }

  // let stateAnimationFrames = undefined
  // let stateFrameIndex = undefined

  // if (!currentExercise) return null
  let trackGridData = []
  let addToTrackGrid = (row, column, e) => {
    if (!trackGridData[row]) trackGridData[row] = []
    trackGridData[row][column] = e
  }

  Object.values(tdesc.trackSegments).forEach((e) => {
    let focused = R.equals(focus, { type: "segment", id: e.id })
    if (e.from === e.to) {
      addToTrackGrid(e.from, e.trackIndex, { segmentId: e.id, type: "fromto", focused })
    } else {
      addToTrackGrid(e.from, e.trackIndex, { segmentId: e.id, type: "from", focused })
      addToTrackGrid(e.to, e.trackIndex, { segmentId: e.id, type: "to", focused })
    }
    for (let i = e.from + 1; i < e.to; i++) {
      addToTrackGrid(i, e.trackIndex, { segmentId: e.id, type: "filler", focused })
    }
  })

  //let numRows = 1 + Math.max(decorations.length, ...description.segments.map((x) => x.to))

  let getTrackData = (row, column) => {
    let r = trackGridData[row]
    if (!r) return undefined
    return r[column]
  }

  let cellContentsAtTrackPosition = (row, trackIndex) => {
    let highlightStyle = { backgroundColor: stateDraggingOverSegmentRow === row && "blue" }
    let d = getTrackData(row, trackIndex)
    let st = cellStyle
    let clickEmpty = () => {
      adminTutorialSeekToRow(row)

      dispatch({ type: "track-cell-clicked", row, trackIndex })
    }

    let onDrop = () => {
      dispatch({ type: "segment-drop", row, trackIndex })
    }

    if (!d)
      return (
        <td
          onDragOver={(e) => {
            if (stateDraggingSegment) dispatch({ type: "dragging-over-segment", row })
            e.preventDefault()
          }}
          onDrop={onDrop}
          style={{ ...st, ...highlightStyle }}
          onClick={clickEmpty}
        ></td>
      )
    let resize = (boxType) => {
      dispatch({ type: "resize-segment-drag-start", segmentId: d.segmentId, boxType })
    }

    let move = () => {
      dispatch({ type: "move-segment-drag-start", segmentId: d.segmentId, boxType: d.type })
    }

    let clickSegment = () => {
      let seg = trackGridData[row][trackIndex]
      if (!seg) {
        throw new Error("expected segment")
      }

      adminTutorialSeekToRow(row)

      dispatch({ type: "tutorial-focus-click", goal: { type: "segment", id: seg.segmentId } })
    }

    if (d.type === "filler")
      return (
        <td
          onDragOver={(e) => {
            if (stateDraggingSegment) dispatch({ type: "dragging-over-segment", row })
            e.preventDefault()
          }}
          onDrop={onDrop}
          onDragStart={move}
          onClick={clickSegment}
          draggable
          style={{
            ...st,
            backgroundColor: d.focused ? "#bbb" : highlightStyle.backgroundColor,
            textAlign: "center",
          }}
        >
          |{/* {d.focused ? "||" : "|"} */}
        </td>
      )

    let extremity = (c, boxType) => (
      <div
        className={"hover-highlight"}
        draggable
        onDragOver={(e) => {
          if (stateDraggingSegment) dispatch({ type: "dragging-over-segment", row })

          e.preventDefault()
        }}
        onDrop={onDrop}
        onDragStart={() => resize(boxType)}
        onClick={() => clickSegment}
        style={{ height: "15px", backgroundColor: d.focused && "#bbb" }}
      >
        {c}
      </div>
    )
    let body = (
      <div
        draggable
        onDragOver={(e) => {
          if (stateDraggingSegment) dispatch({ type: "dragging-over-segment", row })

          e.preventDefault()
        }}
        onDrop={onDrop}
        onDragStart={move}
        onClick={clickSegment}
        style={{ height: "15px", backgroundColor: d.focused && "#bbb" }}
      />
    )

    let inner = () => {
      if (d.type === "fromto") {
        return (
          <>
            {extremity("^", "from")}
            {body}
            {extremity("V", "to")}
          </>
        )
      }
      if (d.type === "from") {
        return (
          <>
            {extremity("^", "from")}
            {body}
          </>
        )
      }
      return (
        <>
          {body}
          {extremity("v", "to")}
        </>
      )
    }
    return (
      <td
        onDragOver={(e) => {
          if (stateDraggingSegment) dispatch({ type: "dragging-over-segment", row })

          e.preventDefault()
        }}
        style={{
          ...st,
          textAlign: "center",
          ...highlightStyle,
        }}
      >
        {inner()}
      </td>
    )
  }

  let pictureView = () => {
    return (
      <div
        style={{
          position: "fixed",
          top: "0px",
          left: "0px",
          right: "0px",
          bottom: "0px",
          backgroundColor: "white",
        }}
      >
        <form>
          <input
            type="file"
            accept={"image/png, image/jpeg"}
            onChange={async (evt) => {
              let file = evt.target.files[0]
              const data = new FormData()
              data.append("file", file)
              let response = await fetch("http://localhost:2777/upload-resource", {
                method: "POST",
                body: data,
              })
              let body = await response.text()
              let parsed = JSON.parse(body)
              if (!parsed.success) {
                console.log("fail")
                alert("fail to upload image " + parsed.reason)
                return
              }
              if (!parsed.fileName) throw new Error("what?")
              dispatch({ type: "resource-uploaded-to-segment", resourceLocation: parsed.fileName })
            }}
          />
        </form>
        <div>{focusedSegmentDetails.resourceLocation}</div>
        <button
          onClick={() => {
            dispatch({ type: "picture-view-shown", shown: false })
          }}
        >
          close
        </button>
      </div>
    )
  }

  // let endingAtIndex = (i) => {
  //   return (

  //   )
  // }

  let rows = []

  for (let i = 0; i < tdesc.numRows; i++) {
    let eventId = tdesc.eventOrder[i]
    let row = []
    for (let j = tdesc.numTracks - 1; j >= 0; j--) {
      row.push(cellContentsAtTrackPosition(i, j))
    }
    if (!segmentFocused) {
      row.push(<MemoViewOfEventAtIndex i={i} eventId={eventId} />)
      // row.push(endingAtIndex(i, eventId))
    }
    rows.push(row)
  }

  if (!stateAdminMode) return null
  //if (stateHideTutorialEditor) return null
  return (
    <>
      <div
        style={{
          opacity: stateHideTutorialEditor && 0,
          pointerEvents: stateHideTutorialEditor && "none",
          backgroundColor: "white",
          position: "absolute",
          top: "50px",
          left: "15px",
          bottom: "10px",
          // right: "10px",
          // bottom: "10px",

          // left: segmentFocused ? "10px" : undefined,
          // top: segmentFocused ? "10px" : undefined,

          // right: segmentFocused ? undefined : "300px",
          // bottom: segmentFocused ? undefined : "0px",
          zIndex: 40000000000,
        }}
      >
        {micSelectShown && (
          <MicSelect
            onClose={() => {
              dispatch({ type: "mic-select-shown", shown: false })
            }}
          />
        )}
        {stateScriptImportWindow && <ScriptImportWindow />}
        {pictureViewShown && pictureView()}
        <div style={{ minHeight: "50px", display: "flex", alignItems: "center" }}>
          {!segmentFocused && (
            <button
              onClick={() => {
                dispatch({ type: "add-tutorial-track" })
              }}
            >
              add track
            </button>
          )}
          <div>
            {!segmentFocused && (
              <button
                onClick={() => {
                  dispatch({ type: "mic-select-shown", shown: true })
                }}
              >
                Mic select
              </button>
            )}
          </div>
          <MemoGetToolsButtons />
        </div>
        {!segmentFocused && (
          <div>
            <button
              onClick={() => {
                adminPlayTutorialFromBeginningButton()
              }}
            >
              ▶ start
            </button>
            <button
              onClick={() => {
                adminPlayTutorialFromSelectedButton()
              }}
            >
              ▶ selected
            </button>
            <button
              onClick={() => {
                stopTutorialButton()
              }}
            >
              stop tut
            </button>
            <button
              onClick={() => {
                dispatch({ type: "set-state-prop", name: "scriptImportWindow", value: true })
              }}
            >
              import script{" "}
            </button>
          </div>
        )}
        <div style={{ overflowY: "scroll", display: "inline-block", maxHeight: "90%" }}>
          <table
            key="tutorial-description-window"
            style={{
              minWidth: segmentFocused ? undefined : "300px",

              borderCollapse: "collapse",
              backgroundColor: "white",
            }}
            border="1"
          >
            {rows.map((r) => (
              <tr>{r}</tr>
            ))}
          </table>
        </div>

        {!segmentFocused && (
          <div>
            <button
              onClick={() => {
                dispatch({ type: "add-tutorial-row" })
              }}
            >
              add row
            </button>
          </div>
        )}
      </div>
    </>
  )
}

export default React.memo(TutorialDescriptionNeedsMemo)
