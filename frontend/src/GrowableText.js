import React, { useEffect, useState, useRef } from "react"
import produce from "immer"
export default function GrowableText({ text }) {
  const [state, setState] = useState({
    calculatedWidth: undefined,
  })
  let innerRef = useRef(undefined)
  useEffect(() => {
    //if (state.calculatedWidth !== undefined) return
    let w = innerRef.current.clientWidth
    setState((state) =>
      produce(state, (state) => {
        state.calculatedWidth = w
      })
    )
  }, [text])
  // useEffect(() => {
  //   setState(state => produce(state, state => {
  //     state.calculatedWidth = undefined
  //   }))

  // }, [text])
  let style = {
    transition: "width 1s, opacity 1s",
    whiteSpace: "nowrap",
    display: "flex",
  }

  if (state.calculatedWidth) {
    style.width = state.calculatedWidth + "px"
  }

  return (
    <div style={style}>
      <div ref={innerRef}>{text}</div>
    </div>
  )
}
