import * as uniqid from "uniqid"
import {
  isLocal,
  sessionId,
  loadedFromLink,
  currentViewLocalStorageId,
  hardcodedVideos,
  overturemp3,
  explicitExId,
} from "./globals"
import {
  toggleDecorationToSegment,
  addTutorialRow,
  addTutorialTrack,
  addEventBefore,
  changeEventType,
  trackCellClicked,
  resizeSegmentDragStart,
  moveSegmentDragStart,
  moveEventDragStart,
  segmentDrop,
  eventDrop,
  delEvent,
  rowIsDeletable,
  delSegment,
} from "./tutorial-desc.js"
import md5 from "md5"
import { parsePython, unparse } from "./parse-python"
import interpretProgram from "./interpreter"

import {
  addChapterAtTop,
  setExerciseData,
  createNewChapter,
  createNewExercise,
  setTextSectionsOfExercise,
  findExercise,
  moveExerciseUp,
  moveExerciseDown,
  deleteExercise,
  addExerciseAfter,
  setExerciseName,
  moveChapterUp,
  moveChapterDown,
  deleteChapter,
  addChapterAfter,
  setChapterName,
  addExerciseAtTop,
} from "./content-manipulation"
import produce from "immer"

import { createStore } from "redux"
import { composeWithDevTools } from "redux-devtools-extension/developmentOnly"

import contentOnFile from "./content2.json"

import * as R from "ramda"
import { ConsoleErrorListener } from "antlr4ts"
import { Star_exprContext, SubscriptContext } from "python-ast"

// const syntaxErrorYoutubeID = "kcOV8S48VD4"
// const drawMismatchErrorYoutubeID = "kcOV8S48VD4"
// const drewTooLittleYoutubeID = "kcOV8S48VD4"
// const drewTooMuchYoutubeID = "kcOV8S48VD4"
// const runtimeErrorYoutubeID = "kcOV8S48VD4"

let indexOfFirstFrameWithToken = (tid, aframes) => {
  let isObject = (yourVariable) => typeof yourVariable === "object" && yourVariable !== null
  let doit = (x) => {
    if (isObject(x)) return doitObj(x)
    if (Array.isArray(x)) return doitArr(x)
    return false
  }
  let doitArr = (arr) => {
    for (let i = 0; i < arr.length; i++) {
      if (doit(arr[i])) return true
    }
    return false
  }
  let doitObj = (obj) => {
    if (obj.id === tid) return true
    let keys = Object.keys(obj)
    for (let i = 0; i < keys.length; i++) {
      if (doit(obj[keys[i]])) return true
    }
    return false
  }

  for (let i = 0; i < aframes.length; i++) {
    if (doit(aframes[i])) return i
  }
  return 0
  throw new Error("what")
}

let isObject = (yourVariable) => typeof yourVariable === "object" && yourVariable !== null

function download(filename, text) {
  var element = document.createElement("a")
  element.setAttribute("href", "data:text/plain;charset=utf-8," + encodeURIComponent(text))
  element.setAttribute("download", filename)

  element.style.display = "none"
  document.body.appendChild(element)

  element.click()

  document.body.removeChild(element)
}

let getSolutionWriteLines = (sections) => {
  let ret = []
  let current = 1
  for (let i = 0; i < sections.length; i++) {
    let section = sections[i]
    let arr = section.text.split("\n")
    for (let j = 0; j < arr.length; j++) {
      if (section.type === "read") {
      } else if (section.type === "write") {
        ret[current] = true
      } else {
        throw new Error("unknown section type:" + section.type)
      }
      current++
    }
  }
  return ret
}

let cloneButRemove = (input, ignoreList) => {
  let doit = (tree) => {
    if (Array.isArray(tree)) return doitArray(tree)
    else if (isObject(tree)) return doitObj(tree)
    else return tree
  }
  let doitObj = (tree) => {
    let ret = {}
    let keys = Object.keys(tree)
    keys = keys.filter((x) => !ignoreList.includes(x))
    for (let i = 0; i < keys.length; i++) {
      ret[keys[i]] = doit(tree[keys[i]])
    }
    return ret
  }
  let doitArray = (tree) => {
    let ret = []
    for (let i = 0; i < tree.length; i++) {
      ret.push(doit(tree[i]))
    }
    return ret
  }
  return doit(input)
}

let getPreviousExercise = (content, view) => {
  let getExerciseFromChapterIndex = (ind) => {
    while (ind >= 0) {
      let chid = content.chapterOrder[ind]
      let ch = content.chapters[chid]
      if (ch.exerciseOrder.length > 0) return { exercise: ch.exerciseOrder[ch.exerciseOrder.length - 1] }
      ind--
    }
    return view
  }

  if (view.chapter) {
    let chid = view.chapter
    let index = content.chapterOrder.findIndex((c) => c.id === chid)
    if (index === -1) return view
    return getExerciseFromChapterIndex(index - 1)
  }
  if (view.exercise) {
    let eid = view.exercise
    let chapter = Object.values(content.chapters).find((c) => c.exerciseOrder.includes(eid))
    if (!chapter) return view
    let exercises = chapter.exerciseOrder
    let ind = exercises.findIndex((x) => x === eid)
    let candidateIndex = ind - 1
    if (candidateIndex >= 0) {
      return { exercise: exercises[candidateIndex] }
    }
    let chapterIndex = content.chapterOrder.findIndex((c) => c === chapter.id)
    if (chapterIndex === -1) return view
    return getExerciseFromChapterIndex(chapterIndex - 1)
  }
}

export let getNextContentElement = (content, view) => {
  if (view.chapter) {
    let ch = content.chapters[view.chapter]
    if (ch.exerciseOrder.length > 0) return { exercise: ch.exerciseOrder[0] }
    return view
    // let index = content.chapterOrder.findIndex((c) => c.id === ch.id)
    // let nextIndex = index + 1
    // if (nextIndex < content.chapterOrder.length) {
    //   return { chapter: content.chapterOrder[nextIndex] }
    // }
    // return view
  }
  if (view.exercise) {
    let eid = view.exercise
    let chapter = Object.values(content.chapters).find((c) => c.exerciseOrder.includes(eid))
    if (!chapter) return view
    let exercises = chapter.exerciseOrder
    let ind = exercises.findIndex((x) => x === eid)
    let candidateIndex = ind + 1
    if (candidateIndex < exercises.length) {
      return { exercise: exercises[candidateIndex] }
    }
    let chapterIndex = content.chapterOrder.findIndex((c) => c === chapter.id)
    let nextChapterIndex = chapterIndex + 1
    if (nextChapterIndex < content.chapterOrder.length) {
      let chid = content.chapterOrder[nextChapterIndex]
      let ch = content.chapters[chid]
      if (ch.exerciseOrder.length > 0) return { exercise: ch.exerciseOrder[0] }
      return view
      //return { chapter: content.chapterOrder[nextChapterIndex] }
    }
    return view
  }
}

let getPreviousContentElement = (content, view) => {
  if (view.chapter) {
    let index = content.chapterOrder.findIndex((c) => c === view.chapter)
    if (index < 1) return view
    let previousChapterId = content.chapterOrder[index - 1]
    let previousChapter = content.chapters[previousChapterId]
    if (previousChapter.exerciseOrder.length === 0) return { chapter: previousChapterId }
    return {
      exercise: previousChapter.exerciseOrder[previousChapter.exerciseOrder.length - 1],
    }
  }
  if (view.exercise) {
    let eid = view.exercise
    let chapter = Object.values(content.chapters).find((c) => c.exerciseOrder.includes(eid))
    if (!chapter) return view
    let exercises = chapter.exerciseOrder
    let ind = exercises.findIndex((x) => x === eid)
    if (ind > 0) return { exercise: exercises[ind - 1] }
    return { chapter: chapter.id }
  }
}

let getNextExercise = (content, view) => {
  let getExerciseFromChapterIndex = (ind) => {
    while (ind < content.chapterOrder.length) {
      let chid = content.chapterOrder[ind]
      let ch = content.chapters[chid]
      if (ch.exerciseOrder.length > 0) return { exercise: ch.exerciseOrder[0] }
      ind++
    }
    return view
  }

  if (view.chapter) {
    let chid = view.chapter
    let index = content.chapterOrder.findIndex((c) => c.id === chid)
    if (index === -1) return view
    return getExerciseFromChapterIndex(index + 1)
  }
  if (view.exercise) {
    let eid = view.exercise
    let chapter = Object.values(content.chapters).find((c) => c.exerciseOrder.includes(eid))
    if (!chapter) return view
    let exercises = chapter.exerciseOrder
    let ind = exercises.findIndex((x) => x === eid)
    let candidateIndex = ind + 1
    if (candidateIndex < exercises.length) {
      return { exercise: exercises[candidateIndex] }
    }
    let chapterIndex = content.chapterOrder.findIndex((c) => c === chapter.id)
    if (chapterIndex === -1) return view
    return getExerciseFromChapterIndex(chapterIndex + 1)
  }
}

let clone = (x) => {
  return R.clone(x)
}

let extractUserTokens = (userInputtedLines, tokens) => {
  let ret = []
  for (let i = 0; i < tokens.length; i++) {
    if (userInputtedLines[tokens[i].lineNumber]) ret.push(tokens[i])
  }
  return ret
}

let userTextOfSections = (arr) => {
  let ret = arr.map(userTextOfSection)
  return ret.join("\n").trimRight()
}

let precomputesUserTextOfSections = (arr) => {
  let ret = arr.map(precomputesUserTextOfSection)
  return ret.join("\n").trimRight()
}

export let possibleRestrictions = [
  {
    label: "token quota",
    id: "token-quota",
  },
  {
    label: "numbers kinshi",
    id: "numbers-allowed",
    presenceId: "number",
    desc: "a number literal",
    instructions: "No number literals allowed",
  },
  {
    label: "strings kinshi",
    id: "strings-allowed",
    presenceId: "string",
    desc: "a string literal",
    instructions: "No string literals allowed",
  },
  {
    label: "booleans kinshi",
    id: "booleans-allowed",
    presenceId: "bool",
    desc: "a boolean literal",
    instructions: "No boolean literals allowed",
  },
  {
    label: "arraylit kinshi",
    id: "arraylit-allowed",
    presenceId: "list",
    desc: "a list literal",
    instructions: "No list literals allowed",
  },
  {
    label: "objlit kinshi",
    id: "objlit-allowed",
    presenceId: "dict",
    desc: "a dictionary literal",
    instructions: "No dictionary literals allowed",
  },
  {
    label: "modulo kinshi",
    id: "modulo-allowed",
    presenceId: "modulo",
    desc: "the modulo operator (%)",
    instructions: "No modulo operator (%) allowed",
  },
  {
    label: "while kinshi",
    id: "while-allowed",
    presenceId: "whilestatement",
    desc: "a while statement",
    instructions: "No while statements allowed",
  },
  {
    label: "for kinshi",
    id: "for-allowed",
    presenceId: "forstatement",
    desc: "a for statement",
    instructions: "No for statements allowed",
  },
]

let violatesRestrictions = (parseResult, restrictions) => {
  for (let i = 0; i < possibleRestrictions.length; i++) {
    let restriction = possibleRestrictions[i]
    let restrictionId = restriction.id
    // let data = possibleRestrictions[restrictionId]
    if (restrictions.includes(restrictionId) && parseResult.presence[restriction.presenceId]) {
      return { desc: "You are not allowed to use " + restriction.desc }
    }
  }
  return undefined
}

// export let isCompletedExercise = (ex) => {
//   return exercisesCompletedData.includes(ex)
// }
// let exercisesCompletedData = []

// let addToCompletedExercises = (ex) => {
//   if (!isCompletedExercise(ex)) exercisesCompletedData.push(ex)
// }
// export let numberOfCompletedExercises = () => {
//   return exercisesCompletedData.length
// }

let userTextOfSection = (x) => {
  let padWithNewlines = (str, n) => {
    let current = str.split("\n").length
    while (current < n) {
      str += "\n"
      current++
    }
    return str
  }
  return x.type === "read" ? x.text : padWithNewlines(x.userTyped || "", x.text.split("\n").length)
}

let precomputesUserTextOfSection = (x) => {
  return x.type === "read" ? x.text : x.startingText
}

let textOfSections = (arr) => {
  let ret = arr.map((x) => x.text)
  return ret.join("\n").trimRight()
}

let userInputtedLines = (sections) => {
  let ret = []
  let current = 1
  for (let i = 0; i < sections.length; i++) {
    let section = sections[i]
    let txt = section.type === "read" ? section.text : section.userTyped || ""
    let arr = txt.split("\n")
    for (let j = 0; j < arr.length; j++) {
      if (section.type === "read") {
      } else if (section.type === "write") {
        ret[current] = true
      } else {
        throw new Error("unknown section type:" + section.type)
      }
      current++
    }
  }
  return ret
}

let linesToBeFilledin = (sections) => {
  let ret = []
  let current = 1
  for (let i = 0; i < sections.length; i++) {
    let section = sections[i]
    let txt = section.text
    let arr = txt.split("\n")
    for (let j = 0; j < arr.length; j++) {
      if (section.type === "read") {
      } else if (section.type === "write") {
        ret[current] = true
      } else {
        throw new Error("unknown section type:" + section.type)
      }
      current++
    }
  }
  return ret
}

export let seenVideoHistory = [
  // {
  //   youtubeID: "Eq-eQKoEycM",
  //   descObj: { description: "Fundamentals - First step - Intro video" },
  // },
  // {
  //   youtubeID: "4fndeDfaWCg",
  //   descObj: { description: "Fundamentals - First step - Pressed stop" },
  // },
  // {
  //   youtubeID: "Eo-KmOd3i7s",
  //   descObj: { description: "Fundamentals - First step - Success" },
  // },
]

try {
  let candidate = JSON.parse(window.localStorage.getItem("__seen-videos"))
  if (Array.isArray(candidate)) seenVideoHistory = candidate
} catch (err) {}

export let addVideoPermanentHistory = (id, youtubeID, descObj) => {
  let alreadyThere = seenVideoHistory.find((e) => e.id === id)
  if (alreadyThere) return
  seenVideoHistory.push({ id, youtubeID, descObj })
  window.localStorage.setItem("__seen-videos", JSON.stringify(seenVideoHistory))
}

// let seenInyourfaceOneShot = (id) => {
//   let alreadyThere = seenVideoHistory.find((e) => e.id === id)
//   return !!alreadyThere
// }
let dstls = undefined
let delayedSaveToLocalStorage = (newContent, currentView) => {
  saveToLocalStorage(newContent, currentView)
  // if (dstls) {
  //   clearTimeout(dstls)
  //   dstls = undefined
  // }
  // let thunk = () => {
  //   dstls = undefined
  //   saveToLocalStorage(newContent, currentView)
  // }
  // dstls = setTimeout(thunk, 10 * 1000)
}

let reducer = (state, action) => {
  if (state.ignoreAll && !action.notIgnored) return state
  if (action.type === "stop-ignore-all") {
    return { ...state, ignoreAll: false }
  }

  let setAndSaveContent = (newContent) => {
    if (!newContent) {
      alert("cannot save, corrupted content")
      return
    }
    delayedSaveToLocalStorage(newContent, state.currentView)
    return produce(state, (state) => {
      state.content = newContent
    })
  }

  let onlyUpdateTutorialDesc = (newTdesc) => {
    let exerciseId = state.currentView?.exercise
    if (!exerciseId) throw new Error("huh")
    let newContent = produce(state.content, (content) => {
      content.tutorialDescriptions[exerciseId] = newTdesc
    })
    return newContent
  }

  let updateTutorialDesc = (newTdesc) => {
    return setAndSaveContent(onlyUpdateTutorialDesc(newTdesc))
  }

  let tdesc = () => {
    let exerciseId = state.currentView?.exercise
    if (!exerciseId) throw new Error("huh?")
    return state.content?.tutorialDescriptions?.[exerciseId] || emptyTutorialDescription()
  }

  if (action.type === "name-current") {
    let opt = action.opt

    if (!state.adminMode) return state
    let setE = (eid) => {
      let newname = window.prompt("name?")
      if (!newname) return
      let newContent = setExerciseName(state.content, eid, newname)
      return setAndSaveContent(newContent)
    }
    let setC = (cid) => {
      let newname = window.prompt("name?")
      if (!newname) return
      let newContent = setChapterName(state.content, cid, newname)
      return setAndSaveContent(newContent)
    }

    if (opt && opt.exerciseId) {
      return setE(opt.exerciseId)
    } else if (opt && opt.chapterId) {
      return setC(opt.chapterId)
    } else {
      let eid = state.currentView && state.currentView.exercise
      let cid = state.currentView && state.currentView.chapter

      if (eid) return setE(eid)
      else if (cid) return setC(cid)
    }
  }

  if (action.type === "delete-exercise") {
    if (!isPermitted(state)) return state
    window.alert("You just deleted, press F5 to undo")
    let newContent = deleteExercise(state.content, state.currentView.exercise)
    return setAndSaveContent(newContent)
  }
  if (action.type === "delete-chapter") {
    if (!isPermitted(state)) return state
    window.alert("press f5 to stop the deletion")
    let newContent = deleteChapter(state.content, state.currentView.chapter)
    return setAndSaveContent(newContent)
  }

  if (action.type === "set-current-view") {
    let ret = produce(state, (state) => {
      state.currentView = action.payload
    })
    saveToLocalStorage(ret.content, ret.currentView)
    return ret
  }
  if (action.type === "add-chapter-after") {
    if (!isPermitted(state)) return state
    let newContent = addChapterAfter(state.content, action.chapterId)
    return setAndSaveContent(newContent)
  }

  if (action.type === "add-exercise-at-top") {
    if (!isPermitted(state)) return state
    let newContent = addExerciseAtTop(state.content, action.chapterId)
    return setAndSaveContent(newContent)
  }

  if (action.type === "open-increment-num-completed") {
    return produce(state, (state) => {
      state.incrementNumCompleted = {
        num: (state.finishedExerciseIds.length || []) - 1,
      }
    })
  }

  if (action.type === "close-increment-num-completed") {
    return produce(state, (state) => {
      state.incrementNumCompleted = undefined
    })
  }

  if (action.type === "stop-button") {
    // let cid = state.currentView.exercise
    // let videoFirstStop = state.content.exercises[cid].videoFirstStop
    // if (!state.adminMode && videoFirstStop && videoFirstStop.trim().length > 0) {
    //   return {
    //     ...watchInyourfaceCurrentExercise(
    //       state,
    //       "stop-button-clicked",
    //       videoFirstStop,

    //       "Let's go",
    //       "Pressing the stop button",
    //       cid
    //     ),
    //     animationFrames: undefined,
    //     intervalLoop: undefined,
    //     initialDrawMode: false,
    //     initialDrawDone: true,
    //     showInitialDrawInstructions: false,
    //     showSmallTurtle: false,
    //     syntaxError: undefined,
    //   }
    // }
    return {
      ...state,
      initialDrawMode: false,
      animationFrames: undefined,
      initialDrawDone: true,
      showInitialDrawInstructions: false,
      showSmallTurtle: false,
      syntaxError: undefined,
      frameIndex: undefined,
      fakePressedPlay: false,
    }
  }

  if (action.type === "run-button") {
    delayedSaveToLocalStorage(state.content, state.currentView)
    let currentExercise = state.content.exercises[state.currentView.exercise]

    let userLines = userInputtedLines(currentExercise.textSections)
    let sourceInputted = state.adminMode
      ? textOfSections(currentExercise.textSections)
      : userTextOfSections(currentExercise.textSections)

    let parseResult = parsePython(sourceInputted, userLines)

    if (parseResult.hasError) {
      //let newState =
      return {
        ...state,
        syntaxError: {
          lineNum: parseResult.lineNum,
          errorMsg: parseResult.errorMsg,
          errorMsgToken: parseResult.errorMsgToken,
        },
      }
    }
    let ast = parseResult.ast

    let allTokens = parseResult.tokens
    let uTokens = extractUserTokens(userLines, allTokens)
    let numUserTokens = uTokens.length

    let expectedNumTokens = state.solutionNumTokens

    let startOfInterpretation = Date.now()
    let overNumTokens =
      currentExercise.restrictions.includes("token-quota") && numUserTokens > expectedNumTokens

    let restrictionViolated = violatesRestrictions(parseResult, currentExercise.restrictions)

    let interpretResult = interpretProgram(ast, state.turtleGoal, {
      alwaysShowExternal: currentExercise.alwaysShowExternalVars,
      overToken: overNumTokens
        ? { maximum: expectedNumTokens, actual: numUserTokens, userTokens: uTokens }
        : undefined,
      restrictionViolated,
    })

    let endOfInterpretation = Date.now()
    console.log("interpretation time " + (endOfInterpretation - startOfInterpretation) / 1000)

    let newState = produce(state, (state) => {
      state.animationFrames = interpretResult.animations
      state.frameIndex = 0
      state.refLookupHighlights = interpretResult.refLookupHighlights

      state.initialDrawMode = false
    })

    // let violationcb = (obj) => {
    //   newState = { ...newState, violatedRestrictions: obj }
    // }

    if (!interpretResult.swimmingly) {
      //   if (interpretResult.misDraw) {
      //     violationcb({
      //       type: "simple",
      //       id: "draw-mismatch",
      //       description: "Your drawing does not match",
      //     })
      //   } else if (interpretResult.drewTooLittle) {
      //     violationcb({
      //       type: "simple",
      //       id: "drew-too-little",
      //       description: "Your drawing is missing some elements",
      //     })
      //   } else if (interpretResult.drewTooMuch) {
      //     violationcb({
      //       type: "simple",
      //       id: "drew-too-much",
      //       description: "You drew too much",
      //     })
      //   } else {
      //     // an exception was thrown
      //     violationcb({
      //       type: "simple",
      //       id: "runtime-error",
      //       description: "You have a runtime error",
      //     })
      //   }
      // } else if (overNumTokens) {
      //   newState = produce(newState, (state) => {
      //     state.violatedRestrictions = {
      //       type: "token-quota",
      //       id: "over-token-quota",
      //       tokens: uTokens,
      //       maximum: state.solutionNumTokens,
      //     }
      //   })
      // } else if (restrictionViolated) {
      //   newState = produce(newState, (state) => {
      //     state.violatedRestrictions = {
      //       type: "simple",
      //       description: "You're not allowed to use " + restrictionViolated.desc,
      //     }
      //   })
    } else {
      if (!state.adminMode) {
        let cid = state.currentView.exercise

        newState = produce(newState, (state) => {
          state.completed = true
        })

        if (!state.finishedExerciseIds.includes(cid)) {
          fetch("https://wakata.io/apiv2/python-foundations-finished-exercise", {
            method: "POST",
            body: JSON.stringify({ uid: sessionId, exerciseId: cid }),
          })

          let fids = state.finishedExerciseIds.concat(cid)

          return produce(newState, (state) => {
            state.finishedExerciseIds = fids
          })
        }
      }
    }
    return newState
  }

  if (action.type === "show-hardcoded-video") {
    let element = hardcodedVideos[action.id]

    return watchInyourfaceOneShot(state, action.id, element.youtubeID, "Understood!")
  }

  if (action.type === "copy-current-exercise") {
    let ex =
      state.currentView && state.currentView.exercise && state.content.exercises[state.currentView.exercise]
    if (!ex) {
      return state
    }

    return produce(state, (state) => {
      state.copyOfCurrentExercise = clone(ex)
    })
  }

  if (action.type === "paste-current-exercise") {
    if (!state.copyOfCurrentExercise) {
      alert("nothing to paste from")
      return state
    }
    let exid = state.currentView && state.currentView.exercise
    if (!exid) return
    let newContent = setExerciseData(state.content, exid, state.copyOfCurrentExercise)
    return setAndSaveContent(newContent)
  }

  if (action.type === "navigation-viewable") {
    return produce(state, (state) => {
      state.navigateContent = action.viewable
      if (!action.viewable) state.chapterExpanded = undefined
    })
  }

  if (action.type === "video-history-viewable") {
    return produce(state, (state) => {
      state.videoHistory = action.viewable
    })
  }

  if (action.type === "set-chapter-video-id") {
    let ret = produce(state, (state) => {
      state.content.chapters[state.currentView.chapter].youtubeID = action.id
    })
    saveToLocalStorage(ret.content, ret.currentView)
    return ret
  }

  if (action.type === "add-exercise-top") {
    let newContent = addChapterAtTop(state.content)
    return setAndSaveContent(newContent)
  }

  if (action.type === "set-exercise-video") {
    let ret = produce(state, (state) => {
      state.content.exercises[state.currentView.exercise][action.interaction] = action.youtubeID
    })
    saveToLocalStorage(ret.content, ret.currentView)
    return ret
  }

  if (action.type === "popup-token-quota") {
    return produce(state, (state) => {
      state.tokenQuotaView = {
        userTokens: action.userTokens,
        maximum: action.maximum,
        actualNumber: action.actualNumber,
      }
    })
  }

  if (action.type === "close-token-quota") {
    return produce(state, (state) => {
      state.tokenQuotaView = undefined
    })
  }

  if (action.type === "close-youtube") {
    return produce(state, (state) => {
      state.watchYoutube = undefined
    })
  }

  if (action.type === "initial-draw") {
    //setCurrentSpeed(2)
    let currentExercise = state.content.exercises[state.currentView.exercise]
    let sourceInputted = textOfSections(currentExercise.textSections)
    let parseResult = parsePython(sourceInputted)

    let ast = parseResult.ast
    let interpretResult = interpretProgram(ast)
    let animationFrames = interpretResult.animations
    animationFrames = animationFrames.filter((af) => {
      return af.description.opts && af.description.opts.category === "turtle"
    })
    let finalTurtle = animationFrames[animationFrames.length - 1].animationFrame.turtle
    let turtleLines = finalTurtle.lines.concat(finalTurtle.lineProcessing || [])
    let turtleGoal = turtleLines.filter(
      (x) => (x.type === "arc" || x.type === "straight-line") && x.actuallyDrawn
    )

    return produce(state, (state) => {
      state.animationFrames = animationFrames
      state.frameIndex = 0
      state.initialDrawMode = true
      state.lookedupByUser = []
      state.turtleAnimationFrames = animationFrames
      state.turtleGoal = turtleGoal
    })
  }

  if (action.type === "syntax-error") {
    return produce(state, (state) => {
      state.syntaxError = action.payload
    })
  }

  if (action.type === "add-exercise") {
    if (!state.adminMode) return state
    if (!isPermitted(state)) return state
    let eid = action.eid || (state.currentView && state.currentView.exercise)
    if (!eid) return state
    let newContent = addExerciseAfter(state.content, eid)
    return setAndSaveContent(newContent)
  }
  if (action.type === "move-up") {
    if (!state.adminMode) return state
    if (!isPermitted(state)) return state
    let view = state.currentView
    if (view.exercise) {
      let eid = view.exercise
      let newContent = moveExerciseUp(state.content, eid)
      return setAndSaveContent(newContent)
    }
    if (view.chapter) {
      let newContent = moveChapterUp(state.content, view.chapter)
      return setAndSaveContent(newContent)
    }
  }
  if (action.type === "move-down") {
    if (!state.adminMode) return state
    if (!isPermitted(state)) return state
    let view = state.currentView
    if (view.exercise) {
      let eid = view.exercise
      let newContent = moveExerciseDown(state.content, eid)
      return setAndSaveContent(newContent)
    }
    if (view.chapter) {
      let newContent = moveChapterDown(state.content, view.chapter)
      return setAndSaveContent(newContent)
    }
  }

  let resetStateWithView = (newv) => {
    return createInitialState(
      newv,
      state.content,
      state.navigateContent,
      state.adminMode,
      state.currentSpeed,
      state.finishedExerciseIds,
      state.narrationVoice,
      state.mic,
      state.narrationText,
      state.menuExpanded,
      state.chapterExpanded,
      state.copyOfCurrentExercise
    )
  }

  let saveAndResetStateWithView = (newv) => {
    let ret = resetStateWithView(newv)
    saveToLocalStorage(ret.content, ret.currentView)
    return ret
  }

  if (action.type === "jump-to-exercise") {
    if (state.currentView && state.currentView.exercise === action.eid) return state
    let newv = { exercise: action.eid }
    return saveAndResetStateWithView(newv)
  }

  // if (action.type === "watch-in-your-face-one-shot") {
  //   if (seenInyourfaceOneShot(action.id)) return state
  //   addVideoPermanentHistory(action.id, action.youtubeID, { description })
  //   return produce(state, (state) => {
  //     state.watchYoutube = {
  //       id: action.id,
  //       text: action.text,
  //       youtubeID: action.youtubeID,
  //       cb: undefined,
  //       descObj: { description: action.description },
  //     }
  //   })
  // }
  if (action.type === "navigate-next-element") {
    let newv = getNextContentElement(state.content, state.currentView)
    return saveAndResetStateWithView(newv)
  }
  if (action.type === "navigate-next-exercise") {
    let newv = getNextExercise(state.content, state.currentView)
    return saveAndResetStateWithView(newv)
  }
  if (action.type === "navigate-previous-exercise") {
    let newv = getPreviousExercise(state.content, state.currentView)
    return saveAndResetStateWithView(newv)
  }
  if (action.type === "set-text-sections-of-exercise") {
    return produce(state, (state) => {
      state.content = setTextSectionsOfExercise(
        state.content,
        action.exerciseId,
        action.sections,
        state.currentSpeed,
        state.finishedExerciseIds,
        state.narrationVoice,
        state.mic
      )
    })
  }
  if (action.type === "set-breakpoints") {
    return produce(state, (state) => {
      state.breakpoints = action.breakpoints
    })
  }

  if (action.type === "toggle-breakpoint") {
    return produce(state, (state) => {
      state.breakpoints[action.row] = !state.breakpoints[action.row]
    })
  }

  if (action.type === "tutorial-focus-click") {
    let goal = action.goal
    if (R.equals(state.tutorialFocus, goal)) {
      return { ...state, tutorialFocus: undefined }
    }
    return { ...state, tutorialFocus: goal, segmentTool: undefined }
  }
  if (action.type === "unset-tutorial-focus") {
    return { ...state, tutorialFocus: undefined }
  }

  if (action.type === "set-point-decorations") {
    return produce(state, (state) => {
      state.pointDecorations = action.pointDecorations
    })
  }

  if (action.type === "set-frame-index") {
    if (action.index >= state.animationFrames.length) {
      alert("set frame index bug, please alert Jacques")
      return state
    }
    return produce(state, (state) => {
      state.frameIndex = action.index
    })
  }
  if (action.type === "prev-frame") {
    if (state.frameIndex === undefined || state.animationFrames === undefined) return state

    return produce(state, (state) => {
      state.lookedupByUser = []
      if (state.frameIndex > 0) state.frameIndex = state.frameIndex - 1
    })
  }
  if (action.type === "next-frame") {
    if (!state.animationFrames) return state
    console.log("instore", state.frameIndex, state.animationFrames.length)
    if (state.frameIndex === undefined || state.animationFrames === undefined) return state
    console.log("instore", state.frameIndex, state.animationFrames.length)
    if (state.frameIndex >= state.animationFrames.length - 1) return state
    return produce(state, (state) => {
      state.lookedupByUser = []
      state.frameIndex = state.frameIndex + 1
    })
  }
  if (action.type === "last-frame") {
    if (state.frameIndex === undefined || state.animationFrames === undefined) return state

    return produce(state, (state) => {
      state.lookedupByUser = []
      state.frameIndex = state.animationFrames.length - 1
    })
  }

  // let restartPlay = () => {
  //   let wasPlaying = state.intervalLoop !== undefined
  //   stopLoop(state.intervalLoop)
  //   if (wasPlaying && state.animationFrames)
  //     return { ...state, intervalLoop: startPlay(state.animationFrames, false) }
  //   return { ...state, intervalLoop: undefined }
  // }
  // if (action.type === "restart-play") {
  //   return restartPlay()
  // }
  // if (action.type === "start-play") {
  //   if (!state.animationFrames) return state
  //   setCurrentSpeed(state.currentSpeed || 3)
  //   return { ...state, intervalLoop: startPlay(state.animationFrames) }
  // }

  if (action.type === "decrease-speed") {
    return { ...state, currentSpeed: Math.max(1, state.currentSpeed - 1) }
  }
  if (action.type === "increase-speed") {
    return { ...state, currentSpeed: Math.min(9, state.currentSpeed + 1) }
  }

  if (action.type === "entered-highlight") {
    return produce(state, (state) => {
      state.lookedupByUser.push(action.tid)
      let others = state.refLookupHighlights[action.tid]
      others.forEach((oth) => state.lookedupByUser.push(oth))
    })
  }
  if (action.type === "leave-highlight") {
    return produce(state, (state) => {
      let others = state.refLookupHighlights[action.tid]
      state.lookedupByUser = state.lookedupByUser.filter((x) => x !== action.tid && !others.includes(x))
    })
  }

  if (action.type === "clear-highlights") {
    return produce(state, (state) => {
      state.lookedupByUser = []
    })
  }

  if (action.type === "jump-to-before") {
    let firstFrameIndex = indexOfFirstFrameWithToken(action.tid, state.animationFrames)
    let frameBefore = Math.max(0, firstFrameIndex - 1)

    return {
      ...state,
      frameIndex: frameBefore,
    }
  }
  if (action.type === "goto-previous-content-element") {
    let newv = getPreviousContentElement(state.content, state.currentView)
    return { ...state, currentView: newv }
  }
  if (action.type === "download-content") {
    let cleanedContent = cloneButRemove(state.content, ["userTyped"])
    download("content2.json", JSON.stringify(cleanedContent, null, 4))
    return state
  }
  if (action.type === "toggle-restriction") {
    let v = action.v
    let newContent = produce(state.content, (content) => {
      let exercise = content.exercises[state.currentView.exercise]
      if (v) {
        exercise.restrictions.push(action.restrictionId)
      } else {
        exercise.restrictions = exercise.restrictions.filter((x) => x !== action.restrictionId)
      }
    })
    return setAndSaveContent(newContent)
  }

  if (action.type === "toggle-show-external-vars") {
    let v = action.v
    let newContent = produce(state.content, (content) => {
      let exercise = content.exercises[state.currentView.exercise]
      exercise.alwaysShowExternalVars = v
    })
    return setAndSaveContent(newContent)
  }

  if (action.type === "toggle-admin-mode") {
    if (!isLocal) return state
    window.localStorage.setItem("__admin-mode", JSON.stringify(!state.adminMode))
    return {
      ...state,
      adminMode: !state.adminMode,
    }
  }

  if (action.type === "toggle-hide-tutorial-editor") {
    if (!isLocal) return state

    return {
      ...state,
      hideTutorialEditor: !state.hideTutorialEditor,
    }
  }

  if (action.type === "big-turtle-dissapear") {
    return { ...state, bigTurtleDissapear: true }
  }
  if (action.type === "ignore-all") {
    return { ...state, ignoreAll: true }
  }

  if (action.type === "show-initial-draw-instructions") {
    return { ...state, showInitialDrawInstructions: true }
  }
  if (action.type === "show-small-turtle") {
    return { ...state, showSmallTurtle: true }
  }
  if (action.type === "show-countdown") {
    return { ...state, timerCountdown: action.duration }
  }
  if (action.type === "hide-countdown") {
    return { ...state, timerCountdown: undefined }
  }
  if (action.type === "save-progress") {
    return { ...state, saveProgressWindow: true }
  }

  if (action.type === "close-save-progress-window") {
    return { ...state, saveProgressWindow: false }
  }

  if (action.type === "bug-report") {
    return { ...state, bugReportWindow: true }
  }
  if (action.type === "close-bug-report-window") {
    return { ...state, bugReportWindow: false }
  }

  if (action.type === "__set-tooltip") {
    return { ...state, tooltip: action.tooltip }
  }

  // if (action.type === "watch-vid-from-id") {
  //   let id = action.id
  //   let vid = hardcodedVideos[id]

  //   return watchInyourfaceOneShot(state, id, vid.youtubeID, "Understood!", vidData.desc)
  // }

  if (action.type === "check-restriction-consistency") {
    let allExercises = Object.values(state.content.exercises)
    for (let i = 0; i < allExercises.length; i++) {
      let ex = allExercises[i]
      let sourceInputted = textOfSections(ex.textSections)
      let linesOfInterest = linesToBeFilledin(ex.textSections)
      let parseResult = parsePython(sourceInputted, linesOfInterest)
      let violations = violatesRestrictions(parseResult, ex.restrictions || [])
      if (violations) {
        alert("exercise has violated restrictions: " + JSON.stringify(violations))
        return resetStateWithView({ exercise: ex.id })
      }
    }
    alert("no restrictions violated")
    return state
  }
  if (action.type === "check-solutions-interpret") {
    let allExercises = Object.values(state.content.exercises)
    for (let i = 0; i < allExercises.length; i++) {
      console.log("interpreting " + i + " / " + allExercises.length)
      let ex = allExercises[i]
      let sourceInputted = textOfSections(ex.textSections)
      let linesOfInterest = linesToBeFilledin(ex.textSections)
      let parseResult = parsePython(sourceInputted, linesOfInterest)
      let ast = parseResult.ast
      interpretProgram(ast)
    }
    alert("no errors")
    return state
  }

  if (action.type === "initial-set-state") {
    return action.state
  }

  if (action.type === "set-voice-narration") {
    return { ...state, narrationVoice: action.value }
  }

  if (action.type === "set-text-narration") {
    return { ...state, narrationText: action.value }
  }

  if (action.type === "set-narration-text-index") {
    return { ...state, narrationTextIndex: action.index }
  }

  if (action.type === "set-show-sound-instructions") {
    return { ...state, soundInstructions: action.shown }
  }

  if (action.type === "restore-content-from-file") {
    return setAndSaveContent(contentOnFile)
  }

  if (action.type === "track-cell-clicked") {
    let trackCellClickedResult = trackCellClicked(tdesc(), action.row, action.trackIndex)
    let newSegmentId = trackCellClickedResult.id
    if (!newSegmentId) return state
    let newTdesc = trackCellClickedResult.tdesc

    let newContent = onlyUpdateTutorialDesc(newTdesc)
    delayedSaveToLocalStorage(newContent, state.currentView)
    return { ...state, tutorialFocus: { type: "segment", id: newSegmentId }, content: newContent }
  }

  if (action.type === "segment-drop") {
    return updateTutorialDesc(segmentDrop(tdesc(), action.row, action.trackIndex))
  }
  if (action.type === "resize-segment-drag-start") {
    let updated = updateTutorialDesc(resizeSegmentDragStart(tdesc(), action.segmentId, action.boxType))
    return { ...updated, draggingSegment: true }
  }

  if (action.type === "move-segment-drag-start") {
    let updated = updateTutorialDesc(moveSegmentDragStart(tdesc(), action.segmentId, action.boxType))
    return { ...updated, draggingSegment: true }
  }

  if (action.type === "set-event-details") {
    let exerciseId = state.currentView?.exercise
    if (!exerciseId) throw new Error("huh")
    //let eventFocused = state.tutorialFocus
    let eventId = action.eventId || state.tutorialFocus?.id
    if (!eventId) throw new Error("what")
    //if (eventFocused.type !== "event") throw new Error("huh")

    let newContent = produce(state.content, (content) => {
      content.eventDescriptions[eventId] = content.eventDescriptions[eventId] || {}
      content.eventDescriptions[eventId][action.propName] = action.propValue
    })
    return setAndSaveContent(newContent)
  }

  if (action.type === "add-definition") {
    let exerciseId = state.currentView?.exercise
    if (!exerciseId) throw new Error("huh")
    let eventFocused = state.tutorialFocus
    let eventId = eventFocused.id
    if (eventFocused.type !== "event") throw new Error("huh")
    if (!eventId) throw new Error("what")
    let newContent = produce(state.content, (content) => {
      content.eventDescriptions[eventId] = content.eventDescriptions[eventId] || {}
      content.eventDescriptions[eventId].definitions = content.eventDescriptions[eventId].definitions || []
      content.eventDescriptions[eventId].definitions.push({ header: "", body: "" })
    })
    return setAndSaveContent(newContent)
  }

  if (action.type === "del-definition") {
    let exerciseId = state.currentView?.exercise
    if (!exerciseId) throw new Error("huh")
    let eventFocused = state.tutorialFocus
    let eventId = eventFocused.id
    if (eventFocused.type !== "event") throw new Error("huh")
    if (!eventId) throw new Error("what")
    let newContent = produce(state.content, (content) => {
      content.eventDescriptions[eventId].definitions.splice(action.index, 1)
    })
    return setAndSaveContent(newContent)
  }

  if (action.type === "set-definition-header") {
    let exerciseId = state.currentView?.exercise
    if (!exerciseId) throw new Error("huh")
    let eventFocused = state.tutorialFocus
    let eventId = eventFocused.id
    if (eventFocused.type !== "event") throw new Error("huh")
    if (!eventId) throw new Error("what")
    let newContent = produce(state.content, (content) => {
      content.eventDescriptions[eventId].definitions[action.index].header = action.text
    })
    return setAndSaveContent(newContent)
  }

  if (action.type === "set-definition-body") {
    let exerciseId = state.currentView?.exercise
    if (!exerciseId) throw new Error("huh")
    let eventFocused = state.tutorialFocus
    let eventId = eventFocused.id
    if (eventFocused.type !== "event") throw new Error("huh")
    if (!eventId) throw new Error("what")
    let newContent = produce(state.content, (content) => {
      content.eventDescriptions[eventId].definitions[action.index].body = action.text
    })
    return setAndSaveContent(newContent)
  }

  if (action.type === "change-event-type") {
    let newContent = changeEventType(state.content, action.id, action.choice)
    return setAndSaveContent(newContent)
  }
  if (action.type === "add-empty-event") {
    let newEvent = { type: "empty", id: uniqid() }
    let newTdesc = produce(tdesc(), (tdesc) => {
      tdesc.eventOrder[action.row] = newEvent.id
    })
    let newContent = onlyUpdateTutorialDesc(newTdesc)
    let newNewContent = produce(newContent, (content) => {
      content.eventDescriptions[newEvent.id] = newEvent
    })
    return setAndSaveContent(newNewContent)
  }

  if (action.type === "populate-script") {
    if (!action.text) throw new Error("what")
    let script = action.text
    let lines = script.split("\n")
    lines = lines.filter((x) => x.trim().length > 0)
    let events = lines.map((t) => ({ type: "voice", script: t, id: uniqid() }))
    let newTdesc = produce(tdesc(), (tdesc) => {
      tdesc.eventOrder = events.map((x) => x.id)
      tdesc.numRows = lines.length
    })
    let newEvents = produce(state.content.eventDescriptions, (descs) => {
      events.forEach((e) => (descs[e.id] = e))
    })
    let newContent = onlyUpdateTutorialDesc(newTdesc)
    let newNewContent = { ...newContent, eventDescriptions: newEvents }
    return setAndSaveContent(newNewContent)

    //return updateTutorialDesc(tdescFromScript(tdesc(), action.text))
  }

  if (action.type === "event-drop") {
    return updateTutorialDesc(eventDrop(tdesc(), action.row))
  }

  if (action.type === "move-event-drag-start") {
    let updated = updateTutorialDesc(moveEventDragStart(tdesc(), action.id))
    return { ...updated, draggingEvent: true }
  }
  if (action.type === "dragging-over-event") {
    return { ...state, draggingOverEventId: action.eventId }
  }

  if (action.type === "dragging-over-segment") {
    return { ...state, draggingOverSegmentRow: action.row }
  }

  if (action.type === "drag-ended") {
    return {
      ...state,
      draggingEvent: false,
      draggingOverEventId: undefined,
      draggingSegment: false,
      draggingOverSegmentRow: undefined,
    }
  }

  if (action.type === "del-event") {
    let td = tdesc()
    if (!rowIsDeletable(td, action.row)) {
      alert("segment not deletable")
      return state
    }
    let event = td.eventOrder[action.row]
    let newContent = onlyUpdateTutorialDesc(delEvent(td, action.row))
    let newNewContent = produce(newContent, (c) => {
      delete c.eventDescriptions[event.id]
    })
    return setAndSaveContent(newNewContent)
  }
  if (action.type === "del-segment") {
    let td = tdesc()
    //let segment = td.trackSegments[segmentId]
    let newContent = onlyUpdateTutorialDesc(delSegment(td, action.segmentId))
    let newNewContent = produce(newContent, (c) => {
      delete c.segmentDescriptions[action.segmentId]
    })
    let newState = setAndSaveContent(newNewContent)
    return { ...newState, tutorialFocus: undefined }
  }

  if (action.type === "add-event-before") {
    let td = tdesc()
    let newEvent = { type: "empty", id: uniqid() }
    let segmentIds = Object.keys(td.trackSegments)
    let newtd = produce(td, (tdesc) => {
      tdesc.numRows = (tdesc.numRows || 0) + 1
      tdesc.eventOrder.splice(action.row, 0, newEvent.id)

      segmentIds.forEach((s) => {
        let seg = tdesc.trackSegments[s]
        if (seg.to >= action.row) seg.to++
        if (seg.from >= action.row) seg.from++
      })
    })
    let newContent = onlyUpdateTutorialDesc(newtd)
    let newNewContent = produce(newContent, (c) => {
      c.eventDescriptions[newEvent.id] = newEvent
    })
    return setAndSaveContent(newNewContent)
  }
  if (action.type === "add-tutorial-track") {
    return updateTutorialDesc(addTutorialTrack(tdesc()))
  }

  if (action.type === "add-tutorial-row") {
    return updateTutorialDesc(addTutorialRow(tdesc()))
  }

  if (action.type === "set-segment-darken") {
    if (!action.segmentId) throw new Error("eh")
    let segmentId = action.segmentId
    let value = action.value

    let newContent = produce(state.content, (content) => {
      content.segmentDescriptions[segmentId].darken = value
    })
    return setAndSaveContent(newContent)
  }

  if (action.type === "clear-decorations") {
    if (!action.segmentId) throw new Error("eh")
    let segmentId = action.segmentId
    let newContent = produce(state.content, (content) => {
      content.segmentDescriptions[segmentId].decorations = {}
    })
    return setAndSaveContent(newContent)
  }

  if (action.type === "segment-tool-click") {
    if (state.segmentTool === action.tool) {
      return { ...state, segmentTool: undefined }
    }
    return { ...state, segmentTool: action.tool }
  }

  if (action.type === "toggle-decoration-to-segment") {
    let newContent = toggleDecorationToSegment(
      state.content,
      action.segmentId,
      action.idInInterface,
      action.tool
    )
    return setAndSaveContent(newContent)
  }

  if (action.type === "initialize-tutorial-description") {
    let exerciseId = state.currentView?.exercise
    if (!exerciseId) throw new Error("huh")
    if (state.content?.tutorialDescriptions?.[exerciseId]) return state
    let newContent = produce(state.content, (content) => {
      content.tutorialDescriptions = content.tutorialDescriptions || {}
      content.tutorialDescriptions[exerciseId] = emptyTutorialDescription()
    })
    return setAndSaveContent(newContent)
  }

  if (action.type === "resource-uploaded-to-event") {
    if (!action.resourceLocation) throw new Error("what")
    let exerciseId = state.currentView?.exercise
    if (!exerciseId) throw new Error("huh")
    let eventFocused = state.tutorialFocus
    let eventId = eventFocused.id
    if (!eventId) throw new Error("what")
    let newContent = produce(state.content, (content) => {
      content.eventDescriptions[eventId] = content.eventDescriptions[eventId] || {}
      content.eventDescriptions[eventId].resourceLocation = action.resourceLocation
    })
    return setAndSaveContent(newContent)
  }

  if (action.type === "resource-uploaded-to-segment") {
    if (!action.resourceLocation) throw new Error("what")
    let exerciseId = state.currentView?.exercise
    if (!exerciseId) throw new Error("huh")
    let segmentFocused = state.tutorialFocus
    let segmentId = segmentFocused.id
    if (!segmentId) throw new Error("what")
    let newContent = produce(state.content, (content) => {
      content.segmentDescriptions[segmentId] = content.segmentDescriptions[segmentId] || {}
      content.segmentDescriptions[segmentId].resourceLocation = action.resourceLocation
    })
    return setAndSaveContent(newContent)
  }

  if (action.type === "set-mic") {
    return { ...state, mic: action.mic }
  }

  if (action.type === "mic-select-shown") {
    return { ...state, micSelectShown: action.shown }
  }
  if (action.type === "audio-recording") {
    return { ...state, audioRecording: action.recording }
  }
  if (action.type === "voice-view-shown") {
    return { ...state, voiceViewShown: action.shown }
  }

  if (action.type === "picture-view-shown") {
    return { ...state, pictureViewShown: action.shown }
  }

  if (action.type === "click-event-target") {
    if (!action.idInInterface) throw new Error("huh")
    let exerciseId = state.currentView?.exercise
    if (!exerciseId) throw new Error("huh")
    let eventFocused = state.tutorialFocus
    let eventId = eventFocused.id
    if (!eventId) throw new Error("what")
    let newContent = produce(state.content, (content) => {
      content.eventDescriptions[eventId] = content.eventDescriptions[eventId] || {}
      if (content.eventDescriptions[eventId].type !== "click") throw new Error("whats happen")
      content.eventDescriptions[eventId].clickTarget = action.idInInterface
    })
    return setAndSaveContent(newContent)
  }

  if (action.type === "set-animation-frames") {
    return { ...state, animationFrames: action.animationFrames }
  }

  if (action.type === "set-active-segments") {
    return { ...state, activeSegments: action.segments }
  }

  // if (action.type === "idInInterface-domNode-association") {
  //   let domNode = action.domNode
  //   let idInInterface = action.idInInterface
  //   let currentAssociations = state.idInInterfaceDomnodeAssociation || {}
  //   let ids = Object.keys(currentAssociations)
  //   let toDelete = []
  //   for (let i = 0; i < ids; i++) {
  //     if (currentAssociations[ids[i]] === domNode) toDelete.push(ids[i])
  //   }
  //   return produce(state, (state) => {
  //     if (!state.idInInterfaceDomnodeAssociation) state.idInInterfaceDomnodeAssociation = {}
  //     toDelete.forEach((k) => {
  //       state.idInInterfaceDomnodeAssociation[k] = undefined
  //     })
  //     state.idInInterfaceDomnodeAssociation[idInInterface] = domNode
  //   })
  // }

  if (action.type === "idininterface-assoc-redraw-needed") {
    return { ...state, assocCounter: (state.assocCounter || 0) + 1 }
  }
  if (action.type === "mouse-over-click") {
    return { ...state, mouseOverClick: action.value }
  }

  if (action.type === "mouse-over-noclick") {
    return { ...state, mouseOverNoclick: action.value }
  }

  if (action.type === "pt-click-big-turtle-button") {
    let af = action.animationFrames
    if (!af) throw new Error("missing frames")
    let turtleAnimationFrames = action.animationFrames.filter((af) => {
      return af.description.opts && af.description.opts.category === "turtle"
    })

    return {
      ...state,
      animationFrames: af,
      initialDrawMode: true,
      frameIndex: 0,
      turtleGoal: action.turtleGoal,
      turtleAnimationFrames,
    }
  }
  if (action.type === "pt-pressed-play") {
    return { ...state, fakePressedPlay: true }
  }
  if (action.type === "pt-press-stop") {
    return { ...state, animationFrames: undefined, fakePressedPlay: undefined }
  }

  if (action.type === "set-play-tutorial-index") {
    return { ...state, playTutorialIndex: action.index }
  }

  if (action.type === "reset-exercise") {
    let ret = resetStateWithView(state.currentView)
    return {
      ...ret,
      playTutorialIndex: state.playTutorialIndex,
      tutorialPressedOnce: state.tutorialPressedOnce,
      loopType: state.loopType,
      hideTutorialEditor: state.hideTutorialEditor,
    }
  }

  if (action.type === "playing-loop-type") {
    return { ...state, loopType: action.loopType }
  }
  if (action.type === "voice-script") {
    return { ...state, voiceScript: action.script, scriptDefinitions: action.definitions }
  }
  if (action.type === "toggle-action-list-enabled") {
    return { ...state, turtleActionListEnabled: !state.turtleActionListEnabled }
  }
  if (action.type === "set-attr") {
    return { ...state, [action.name]: action.value }
  }
  if (action.type === "toggle-chapter-in-navigation") {
    let current = state.chapterExpanded || []
    if (current.includes(action.chapterId))
      return {
        ...state,
        chapterExpanded: current.filter((x) => x !== action.chapterId),
      }
    return { ...state, chapterExpanded: [...current, action.chapterId] }
    // return { ...state, chapterExpanded: action.chapterId }
  }
  if (action.type === "set-menu-expanded") {
    return { ...state, menuExpanded: action.expanded }
  }
  if (action.type === "set-play-tutorial-length") {
    return { ...state, playTutorialLength: action.length }
  }

  if (action.type === "set-state-prop") {
    return { ...state, [action.name]: action.value }
  }
  if (action.type === "next-voice-event") {
    let td = tdesc()
    let ind = td.eventOrder.indexOf(state.tutorialFocus.id)
    //debugger
    if (ind === -1) throw new Error("what")
    for (let i = ind + 1; i < td.eventOrder.length; i++) {
      let eventid = td.eventOrder[i]
      if (state.content.eventDescriptions[eventid].type === "voice") {
        return { ...state, tutorialFocus: { type: "event", id: eventid } }
      }
    }
    return state
  }
  if (action.type === "prev-voice-event") {
    let td = tdesc()
    let ind = td.eventOrder.indexOf(state.tutorialFocus.id)
    //debugger
    if (ind === -1) throw new Error("what")
    for (let i = ind - 1; i >= 0; i--) {
      let eventid = td.eventOrder[i]
      if (state.content.eventDescriptions[eventid].type === "voice") {
        return { ...state, tutorialFocus: { type: "event", id: eventid } }
      }
    }
    return state
  }
  if (action.type === "increment-counter") {
    let name = action.name
    let cv = state[name] || 0
    return { ...state, [name]: cv + 1 }
  }
  if (action.type === "@@INIT")
    // reducer end
    return state
  if (action.type.includes("@@redux")) return state

  debugger
  alert("unrecognized action " + action.type)
  return state
}

let watchInyourfaceOneShot = (state, id, youtubeID, text, description) => {
  // if (state.adminMode || seenInyourfaceOneShot(id)) {
  //   return state
  // }
  addVideoPermanentHistory(id, youtubeID, { description })
  return produce(state, (state) => {
    state.watchYoutube = {
      id,
      text,
      youtubeID,
      descObj: { description },
    }
  })
}

let watchInyourfaceCurrentExercise = (
  state,
  id,
  youtubeID,

  text,
  description,
  exerciseId
) => {
  let chapter = Object.values(state.content.chapters).find((ch) => ch.exerciseOrder.includes(exerciseId))
  let chapterId = chapter && chapter.id
  if (state.adminMode || (state.videosSeenCurrentExercise || []).includes(id)) {
    return state
  } else {
    state = produce(state, (state) => {
      state.videosSeenCurrentExercise = state.videosSeenCurrentExercise || []
      state.videosSeenCurrentExercise.push(id)
    })
  }

  return produce(state, (state) => {
    state.watchYoutube = {
      id,
      text,
      youtubeID,
      cb: undefined,
      descObj: { description, exerciseId, chapterId },
    }
  })
}

let saveToLocalStorage = (content, view) => {
  if (dstls) {
    clearTimeout(dstls)
    dstls = undefined
  }
  if (view) window.localStorage.setItem(currentViewLocalStorageId, JSON.stringify(view))
  if (content) window.localStorage.setItem("__content", JSON.stringify(content))
}

export let isPermitted = (state) => {
  if (!state.adminMode || !isLocal || !window.localStorage.getItem("__permitted")) {
    return false
    //throw new Error("This browser session is not permitted")
  }
  return true
}

let initiallyAdmin = window.localStorage.getItem("__admin-mode") === JSON.stringify(true) && isLocal

//let originalContent = { exercises: {}, chapters: {}, chapterOrder: [], tutorialDescriptions: {} }

let getLastLoad = () => {
  let ret = parseInt(window.localStorage.getItem("last-load-time"))
  if (isNaN(ret)) return undefined
  return ret
}

let needsReloadFromFile = () => {
  if (!isLocal) return true

  // let lastLoad = getLastLoad()
  // if (lastLoad && Date.now() - lastLoad > 1000 * 60 * 60 * 12) return true

  return false
}

let contentToLoad = () => {
  // if (true) {
  // if (!isLocal) {
  //   window.localStorage.setItem("__content", JSON.stringify(contentOnFile))
  //   originalContent = contentOnFile
  //   return
  // }
  let nr = needsReloadFromFile()
  window.localStorage.setItem("last-load-time", "" + Date.now())
  if (nr) {
    if (isLocal) window.localStorage.setItem("__content", JSON.stringify(contentOnFile))
    return contentOnFile
  }

  try {
    let nc = window.localStorage.getItem("__content")
    if (nc) {
      let ret = JSON.parse(nc)
      if (!ret.chapterOrder) return contentOnFile
      return ret
    }
    return contentOnFile
  } catch (err) {
    return contentOnFile
  }
}

let originalContent = contentToLoad()

let calculateTotalNumberExercises = (content) => {
  let chapters = content.chapterOrder
  let ret = 0
  for (let i = 0; i < chapters.length; i++) {
    let chapter = content.chapters[chapters[i]]
    ret += chapter.exerciseOrder.filter((x) => !content.tutorialDescriptions[x]).length
  }
  return ret
}
export let totalNumberExercises = calculateTotalNumberExercises(originalContent)

let getStoredInitialView = () => {
  if (explicitExId) return { exercise: explicitExId }

  let storedInitialView = window.localStorage.getItem(currentViewLocalStorageId)

  if (storedInitialView) {
    try {
      let initialView = JSON.parse(storedInitialView)

      if (initialView.exercise) {
        if (!originalContent.exercises[initialView.exercise]) return undefined
        return initialView
      }
      return initialView
    } catch (err) {
      return undefined
    }
  }
  return undefined
}

let defaultOrStoredView = () => {
  let first = { chapter: originalContent.chapterOrder[0] }
  let v = getStoredInitialView()

  if (!v) return first
  return v
}

let initialView = defaultOrStoredView()

let emptyTutorialDescription = () => ({
  eventOrder: [],

  trackSegments: {},
  numTracks: 1,
  numRows: 8,
})

let getTurtlePrecomputes = (content, view) => {
  let currentExercise = view?.exercise && content.exercises[view?.exercise]
  if (!currentExercise) return undefined
  let tdesc = content.tutorialDescriptions[view.exercise]
  if (!tdesc) return undefined
  let sourceInputted = textOfSections(currentExercise.textSections)

  //let parseResult = parsePython("from turtle import *\nforward(100)")
  let parseResult = parsePython(sourceInputted)
  let ast = parseResult.ast
  let interpretResult = interpretProgram(ast)
  let animationFrames = interpretResult.animations
  let turtleAnimationFrames = animationFrames.filter((af) => {
    return af.description.opts && af.description.opts.category === "turtle"
  })

  let finalTurtle =
    turtleAnimationFrames.length === 0
      ? { lines: [] }
      : turtleAnimationFrames[turtleAnimationFrames.length - 1].animationFrame.turtle

  let turtleLines = finalTurtle.lines.concat(finalTurtle.lineProcessing || [])
  let turtleGoal = turtleLines.filter(
    (x) => (x.type === "arc" || x.type === "straight-line") && x.actuallyDrawn
  )
  return { turtleGoal, turtleAnimationFrames: turtleAnimationFrames }
}

export let getTutorialPrecomputes = (content, view) => {
  let { turtleGoal, turtleAnimationFrames } = getTurtlePrecomputes(content, view)

  let currentExercise = view?.exercise && content.exercises[view?.exercise]
  if (!currentExercise) return undefined
  let tdesc = content.tutorialDescriptions[view.exercise]
  if (!tdesc) return undefined
  let sourceInputted = precomputesUserTextOfSections(currentExercise.textSections)
  //let parseResult = parsePython("from turtle import *\nforward(100)")
  let parseResult = parsePython(sourceInputted)
  let ast = parseResult.ast
  let interpretResult = interpretProgram(ast, turtleGoal, {
    alwaysShowExternal: currentExercise.alwaysShowExternalVars,
  })
  let animationFrames = interpretResult.animations

  return { turtleGoal, turtleAnimationFrames: turtleAnimationFrames, animationFrames }
}

function isSuperset(set, subset) {
  for (let elem of subset) {
    if (!set.has(elem)) {
      return false
    }
  }
  return true
}
let subset = (a, b) => {
  return isSuperset(b, a)
}

let createInitialState = (
  view,
  content,
  navigateContent,
  adminMode,
  speed,
  finishedExerciseIds,
  narrationVoice,
  mic,
  narrationText,
  menuExpanded,
  chapterExpanded,
  copyOfCurrentExercise
) => {
  let compAllExercises = () => {
    let ret = []
    content.chapterOrder.forEach((chid) => {
      let ch = content.chapters[chid]
      ch.exerciseOrder.forEach((eid) => ret.push(eid))
    })
    return ret
  }
  let compFinishedAllExercises = () => {
    let allExerciseIds = compAllExercises()
    return subset(new Set(allExerciseIds), new Set(finishedExerciseIds))
  }
  let compIsLastExercise = () => {
    let exid = view?.exercise
    if (!exid) return false
    let lastChapterId = content.chapterOrder[content.chapterOrder.length - 1]
    let lastChapter = content.chapters[lastChapterId]
    let exercises = lastChapter.exerciseOrder
    if (!exercises.includes(exid)) return false
    let ind = exercises.indexOf(exid)
    if (ind === exercises.length - 1) return true
    return false
  }

  let isLastExercise = compIsLastExercise()
  let finishedAllExercises = compFinishedAllExercises()

  let watchYoutube = undefined

  let solutionNumTokens = undefined
  if (view?.exercise) {
    try {
      let exercise = content.exercises[view.exercise]

      let solutionSource = textOfSections(exercise.textSections)

      let parseResult = parsePython(solutionSource)
      let solutionWriteLines = getSolutionWriteLines(exercise.textSections)
      let solutionTokens = extractUserTokens(solutionWriteLines, parseResult.tokens)
      solutionNumTokens = solutionTokens.length
    } catch (err) {
      alert("Unable to count tokens. This is a bug. Please tell Jacques")
      solutionNumTokens = -1
    }

    let exerciseId = view.exercise
    let chapter = Object.values(content.chapters).find((ch) => ch.exerciseOrder.includes(exerciseId))
    let chapterId = chapter && chapter.id
    let cid = view.exercise
    let ex = content.exercises[cid]
    let videoIntro = ex && ex.videoIntro
    if (!adminMode && videoIntro && videoIntro.trim().length > 0) {
      watchYoutube = {
        id: "intro-exercise",
        text: "next step",
        youtubeID: videoIntro,
        cb: undefined,
        descObj: { description: "Intro video", exerciseId, chapterId },
      }
    }
    // let xx = content.exercises[exerciseId]
    // debugger
    content = produce(content, (content) => {
      let ex = content.exercises[exerciseId]

      ex.textSections.forEach((s) => {
        if (s.type === "write") s.userTyped = s.startingText || ""
      })
    })
  }

  let exerciseIdIsTutorial = (exid) => {
    if (!exid) return false
    return !!content?.tutorialDescriptions?.[exid]
  }

  return {
    narrationText,
    narrationVoice,
    navigateContent: navigateContent,
    currentView: view,
    breakpoints: [],
    animationFrames: undefined,
    frameIndex: undefined,
    syntaxError: undefined,
    lookedupByUser: [],
    content: content,
    watchYoutube,
    solutionNumTokens,
    isLastExercise,
    initialDrawDone: false,
    adminMode,
    currentSpeed: speed,
    finishedExerciseIds,
    tutorialPrecomputes: exerciseIdIsTutorial(view?.exercise) && getTutorialPrecomputes(content, view),
    mic,
    menuExpanded,
    chapterExpanded,
    copyOfCurrentExercise,
    finishedAllExercises,
    //    saveProgress: true,
  }
}
let getLastMicUsed = async () => {
  let lastDeviceId = window.localStorage.getItem("lastMicDeviceId")
  let lastDeviceLabel = window.localStorage.getItem("lastMicDeviceLabel")
  if (!lastDeviceId && !lastDeviceLabel) {
    return
  }
  function gotDevices(deviceInfos) {
    for (let i = 0; i !== deviceInfos.length; ++i) {
      var deviceInfo = deviceInfos[i]
      if (deviceInfo.kind === "audioinput") {
        let deviceIdMatch = lastDeviceId && deviceInfo.deviceId === lastDeviceId
        let deviceLabelMatch = deviceInfo.label && deviceInfo.label === lastDeviceLabel
        if (deviceIdMatch || deviceLabelMatch) {
          return { deviceId: deviceInfo.deviceId, label: deviceInfo.label }
        }
      }
      // if (deviceInfo.kind === 'audioinput') {
      //     console.log("device", deviceInfo.label || 'Microphone ' + i,
      //         deviceInfo.deviceId);
      // }
    }
  }
  let stream = await navigator.mediaDevices.getUserMedia({ audio: true })
  let devices = await navigator.mediaDevices.enumerateDevices()

  let previouslyUsedDevice = gotDevices(devices)
  stream
    .getTracks() // get all tracks from the MediaStream
    .forEach((track) => track.stop()) // stop each of them

  return previouslyUsedDevice
}

let initialMic = async () => {
  if (!isLocal) return undefined
  return getLastMicUsed()
}

let getInitialState = () => {
  let run = async () => {
    if (!loadedFromLink) {
      let m = await initialMic()
      let ret = createInitialState(
        initialView,
        originalContent,
        false,
        initiallyAdmin,
        3,
        [],
        true,
        m,
        true,
        false,
        undefined,
        undefined
      )
      dispatch({ type: "initial-set-state", state: ret })
    }

    let response = await fetch("https://wakata.io/apiv2/python-foundations-get-exercise-history", {
      method: "POST",
      body: JSON.stringify({ uid: sessionId }),
    })
    let responseBody = await response.text()
    let parsed = JSON.parse(responseBody)

    let finishedUnfiltered = parsed.finishedExercises

    let allExercises = new Set()
    originalContent.chapterOrder.forEach((chid) =>
      originalContent.chapters[chid].exerciseOrder.forEach((eid) => allExercises.add(eid))
    )
    let finishedExercises = finishedUnfiltered.filter((eid) => allExercises.has(eid))
    let m = await initialMic()
    let getSpeed = () => {
      try {
        return JSON.parse(window.localStorage.getItem("currentSpeed")) || 5
      } catch (err) {
        return 3
      }
    }

    let ret = createInitialState(
      initialView,
      originalContent,
      false,
      initiallyAdmin,
      getSpeed(),
      finishedExercises,
      true,
      m,
      true,
      false,
      undefined,
      undefined
    )
    ret = { ...ret, loadingFinishedExercises: false }
    dispatch({ type: "initial-set-state", state: ret })
  }
  run()
  return { loadingFinishedExercises: true }
}

let initialState = getInitialState()

export let store = createStore(reducer, initialState, composeWithDevTools())

export let dispatch = (action) => {
  // if (action.type === "del") debugger
  // try {
  //   console.log("dispatching", JSON.stringify(action))
  // } catch (err) {
  //   debugger
  // }
  store.dispatch(action)
}
