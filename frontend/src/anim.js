import { tutorialOnClick } from "./TutorialButton"
import { drive } from "./drive"
import React, { forwardRef, useEffect, useState } from "react"
import { useSelector } from "react-redux"
import produce from "immer"
import cursorpng from "./arrow.png"
import { GiTurtle } from "react-icons/gi"
import { CountdownCircleTimer } from "react-countdown-circle-timer"
import { getDecorationsForID, populateDecorationsRef } from "./populate-decorations-ref"
import { getMultiplierOfSpeed } from "./speed.js"
import { store } from "./store.js"

const BASETIMEDELTA = 1400

const optimizeForSpeed = false

//let setTimeDelta = x => TIMEDELTA = x

let getTimeDelta = () => {
  return BASETIMEDELTA / getMultiplierOfSpeed(getCurrentSpeed())
}

let setNodeSpeeds = () => {
  for (let p of mirrorNode) {
    let n = p[1]
    n.style.animationDuration = getTimeDelta() / 1500 + "s"
  }
}

let getCurrentSpeed = () => store.getState().currentSpeed

let width = 400
let height = 400

let pathOfArc = (centerx, centery, radius, degrees, x1, y1, circleStartingAngle) => {
  let startingRads = (2 * Math.PI * circleStartingAngle) / 360
  let drads = (degrees * 2 * Math.PI) / 360
  let endingX = radius * Math.cos(startingRads + drads) + centerx
  let endingY = radius * Math.sin(startingRads + drads) + centery
  if (degrees <= 180) {
    let sweepFlag = 0
    let largeArcFlag = 0
    return `M ${x1 + width / 2} ${-y1 + height / 2} A ${radius} ${radius} 0 ${largeArcFlag} ${sweepFlag} ${
      endingX + width / 2
    } ${-endingY + height / 2}`
  }
  let sweepFlag = 0
  let largeArcFlag = 0

  let midX = radius * Math.cos(startingRads + Math.PI) + centerx
  let midY = radius * Math.sin(startingRads + Math.PI) + centery
  return `M ${x1 + width / 2} ${-y1 + height / 2} A ${radius} ${radius} 0 ${largeArcFlag} ${sweepFlag} ${
    midX + width / 2
  } ${-midY + height / 2} A ${radius} ${radius} 0 ${largeArcFlag} ${sweepFlag} ${endingX + width / 2} ${
    -endingY + height / 2
  }`
}

let paint = (drawn, stroke) => {
  return drawn.map((candidate) => {
    if (candidate.type === "straight-line") {
      let { x1, y1, x2, y2, actuallyDrawn } = candidate
      if (!actuallyDrawn) return null
      return (
        <line
          x1={x1 + width / 2}
          y1={-y1 + height / 2}
          x2={x2 + width / 2}
          y2={-y2 + height / 2}
          stroke={stroke}
        />
      )
    } else if (candidate.type === "arc") {
      if (!candidate.actuallyDrawn) return null
      let c = candidate

      let d = pathOfArc(c.center.x, c.center.y, c.radius, c.degrees, c.x1, c.y1, c.startingCircleAngle)

      return <path d={d} fill="none" stroke={stroke} stroke-width="1" />
    } else if (["pendown", "turn"].includes(candidate.type)) {
    } else {
      throw new Error("something went wrong")
    }
  })
}

function GrayLines(lines) {
  return (
    <div style={{ position: "relative", height: "400px", width: "400px" }}>
      <svg
        style={{ width: `${width}px`, height: `${height}px` }}
        viewBox={`0 0 ${width} ${height}`}
        xmlns="http://www.w3.org/2000/svg"
      >
        {paint(lines)}
      </svg>
    </div>
  )
}

let requestAnimationFramePromise = () => new Promise((res) => requestAnimationFrame(() => res()))

let animateSingleton = undefined

let stopAnimate = () => {
  if (animateSingleton !== undefined) {
    animateSingleton.cancel()
    animateSingleton = undefined
  }
}

let startAnimate = (f) => {
  stopAnimate()
  function* ani(f) {
    let startTime = Date.now()
    while (true) {
      yield requestAnimationFramePromise()
      f(Date.now() - startTime)
    }
  }
  animateSingleton = drive(ani(f))
}

function DrawLines({ turtle, onClick, turtleGoal }) {
  let stateActiveSegments = useSelector((s) => s.activeSegments) || []
  let focusedSegmentDetails = useSelector(
    (s) =>
      s.currentView?.exercise &&
      s.content.tutorialDescriptions?.[s.currentView.exercise] &&
      s.tutorialFocus?.type === "segment" &&
      s.content.segmentDescriptions[s.tutorialFocus.id]
  )
  let mouseOverClick = useSelector((s) => s.mouseOverClick)
  let mouseOverNoclick = useSelector((s) => s.mouseOverNoclick)
  let activeSegments = [...stateActiveSegments, ...(focusedSegmentDetails ? [focusedSegmentDetails] : [])]
  let bigTurtleDissapear = useSelector((s) => s.bigTurtleDissapear)
  let showSmallTurtle = useSelector((s) => s.showSmallTurtle)
  let { lines, lineProcessing } = turtle || {
    lines: [],
    lineProcessing: undefined,
  }

  // const requestRef = React.useRef()
  // const initialTimeRef = React.useRef()

  const [state, setState] = useState({
    drawing: false,
    diff: 0,
  })

  const animate = (timeDiff) => {
    let part = timeDiff / getTimeDelta()
    if (part > 1) part = 1
    setState((state) =>
      produce(state, (state) => {
        state.diff = timeDiff
      })
    )
  }

  useEffect(() => {
    if (lineProcessing) {
      startAnimate(animate)
    } else {
      stopAnimate()
      setState((state) =>
        produce(state, (state) => {
          state.diff = undefined
        })
      )
    }
  }, [lineProcessing])

  useEffect(() => {
    return stopAnimate
  }, [])

  // let decorations = []

  // activeSegments.forEach((x) => {
  //   let candidate = x?.decorations?.[idInInterface]
  //   if (candidate) candidate.forEach((x) => decorations.push(x))
  // })

  let cursorX = 0
  let cursorY = 0
  let angle = 0
  let drawn = [...lines]

  if (drawn.length > 0) {
    let c = drawn[drawn.length - 1]
    cursorX = c.x2
    cursorY = c.y2
    angle = c.endingAngle
  }
  if (lineProcessing && state.diff !== undefined) {
    let candidate = lineProcessing
    let part = state.diff / getTimeDelta()
    part = Math.min(1, part)
    // if (part > 0.999) {
    //   cancelAnimationFrame(requestRef.current)
    // }
    if (candidate.type === "turn") {
      angle = angle + part * candidate.dangle
    } else if (candidate.type === "straight-line") {
      let rise = candidate.y2 - candidate.y1
      let finaly2 = candidate.y1 + part * rise

      let run = candidate.x2 - candidate.x1
      let finalx2 = candidate.x1 + part * run

      drawn.push({
        type: "straight-line",
        x1: candidate.x1,
        y1: candidate.y1,
        x2: finalx2,
        y2: finaly2,
        actuallyDrawn: candidate.actuallyDrawn,
      })
      cursorX = finalx2
      cursorY = finaly2
    } else if (candidate.type === "arc") {
      // let getAngle = (cx, cy, x, y) => {
      //     x -= cx
      //     y -= cy
      //     return Math.atan2(y, x)
      // }

      let centerOfCircle = candidate.center
      let radius = candidate.radius
      //let startingAngle = getAngle(centerOfCircle.x, centerOfCircle.y, candidate.x1, candidate.y1)

      let arcDegrees = (2 * Math.PI * candidate.degrees) / 360

      let startingCircleAngle = (2 * Math.PI * candidate.startingCircleAngle) / 360

      let currentCircleAngle = startingCircleAngle + arcDegrees * part

      let finalx2 = radius * Math.cos(currentCircleAngle) + centerOfCircle.x
      let finaly2 = radius * Math.sin(currentCircleAngle) + centerOfCircle.y

      cursorX = finalx2
      cursorY = finaly2

      drawn.push({
        ...candidate,
        degrees: candidate.degrees * part,
      })

      angle = angle + candidate.degrees * part
    }
  }

  let cursorWidth = 26
  let cursorHeight = 44
  let finalCursorX = cursorX - cursorWidth / 2 + width / 2
  let finalCursorY = -cursorY - cursorHeight / 2 + height / 2
  let inner = undefined

  inner = (
    <>
      <div
        style={{
          position: "absolute",
          left: "0px",
          top: "0px",
          right: "0px",
          bottom: "0px",
          transition: "opacity 0.3s, text-size 0.3s",
          opacity: turtle || turtleGoal || bigTurtleDissapear ? 0 : 1,

          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          color: "#aaa",
        }}
      >
        <GiTurtle color="#555" />
      </div>
      <svg
        style={{ width: `${width}px`, height: `${height}px` }}
        viewBox={`0 0 ${width} ${height}`}
        xmlns="http://www.w3.org/2000/svg"
      >
        {turtleGoal && paint(turtleGoal, "#C0C0C0")}
        {paint(drawn, "black")}
      </svg>

      <span
        src={cursorpng}
        style={{
          transform: `rotate(${-angle + 90}deg)`,
          height: `${cursorHeight}px`,
          width: `${cursorWidth}px`,
          position: "absolute",
          left: `${finalCursorX}px`,
          top: `${finalCursorY}px`,
          fontSize: "25px",
          transition: `opacity ${getTimeDelta() / 1000}s`,
          opacity: !turtle && !turtleGoal && !showSmallTurtle ? 0 : turtle && turtle.pendown ? 1 : 0.4,
        }}
      >
        <GiTurtle />
      </span>
    </>
  )
  // big turtle button
  return (
    <div
      onClick={onClick ? tutorialOnClick("big-turtle-button", onClick) : undefined}
      ref={(r) => {
        let decorations = getDecorationsForID(
          "big-turtle-button",
          activeSegments,
          mouseOverClick,
          mouseOverNoclick
        )
        populateDecorationsRef("big-turtle-button", r, decorations)
      }}
      className={onClick ? "__shadow-2-hover-shadow-4 __80-to-85-on-hover" : "__shadow-2"}
      style={{
        cursor: onClick ? "pointer" : "auto",
        position: "relative",
        height: "400px",
        width: "400px",
      }}
    >
      {inner}
    </div>
  )
}

let finalDestination = new Map()
let mirrorNode = new Map()
let origins = new Map()
let copyText = new Set()

let relativeTo = new Map()
let leftOffset = new Map()
let topOffset = new Map()

// let highlightElements = ids => {
//     for (let p of mirrorNode) {
//         if (ids.includes(p[0])) {
//             p[1].style.backgroundColor = "black"
//             p[1].style.color = "white"
//         }
//         let n = p[1]
//         n.style.animationDuration = "0s"
//     }
// }

let animRef = (ref, id, options) => {
  if (!ref) return
  finalDestination.set(id, ref)
  if (options && options.origin) {
    origins.set(id, options.origin)
  }
  if (options && options.copyText) {
    copyText.add(id)
  }

  if (options && options.relative) {
    relativeTo.set(id, options.relative)
  }
  if (options && options.left) {
    leftOffset.set(id, options.left)
  }
  if (options && options.top) {
    topOffset.set(id, options.top)
  }
}
let animStyle = { opacity: 0 } // we still want events to trigger, which is why we can't use visibility: hidden

let getStats = (node) => {
  if (!node) debugger
  var rect = node.getBoundingClientRect()
  let computedStyle = window.getComputedStyle(node)
  let ret = {
    color: computedStyle.color,
    backgroundColor: computedStyle.backgroundColor,
    left: rect.left + window.scrollX,
    top: rect.top + window.scrollY,
    height: computedStyle.height,
    width: computedStyle.width,
    // height: node.offsetHeight + "px",
    // width: node.offsetWidth + "px",
    font: computedStyle.font,
    whiteSpace: computedStyle.whiteSpace,
    zIndex: computedStyle.zIndex,
  }
  if (ret.left === undefined || ret.top === undefined || ret.height === undefined || ret.width === undefined)
    throw new Error("eh")
  return ret
}

let setStats = (node, stats, setSize, leftOffset, topOffset) => {
  leftOffset = leftOffset || 0
  topOffset = topOffset || 0

  if (
    stats.left === undefined ||
    stats.top === undefined ||
    stats.height === undefined ||
    stats.width === undefined
  )
    throw new Error("eh")
  //node.style.transform = `translate(${stats.left}px,${stats.top}px)`
  node.style.position = "absolute"
  node.style.whiteSpace = stats.whiteSpace

  if (optimizeForSpeed) {
    node.style.left = "0px"
    node.style.top = "0px"
    console.log(stats.top + topOffset + "px")
    node.style.transform = `translate(
      ${stats.left + leftOffset + "px"},
      ${stats.top + topOffset + "px"}
    )`
  } else {
    node.style.left = stats.left + leftOffset + "px"
    node.style.top = stats.top + topOffset + "px"
  }
  node.style.animationDuration = getTimeDelta() / 1500 + "s"
  if (stats.font) node.style.font = stats.font
  //if (setSize) {
  node.style.height = stats.height
  node.style.width = stats.width
  node.style.color = stats.color
  node.style.backgroundColor = stats.backgroundColor
  node.style.zIndex = stats.zIndex
  //}
}

let copyShell = (node, cptext) => {
  let ret = node.cloneNode(false)
  ret.style.font = window.getComputedStyle(node).font
  if (cptext) ret.textContent = node.textContent
  ret.style.color = node.style.color
  ret.style.backgroundColor = node.style.backgroundColor
  ret.style.removeProperty("opacity")
  // while (ret.firstChild) {
  //   ret.removeChild(ret.firstChild);
  // }
  //ret.innerHTML = "" // remove the children
  return ret
}
// let newguy = undefined

let refresh = () => {
  // if (newguy) debugger
  let dead = []
  let born = []
  let moved = []

  let keys = Array.from(finalDestination.keys())

  for (let key of keys) {
    let destination = finalDestination.get(key)
    if (!destination) throw Error("what")
    let currentlyInDom = document.contains(destination)
    let wasInDom = mirrorNode.has(key)
    let replaceXY = undefined
    if (relativeTo.has(key)) {
      let relativeId = relativeTo.get(key)
      let relativeNode = finalDestination.get(relativeId)
      if (relativeNode) {
        let relativePosition = getStats(relativeNode)
        replaceXY = {
          top: relativePosition.top + (topOffset.get(key) || 0),
          left: relativePosition.left + (leftOffset.get(key) || 0),
        }
      } else {
        //                debugger
      }
    }

    if (!wasInDom) {
      born.push({
        key,
        destination,
        origin: origins.get(key),
        leftOffset: leftOffset.get(key),
        topOffset: topOffset.get(key),
        replaceXY,
      })
    } else if (wasInDom && !currentlyInDom) {
      dead.push({
        key,
        destination,
        leftOffset: leftOffset.get(key),
        topOffset: topOffset.get(key),
      })
    } else if (currentlyInDom && wasInDom) {
      let mirror = mirrorNode.get(key)
      let currentStats = getStats(destination)
      if (replaceXY) {
        currentStats.top = replaceXY.top
        currentStats.left = replaceXY.left
      }
      if (!currentStats) throw new Error("huh")
      moved.push({
        key,
        mirror,
        destination,
        currentStats,
        replaceXY,
        leftOffset: leftOffset.get(key),
        topOffset: topOffset.get(key),
      })
    } else {
      // wasnt in dom and isn't in dom, nothing should happen
    }
  }

  // born

  for (let i = 0; i < born.length; i++) {
    let origin = born[i].origin
    if (mirrorNode.has(born[i].key)) throw Error("what")
    let stats =
      origin && mirrorNode.get(origin) ? getStats(mirrorNode.get(origin)) : getStats(born[i].destination)
    if (born[i].replaceXY) {
      stats.top = born[i].replaceXY.top
      stats.left = born[i].replaceXY.left
    }

    let mirror = copyShell(born[i].destination, copyText.has(born[i].key))

    setStats(mirror, stats, !copyText.has(born[i].key))
    if (!mirror) throw new Error("eh")
    let td = `${getTimeDelta() / 1000}s`
    let shortTD = `${getTimeDelta() / 3000}s`
    if (optimizeForSpeed) {
      mirror.style.transition = `height ${td}, width ${td}, transform ${td}, color ${shortTD}, background-color ${shortTD}`
    } else {
      mirror.style.transition = `height ${td}, width ${td}, left ${td}, top ${td}, color ${shortTD}, background-color ${shortTD}`
    }

    mirror.style.pointerEvents = "none" // the mirror does not have any events
    mirror.classList.add("born-animation") // opacity will be 1 after this is done
    mirrorNode.set(born[i].key, mirror)
    document.body.appendChild(mirror)
    if (origin && mirrorNode.get(origin)) {
      setStats(mirror, getStats(born[i].destination), !copyText.has(born[i].key))
    }
  }

  // dead
  for (let i = 0; i < dead.length; i++) {
    let mirror = mirrorNode.get(dead[i].key)
    if (!mirror) throw new Error("what")
    mirror.style.visibility = "visible"
    mirror.classList.remove("born-animation")
    mirror.classList.add("death-animation")
    mirror.style.opacity = 0
    //

    setTimeout(() => {
      mirror.style.opacity = 0
      document.body.removeChild(mirror)
    }, getTimeDelta / 2)
    mirrorNode.delete(dead[i].key)
    finalDestination.delete(dead[i].key)
  }

  // moved

  for (let i = 0; i < moved.length; i++) {
    setStats(moved[i].mirror, moved[i].currentStats, moved[i].leftOffset, moved[i].topOffset)
  }
}

let abruptStop = () => {
  for (let p of mirrorNode) {
    let n = p[1]
    n.style.animationDuration = "0s"
  }
  for (let p of mirrorNode) {
    let n = p[1]
    n.style.animationDuration = getTimeDelta() / 2000 + "s"
  }
}

let canvasHeight = height
let canvasWidth = width

export {
  getCurrentSpeed,
  animRef,
  animStyle,
  refresh,
  getTimeDelta,
  DrawLines,
  abruptStop,
  canvasHeight,
  canvasWidth,
  GrayLines,
  setNodeSpeeds,
}
