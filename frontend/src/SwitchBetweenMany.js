import React, { useState, useRef, useEffect } from "react"
import produce from "immer"

export default function SwitchBetweenMany({ options }) {
  const [state, setState] = useState({
    switchingTo: undefined,
    displayed: undefined,
    // mode: tog ? "current-a" : "current-b",
    opacity: 1,
  })

  const timeoutRef = useRef(undefined)

  useEffect(() => {
    let selected = options.find((x) => x[1])
    if (!selected) return
    let selectedId = selected[0]
    if (selectedId === state.switchingTo || selectedId === state.displayed) return
    setState((state) =>
      produce(state, (state) => {
        state.opacity = 0
        state.switchingTo = selectedId
      })
    )

    if (timeoutRef.current) {
      clearTimeout(timeoutRef.current)
      timeoutRef.current = undefined
    }

    timeoutRef.current = setTimeout(() => {
      setState((state) =>
        produce(state, (state) => {
          state.opacity = 1
          state.switchingTo = undefined
          state.displayed = selectedId
        })
      )
    }, 250)
  }, [options])
  let selected = options.find((x) => x[0] === state.displayed)
  if (!selected) return null
  return (
    <div
      style={{
        opacity: state.opacity,
        transition: "opacity 0.25s",
        display: "flex",
        alignItems: "stretch",
      }}
    >
      {selected[2]}
    </div>
  )
}
