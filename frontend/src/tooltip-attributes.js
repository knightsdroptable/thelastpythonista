import { dispatch, store } from "./store.js"
export default (text, alwaysShow) => {
  if (!text) return {}
  return {
    onMouseLeave: (ev) => {
      if (!alwaysShow && store.getState().adminMode) return
      dispatch({ type: "__set-tooltip", tooltip: undefined })
    },

    onMouseEnter: (ev) => {
      if (!alwaysShow && store.getState().adminMode) return
      var rect = ev.target.getBoundingClientRect()

      dispatch({
        type: "__set-tooltip",
        //tooltip: { x: ev.clientX, y: ev.clientY, text },
        tooltip: { x: rect.left, y: rect.top, text },
      })
    },
  }
}
