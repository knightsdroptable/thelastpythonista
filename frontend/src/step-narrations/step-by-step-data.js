import NONLOCAL_IN_CONTEXT_snd_ from "./NONLOCAL_IN_CONTEXT.mp3"
import GLOBAL_IN_CONTEXT_snd_ from "./GLOBAL_IN_CONTEXT.mp3"
import LOOKUP_VAR_INTRO_snd_ from "./LOOKUP_VAR_INTRO.mp3"
import VAR_LOOKUP_HIGHLIGHT_snd_ from "./VAR_LOOKUP_HIGHLIGHT.mp3"
import LOOKUP_PARENT_FRAME_snd_ from "./LOOKUP_PARENT_FRAME.mp3"
import LOOKUP_CURRENT_FRAME_snd_ from "./LOOKUP_CURRENT_FRAME.mp3"
import HIGHLIGHT_NONLOCAL_snd_ from "./HIGHLIGHT_NONLOCAL.mp3"
import EMPHASIZE_CLASSFRAME_snd_ from "./EMPHASIZE_CLASSFRAME.mp3"
import ADD_TO_FRAMEHEAVEN_snd_ from "./ADD_TO_FRAMEHEAVEN.mp3"
import HIGHLIGHT_FUNCTION_CALL_snd_ from "./HIGHLIGHT_FUNCTION_CALL.mp3"
import PUSH_EXCEPT_CONTEXT_snd_ from "./PUSH_EXCEPT_CONTEXT.mp3"
import EXCEPT_POP_snd_ from "./EXCEPT_POP.mp3"
import ADD_EXCEPT_MARKER_snd_ from "./ADD_EXCEPT_MARKER.mp3"
import WHILE_ITERATE_snd_ from "./WHILE_ITERATE.mp3"
import PUSH_WHILE_IN_CONTEXT_snd_ from "./PUSH_WHILE_IN_CONTEXT.mp3"
import WHILE_LOOP_POP_snd_ from "./WHILE_LOOP_POP.mp3"
import ADD_WHILE_MARKER_snd_ from "./ADD_WHILE_MARKER.mp3"
import RESTRICTION_VIOLATED_snd_ from "./RESTRICTION_VIOLATED.mp3"
import TOKEN_QUOTA_snd_ from "./TOKEN_QUOTA.mp3"
import INTERPRETATION_STARTS_snd_ from "./INTERPRETATION_STARTS.mp3"
import HIDE_EXTERNAL_snd_ from "./HIDE_EXTERNAL.mp3"
import WHILE_COND_RESET_snd_ from "./WHILE_COND_RESET.mp3"
import UPDATE_NONLOCAL_ROW_snd_ from "./UPDATE_NONLOCAL_ROW.mp3"
import TURTLE_CIRCLE_snd_ from "./TURTLE_CIRCLE.mp3"
import SUPERCLASS_LIST_RESET_snd_ from "./SUPERCLASS_LIST_RESET.mp3"
import RUNTIME_ERROR_snd_ from "./RUNTIME_ERROR.mp3"
import RESTORED_ORIGINAL_STATE_EXCEPT_snd_ from "./RESTORED_ORIGINAL_STATE_EXCEPT.mp3"
import RESET_FOR_LOOP_snd_ from "./RESET_FOR_LOOP.mp3"
import RESET_ELIF_snd_ from "./RESET_ELIF.mp3"
import REINTEGRATE_EXPRESSION_snd_ from "./REINTEGRATE_EXPRESSION.mp3"
import PUSHED_CONTEXT_snd_ from "./PUSHED_CONTEXT.mp3"
import POPULATE_EXTERNAL_snd_ from "./POPULATE_EXTERNAL.mp3"
import POPPED_CONTEXT_NO_HEAVEN_snd_ from "./POPPED_CONTEXT_NO_HEAVEN.mp3"
import POPPED_CONTEXT_snd_ from "./POPPED_CONTEXT.mp3"
import PENDOWN_snd_ from "./PENDOWN.mp3"
import OR_REDUCTION_snd_ from "./OR_REDUCTION.mp3"
import NO_RETURN_snd_ from "./NO_RETURN.mp3"
import NEW_DICT_snd_ from "./NEW_DICT.mp3"
import METHODS_CREATED_snd_ from "./METHODS_CREATED.mp3"
import LIST_REF_snd_ from "./LIST_REF.mp3"
import LINEMARKER_ELIF_snd_ from "./LINEMARKER_ELIF.mp3"
import INSTANCE_CREATED_snd_ from "./INSTANCE_CREATED.mp3"
import HIGHLIGHT_LIST_LOOKUP_snd_ from "./HIGHLIGHT_LIST_LOOKUP.mp3"
import HIGHLIGHT_GLOBAL_ROW_snd_ from "./HIGHLIGHT_GLOBAL_ROW.mp3"
import FOR_NEXT_ELEM_snd_ from "./FOR_NEXT_ELEM.mp3"
import DICT_REF_snd_ from "./DICT_REF.mp3"
import LIST_APPEND_snd_ from "./LIST_APPEND.mp3"
import LIST_POP_NOARG_snd_ from "./LIST_POP_NOARG.mp3"
import LIST_POP_ARG_snd_ from "./LIST_POP_ARG.mp3"
import RANGE_LIST_OBJECT_CREATED_snd_ from "./RANGE_LIST_OBJECT_CREATED.mp3"
import DREW_TOO_MUCH_snd_ from "./DREW_TOO_MUCH.mp3"
import DREW_TOO_LITTLE_snd_ from "./DREW_TOO_LITTLE.mp3"
import INCORRECT_DRAWING_snd_ from "./INCORRECT_DRAWING.mp3"
import MY_BAD_snd_ from "./MY_BAD.mp3"
import TURTLE_FORWARD_snd_ from "./TURTLE_FORWARD.mp3"
import TURTLE_RIGHT_snd_ from "./TURTLE_RIGHT.mp3"
import TURTLE_LEFT_snd_ from "./TURTLE_LEFT.mp3"
import PENUP_snd_ from "./PENUP.mp3"
import GOBACK_MARKER_GUTTER_snd_ from "./GOBACK_MARKER_GUTTER.mp3"
import HIGHLIGHT_RETURN_snd_ from "./HIGHLIGHT_RETURN.mp3"
import HIGHLIGHT_LIST_OBJECT_snd_ from "./HIGHLIGHT_LIST_OBJECT.mp3"
import HIGHLIGHT_LIST_INDEX_snd_ from "./HIGHLIGHT_LIST_INDEX.mp3"
import HIGHLIGHT_DICT_OBJECT_snd_ from "./HIGHLIGHT_DICT_OBJECT.mp3"
import HIGHLIGHT_DICT_KEY_snd_ from "./HIGHLIGHT_DICT_KEY.mp3"
import HIGHLIGHT_VAR_IN_FRAME_snd_ from "./HIGHLIGHT_VAR_IN_FRAME.mp3"
import REPLACE_VAR_snd_ from "./REPLACE_VAR.mp3"
import PLACE_VAR_snd_ from "./PLACE_VAR.mp3"
import NEXT_LINE_snd_ from "./NEXT_LINE.mp3"
import FILL_HOLE_NONE_snd_ from "./FILL_HOLE_NONE.mp3"
import FILL_HOLE_EXP_snd_ from "./FILL_HOLE_EXP.mp3"
import HIGHLIGHT_NONLOCAL_ROW_snd_ from "./HIGHLIGHT_NONLOCAL_ROW.mp3"
import HIGHLIGHT_GLOBAL_snd_ from "./HIGHLIGHT_GLOBAL.mp3"
import UPDATE_GLOBAL_ROW_snd_ from "./UPDATE_GLOBAL_ROW.mp3"
import LIST_UPDATED_snd_ from "./LIST_UPDATED.mp3"
import RESET_STATEMENT_snd_ from "./RESET_STATEMENT.mp3"
import ISOLATED_SO_RESET_EXPR_snd_ from "./ISOLATED_SO_RESET_EXPR.mp3"
import ATTRIBUTE_UPDATED_snd_ from "./ATTRIBUTE_UPDATED.mp3"
import CREATE_CLASSFRAME_snd_ from "./CREATE_CLASSFRAME.mp3"
import CLASS_IN_HEAP_CREATED_snd_ from "./CLASS_IN_HEAP_CREATED.mp3"
import CLASS_CONTEXT_POPPED_snd_ from "./CLASS_CONTEXT_POPPED.mp3"
import CLASSDEF_REF_FRAME_snd_ from "./CLASSDEF_REF_FRAME.mp3"
import ADD_FUNCDEF_MARKER_snd_ from "./ADD_FUNCDEF_MARKER.mp3"
import CREATE_FUNCTION_snd_ from "./CREATE_FUNCTION.mp3"
import FUNCDEF_POPULATE_FRAME_snd_ from "./FUNCDEF_POPULATE_FRAME.mp3"
import FOR_ITERATOR_CONTEXT_snd_ from "./FOR_ITERATOR_CONTEXT.mp3"
import FOR_LOOP_POP_snd_ from "./FOR_LOOP_POP.mp3"
import EXTERNAL_FUNC_FINISHED_snd_ from "./EXTERNAL_FUNC_FINISHED.mp3"
import DETACH_EXPR_snd_ from "./DETACH_EXPR.mp3"
import HIGHLIGHT_METHOD_CALL_snd_ from "./HIGHLIGHT_METHOD_CALL.mp3"
import HIGHLIGHT_FUNCALL_LINE_snd_ from "./HIGHLIGHT_FUNCALL_LINE.mp3"
import MAKE_HOLE_snd_ from "./MAKE_HOLE.mp3"
import BINARY_DONE_snd_ from "./BINARY_DONE.mp3"
import AND_REDUCTION_snd_ from "./AND_REDUCTION.mp3"
import OBJECT_REF_snd_ from "./OBJECT_REF.mp3"
import POPULATE_ONE_LEVEL_snd_ from "./POPULATE_ONE_LEVEL.mp3"
import METHODS_REFS_CREATED_snd_ from "./METHODS_REFS_CREATED.mp3"
import NEW_LIST_snd_ from "./NEW_LIST.mp3"
import HIGHLIGHT_LIST_LOOKUP_INNER_snd_ from "./HIGHLIGHT_LIST_LOOKUP_INNER.mp3"
import BRACKET_LOOKUP_DONE_snd_ from "./BRACKET_LOOKUP_DONE.mp3"
import HIGHLIGHT_DICT_LOOKUP_snd_ from "./HIGHLIGHT_DICT_LOOKUP.mp3"
import HIGHLIGHT_DICT_LOOKUP_INNER_snd_ from "./HIGHLIGHT_DICT_LOOKUP_INNER.mp3"
import DICT_BRACKET_LOOKUP_DONE_snd_ from "./DICT_BRACKET_LOOKUP_DONE.mp3"
import HIGHLIGHT_ATTRIBUTE_LOOKUP_snd_ from "./HIGHLIGHT_ATTRIBUTE_LOOKUP.mp3"
import HIGHLIGHT_ATTRIBUTE_LOOKUP_NAME_snd_ from "./HIGHLIGHT_ATTRIBUTE_LOOKUP_NAME.mp3"
import ATTRIBUTE_LOOKUP_DONE_snd_ from "./ATTRIBUTE_LOOKUP_DONE.mp3"
import TERNARY_COND_snd_ from "./TERNARY_COND.mp3"
import TERNARY_TRUE_snd_ from "./TERNARY_TRUE.mp3"
import TERNARY_FALSE_snd_ from "./TERNARY_FALSE.mp3"
import VAR_LOOKUP_snd_ from "./VAR_LOOKUP.mp3"
import VAR_LOOKUP_EXTERNAL_snd_ from "./VAR_LOOKUP_EXTERNAL.mp3"
import PAREN_snd_ from "./PAREN.mp3"
import NOT_EXPR_snd_ from "./NOT_EXPR.mp3"
export const NONLOCAL_IN_CONTEXT = {name: "NONLOCAL_IN_CONTEXT", text:"We mark the variable as nonlocal in the context", sndFile: NONLOCAL_IN_CONTEXT_snd_}
export const GLOBAL_IN_CONTEXT = {name: "GLOBAL_IN_CONTEXT", text:"We mark the variable as global in the context", sndFile: GLOBAL_IN_CONTEXT_snd_}
export const LOOKUP_VAR_INTRO = {name: "LOOKUP_VAR_INTRO", text:"We need to evaluate this variable.", sndFile: LOOKUP_VAR_INTRO_snd_}
export const VAR_LOOKUP_HIGHLIGHT = {name: "VAR_LOOKUP_HIGHLIGHT", text:"The variable is in the frame.", sndFile: VAR_LOOKUP_HIGHLIGHT_snd_}
export const LOOKUP_PARENT_FRAME = {name: "LOOKUP_PARENT_FRAME", text:"The variable is not in that frame, so we look in the parent frame.", sndFile: LOOKUP_PARENT_FRAME_snd_}
export const LOOKUP_CURRENT_FRAME = {name: "LOOKUP_CURRENT_FRAME", text:"We first try to find the variable in the current frame.", sndFile: LOOKUP_CURRENT_FRAME_snd_}
export const HIGHLIGHT_NONLOCAL = {name: "HIGHLIGHT_NONLOCAL", text:"It is a nonlocal variable, so we look for it in an ancestor of the current frame.", sndFile: HIGHLIGHT_NONLOCAL_snd_}
export const EMPHASIZE_CLASSFRAME = {name: "EMPHASIZE_CLASSFRAME", text:"Since the current frame is a class frame, it cannot be in the environment of a function object. Therefore, we take the current frame's parent as the environment of the function object.", sndFile: EMPHASIZE_CLASSFRAME_snd_}
export const ADD_TO_FRAMEHEAVEN = {name: "ADD_TO_FRAMEHEAVEN", text:"We cannot destroy the frame. This is because the frame is in the environment of some function object. When that function gets called, its body may refer to variables in that frame, so we place the frame in frame heaven instead of destroying it.", sndFile: ADD_TO_FRAMEHEAVEN_snd_}
export const HIGHLIGHT_FUNCTION_CALL = {name: "HIGHLIGHT_FUNCTION_CALL", text:"We have to find the function object because it contains two important pieces of information: the function definition line and the environment.", sndFile: HIGHLIGHT_FUNCTION_CALL_snd_}
export const PUSH_EXCEPT_CONTEXT = {name: "PUSH_EXCEPT_CONTEXT", text:"The interpreter needs to keep track of what block to interpret if it encounters an exception while interpreting the try block, so it places the except marker in the context.", sndFile: PUSH_EXCEPT_CONTEXT_snd_}
export const EXCEPT_POP = {name: "EXCEPT_POP", text:"We have finished interpreting the try except statement, so we remove the except marker from the context.", sndFile: EXCEPT_POP_snd_}
export const ADD_EXCEPT_MARKER = {name: "ADD_EXCEPT_MARKER", text:"We mark the beginning of the except block because this is where the interpreter will jump to if it encounters an exception while interpreting the try block.", sndFile: ADD_EXCEPT_MARKER_snd_}
export const WHILE_ITERATE = {name: "WHILE_ITERATE", text:"We finished interpreting the statement block and there is a while loop marker at the bottom of our context so the interpreter jumps back to the marker location.", sndFile: WHILE_ITERATE_snd_}
export const PUSH_WHILE_IN_CONTEXT = {name: "PUSH_WHILE_IN_CONTEXT", text:"The interpreter needs to keep track that it is currently interpreting a while statement so we place the while statement marker in the context.", sndFile: PUSH_WHILE_IN_CONTEXT_snd_}
export const WHILE_LOOP_POP = {name: "WHILE_LOOP_POP", text:"Since the condition expression evaluated to falsy, we have finished interpreting the while statement and we remove it from our context.", sndFile: WHILE_LOOP_POP_snd_}
export const ADD_WHILE_MARKER = {name: "ADD_WHILE_MARKER", text:"We mark the beginning of the while statement because the interpreter will jump back to this line after the statement block has finished being interpreted.", sndFile: ADD_WHILE_MARKER_snd_}
export const RESTRICTION_VIOLATED = {name: "RESTRICTION_VIOLATED", text:"You used something you're not allowed to use. Refer to the box on the top left for more details.", sndFile: RESTRICTION_VIOLATED_snd_}
export const TOKEN_QUOTA = {name: "TOKEN_QUOTA", text:"There are more tokens than are allowed. Refer to the box on the top left for more details.", sndFile: TOKEN_QUOTA_snd_}
export const INTERPRETATION_STARTS = {name: "INTERPRETATION_STARTS", text:"The interpreter starts at the first line.", sndFile: INTERPRETATION_STARTS_snd_}
export const HIDE_EXTERNAL = {name: "HIDE_EXTERNAL", text:"We hide the external variables in the global frame to save space in the interface.", sndFile: HIDE_EXTERNAL_snd_}
export const WHILE_COND_RESET = {name: "WHILE_COND_RESET", text:"The condition expression is reset to its original state.", sndFile: WHILE_COND_RESET_snd_}
export const UPDATE_NONLOCAL_ROW = {name: "UPDATE_NONLOCAL_ROW", text:"We update the value of the variable with the value on the right hand side of the equal symbol.", sndFile: UPDATE_NONLOCAL_ROW_snd_}
export const TURTLE_CIRCLE = {name: "TURTLE_CIRCLE", text:"The turtle draws an arc", sndFile: TURTLE_CIRCLE_snd_}
export const SUPERCLASS_LIST_RESET = {name: "SUPERCLASS_LIST_RESET", text:"The superclass expressions are reset to their original states.", sndFile: SUPERCLASS_LIST_RESET_snd_}
export const RUNTIME_ERROR = {name: "RUNTIME_ERROR", text:"A runtime error has occurred. A description of the error is displayed on the top left corner of the screen.", sndFile: RUNTIME_ERROR_snd_}
export const RESTORED_ORIGINAL_STATE_EXCEPT = {name: "RESTORED_ORIGINAL_STATE_EXCEPT", text:"We clear the contexts that were created after the try block started being interpreted.", sndFile: RESTORED_ORIGINAL_STATE_EXCEPT_snd_}
export const RESET_FOR_LOOP = {name: "RESET_FOR_LOOP", text:"The expression before the colon is reset to its original state.", sndFile: RESET_FOR_LOOP_snd_}
export const RESET_ELIF = {name: "RESET_ELIF", text:"The condition expression is reset to its original state.", sndFile: RESET_ELIF_snd_}
export const REINTEGRATE_EXPRESSION = {name: "REINTEGRATE_EXPRESSION", text:"We replace the line with the computed value of the expression.", sndFile: REINTEGRATE_EXPRESSION_snd_}
export const PUSHED_CONTEXT = {name: "PUSHED_CONTEXT", text:"We add a new context which contains a frame to store our local variables and their values.", sndFile: PUSHED_CONTEXT_snd_}
export const POPULATE_EXTERNAL = {name: "POPULATE_EXTERNAL", text:"We add the variables of the package to the global frame.", sndFile: POPULATE_EXTERNAL_snd_}
export const POPPED_CONTEXT_NO_HEAVEN = {name: "POPPED_CONTEXT_NO_HEAVEN", text:"The body of the function has finished being interpreted so the local variables are no longer accessible so we destroy the current context.", sndFile: POPPED_CONTEXT_NO_HEAVEN_snd_}
export const POPPED_CONTEXT = {name: "POPPED_CONTEXT", text:"The body of the function has finished being interpreted, so we remove the context.", sndFile: POPPED_CONTEXT_snd_}
export const PENDOWN = {name: "PENDOWN", text:"The pen is placed on the paper. The turtle is drawing again.", sndFile: PENDOWN_snd_}
export const OR_REDUCTION = {name: "OR_REDUCTION", text:"The expression on the left of the or operator evaluated to falsy, so the expression is replaced with what is on the right hand side of the or expression", sndFile: OR_REDUCTION_snd_}
export const NO_RETURN = {name: "NO_RETURN", text:"The function did not return, so the return value is None. The hole symbol is replaced with this value.", sndFile: NO_RETURN_snd_}
export const NEW_DICT = {name: "NEW_DICT", text:"A new dictionary is created in the heap.", sndFile: NEW_DICT_snd_}
export const METHODS_CREATED = {name: "METHODS_CREATED", text:"For every attribute that contains a function reference, we create a method object.", sndFile: METHODS_CREATED_snd_}
export const LIST_REF = {name: "LIST_REF", text:"The list literal expression is replaced with a reference to the list in the heap.", sndFile: LIST_REF_snd_}
export const LINEMARKER_ELIF = {name: "LINEMARKER_ELIF", text:"We move the line marker.", sndFile: LINEMARKER_ELIF_snd_}
export const INSTANCE_CREATED = {name: "INSTANCE_CREATED", text:"An instance object is created in the heap.", sndFile: INSTANCE_CREATED_snd_}
export const HIGHLIGHT_LIST_LOOKUP = {name: "HIGHLIGHT_LIST_LOOKUP", text:"Using the reference, we find the object in the heap. It is a list", sndFile: HIGHLIGHT_LIST_LOOKUP_snd_}
export const HIGHLIGHT_GLOBAL_ROW = {name: "HIGHLIGHT_GLOBAL_ROW", text:"We find the variable.", sndFile: HIGHLIGHT_GLOBAL_ROW_snd_}
export const FOR_NEXT_ELEM = {name: "FOR_NEXT_ELEM", text:"The variable moves on to the next element in the list.", sndFile: FOR_NEXT_ELEM_snd_}
export const DICT_REF = {name: "DICT_REF", text:"The dictionary literal is replaced with a reference to the dictionary object in the heap.", sndFile: DICT_REF_snd_}
export const LIST_APPEND = {name: "LIST_APPEND", text:"We append an element to a list object and thereby increase the list's size.", sndFile: LIST_APPEND_snd_}
export const LIST_POP_NOARG = {name: "LIST_POP_NOARG", text:"We pop the last element of the list.", sndFile: LIST_POP_NOARG_snd_}
export const LIST_POP_ARG = {name: "LIST_POP_ARG", text:"We pop an element at a specific index of the list.", sndFile: LIST_POP_ARG_snd_}
export const RANGE_LIST_OBJECT_CREATED = {name: "RANGE_LIST_OBJECT_CREATED", text:"range creates a list and returns a reference to that list.", sndFile: RANGE_LIST_OBJECT_CREATED_snd_}
export const DREW_TOO_MUCH = {name: "DREW_TOO_MUCH", text:"Too much was drawn.", sndFile: DREW_TOO_MUCH_snd_}
export const DREW_TOO_LITTLE = {name: "DREW_TOO_LITTLE", text:"You're missing elements from your drawing.", sndFile: DREW_TOO_LITTLE_snd_}
export const INCORRECT_DRAWING = {name: "INCORRECT_DRAWING", text:"Your drawing is incorrect.", sndFile: INCORRECT_DRAWING_snd_}
export const MY_BAD = {name: "MY_BAD", text:"Jacques made a mistake. Please contact Jacques to tell him.", sndFile: MY_BAD_snd_}
export const TURTLE_FORWARD = {name: "TURTLE_FORWARD", text:"The turtle moves forward.", sndFile: TURTLE_FORWARD_snd_}
export const TURTLE_RIGHT = {name: "TURTLE_RIGHT", text:"The turtle turns right.", sndFile: TURTLE_RIGHT_snd_}
export const TURTLE_LEFT = {name: "TURTLE_LEFT", text:"The turtle turns left.", sndFile: TURTLE_LEFT_snd_}
export const PENUP = {name: "PENUP", text:"The pen is lifted. The turtle is no longer drawing.", sndFile: PENUP_snd_}
export const GOBACK_MARKER_GUTTER = {name: "GOBACK_MARKER_GUTTER", text:"We mark the line that we are isolating with an identifier so that we can jump back to it once the expression is evaluated.", sndFile: GOBACK_MARKER_GUTTER_snd_}
export const HIGHLIGHT_RETURN = {name: "HIGHLIGHT_RETURN", text:"We look for the identifier that corresponds to our line.", sndFile: HIGHLIGHT_RETURN_snd_}
export const HIGHLIGHT_LIST_OBJECT = {name: "HIGHLIGHT_LIST_OBJECT", text:"We look in the heap for the object at the reference's address. It is a list object.", sndFile: HIGHLIGHT_LIST_OBJECT_snd_}
export const HIGHLIGHT_LIST_INDEX = {name: "HIGHLIGHT_LIST_INDEX", text:"We look for the index indicated by the value inside the brackets.", sndFile: HIGHLIGHT_LIST_INDEX_snd_}
export const HIGHLIGHT_DICT_OBJECT = {name: "HIGHLIGHT_DICT_OBJECT", text:"We look in the heap for the object at the reference's address. It is a dictionary object.", sndFile: HIGHLIGHT_DICT_OBJECT_snd_}
export const HIGHLIGHT_DICT_KEY = {name: "HIGHLIGHT_DICT_KEY", text:"We look for the key equal to the value inside the brackets.", sndFile: HIGHLIGHT_DICT_KEY_snd_}
export const HIGHLIGHT_VAR_IN_FRAME = {name: "HIGHLIGHT_VAR_IN_FRAME", text:"We look for the variable in the frame.", sndFile: HIGHLIGHT_VAR_IN_FRAME_snd_}
export const REPLACE_VAR = {name: "REPLACE_VAR", text:"We replace the value of the variable in the frame with the value on the right hand side of the equal sign in our statement.", sndFile: REPLACE_VAR_snd_}
export const PLACE_VAR = {name: "PLACE_VAR", text:"We add the variable to the current frame.", sndFile: PLACE_VAR_snd_}
export const NEXT_LINE = {name: "NEXT_LINE", text:"The next line is interpreted", sndFile: NEXT_LINE_snd_}
export const FILL_HOLE_NONE = {name: "FILL_HOLE_NONE", text:"Since the return statement does not have an expression, it fills the hole that was previously created with the value None.", sndFile: FILL_HOLE_NONE_snd_}
export const FILL_HOLE_EXP = {name: "FILL_HOLE_EXP", text:"The hole that was previously created is replaced with the value to the right of the return keyword.", sndFile: FILL_HOLE_EXP_snd_}
export const HIGHLIGHT_NONLOCAL_ROW = {name: "HIGHLIGHT_NONLOCAL_ROW", text:"We find the variable.", sndFile: HIGHLIGHT_NONLOCAL_ROW_snd_}
export const HIGHLIGHT_GLOBAL = {name: "HIGHLIGHT_GLOBAL", text:"It is a global variable, so we look for it in the global frame.", sndFile: HIGHLIGHT_GLOBAL_snd_}
export const UPDATE_GLOBAL_ROW = {name: "UPDATE_GLOBAL_ROW", text:"We update the value of the variable with the value on the right hand side of the equal symbol.", sndFile: UPDATE_GLOBAL_ROW_snd_}
export const LIST_UPDATED = {name: "LIST_UPDATED", text:"The list has been updated.", sndFile: LIST_UPDATED_snd_}
export const RESET_STATEMENT = {name: "RESET_STATEMENT", text:"The statement is reset to its original state.", sndFile: RESET_STATEMENT_snd_}
export const ISOLATED_SO_RESET_EXPR = {name: "ISOLATED_SO_RESET_EXPR", text:"The expression was isolated so it is reset in the sourcecode.", sndFile: ISOLATED_SO_RESET_EXPR_snd_}
export const ATTRIBUTE_UPDATED = {name: "ATTRIBUTE_UPDATED", text:"The value of the attribute is updated.", sndFile: ATTRIBUTE_UPDATED_snd_}
export const CREATE_CLASSFRAME = {name: "CREATE_CLASSFRAME", text:"We create a frame. In the future, the variables in this frame will become attributes of a class object in the heap.", sndFile: CREATE_CLASSFRAME_snd_}
export const CLASS_IN_HEAP_CREATED = {name: "CLASS_IN_HEAP_CREATED", text:"We create a class object in the heap.", sndFile: CLASS_IN_HEAP_CREATED_snd_}
export const CLASS_CONTEXT_POPPED = {name: "CLASS_CONTEXT_POPPED", text:"The class context is no longer needed, so it is destroyed.", sndFile: CLASS_CONTEXT_POPPED_snd_}
export const CLASSDEF_REF_FRAME = {name: "CLASSDEF_REF_FRAME", text:"A variable is added to the current frame. The value of that variable is a reference to the class object.", sndFile: CLASSDEF_REF_FRAME_snd_}
export const ADD_FUNCDEF_MARKER = {name: "ADD_FUNCDEF_MARKER", text:"A marker is added to the function definition line. That marker will be used to associate function objects with the function definition.", sndFile: ADD_FUNCDEF_MARKER_snd_}
export const CREATE_FUNCTION = {name: "CREATE_FUNCTION", text:"A function object is created.", sndFile: CREATE_FUNCTION_snd_}
export const FUNCDEF_POPULATE_FRAME = {name: "FUNCDEF_POPULATE_FRAME", text:"A variable with the funtion's name is added to the current frame. The value of that variable is a reference to the function object.", sndFile: FUNCDEF_POPULATE_FRAME_snd_}
export const FOR_ITERATOR_CONTEXT = {name: "FOR_ITERATOR_CONTEXT", text:"The list and the variable are copied into the current context to keep track of the value of the variable.", sndFile: FOR_ITERATOR_CONTEXT_snd_}
export const FOR_LOOP_POP = {name: "FOR_LOOP_POP", text:"The for loop is done so we no longer need the list in the context.", sndFile: FOR_LOOP_POP_snd_}
export const EXTERNAL_FUNC_FINISHED = {name: "EXTERNAL_FUNC_FINISHED", text:"We replace the function call with the return value.", sndFile: EXTERNAL_FUNC_FINISHED_snd_}
export const DETACH_EXPR = {name: "DETACH_EXPR", text:"We will move the expression to another part of our interface so that we can focus on it.", sndFile: DETACH_EXPR_snd_}
export const HIGHLIGHT_METHOD_CALL = {name: "HIGHLIGHT_METHOD_CALL", text:"We're calling a method, so there is an implicit first argument to the function call.", sndFile: HIGHLIGHT_METHOD_CALL_snd_}
export const HIGHLIGHT_FUNCALL_LINE = {name: "HIGHLIGHT_FUNCALL_LINE", text:"According to our function object, this is the function definition line, which determines the parameters and the statements to be interpreted.", sndFile: HIGHLIGHT_FUNCALL_LINE_snd_}
export const MAKE_HOLE = {name: "MAKE_HOLE", text:"This newly created hole symbol will be replaced with the function call return value.", sndFile: MAKE_HOLE_snd_}
export const BINARY_DONE = {name: "BINARY_DONE", text:"The binary expression is evaluated", sndFile: BINARY_DONE_snd_}
export const AND_REDUCTION = {name: "AND_REDUCTION", text:"The expression on the left of the and operator evaluated to truthy, so the expression is replaced with what is on the right hand side of the or expression", sndFile: AND_REDUCTION_snd_}
export const OBJECT_REF = {name: "OBJECT_REF", text:"The expression is replaced by a reference to the object in the heap.", sndFile: OBJECT_REF_snd_}
export const POPULATE_ONE_LEVEL = {name: "POPULATE_ONE_LEVEL", text:"The superclass attributes are copied to the new object.", sndFile: POPULATE_ONE_LEVEL_snd_}
export const METHODS_REFS_CREATED = {name: "METHODS_REFS_CREATED", text:"For every attribute that contains a function reference, we replace its value with the corresponding method reference.", sndFile: METHODS_REFS_CREATED_snd_}
export const NEW_LIST = {name: "NEW_LIST", text:"A list object is created in the heap.", sndFile: NEW_LIST_snd_}
export const HIGHLIGHT_LIST_LOOKUP_INNER = {name: "HIGHLIGHT_LIST_LOOKUP_INNER", text:"Using the index inside the brackets, we locate the element of the list that we're retrieving.", sndFile: HIGHLIGHT_LIST_LOOKUP_INNER_snd_}
export const BRACKET_LOOKUP_DONE = {name: "BRACKET_LOOKUP_DONE", text:"We replace the bracket expression with the value in the list object.", sndFile: BRACKET_LOOKUP_DONE_snd_}
export const HIGHLIGHT_DICT_LOOKUP = {name: "HIGHLIGHT_DICT_LOOKUP", text:"Using the reference, we find the object in the heap. It is a dictionary object", sndFile: HIGHLIGHT_DICT_LOOKUP_snd_}
export const HIGHLIGHT_DICT_LOOKUP_INNER = {name: "HIGHLIGHT_DICT_LOOKUP_INNER", text:"Using the value inside the brackets, we locate the element of the dictionary that we're retrieving.", sndFile: HIGHLIGHT_DICT_LOOKUP_INNER_snd_}
export const DICT_BRACKET_LOOKUP_DONE = {name: "DICT_BRACKET_LOOKUP_DONE", text:"We replace the bracket expression with the value in the dictionary object.", sndFile: DICT_BRACKET_LOOKUP_DONE_snd_}
export const HIGHLIGHT_ATTRIBUTE_LOOKUP = {name: "HIGHLIGHT_ATTRIBUTE_LOOKUP", text:"Using the reference, we find the object in the heap.", sndFile: HIGHLIGHT_ATTRIBUTE_LOOKUP_snd_}
export const HIGHLIGHT_ATTRIBUTE_LOOKUP_NAME = {name: "HIGHLIGHT_ATTRIBUTE_LOOKUP_NAME", text:"Using the identifier beside the dot, we locate the attribute of the object that we're retrieving.", sndFile: HIGHLIGHT_ATTRIBUTE_LOOKUP_NAME_snd_}
export const ATTRIBUTE_LOOKUP_DONE = {name: "ATTRIBUTE_LOOKUP_DONE", text:"We replace the bracket expression with the value in the instance object.", sndFile: ATTRIBUTE_LOOKUP_DONE_snd_}
export const TERNARY_COND = {name: "TERNARY_COND", text:"We evaluate the condition expression of the ternary operator.", sndFile: TERNARY_COND_snd_}
export const TERNARY_TRUE = {name: "TERNARY_TRUE", text:"The condition expression evaluated to truthy, so we only evaluate the middle expression.", sndFile: TERNARY_TRUE_snd_}
export const TERNARY_FALSE = {name: "TERNARY_FALSE", text:"The condition expression evaluated to false, so we only evaluate the last expression.", sndFile: TERNARY_FALSE_snd_}
export const VAR_LOOKUP = {name: "VAR_LOOKUP", text:"We replace the variable with its associated value in the frame.", sndFile: VAR_LOOKUP_snd_}
export const VAR_LOOKUP_EXTERNAL = {name: "VAR_LOOKUP_EXTERNAL", text:"We replace the variable with its associated value.", sndFile: VAR_LOOKUP_EXTERNAL_snd_}
export const PAREN = {name: "PAREN", text:"The value inside the parentheses is the value that we're computing.", sndFile: PAREN_snd_}
export const NOT_EXPR = {name: "NOT_EXPR", text:"We apply the not operator, which evaluates falsy values to true and truthy values to false.", sndFile: NOT_EXPR_snd_}
