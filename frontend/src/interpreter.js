import { diff, addedDiff, deletedDiff, updatedDiff, detailedDiff } from "deep-object-diff"
import { collectTokens } from "./collect-tokens"
import { flatten } from "array-flatten"
import { deepEqual } from "fast-equals"
import fastcopy from "fast-copy"
import * as instr from "./step-narrations/step-by-step-data.js"

let mkTooltipf = (arr) => {
  return ["Address in memory of the function", ...arr]
}
let mkTooltip = (arr) => arr

function download(filename, text) {
  var element = document.createElement("a")
  element.setAttribute("href", "data:text/plain;charset=utf-8," + encodeURIComponent(text))
  element.setAttribute("download", filename)

  element.style.display = "none"
  document.body.appendChild(element)

  element.click()

  document.body.removeChild(element)
}

// let downloadSounds = () => {
//   for (let x of instr) {
//     console.log(x)
//   }
// }
// downloadSounds()

let isObject = (yourVariable) => typeof yourVariable === "object" && yourVariable !== null

let distance = (x1, y1, x2, y2) => {
  let sqr = (x) => x * x
  return Math.sqrt(sqr(x1 - x2) + sqr(y1 - y2))
}

let clone = (x) => {
  return fastcopy(x)
}

// let cloneIfDifferent = (before, now) => {
//     if (fasteq(before, now)) return before
//     return fastclone(now)
// }

// let whichIsDifferent = (a, b) => {
//     let keys = Object.keys(a)
//     for (let i = 0; i < keys.length; i++) {
//         let k = keys[i]
//         if (!R.equals(a[k], b[k])) debugger
//     }
// }

let addToStepDescriptor = (desc, title) => {
  if (!desc.steps[title]) {
    desc.steps[title] = 0
  }
  desc.steps[title]++
}

let getCurrentStepDescriptor = (desc, title) => {
  let lineNum = desc.lineNum
  if (lineNum === undefined) throw new Error("huh3")
  let lineTimesSeen = desc.lineSeenMap[lineNum] || 1
  if (lineTimesSeen === undefined) throw new Error("huh")
  let stepTimesSeen = desc.steps[title]
  if (stepTimesSeen === undefined) throw new Error("huh2")

  if (title === undefined) throw new Error("huh4")
  return {
    lineNum,
    lineTimesSeen,
    stepTimesSeen,
    stepName: title,
  }
}

let replace = (a, b) => {
  if (a === b) return
  let keys = Object.keys(a)
  for (let i = 0; i < keys.length; i++) {
    delete a[keys[i]]
  }
  Object.assign(a, b)
}

let isFalsyValue = (x) => {
  return (
    (x.primitiveType === "int" && x.value === 0) ||
    (x.primitiveType === "bool" && x.value === false) ||
    (x.primitiveType === "string" && x.value.length === 0) ||
    x.primitiveType === "none"
  )
}

let addOrigin = (x, id) => {
  return { ...x, origin: id }
}

let nontokens = (arr) => {
  let ret = []
  for (let i = 0; i < arr.length; i++) {
    let e = arr[i]
    if (e.category !== "token") {
      ret.push(e)
    }
  }
  return ret
}

let findDifference = (actual, expected) => {
  let neq = (a, b) => {
    return Math.abs(a - b) > 0.1
  }
  if (actual.type === "arc" && expected.type === "straight-line") {
    return "You drew an arc but a line was expected"
  }
  if (actual.type === "straight-line" && expected.type === "arc") {
    return "You drew a straight line but an arc was expected"
  }
  if (actual.type !== expected.type) {
    return "You drew the wrong thing"
  }

  if (actual.type === "straight-line") {
    if (neq(actual.x1, expected.x1) || neq(actual.y1, expected.y1)) {
      return "The line is starting at the wrong position"
    }
    if (neq(actual.x2, expected.x2) || neq(actual.y2, expected.y2)) {
      return "The line ends at the wrong position"
    }
  }
  if (actual.type === "arc") {
    if (neq(actual.x1, expected.x1) || neq(actual.y1, expected.y1)) {
      return "The arc is starting at the wrong position"
    }
    if (neq(actual.radius, expected.radius)) {
      return "The arc has the wrong radius"
    }
    if (neq(actual.degrees, expected.degrees)) {
      return "The arc degree is different"
    }
    if (neq(actual.startingAngle, expected.startingAngle)) {
      return "The starting rotation is different"
    }
    if (neq(actual.center.x, expected.center.x) || neq(actual.center.y, expected.center.y)) {
      return "The center of the arc is off"
    }
  }
}

let interpretProgram = (topAST, expectedLines, interpreterOpts) => {
  let masterGenerCounter = 0
  let masterGener = () => {
    masterGenerCounter++
    let hex = masterGenerCounter.toString(16)
    let padded = hex.padStart(2, 0)
    return `0x${padded}`
  }

  let genAppendAddrCounter = 0
  let genAppendAddr = () => {
    return masterGener()
    genAppendAddrCounter++
    return "appn-" + genAppendAddrCounter
  }

  let genPopAddrCounter = 0
  let genPopAddr = () => {
    return masterGener()
    genPopAddrCounter++
    return "pop-" + genPopAddrCounter
  }

  let genMethodHeapIdCounter = 0
  let genMethodHeapId = () => {
    return masterGener()
    genMethodHeapIdCounter++
    return "h-" + genMethodHeapIdCounter
  }

  let genMethodrefIdCounter = 0
  let genMethodrefId = () => {
    genMethodrefIdCounter++
    return "mref-" + genMethodrefIdCounter
  }

  let genInstanceAttributeRowIdCounter = 0
  let genInstanceAttributeRowId = () => {
    genInstanceAttributeRowIdCounter++
    return "inst-" + genInstanceAttributeRowIdCounter
  }

  let genObjectrefIdCounter = 0
  let genObjectrefId = () => {
    genObjectrefIdCounter++
    return "objref-" + genObjectrefIdCounter
  }

  let genObjectHeapIdCounter = 0
  let genObjectHeapId = () => {
    return masterGener()
    genObjectHeapIdCounter++
    return "obj-" + genObjectHeapIdCounter
  }

  let genClassrefIdCounter = 0
  let genClassrefId = () => {
    genClassrefIdCounter++
    return "cr-" + genClassrefIdCounter
  }

  let genAttributeRowIdCounter = 0
  let genAttributeRowId = () => {
    genAttributeRowIdCounter++
    return "attr-" + genAttributeRowIdCounter
  }
  let genClassIdHeapIdCounter = 0
  let genClassIdHeapId = () => {
    return masterGener()
    genClassIdHeapIdCounter++
    return "cl-" + genClassIdHeapIdCounter
  }

  let genExternalVarIdCounter = 0
  let genExternalVarId = () => {
    genExternalVarIdCounter++
    return "ext-" + genExternalVarIdCounter
  }

  let genFuncdefMarkerIdCounter = 0
  let genFuncdefMarkerId = () => {
    genFuncdefMarkerIdCounter++
    return "fdmarker-" + genFuncdefMarkerIdCounter
  }

  let genWhileLoopMarkerIdCounter = 0
  let genwhileLoopMarkerId = () => {
    genWhileLoopMarkerIdCounter++
    return "while-" + genWhileLoopMarkerIdCounter
  }

  let genExceptMarkerIdCounter = 0
  let genExceptMarkerId = () => {
    genExceptMarkerIdCounter++
    return "ex-" + genExceptMarkerIdCounter
  }

  let genForloopIdCounter = 0
  let genForloopId = () => {
    genForloopIdCounter++
    return "for-loop-" + genForloopIdCounter
  }

  let genWhileLoopIdCounter = 0
  let genWhileloopId = () => {
    genWhileLoopIdCounter++
    return "while-loop-" + genWhileLoopIdCounter
  }

  let genExceptIdCounter = 0
  let genExceptId = () => {
    genExceptIdCounter++
    return "ex-" + genExceptIdCounter
  }

  let genFrameRowIdCounter = 0
  let genFrameRowId = () => {
    genFrameRowIdCounter++
    return "frm-row-" + genFrameRowIdCounter
  }

  let genDictRefValueIdCounter = 0
  let genDictRefValueId = () => {
    genDictRefValueIdCounter++
    return "dict-ref-value" + genDictRefValueIdCounter
  }

  let genArrayRefValueIdCounter = 0
  let genArrayRefValueId = () => {
    genArrayRefValueIdCounter++
    return "array-ref" + genArrayRefValueIdCounter
  }

  let tokenIdCounter = 0
  let genTokenId = () => {
    tokenIdCounter++
    return "tok-interp-" + tokenIdCounter
  }

  let valueIdCounter = 0
  let genValueId = () => {
    valueIdCounter++
    return "val-" + valueIdCounter
  }

  let funcrefcounter = 0
  let genFuncrefId = () => {
    funcrefcounter++
    return "funcref-" + funcrefcounter
  }

  let heapfunccounter = 0
  let genHeapfuncId = () => {
    return masterGener()
    heapfunccounter++
    return "f-" + heapfunccounter
  }

  let genFrameCounter = 0
  let genFrameId = () => {
    genFrameCounter++
    return "frm-" + genFrameCounter
  }

  let arrayRowCounter = 0
  let genArrayRowId = () => {
    arrayRowCounter++
    return "arr-row-" + arrayRowCounter
  }

  // let genHeapArrayIdCounter = 0
  // let genHeapArrayId = () => {
  //   genHeapArrayIdCounter++
  //   return "a-" + genHeapArrayIdCounter
  // }

  let genDictRowIdCounter = 0
  let genDictRowId = () => {
    genDictRowIdCounter++
    return "dict-row-" + genDictRowIdCounter
  }

  // let genHeapDictIdCounter = 0
  // let genHeapDictId = () => {
  //   genHeapDictIdCounter++
  //   return "d-" + genHeapDictIdCounter
  // }

  let genContextIdCounter = 0
  let genContextId = () => {
    genContextIdCounter++
    return "ctx-" + genContextIdCounter
  }

  let newFrame = (closure, closureToken, classFrame) => {
    if (!closure || !closureToken) throw Error("expecting closure")
    let id = genFrameId()
    return {
      externalLabelToken: mkToken("..."),
      id: id,
      idToken: mkToken(id, { lineMarker: true }),
      label: mkToken("frame"),
      closure,
      rows: [],
      emptyToken: mkToken("empty frame"),
      closureToken,
      closureLabel: mkToken("parent"),
      classFrameLabel: mkToken("class frame"),
      classFrame,
    }
  }

  let propagateUserError = (node) => {
    if (node.category === "token") {
      node.options = { ...node.options, userError: true }
      node.id = genTokenId()
      node.origin = undefined
      return
    }

    for (let i = 0; i < node.children.length; i++) {
      propagateUserError(node.children[i])
    }
  }

  let removeUserError = (node) => {
    if (node.category === "token") {
      node.options = { ...node.options, userError: false }
      node.id = genTokenId()
      node.origin = undefined
      return
    }

    for (let i = 0; i < node.children.length; i++) {
      removeUserError(node.children[i])
    }
  }

  let uicpNoOrigin = (node) => {
    if (node.category === "token") {
      return {
        ...node,
        id: genTokenId(),
        origin: undefined,
      }
    }
    let newch = []
    for (let i = 0; i < node.children.length; i++) {
      newch.push(uicpNoOrigin(node.children[i]))
    }
    return { ...node, children: newch }
  }

  let mkToken = (str, options) => {
    //if (typeof str !== "string") throw myBad(undefined, "only strings can be tokens")
    return {
      id: genTokenId(),
      category: "token",
      text: str,
      options,
    }
  }

  let makeCommas = (n) => {
    let ret = []
    for (let i = 0; i < n; i++) {
      ret.push(mkToken(","))
    }
    return ret
  }

  let makeNoneValue = (ch) => {
    return {
      category: "value",
      children: ch ? ch : [mkToken("None")],
      primitiveType: "none",
      value: null,
    }
  }

  // end of helpers

  interpreterOpts = interpreterOpts || {}
  let overToken = interpreterOpts.overToken
  let restrictionViolated = interpreterOpts.restrictionViolated
  let swimmingly = true
  expectedLines = expectedLines && [...expectedLines]

  let compareLine = (actual, sourceNode) => {
    if (!expectedLines) return
    if (expectedLines.length === 0) {
      throw drewTooMuchExc(sourceNode, "You drew too much")
    }
    let expected = expectedLines.shift()
    let difference = findDifference(actual, expected)
    if (difference) {
      if (turtle.lineProcessing) {
        turtle.lines.push(turtle.lineProcessing)
        turtle.lineProcessing = undefined
      }
      throw misDrawExc(sourceNode, difference)
    }
  }

  let copysnaptime = 0
  let cloningSourceTokenTime = 0
  let totalclonesourcetime = 0
  let totalCheckDups = 0
  let annotateWithOriginalExpressions = (input) => {
    let doit = (tree) => {
      if (isObject(tree)) doitObj(tree)
      else if (Array.isArray(tree)) doitArr(tree)
      return
    }
    let doitObj = (tree) => {
      if (tree.kind === "statement") {
        let originalExpressions = []
        for (let i = 0; i < tree.children.length; i++) {
          let ch = tree.children[i]
          if (ch.kind === "expr") {
            originalExpressions.push({ index: i, expr: clone(ch) })
          }
        }
        tree.originalExpressions = originalExpressions
      }
      let subs = tree.children || []
      subs.forEach(doit)
    }
    let doitArr = (arr) => {
      for (let i = 0; i < arr.length; i++) {
        doit(arr[i])
      }
    }
    return doit(input)
  }

  let restoreOriginalExpressions = (input) => {
    let doit = (tree) => {
      if (isObject(tree)) doitObj(tree)
      else if (Array.isArray(tree)) doitArr(tree)
      return
    }
    let doitObj = (tree) => {
      if (tree.kind === "statement") {
        if (!tree.originalExpressions) throw myBad("expected original expresions")

        let originals = tree.originalExpressions

        for (let i = 0; i < originals.length; i++) {
          let candidate = originals[i]
          tree.children[candidate.index] = clone(candidate.expr)
        }
      }
      let chs = tree.children || []
      chs.forEach(doit)
    }
    let doitArr = (arr) => {
      for (let i = 0; i < arr.length; i++) {
        doit(arr[i])
      }
    }
    return doit(input)
  }
  annotateWithOriginalExpressions(topAST)

  let restoreAllExpressions = () => {
    restoreOriginalExpressions(topAST)
  }

  let makeArrayInHeap = (arrayId, values, sourceNode) => {
    let arrayRows = []
    for (let i = 0; i < values.length; i++) {
      arrayRows.push({
        id: genArrayRowId(),
        indexToken: mkToken("" + i),
        value: values[i],
      })
    }

    let appendAddr = genAppendAddr()
    let popAddr = genPopAddr()

    let arrayInHeap = {
      idToken: mkToken(arrayId, { ref: true }),
      id: arrayId,
      label: mkToken("list"),
      type: "heap-array",
      attributeLabelToken: mkToken("attributes"),
      rows: arrayRows,
      attributes: [
        {
          id: genAttributeRowId(),
          key: "append",
          keyToken: mkToken("append"),
          value: {
            id: genFuncrefId(),
            category: "value",
            type: "reference",
            referenceType: "function",
            address: appendAddr,
            children: [
              mkToken(appendAddr, {
                ref: true,
                tooltip: mkTooltipf(["to add an element to the end of", `the array at address ${arrayId}`]),
              }),
            ],
          },
        },
        {
          id: genAttributeRowId(),
          key: "pop",
          keyToken: mkToken("pop"),
          value: {
            id: genFuncrefId(),
            category: "value",
            type: "reference",
            referenceType: "function",
            address: popAddr,
            children: [
              mkToken(popAddr, {
                ref: true,
                tooltip: mkTooltipf(["to pop the last element element from", `the array at address ${arrayId}`]),
              }),
            ],
          },
        },
      ],
    }

    generatedBuiltinFunctions.push({
      id: appendAddr,
      func: (args, sourceNode) => {
        if (args.length !== 1) throw userError(sourceNode, "append takes one argument")
        let arg = args[0]
        if (arg.category !== "value") throw myBad(sourceNode, "expected value")
        arrayInHeap.rows.push({
          id: genArrayRowId(),
          indexToken: mkToken("" + arrayInHeap.rows.length),
          value: uicp(arg),
        })
        snap(instr.LIST_APPEND)
        return makeNoneValue()
      },
    })

    generatedBuiltinFunctions.push({
      id: popAddr,
      func: (args, sourceNode) => {
        if (args.length > 1) throw userError(sourceNode, "pop takes 0 or 1 argument")
        if (args.length === 0) {
          if (arrayInHeap.rows.length === 0) throw userError(sourceNode, "cannot pop empty list")
          let r = arrayInHeap.rows.pop()
          snap(instr.LIST_POP_NOARG)
          return uicp(r.value)
        }
        let arg = args[0]
        if (arg.category !== "value") throw myBad(sourceNode, "expected value")
        if (arg.primitiveType !== "int") throw userError(sourceNode, "If you give an argument to pop, it has to be a number")
        let index = arg.value
        if (index >= arrayInHeap.rows.length) throw userError(sourceNode, "Popping index out of range")
        for (let i = index; i < arrayInHeap.rows.length - 1; i++) {
          arrayInHeap.rows[i].value = arrayInHeap.rows[i + 1].value
        }
        let r = arrayInHeap.rows.pop()
        // arrayInHeap.rows.splice(index, 1)
        // arrayInHeap.rows.forEach((row, index) => {
        //     if (row.index !== index) {
        //         row.index = index
        //         row.indexToken = mkToken("" + index)
        //     }
        // })
        snap(instr.LIST_POP_ARG)
        return uicp(r.value)
      },
    })
    return arrayInHeap
  }

  let endoflineTokens = []
  let animationFrames = []
  let externalVarsShown = interpreterOpts.alwaysShowExternal ? undefined : []
  let excAddr = masterGener()
  let lenAddr = "📏"
  let rangeAddr = "🔢"

  let externalVars = {
    rows: [
      {
        name: "Exception",
        value: {
          id: genValueId(),
          category: "value",
          type: "reference",
          referenceType: "class",
          address: excAddr,
          children: [
            mkToken(excAddr, {
              ref: true,
              tooltip: mkTooltip(["Address in memory of the", `Exception class object`]),
            }),
          ],
        },
        id: genExternalVarId(),
        leftToken: mkToken("Exception"),
      },
      {
        name: "len",
        func: (args, sourceNode) => {
          if (args.length !== 1) throw userError(sourceNode, "len takes one argument")
          let arg = args[0]
          if (arg.referenceType !== "array") {
            throw userError(sourceNode, "The argument to len should be a list reference")
          }
          let address = arg.address
          let arr = heap.find((x) => x.id === address)
          if (!arr) throw myBad(sourceNode, "could not find object in heap")
          return makeIntValue(arr.rows.length)
        },
        value: {
          id: genValueId(),
          category: "value",
          type: "reference",
          referenceType: "function",
          address: lenAddr,
          children: [
            mkToken(lenAddr, {
              ref: true,

              tooltip: mkTooltipf([`that returns the length of its argument`]),
            }),
          ],
        },
        id: "genExternalVarId()",
        leftToken: mkToken("len"),
      },
      {
        name: "range",
        func: (args, sourceNode) => {
          if (args.length !== 1 && args.length !== 2) throw userError(sourceNode, "range takes one or two arguments")
          if (args.some((x) => x.primitiveType !== "int")) throw userError(sourceNode, "The arguments to range should be numbers")
          let start, finish
          if (args.length === 1) {
            start = 0
            finish = args[0].value
          } else if (args.length === 2) {
            start = args[0].value
            finish = args[1].value
          }
          let arrayId = masterGener()
          let values = []
          for (let i = start; i < finish; i++) {
            values.push(makeIntValue(i))
          }
          let arrayInHeap = makeArrayInHeap(arrayId, values, sourceNode)
          pushToHeap(arrayInHeap, sourceNode)
          snap(instr.RANGE_LIST_OBJECT_CREATED)
          let newValue = {
            id: genArrayRefValueId(),
            category: "value",
            type: "reference",
            referenceType: "array",
            address: arrayId,
            children: [cpTokenHover(arrayInHeap.idToken)],
          }
          return newValue
        },
        value: {
          id: genValueId(),
          category: "value",
          type: "reference",
          referenceType: "function",
          address: rangeAddr,
          children: [
            mkToken(rangeAddr, {
              ref: true,
              tooltip: mkTooltipf(["that returns an array of numbers", "in the range specified by its arguments"]),
            }),
          ],
        },
        id: genExternalVarId(),
        leftToken: mkToken("range"),
      },
    ],
    hiddenTokens: ["stdlib", "\n", " & ", "imported"].map(mkToken),
  }
  let frameHeaven = []
  //let lineMarker = undefined
  let contexts = [
    {
      label: mkToken(" context "),
      gobackMarker: undefined,
      id: "global-context",
      isolatedExpression: undefined,
      nonlocals: [],
      globals: [],
      loops: [],
      currentlyDefining: [],
      nonlocalToken: mkToken("nonlocal: "),
      globalToken: mkToken("global: "),

      frame: {
        externalLabelToken: mkToken("..."),
        emptyToken: mkToken("empty frame"),
        id: "global-frame",
        label: mkToken("frame"),
        idToken: mkToken("global-frame", { lineMarker: true }),
        closure: null,
        rows: externalVars.rows.map((ev) => {
          if (!ev.name) debugger
          if (!ev.value) debugger
          return {
            id: genFrameRowId(),
            left: {
              name: ev.name,
              token: mkToken(ev.name),
            },
            right: ev.value,
            isExternal: true,
          }
        }),

        closureLabel: mkToken("parent"),
      },
    },
  ]

  let exprInSource = undefined
  let resetExpression = undefined
  let markCurrentLine = undefined
  let heap = []

  let newContext = (frame) => {
    return {
      label: mkToken(" context "),
      id: genContextId(),
      frame,
      loops: [],
      isolatedExpression: undefined,
      nonlocals: [],
      globals: [],
      currentlyDefining: [],
      gobackMarker: undefined,
      nonlocalToken: mkToken("nonlocal: "),
      globalToken: mkToken("global: "),
    }
  }
  let hasIsolatedExpresionInCurrentFrame = () => {
    return contexts[contexts.length - 1].isolatedExpression !== undefined
  }

  let setIsolatedExpresionInCurrentFrame = (e) => {
    contexts[contexts.length - 1].isolatedExpression = e
  }
  let getIsolatedExpresionInCurrentFrame = (e) => {
    return contexts[contexts.length - 1].isolatedExpression
  }

  // let assignVar = (varname, varvalue) => {
  //     let rows = contexts[contexts.length - 1].frame.rows

  //     for (let i = 0; i < rows.length; i++) {
  //         if (rows[i].left.name === varname.name) {
  //             rows[i].right = varvalue
  //             return
  //         }
  //     }
  //     rows.push({ left: varname, right: varvalue })
  // }

  let turtle = {
    pendown: true,
    angle: 0,
    turtleX: 0,
    turtleY: 0,
    lines: [],
    processing: false,
  }

  let currentFocusedStatement = undefined
  let gobackGutter = []
  //let stdlibImports = { shown: false, hiddenTokens: ["stdlib", "\n", " & ", "imported"].map(mkToken) }

  let highlightedElements = []

  let setHighlightedElements = (arr) => {
    for (let i = 0; i < arr.length; i++) {
      if (!arr[i]) throw myBad(undefined, "missing highlighted element")
    }
    let unflattened = arr.map((e) => collectTokens(e))

    let toks = flatten(unflattened)
    let ids = toks.map((t) => t.id)
    for (let i = 0; i < ids.length; i++) {
      if (!ids[i]) debugger
    }
    highlightedElements = ids
  }

  // let eqWithIgnore = (left, right, ignored) => {
  //     let doit = (a, b) => {

  //         if (Array.isArray(a) && Array.isArray(b)) return doitArr(a, b)
  //         if (Array.isArray(a) || Array.isArray(b)) return false // they're different

  //         if (isObject(a) && isObject(b)) return doitObj(a, b)
  //         if (isObject(a) || isObject(b)) return false // they're different
  //         return a === b // two primitives
  //     }
  //     let doitArr = (a, b) => {
  //         if (a.length !== b.length) return false
  //         for (let i = 0; i < a.length; i++) {
  //             if (!doit(a[i], b[i])) return false
  //         }
  //         return true
  //     }
  //     let doitObj = (a, b) => {
  //         let keysA = Object.keys(a)
  //         let keysB = Object.keys(b)
  //         if (keysA.length !== keysB.length) return false
  //         keysA.sort()
  //         keysB.sort()
  //         for (let i = 0; i < keysA.length; i++) {
  //             if (keysA[i] !== keysB[i]) return false
  //         }
  //         keysA = keysA.filter(x => !ignored.includes(x))
  //         for (let i = 0; i < keysA.length; i++) {
  //             let k = keysA[i]
  //             if (k === "description") debugger
  //             if (!doit(a[k], b[k])) return false
  //         }
  //         return true
  //     }
  //     return doit(left, right)
  // }

  let hoverAssociations = {}

  let addToHoverAssociations = (a, b) => {
    if (!hoverAssociations[a]) hoverAssociations[a] = []
    hoverAssociations[a].push(b)
  }

  let uicp = (node) => {
    if (node.category === "token") {
      let id = genTokenId()
      if (hoverAssociations[node.id]) {
        hoverAssociations[id] = hoverAssociations[node.id]
      }
      return {
        ...node,
        origin: node.id,
        id,
      }
    }
    let newch = []
    for (let i = 0; i < node.children.length; i++) {
      newch.push(uicp(node.children[i]))
    }
    return { ...node, children: newch }
  }

  let cpTokenHover = (token) => {
    let newid = genTokenId()
    hoverAssociations[newid] = [token.id]
    return {
      ...token,
      origin: token.id,
      id: newid,
    }
  }

  let cloneContexts = (contexts) => {
    let before = clone(contexts)
    contexts.forEach((x) => {
      delete before.currentlyDefining
    })
    return before
  }

  let markerStatementAssociation = new Map()

  let originalSourceTokens = undefined
  let previousLineNumber = undefined
  let lastFrameInstructions = undefined
  let stepDescriptorSofar = undefined
  let snap = (stepDesc, opts) => {
    let sndFile = stepDesc.sndFile
    let title = stepDesc.name
    let stepNarrationText = stepDesc.text

    let description = { title, opts, sndFile, stepNarrationText }
    let lineNum = opts && opts.lineNumber

    let startofsnap = Date.now()

    let refLookupHighlights = hoverAssociations

    let lineMarker = currentFocusedStatement && firstToken(currentFocusedStatement.statement).id
    // if (currentFocusedStatement && currentFocusedStatement.index !== 0) debugger

    let clonedFrameHeaven = clone(frameHeaven)

    let clonedHighlighted = clone(highlightedElements)

    let prev = (str) => {
      let p = animationFrames[animationFrames.length - 1]
      if (!p) return undefined
      return p[str]
    }

    let clonedTurtle = clone(turtle)
    let clonedEndOfLineTokens = clone(endoflineTokens.concat(gobackGutter))
    let clonedLineMarker = clone(lineMarker)

    let startCloningSourceTokens = Date.now()

    //   let clonedRefLookupHighlights = clone(refLookupHighlights)
    let clonedHeap = clone(heap).map((x) => ({ ...x, defASTNode: undefined }))
    let clonedSourceTokens = clone(collectTokens(topAST))
    let clonedContexts = cloneContexts(contexts) // about a third of the time
    let clonedExternalShown = clone(externalVarsShown) // about a third of the time

    cloningSourceTokenTime += Date.now() - startCloningSourceTokens

    let nextFrame = {
      //refLookupHighlights: clonedRefLookupHighlights,
      lastFrameInstructions: lastFrameInstructions,
      highlightedElements: clonedHighlighted,
      newLineNum: lineNum,

      externalVarsShown: clonedExternalShown,
      heap: clonedHeap, // functions, dicts, arrays
      contexts: clonedContexts, // frames, iterations
      sourceTokens: clonedSourceTokens, // sourcecode
      lineMarker: clonedLineMarker, // current line in the sourcecode
      endoflineTokens: clonedEndOfLineTokens, // placed beside a def. relates functions in the heap with the sourcecode
      frameHeaven: clonedFrameHeaven, // frames that are part of some closure so they can't be destroyed
      turtle: clonedTurtle, // lines, pen is up or down, etc...
    }
    copysnaptime += Date.now() - startofsnap
    let checkDupStart = Date.now()

    let lineNumsDiffer = lineNum !== undefined && previousLineNumber !== lineNum

    if (lineNumsDiffer) {
      if (lineNum === undefined) throw new Error("expected line number")
      let newLineSeenMap = { ...(stepDescriptorSofar?.lineSeenMap || {}) }
      newLineSeenMap[lineNum] = (newLineSeenMap[lineNum] || 0) + 1
      stepDescriptorSofar = {
        steps: {},
        lineSeenMap: newLineSeenMap,
        lineNum: lineNum,
      }
    }

    let getNextStepDescriptor = () => {
      if (!expectedLines) return undefined
      addToStepDescriptor(stepDescriptorSofar, stepDesc.name)
      if (stepDesc.name === undefined) debugger
      return getCurrentStepDescriptor(stepDescriptorSofar, stepDesc.name)
    }
    let stepDescriptor = getNextStepDescriptor()

    let differentFromLastFrame =
      animationFrames.length === 0 || lineNumsDiffer || !deepEqual(animationFrames[animationFrames.length - 1].animationFrame, nextFrame)
    if (expectedLines && !stepDescriptor) debugger
    if (differentFromLastFrame) {
      //if (description.opts && description.opts.category === "turtle") debugger
      if (lineNum !== undefined) previousLineNumber = lineNum
      animationFrames.push({
        description,
        animationFrame: nextFrame,
        originalSourceTokens: originalSourceTokens || clonedSourceTokens,
        stepDescriptor,
      })
    }
    totalCheckDups += Date.now() - checkDupStart
  }

  let generatedBuiltinFunctions = []

  let misDrawExc = (node, msg) => {
    if (node && !["ast-node", "value", "token"].includes(node.category))
      alert("unable to display error location. Please talk to jacques about this.")

    let ret = new Error(msg)
    ret.lastFrameInstructions = { type: "draw-mismatch", msg: msg, node }
    return ret
  }

  let drewTooMuchExc = (node, msg) => {
    //if (!node) alert("Missing source of user error. Please contact Jacques.")
    if (node && !["ast-node", "value", "token"].includes(node.category))
      alert("unable to display error location. Please talk to jacques about this.")

    let ret = new Error(msg)
    ret.lastFrameInstructions = { type: "drew-too-much", msg: msg, node }
    //ret.errorType = "user-error"
    return ret
  }

  let drewTooLittleExc = (node, msg) => {
    if (node && !["ast-node", "value", "token"].includes(node.category))
      alert("unable to display error location. Please talk to jacques about this.")

    let ret = new Error(msg)
    ret.lastFrameInstructions = { type: "drew-too-little", msg: msg, node }
    //ret.errorType = "user-error"
    return ret
  }

  let userError = (node, msg) => {
    if (node && !["ast-node", "value", "token"].includes(node.category))
      alert("unable to display error location. Please talk to jacques about this.")

    let ret = new Error(msg)
    ret.lastFrameInstructions = { type: "runtime-error", msg: msg, node }
    ret.userError = true
    return ret
  }

  let myBad = (node, msg) => {
    swimmingly = false
    msg = msg + ". Please contact Jacques."
    let extended = node ? "" : " and also missing node"

    let ret = new Error(msg + extended)
    ret.lastFrameInstructions = { type: "runtime-error", msg: msg }

    return ret
  }

  let newPoint = (x0, y0, deg, d) => {
    let radians = (2 * Math.PI * deg) / 360
    let x = x0 + d * Math.cos(radians)
    let y = y0 + d * Math.sin(radians)
    return { x, y }
  }

  let modules = {
    turtle: {
      id: "simpturt",
      variables: [
        {
          name: "forward",
          value: {
            address: "⟶", //masterGener(),
            type: "reference",
            referenceType: "function",
            tooltip: mkTooltipf(["that moves the turtle forward"]),
          },
          func: (args, sourceNode) => {
            if (args.length !== 1) throw userError(sourceNode, "forward takes one argument")
            if (args[0].primitiveType !== "int") throw userError(sourceNode, "forward argument should be a number")
            let v = args[0].value
            if (v < 0) throw userError(sourceNode, "forward argument should be a positive number")
            if (v === 0) return makeNoneValue()
            let x1 = turtle.turtleX
            let y1 = turtle.turtleY
            let ending = newPoint(turtle.turtleX, turtle.turtleY, turtle.angle, v)
            let x2 = ending.x
            let y2 = ending.y
            // console.log("turtle", turtle)
            // let radians = 2 * Math.PI * turtle.angle / 360
            // let x2 = turtle.turtleX + v * Math.cos(radians)
            // let y2 = turtle.turtleY + v * Math.sin(radians)
            let elem = {
              type: "straight-line",
              x1,
              y1,
              x2,
              y2,
              actuallyDrawn: turtle.pendown,
              endingAngle: turtle.angle,
            }

            turtle.lineProcessing = elem
            turtle.lineDescriptionToken = mkToken("Move forward by " + v)
            snap(instr.TURTLE_FORWARD, { category: "turtle" })

            if (turtle.pendown) compareLine(elem, sourceNode)

            turtle.lines.push(elem)
            turtle.lineProcessing = undefined
            turtle.lineDescriptionToken = undefined

            turtle.turtleX = x2
            turtle.turtleY = y2

            //let toPrint = JSON.stringify(args[0], null, 4)
            // console.log(toPrint)
            return makeNoneValue()
          },
        },
        {
          name: "right",
          value: {
            address: "⤵",
            type: "reference",
            referenceType: "function",
            tooltip: mkTooltipf(["that turns the turtle right"]),
          },

          func: (args, sourceNode) => {
            if (args.length !== 1) throw userError(sourceNode, "right takes one argument")
            if (args[0].primitiveType !== "int") throw userError(sourceNode, "right argument should be a number")
            let v = args[0].value
            if (v === 0) return makeNoneValue()

            //let toPrint = JSON.stringify(args[0], null, 4)
            // console.log(toPrint)

            turtle.lineDescriptionToken = mkToken("Turn right by " + v + " degrees")

            let elem = {
              type: "turn",
              dangle: -v,
              startingAngle: turtle.angle,
              endingAngle: turtle.angle - v,
              x2: turtle.turtleX,
              y2: turtle.turtleY,
            }

            turtle.lineProcessing = elem
            snap(instr.TURTLE_RIGHT, { category: "turtle" })
            turtle.lines.push(elem)
            turtle.angle = turtle.angle - v

            turtle.lineProcessing = undefined

            turtle.lineDescriptionToken = undefined

            return makeNoneValue()
          },
        },
        {
          name: "left",
          value: {
            address: "⤴",
            type: "reference",
            referenceType: "function",
            tooltip: mkTooltipf(["that turns the turtle left"]),
          },
          func: (args, sourceNode) => {
            if (args.length !== 1) throw userError(sourceNode, "left takes one argument")
            if (args[0].primitiveType !== "int") throw userError(sourceNode, "right argument should be a number")
            let v = args[0].value
            if (v === 0) return makeNoneValue()
            //let toPrint = JSON.stringify(args[0], null, 4)
            // console.log(toPrint)
            let elem = {
              type: "turn",
              dangle: v,
              startingAngle: turtle.angle,
              endingAngle: turtle.angle + v,
              x2: turtle.turtleX,
              y2: turtle.turtleY,
            }
            turtle.lineProcessing = elem
            turtle.lineDescriptionToken = mkToken("Turn left by " + v + " degrees")

            snap(instr.TURTLE_LEFT, { category: "turtle" })
            turtle.lines.push(elem)

            turtle.angle = turtle.angle + v
            turtle.lineProcessing = undefined
            turtle.lineDescriptionToken = undefined

            return makeNoneValue()
          },
        },
        {
          name: "penup",
          value: {
            address: "🤏",
            type: "reference",
            referenceType: "function",
            tooltip: mkTooltipf(["that lifts the pen up"]),
          },
          func: (args, sourceNode) => {
            if (args.length !== 0) throw userError(sourceNode, "penup takes no arguments")
            if (!turtle.pendown) return makeNoneValue()

            let elem = {
              type: "pendown",
              status: false,
              endingAngle: turtle.angle,
              x2: turtle.turtleX,
              y2: turtle.turtleY,
            }
            turtle.lineProcessing = elem

            turtle.lineDescriptionToken = mkToken("Pen is up")
            turtle.pendown = false
            snap(instr.PENUP, { category: "turtle" })
            turtle.lineProcessing = undefined

            turtle.lines.push(elem)

            turtle.lineDescriptionToken = undefined

            // console.log("***penup")
            return makeNoneValue()
          },
        },
        {
          name: "pendown",
          value: {
            address: "✍️",
            type: "reference",
            referenceType: "function",
            tooltip: mkTooltipf(["that places the pen down"]),
          },

          func: (args, sourceNode) => {
            if (args.length !== 0) throw userError(sourceNode, "pendown takes no arguments")
            if (turtle.pendown) return makeNoneValue()

            let elem = {
              type: "pendown",
              status: true,
              endingAngle: turtle.angle,
              x2: turtle.turtleX,
              y2: turtle.turtleY,
            }
            turtle.lineProcessing = elem

            turtle.lineDescriptionToken = mkToken("Pen is down")
            turtle.pendown = true
            snap(instr.PENDOWN, { category: "turtle" })

            turtle.lineProcessing = undefined

            turtle.lines.push(elem)

            turtle.lineDescriptionToken = undefined
            // console.log("***penup")
            return makeNoneValue()
          },
        },
        {
          name: "circle",
          value: {
            address: "↺",
            type: "reference",
            referenceType: "function",
            tooltip: mkTooltipf(["that draws an arc"]),
          },
          func: (args, sourceNode) => {
            if (args.length !== 1 && args.length !== 2) throw userError(sourceNode, "circle takes one or two arguments")
            let degrees = (args[1] && args[1].value) || 360
            if (degrees < 0) throw userError(sourceNode, "negative angles are not permitted")

            if (degrees > 360) throw userError(sourceNode, "The angle has to be smaller or equal to 360 degrees")

            if (args[0].primitiveType !== "int") throw userError(sourceNode, "circle argument should be a number")
            let r = args[0].value
            if (r < 0) throw userError(sourceNode, "radius argument should be a positive number")

            if (r === 0) return makeNoneValue()
            let center = newPoint(turtle.turtleX, turtle.turtleY, turtle.angle + 90, r)
            let startingPoint = { x: turtle.turtleX, y: turtle.turtleY }

            let radius = distance(center.x, center.y, startingPoint.x, startingPoint.y)

            let torad = (d) => (2 * Math.PI * d) / 360
            let x2 = radius * Math.cos(torad(turtle.angle - 90 + degrees)) + center.x
            let y2 = radius * Math.sin(torad(turtle.angle - 90 + degrees)) + center.y

            let elem = {
              type: "arc",
              center,
              x1: turtle.turtleX,
              y1: turtle.turtleY,
              radius,
              actuallyDrawn: turtle.pendown,
              degrees,
              startingAngle: turtle.angle,
              endingAngle: turtle.angle + degrees,
              startingCircleAngle: turtle.angle - 90,
              x2,
              y2,
            }
            turtle.lineProcessing = elem
            if (degrees === 360) turtle.lineDescriptionToken = mkToken("Circle of radius " + r)
            else turtle.lineDescriptionToken = mkToken(`Arc, ${degrees} degrees and radius, ${r}`)
            snap(instr.TURTLE_CIRCLE, { category: "turtle" })
            if (turtle.pendown) compareLine(elem, sourceNode)

            turtle.lines.push(elem)
            turtle.turtleX = x2
            turtle.turtleY = y2
            turtle.angle = turtle.angle + degrees
            turtle.lineProcessing = undefined

            turtle.lineDescriptionToken = undefined
            turtle.lineProcessing = false
            return makeNoneValue()
          },
        },
      ],
    },
  }

  let getModule = (str, sourceNode) => {
    let ret = modules[str]
    if (ret === undefined) throw userError(sourceNode, "Module " + str + " does not exist")
    return ret
  }

  // let getFuncDefName = stmt => {

  //     let nt = nontokens(stmt)
  //     if (nt.length === 0) throw myBad(stmt, "woops")
  //     let ret = nt[0].name
  //     if (ret === undefined) throw myBad(stmt, "double woops")
  //     return ret
  // }

  let getFuncDefName = (stmt) => {
    // funcdef statement
    let nt = nontokens(stmt.children)
    let ret = nt[0].name
    if (!ret) {
      throw myBad(stmt, "wrong function name")
    }
    if (typeof ret !== "string") throw myBad(stmt, "unexpected non string")
    return ret
  }

  let getFuncDefNameToken = (stmt) => {
    let nt = nontokens(stmt.children)
    let ret = nt[0].children[0]
    if (!ret) {
      myBad(stmt, "wrong function name")
    }
    if (ret.category !== "token") throw myBad(stmt, "expected token")
    return ret
  }

  let externalValueToValue = (value) => {
    if (value.type === "reference") {
      let { address, type, referenceType, tooltip } = value
      return {
        id: genValueId(),
        category: "value",
        type,
        referenceType,
        address,
        children: [mkToken(address, { ref: true, tooltip })],
      }
    }
  }

  let getFocusedExp = () => {
    return contexts[contexts.length - 1].focusedExp
  }
  let hasFocusedExp = () => {
    return contexts[contexts.length - 1].focusedExp !== undefined
  }
  let clearFocusedExp = () => {
    contexts[contexts.length - 1].focusedExp = undefined
  }

  let popContext = () => {
    if (contexts.length === 1) throw myBad(undefined, "popping global context")

    contexts.pop()
  }

  let topEval = (e, resetThunk, statement, index) => {
    exprInSource = e
    let clonedE = uicpNoOrigin(e)
    let expressionLineNumber = e.lineNumber
    markCurrentLine = () => {
      let gobackGutterToken = mkToken("r-" + gobackGutter.length, {
        lineMarker: true,
      })
      gobackGutter.push({
        whitespaceToken: mkToken("  "),
        lineNumber: expressionLineNumber,
        token: gobackGutterToken,
      })
      snap(instr.GOBACK_MARKER_GUTTER)
    }
    resetExpression = () => {
      let contextGobackMarkerToken = uicp(gobackGutter[gobackGutter.length - 1].token)
      addToHoverAssociations(gobackGutter[gobackGutter.length - 1].token.id, contextGobackMarkerToken.id)
      addToHoverAssociations(contextGobackMarkerToken.id, gobackGutter[gobackGutter.length - 1].token.id)
      contexts[contexts.length - 1].gobackMarker = {
        token: contextGobackMarkerToken,
      }
      setIsolatedExpresionInCurrentFrame(exprInSource) // no snap
      resetThunk(clonedE)
      snap(instr.ISOLATED_SO_RESET_EXPR)
    }
    exp(e)
    if (e.category !== "value") {
      debugger
      throw myBad(e, "should be value after evaluation")
    }

    // expression has been evaluated
    //debugger
    if (hasIsolatedExpresionInCurrentFrame()) {
      setHighlightedElements([contexts[contexts.length - 1].gobackMarker.token, gobackGutter[gobackGutter.length - 1].token])

      snap(instr.HIGHLIGHT_RETURN)
      setHighlightedElements([])

      // the expression was detached, gotta bring it back
      let v = getIsolatedExpresionInCurrentFrame()
      if (v.category !== "value") throw myBad(e, "value expected")
      resetThunk(v)
      contexts[contexts.length - 1].gobackMarker = undefined
      gobackGutter.pop()
      currentFocusedStatement = { statement, index }
      //replace(e, v)
      setIsolatedExpresionInCurrentFrame(undefined) // otherwise the value will be in 2 places
      // mark mark

      snap(instr.REINTEGRATE_EXPRESSION, { lineNumber: statement.lineNumber })
    }
    //debugger
    resetExpression = undefined
    exprInSource = undefined
  }

  let setNonToken = (arr, index, e) => {
    let current = 0
    for (let i = 0; i < arr.length; i++) {
      let candidate = arr[i]
      if (candidate.category !== "token") {
        if (current === index) {
          arr[i] = e
          return
        }
        current++
      }
    }
    throw myBad("index out of range" + index)
  }

  let firstToken = (tree) => {
    if (tree.category === "token") return tree
    for (let i = 0; i < tree.children.length; i++) {
      let e = tree.children[i]
      let r = firstToken(e)
      if (r !== undefined) return r
    }
    return undefined
  }

  let nthToken = (tree, n) => {
    let run = (tree) => {
      if (tree.category === "token") {
        n--
        if (n < 0) return tree
        return
      }
      for (let i = 0; i < tree.children.length; i++) {
        let e = tree.children[i]
        let r = run(e)
        if (r !== undefined) return r
      }
      return undefined
    }
    return run(tree)
  }

  let isDictReference = (n) => {
    if (n.category !== "value") {
      throw myBad("expected value")
    }
    return n.referenceType === "dict"
  }

  let isArrayReference = (t) => {
    return t.referenceType === "array"
  }

  let isReference = (t) => {
    return t.type === "reference"
  }

  let findFrameById = (id) => {
    return frameHeaven.find((c) => c.id === id) || contexts.find((c) => c.frame.id === id).frame
  }

  let createFuncInHeap = (stmt, copyOfFuncdefMarker, funcdefMarkerId) => {
    let currentContext = contexts[contexts.length - 1]
    let currentFrame = currentContext.frame
    let id = genHeapfuncId()

    let closure = currentFrame.id
    let closureToken = uicp(currentFrame.idToken)
    if (currentFrame.classFrame) {
      setHighlightedElements([currentFrame.classFrameLabel])
      snap(instr.EMPHASIZE_CLASSFRAME)
      setHighlightedElements([])

      closure = currentFrame.closure
      //let closureFrame = findFrameById(closure)
      closureToken = uicp(currentFrame.closureToken) //uicp(closureFrame.idToken)
    }

    addToHoverAssociations(closureToken.id, currentFrame.idToken.id)

    return {
      funcdefMarkerId,
      id,
      idToken: mkToken(id, { ref: true }),
      defASTNode: stmt,
      type: "heap-function",
      funcdefMarkerLabel: mkToken("line"),
      funcdefMarker: copyOfFuncdefMarker,
      label: mkToken("function"),
      closure,
      closureToken,
      closureLabel: mkToken("environment"),
    }
  }

  let addToFrame = (frame, key, value) => {
    frame.rows.push({ left: key, right: value, id: genFrameRowId() })
  }

  let isExpression = (x) => {
    return x.kind === "expr"
  }

  let isBlock = (x) => {
    return x.kind === "block"
  }

  let boolOfValue = (x) => {
    if (x.primitiveType !== "bool") throw myBad(undefined, "expected bool")
    return x.value
  }

  let updateArray = (arrRef, indexValue, rhs, indexNode) => {
    if (indexValue.primitiveType !== "int") throw userError(indexNode, "Index must be a number")

    let arrInHeap = heap.find((x) => x.id === arrRef.address)
    if (!arrInHeap) throw myBad(undefined, "no array in heap")

    setHighlightedElements([arrRef, arrInHeap.idToken])
    snap(instr.HIGHLIGHT_LIST_OBJECT)
    setHighlightedElements([])

    let rows = arrInHeap.rows
    if (indexValue.value >= rows.length) {
      throw userError(indexNode, "Cannot assign to an index that is out of range")
    }
    let row = rows[indexValue.value]

    setHighlightedElements([indexValue, row.indexToken])
    snap(instr.HIGHLIGHT_LIST_INDEX)
    setHighlightedElements([])

    row.value = rhs
  }

  let updateDict = (dictRef, keyValue, rhs) => {
    let dictInHeap = heap.find((x) => x.id === dictRef.address)
    if (!dictInHeap) throw myBad(undefined, "could not find dict in heap")

    setHighlightedElements([dictRef, dictInHeap.idToken])
    snap(instr.HIGHLIGHT_DICT_OBJECT)
    setHighlightedElements([])

    let rows = dictInHeap.rows
    for (let i = 0; i < rows.length; i++) {
      let row = rows[i]
      if (equalAsValues(keyValue, row.key)) {
        setHighlightedElements([keyValue, row.key])
        snap(instr.HIGHLIGHT_DICT_KEY)
        setHighlightedElements([])

        row.value = rhs
        return
      }
    }
    rows.push({ id: genDictRowId(), key: uicp(keyValue), value: rhs })
  }

  let updateCurrentFrame = (name, children, v, source, sourceToken) => {
    let frame = contexts[contexts.length - 1].frame
    let frameRow = lookupVarInFrame(name, frame)
    if (frameRow) {
      setHighlightedElements([sourceToken, frameRow.left.token])
      snap(instr.HIGHLIGHT_VAR_IN_FRAME)
      setHighlightedElements([])

      frameRow.right = v
      snap(instr.REPLACE_VAR)
    } else {
      if (children.length !== 1) throw myBad("variables should have on child")
      let frameKey = {
        name: name,
        token: children[0],
      }
      if (!frameKey.name) throw myBad(source, "missing name")
      if (!frameKey.token) throw myBad(source, "missing token")

      addToFrame(frame, frameKey, v)
      snap(instr.PLACE_VAR)
    }
  }

  let statementsInterpreted = 0

  let interpretationStarted = false

  let interpretBlock = (tree, destination) => {
    if (tree.kind !== "block") throw myBad(tree, "expected block")
    let statements = nontokens(tree.children)
    for (let i = 0; i < statements.length; i++) {
      let statement = statements[i]
      statementsInterpreted++
      if (statementsInterpreted > 300) throw userError(statement, "You've interpreted over 300 statements")
      if (statement.kind === "padding") {
        let nt = nontokens(statement.children)
        if (nt.length !== 1) throw myBad(tree, "padding should only have one child")
        statement = nt[0]
      }
      //let original = clone(statement)
      currentFocusedStatement = { statement: statement, index: 0 }
      if (interpretationStarted) {
        snap(instr.NEXT_LINE, { lineNumber: statement.lineNumber })
      } else {
        interpretationStarted = true
        snap(instr.INTERPRETATION_STARTS, { lineNumber: statement.lineNumber })
      }

      //if (lineMarker === undefined) throw myBad(tree, "firstToken unexpectedly returned undefined")

      if (statement.type === "return") {
        if (!destination) throw userError(statement, "return must be inside a def")
        let nt = nontokens(statement.children)
        if (nt.length === 0) {
          if (!destination.fromConstructor) {
            let retVal = makeNoneValue()

            replace(destination, retVal)
            snap(instr.FILL_HOLE_NONE) // value has been moved to the hole
            return { hitReturn: true }
          } else {
            return {}
          }
        }
        if (nt.length === 1) {
          let child = nt[0]

          let originalE = clone(child)

          if (child.category === "value") debugger
          topEval(
            child,
            (e) => {
              setNonToken(statement.children, 0, e)
            },
            statement,
            0
          )
          if (destination.fromConstructor) {
            if (child.primitiveType !== "none") {
              throw userError(child, "Can only return None from constructor")
            }
            setNonToken(statement.children, 0, originalE) // resets the return statement

            snap(instr.RESET_STATEMENT)
            return {}
          } else {
            // snap(instr."return-expression")

            let copiedReturnValue = uicp(child)
            replace(destination, copiedReturnValue) // fills the hole of the function call
            snap(instr.FILL_HOLE_EXP) // value has been moved to the hole

            setNonToken(statement.children, 0, originalE) // resets the return statement
            snap(instr.RESET_STATEMENT)
            return { hitReturn: true }
          }
        } else {
          throw myBad(statement, "return assumption violated")
        }
      }
      if (statement.type === "assignment") {
        let nt = nontokens(statement.children)
        if (nt.length !== 2) throw myBad(statement, "assignment should have two children")
        let assigned = nt[1]

        let assignee = nt[0]
        let originalAssigned = clone(assigned)
        let originalAssignee = clone(assignee)
        if (assignee.kind !== "expr") throw myBad(statement, "assignee should be a var")
        if (assignee.type === "var") {
          let nonlocalsE = contexts[contexts.length - 1].nonlocals.find((x) => x.name === assignee.name)
          let globalsE = contexts[contexts.length - 1].globals.find((x) => x.name === assignee.name)
          if (nonlocalsE) {
            topEval(
              assigned,
              (e) => {
                setNonToken(statement.children, 1, e)
              },
              statement,
              0
            )

            setHighlightedElements([nonlocalsE.nameToken, assignee.children[0]])
            snap(instr.HIGHLIGHT_NONLOCAL)
            setHighlightedElements([])

            let r = nonlocalrow(assignee.name)
            if (!r) throw userError(statement, "Could not find nonlocal variable " + assignee.name)

            setHighlightedElements([assignee, r.left.token])
            snap(instr.HIGHLIGHT_NONLOCAL_ROW)
            setHighlightedElements([])
            r.right = uicp(assigned)
            snap(instr.UPDATE_NONLOCAL_ROW)
          } else if (globalsE) {
            topEval(
              assigned,
              (e) => {
                setNonToken(statement.children, 1, e)
              },
              statement,
              0
            )

            setHighlightedElements([globalsE.nameToken, assignee.children[0]])
            snap(instr.HIGHLIGHT_GLOBAL)
            setHighlightedElements([])

            let r = globalrow(assignee.name)
            if (!r) throw userError(statement, "Could not find global variable " + assignee.name)

            if (r.isExternal && !interpreterOpts.alwaysShowExternal) {
              externalVarsShown = [r.id]
            }

            setHighlightedElements([assignee, r.left.token])
            snap(instr.HIGHLIGHT_GLOBAL_ROW)
            setHighlightedElements([])

            r.right = uicp(assigned)

            if (r.isExternal && !interpreterOpts.alwaysShowExternal) {
              externalVarsShown = []
            }

            snap(instr.UPDATE_GLOBAL_ROW)
          } else {
            let row = contexts[contexts.length - 1].frame.rows.find((x) => x.left.name === assignee.name)

            if (!row) {
              contexts[contexts.length - 1].currentlyDefining.push(assignee.name)
            } else {
              if (row.isExternal && !interpreterOpts.alwaysShowExternal) {
                externalVarsShown = [row.id]
              }
            }

            topEval(
              assigned,
              (e) => {
                setNonToken(statement.children, 1, e)
              },
              statement,
              0
            )

            contexts[contexts.length - 1].currentlyDefining = contexts[contexts.length - 1].currentlyDefining.filter(
              (x) => x !== assignee.name
            )
            let left = uicp(assignee).children
            updateCurrentFrame(assignee.name, left, uicp(assigned), statement, originalAssignee)
            if (row && row.isExternal && !interpreterOpts.alwaysShowExternal) {
              externalVarsShown = [row.id]
            }
          }
        } else if (assignee.type === "bracket") {
          topEval(
            assigned,
            (e) => {
              setNonToken(statement.children, 1, e)
            },
            statement,
            0
          )

          // only for objects

          let assigneeNonTokens = nontokens(assignee.children)
          if (assigneeNonTokens.length !== 2) throw myBad(assignee, "interpreter assignment bracket 2 children")

          let assigneePreBracket = assigneeNonTokens[0]

          topEval(
            assigneePreBracket,
            (e) => {
              setNonToken(assignee.children, 0, e)
            },
            statement,
            0
          )

          if (!isDictReference(assigneePreBracket) && !isArrayReference(assigneePreBracket)) {
            throw userError(statement, "Expected dictionary or array")
          }

          let assigneeInnerBracket = assigneeNonTokens[1]
          topEval(
            assigneeInnerBracket,
            (e) => {
              setNonToken(assignee.children, 1, e)
            },
            statement,
            0
          )

          if (isArrayReference(assigneePreBracket) && !isNumberValue(assigneeInnerBracket)) {
            throw userError(statement, "array index must be a number")
          }

          if (isDictReference(assigneePreBracket) && isReference(assigneeInnerBracket)) {
            throw userError(statement, "dict keys cannot be references")
          }

          if (isArrayReference(assigneePreBracket)) {
            updateArray(assigneePreBracket, assigneeInnerBracket, uicp(assigned), assigneeInnerBracket)
            snap(instr.LIST_UPDATED)
          } else {
            if (isReference(assigneeInnerBracket)) throw userError(assigneeInnerBracket, "key cannot be a reference")
            updateDict(assigneePreBracket, assigneeInnerBracket, uicp(assigned))
          }

          // setNonToken(tree.children, i, original) // reset the whole statement
          // snap(instr."reset-statement")
        } else if (assignee.type === "dot") {
          topEval(
            assigned,
            (e) => {
              setNonToken(statement.children, 1, e)
            },
            statement,
            0
          )

          // only for objects

          let assigneeNonTokens = nontokens(assignee.children)
          if (assigneeNonTokens.length !== 2) throw myBad(assignee, "interpreter assignment bracket 2 children")

          let assigneePreBracket = assigneeNonTokens[0]

          topEval(
            assigneePreBracket,
            (e) => {
              setNonToken(assignee.children, 0, e)
            },
            statement,
            0
          )

          if (assigneePreBracket.type !== "reference") throw userError(assigneePreBracket, "expected reference")
          // if (!isInstanceReference(assigneePreBracket) && !isDictReference(assigneePreBracket) && !isArrayReference(assigneePreBracket)) {
          //     throw userError(statement, "Expected dictionary or array")
          // }

          let obj = heap.find((x) => x.id === assigneePreBracket.address)
          if (!obj) throw myBad(statement, "could not find object in heap")

          let nameNonToken = assigneeNonTokens[1]

          let attributes = obj.attributes
          let attr = attributes.find((x) => x.key === nameNonToken.name)
          if (!attr) {
            attributes.push({
              key: nameNonToken.name,
              keyToken: uicp(nameNonToken.children[0]),
              value: uicp(assigned),
            })
          } else {
            attr.value = uicp(assigned)
          }
          snap(instr.ATTRIBUTE_UPDATED)
        } else {
          throw userError(assignee, "The left hand side of an assignment must be a variable, a bracket expression or a dot expression")
        }
        setNonToken(statement.children, 0, originalAssignee)
        setNonToken(statement.children, 1, originalAssigned)
        // setNonToken(tree.children, i, original) // reset the whole statement
        snap(instr.RESET_STATEMENT)
      } else if (statement.type === "import") {
        let moduleName = statement.moduleName
        let module = getModule(moduleName, statement)
        let variables = module.variables

        let externalRowsToShow = []
        let toRemoveOrigin = []
        for (let i = 0; i < variables.length; i++) {
          let v = variables[i]
          let left = mkToken(v.name)
          //let right = mkToken(v.address, { ref: true })
          left = addOrigin(left, firstToken(statement).id)
          //right = addOrigin(right, firstToken(statement).id)
          let id = genExternalVarId()
          let value = externalValueToValue(v.value)
          let valueToken = firstToken(value)
          valueToken = addOrigin(valueToken, firstToken(statement).id)
          let newRow = {
            isExternal: true,
            left: {
              name: v.name,
              token: { ...left, options: { ...left?.options } },
            },
            right: value,
            id,
          }
          contexts[0].frame.rows.push(newRow)
          toRemoveOrigin.push({ r: newRow, noOrigin: mkToken(v.name) })
        }

        if (!interpreterOpts.alwaysShowExternal) {
          externalVarsShown = externalRowsToShow
        }

        contexts[0].frame.rows.forEach((x) => {
          externalRowsToShow.push(x.id)
        })

        snap(instr.POPULATE_EXTERNAL)

        if (!interpreterOpts.alwaysShowExternal) {
          externalVarsShown = []
          toRemoveOrigin.forEach(({ r, noOrigin }) => {
            r.left.token = noOrigin
          })
        }

        snap(instr.HIDE_EXTERNAL)
        // ui is not updated. Need to add some flashy animation to indicate something happened
      } else if (statement.type === "expr-statement") {
        let nt = nontokens(statement.children)
        if (nt.length !== 1) throw myBad(statement, "not supposed to happen")
        let e = nt[0]
        let originale = clone(e)
        topEval(
          e,
          (e) => {
            setNonToken(statement.children, 0, e)
          },
          statement,
          0
        )

        setNonToken(statement.children, 0, originale)

        snap(instr.RESET_STATEMENT)
      } else if (statement.type === "classdef") {
        let currentFrame = contexts[contexts.length - 1].frame
        let closure = contexts[contexts.length - 1].frame.id
        let closureToken = uicp(contexts[contexts.length - 1].frame.idToken)

        if (currentFrame.classFrame) {
          setHighlightedElements([currentFrame.classFrameLabel])
          snap(instr.EMPHASIZE_CLASSFRAME)
          setHighlightedElements([])

          closure = currentFrame.closure
          closureToken = uicp(currentFrame.closureToken)
        }

        addToHoverAssociations(closureToken.id, contexts[contexts.length - 1].frame.idToken.id)

        let frame = newFrame(closure, closureToken, true) // no snap.
        let context = newContext(frame)
        pushToContexts(context, statement)
        snap(instr.CREATE_CLASSFRAME)
        let nt = nontokens(statement.children)
        let body = nt[nt.length - 1]
        interpretBlock(body)
        currentFrame = contexts[contexts.length - 1].frame // the current frame has changed
        // now the current frame has a bunch of variables

        let frameRows = currentFrame.rows
        let attributes = frameRows.map((row) => {
          let left = row.left
          let right = row.right // must be a value
          return {
            id: genAttributeRowId(),
            key: left.name,
            keyToken: uicp(left.token),
            value: uicp(right),
          }
        })
        let superclasses = []
        let originalSuperclassList = undefined
        if (nt.length == 3) {
          let superclassList = nt[1]
          let superclassnt = nontokens(superclassList.children)
          if (superclassnt.length > 0) {
            originalSuperclassList = clone(superclassList)

            for (let i = 0; i < superclassnt.length; i++) {
              let expr = superclassnt[i]

              topEval(
                expr,
                (e) => {
                  setNonToken(superclassList.children, i, e)
                },
                statement,
                0
              )
              if (expr.referenceType !== "class") throw userError(statement, "baseclass must be a class")
              superclasses.push(uicp(expr))
            }
          }
        }

        let id = genClassIdHeapId()
        let idToken = mkToken(id, { ref: true })
        let classInHeap = {
          type: "heap-class",
          label: mkToken("class"),
          id,
          idToken,
          attributes,
          superclasses,
          superclassesToken: mkToken("baseclasses"),
          superclassesCommas: makeCommas(superclasses.length),
        }
        pushToHeap(classInHeap, statement)
        snap(instr.CLASS_IN_HEAP_CREATED)
        if (originalSuperclassList) {
          setNonToken(statement.children, 1, originalSuperclassList)
          snap(instr.SUPERCLASS_LIST_RESET)
        }

        contexts.pop()
        snap(instr.CLASS_CONTEXT_POPPED)
        let name = getFuncDefName(statement)
        let nameToken = getFuncDefNameToken(statement)
        let frameKey = {
          name: name,
          token: uicp(nameToken),
        }
        let fr = contexts[contexts.length - 1].frame

        let funcref = {
          id: genClassrefId(),
          category: "value",
          type: "reference",
          referenceType: "class",
          address: id,
          children: [cpTokenHover(idToken)],
        }

        addToFrame(fr, frameKey, funcref)

        snap(instr.CLASSDEF_REF_FRAME)
      } else if (statement.type === "funcdef") {
        let nt = nontokens(statement.children)
        let parameters = nt[1]
        if (parameters === undefined) throw myBad(statement, "undefined parameters")
        function hasDuplicates(array) {
          return new Set(array).size !== array.length
        }
        let checkNoDuplicates = (parameters) => {
          let nt = nontokens(parameters.children)
          if (nt.length === 0) return
          if (nt.length !== 1) throw myBad(statement, "param assumption violated")
          let paramlist = nt[0]
          let ps = nontokens(paramlist.children)
          let names = ps.map((p) => p.name)
          if (hasDuplicates(names)) throw myBad(statement, "duplicate parameters")
        }
        checkNoDuplicates(parameters)
        let funcdefId = statement.id
        let funcdefMarker = undefined

        if (markerStatementAssociation.has(statement)) {
          funcdefMarker = markerStatementAssociation.get(statement)
        } else {
          let funcdefMarkerToken = mkToken(funcdefId, { lineMarker: true })
          let funcdefMarkerId = genFuncdefMarkerId()
          funcdefMarker = {
            id: funcdefMarkerId,
            whitespaceToken: mkToken("  "),
            name: funcdefId,
            token: funcdefMarkerToken,
            lineNumber: statement.lineNumber,
            //relativeTo: firstToken(statement).id // the first token never changes
          }
          endoflineTokens.push(funcdefMarker)
          markerStatementAssociation.set(statement, funcdefMarker)
        }

        snap(instr.ADD_FUNCDEF_MARKER) // first place marker in sourcecode
        let copyOfFuncdefMarker = uicp(funcdefMarker.token)

        addToHoverAssociations(funcdefMarker.token.id, copyOfFuncdefMarker.id)
        addToHoverAssociations(copyOfFuncdefMarker.id, funcdefMarker.token.id)

        let name = getFuncDefName(statement)
        let nameToken = getFuncDefNameToken(statement)
        let funcInHeap = createFuncInHeap(statement, copyOfFuncdefMarker, funcdefMarker.id) // closure will be decided here

        pushToHeap(funcInHeap)
        snap(instr.CREATE_FUNCTION)
        let funcref = {
          id: genFuncrefId(),
          category: "value",
          type: "reference",
          referenceType: "function",
          address: funcInHeap.id,
          children: [cpTokenHover(funcInHeap.idToken)],
        }
        //setNonToken(statement.children, 2, funcref)
        let frameKey = {
          name: name,
          token: uicp(nameToken),
        }
        let frame = contexts[contexts.length - 1].frame
        addToFrame(frame, frameKey, funcref)

        snap(instr.FUNCDEF_POPULATE_FRAME)

        // setNonToken(tree.children, i, original)  <-- DO NOT DO THIS!
        // snap(instr."reset-statement")
      } else if (statement.type === "for") {
        let nt = nontokens(statement.children)
        if (nt.length !== 3) throw myBad(statement, "interpreter for 1")

        let listValue = nt[1]

        let originalList = clone(listValue)

        topEval(
          listValue,
          (e) => {
            setNonToken(statement.children, 1, e)
          },
          statement,
          0
        )

        //  snap(instr."for-list") // value has been moved back to sourcecode

        //if (!isArrayReference(listValue)) throw userError(listValue, "interpreter for 2")

        let valuesFor = (ref) => {
          if (!isArrayReference(ref) && !isDictReference(ref)) {
            throw myBad(statement, "can only iterate over dicts and arrays")
          }
          if (isArrayReference(ref)) {
            let arrInHeap = heap.find((x) => x.id === ref.address)
            if (!arrInHeap) throw myBad(statement, "could not find array in heap")
            let ret = arrInHeap.rows.map((x) => x.value)
            return ret
          }
          if (isDictReference(ref)) {
            let dictInHeap = heap.find((x) => x.id === ref.address)
            if (!dictInHeap) throw myBad(statement, "could not find dict in heap")
            let ret = dictInHeap.rows.map((x) => x.key)
            return ret
          }
        }

        let refElements = valuesFor(listValue)
        if (refElements.length === 0) continue
        let copiedRefElements = refElements.map((x) => uicp(x))
        let iteratingVariable = nt[0].children.map(uicp)[0]

        let loopElem = {
          index: 0,
          id: genForloopId(),
          iteratingVariable,
          elements: copiedRefElements,
          commas: makeCommas(Math.max(0, refElements.length - 1)),
          leftBracket: mkToken("["),
          rightBracket: mkToken("]"),
          type: "for-loop",
        }
        contexts[contexts.length - 1].loops.push(loopElem)
        snap(instr.FOR_ITERATOR_CONTEXT)

        setNonToken(statement.children, 1, originalList)
        snap(instr.RESET_FOR_LOOP)
        //snap(instr."for-loop-list-reset") // resets the for loop in the source
        for (let i = 0; i < copiedRefElements.length; i++) {
          loopElem.index = i
          snap(instr.FOR_NEXT_ELEM)

          updateCurrentFrame(nt[0].name, nt[0].children.map(uicp), uicp(copiedRefElements[i]), statement, iteratingVariable)

          //                    assign(nt[0], copiedRefElements[i])
          let res = interpretBlock(nt[2], destination)
          if (res && res.break) break
          if (res && res.hitReturn) return res
        }
        contexts[contexts.length - 1].loops.pop()
        snap(instr.FOR_LOOP_POP)
      } else if (statement.type === "while") {
        // while mark
        let whileId = statement.id
        let whileLoopMarker = undefined

        if (markerStatementAssociation.has(statement)) {
          whileLoopMarker = markerStatementAssociation.get(statement)
        } else {
          let whileLoopMarkerToken = mkToken(whileId, { lineMarker: true })
          let whileLoopMarkerId = genwhileLoopMarkerId()
          whileLoopMarker = {
            id: whileLoopMarkerId,
            whitespaceToken: mkToken("  "),
            name: whileId,
            token: whileLoopMarkerToken,
            lineNumber: statement.lineNumber,
            //relativeTo: firstToken(statement).id // the first token never changes
          }
          endoflineTokens.push(whileLoopMarker)
          markerStatementAssociation.set(statement, whileLoopMarker)
          snap(instr.ADD_WHILE_MARKER) // first place marker in sourcecode
        }
        let copyOfWhileMarker = uicp(whileLoopMarker.token)

        let loopElem = {
          id: genWhileloopId(),
          type: "while-loop",
          token: copyOfWhileMarker,
          whileKeywordToken: mkToken("while "),
        }
        contexts[contexts.length - 1].loops.push(loopElem)
        snap(instr.PUSH_WHILE_IN_CONTEXT)

        let nt = nontokens(statement.children)
        if (nt.length !== 2) throw myBad(statement, "Expected while to have two children")
        let condition = nt[0]
        let originalCond = clone(condition)
        topEval(
          condition,
          (e) => {
            setNonToken(statement.children, 0, e)
          },
          statement,
          0
        )

        let condTrue = !isFalsyValue(condition)

        replace(condition, originalCond)

        snap(instr.WHILE_COND_RESET)

        while (condTrue) {
          let res = interpretBlock(nt[1], destination)
          currentFocusedStatement = { statement: statement, index: 0 }
          setHighlightedElements([loopElem.token, whileLoopMarker.token])
          snap(instr.WHILE_ITERATE, { lineNumber: statement.lineNumber })
          setHighlightedElements([])
          if (res && res.break) {
            break
          }
          if (res && res.hitReturn) return res

          let originalCond = clone(condition)
          topEval(
            condition,
            (e) => {
              setNonToken(statement.children, 0, e)
            },
            statement,
            0
          )

          condTrue = !isFalsyValue(condition)
          replace(condition, originalCond) // after evaluation is finished
          snap(instr.WHILE_COND_RESET)
        }
        contexts[contexts.length - 1].loops.pop()
        snap(instr.WHILE_LOOP_POP)
      } else if (statement.type === "raise") {
        let nt = nontokens(statement.children)
        if (nt.length !== 1) throw myBad(statement, "expected raise to have 1 nontoken child")
        topEval(
          nt[0],
          (e) => {
            setNonToken(statement.children, 0, e)
          },
          statement,
          0
        )
        throw userError(statement, "user has thrown an error")
      } else if (statement.type === "if") {
        let nt = nontokens(statement.children)

        for (let i = 0; i < nt.length; i += 2) {
          let c = nt[i]
          if (isExpression(c)) {
            let originalCond = clone(c)

            let getNonTokensBeforeToken = (chs, nontokenIndex) => {
              let ret = []
              let seen = 0
              for (let i = 0; i < chs.length; i++) {
                let c = chs[i]
                if (c.category === "token") ret.push(c)
                else {
                  if (seen === nontokenIndex) {
                    return ret
                  }
                  seen++
                }
              }
              throw myBad(statement, "could not find index")
            }
            let elifArray = getNonTokensBeforeToken(statement.children, i).filter((t) => t.text === "elif")
            let ms = i === 0 ? statement : elifArray[elifArray.length - 1]
            currentFocusedStatement = { statement: ms, index: 0 }

            if (i !== 0) snap(instr.LINEMARKER_ELIF, { lineNumber: statement.lineNumber })
            topEval(
              c,
              (e) => {
                setNonToken(statement.children, i, e)
              },
              ms,
              0
            )
            if (c.category !== "value") throw myBad(c, "expected value")
            if (c.primitiveType !== "bool") throw userError(c, "expected bool as condition of if")
            let result = boolOfValue(c)
            setNonToken(statement.children, i, originalCond)
            snap(instr.RESET_ELIF)
            if (result) {
              let res = interpretBlock(nt[i + 1], destination)
              if (res && res.hitReturn) return res

              break
            }
          } else if (isBlock(c)) {
            // no condition
            let res = interpretBlock(nt[i], destination)
            if (res && res.hitReturn) return res
          } else {
            throw myBad(c, "unrecognized if child")
          }
        }
      } else if (statement.type === "try") {
        let nt = nontokens(statement.children)
        let body = nt[0]
        if (nt.length !== 3) {
          throw userError(statement, "only simple try statements are supported")
        }
        let except = nt[1]
        if (except.kind !== "except") throw userError(statement, "only simple try statements are supported")
        let exceptBody = nt[2]
        if (exceptBody.tryPart !== "except") throw myBad(statement, "unexpected tryPart")

        let exceptId = statement.firstExceptData.id
        if (!exceptId) {
          debugger
          throw myBad(statement, "expected except to have id")
        }
        let exceptMarker = undefined

        if (markerStatementAssociation.has(statement)) {
          exceptMarker = markerStatementAssociation.get(statement)
        } else {
          let exceptMarkerToken = mkToken(exceptId, { lineMarker: true })
          let exceptMarkerId = genExceptMarkerId()
          let exceptLineNumber = statement.firstExceptData.lineNumber
          if (!exceptLineNumber) {
            debugger
            throw myBad(statement, "missing except line number")
          }
          exceptMarker = {
            id: exceptMarkerId,
            whitespaceToken: mkToken("  "),
            name: exceptId,
            token: exceptMarkerToken,
            lineNumber: exceptLineNumber,
            //relativeTo: firstToken(statement).id // the first token never changes
          }
          endoflineTokens.push(exceptMarker)
          markerStatementAssociation.set(statement, exceptMarker)
          snap(instr.ADD_EXCEPT_MARKER) // first place marker in sourcecode
        }
        let copyOfExceptMarker = uicp(exceptMarker.token)

        let loopElem = {
          id: genExceptId(),
          type: "except",
          token: copyOfExceptMarker,
          exceptKeywordToken: mkToken("except "),
        }
        contexts[contexts.length - 1].loops.push(loopElem)
        snap(instr.PUSH_EXCEPT_CONTEXT)

        let savedContexts = clone(contexts)

        try {
          interpretBlock(body)
        } catch (err) {
          if (!err.userError) {
            throw err
          }
          swimmingly = true
          savedContexts.forEach((ctx) => {
            let frame = ctx.frame
            let newFrame = undefined
            for (let i = 0; i < contexts.length; i++) {
              let fr = contexts[i].frame
              if (fr.id === frame.id) {
                newFrame = fr
                break
              }
            }
            if (!newFrame) throw myBad("Could not find frame")
            ctx.frame = newFrame
          })
          contexts = savedContexts
          removeUserError(topAST)
          restoreAllExpressions()

          snap(instr.RESTORED_ORIGINAL_STATE_EXCEPT)
          interpretBlock(exceptBody)
        }
        contexts[contexts.length - 1].loops.pop()
        snap(instr.EXCEPT_POP)
      } else if (statement.type === "nonlocal") {
        let nt = nontokens(statement.children)
        let varname = statement.name
        let alreadyInCurrentFrame = (name) => {
          let frame = contexts[contexts.length - 1].frame
          let rows = frame.rows
          for (let i = 0; i < rows.length; i++) {
            if (rows[i].left.name === name) return true
          }
          return false
        }
        if (alreadyInCurrentFrame(varname)) throw userError(statement, "non local variable already in frame")

        contexts[contexts.length - 1].nonlocals.push({
          name: varname,
          nameToken: uicp(nt[0].children[0]),
          commaToken: mkToken(", "),
        })
        snap(instr.NONLOCAL_IN_CONTEXT)
      } else if (statement.type === "global") {
        let nt = nontokens(statement.children)
        let varname = statement.name
        let alreadyInCurrentFrame = (name) => {
          let frame = contexts[contexts.length - 1].frame
          let rows = frame.rows
          for (let i = 0; i < rows.length; i++) {
            if (rows[i].left.name === name) return true
          }
          return false
        }
        if (alreadyInCurrentFrame(varname)) throw userError(statement, "global variable already in frame")

        contexts[contexts.length - 1].globals.push({
          name: varname,
          nameToken: uicp(nt[0].children[0]),
          commaToken: mkToken(", "),
        })
        snap(instr.GLOBAL_IN_CONTEXT)
      } else {
        throw myBad(statement, "unrecognized statement:" + statement.type)
      }
    }
    return { reachedBlockEnd: true }
  }

  let evalChildren = (arr) => {
    for (let i = 0; i < arr.length; i++) {
      let e = arr[i]

      if (e.kind === "expr") {
        exp(e)
        if (e.category !== "value") throw myBad(e, "should be a a value")
        //  snap(instr."evaluation")
      }
    }
    for (let i = 0; i < arr.length; i++) {}
  }

  let getParametersFromFuncDef = (stmt) => {
    // returns array of tokens
    let nt = nontokens(stmt.children)
    let ps = nt[1]
    if (ps.kind !== "parameters") throw myBad(stmt, "parameters are supposed to be here")
    let gnt = nontokens(ps.children)
    if (gnt.length === 0) return []

    let paramlist = gnt[0]
    if (paramlist.kind !== "paramlist") throw myBad(stmt, "paramlist expected")

    let ggnt = nontokens(paramlist.children)
    if (ggnt.some((x) => x.kind !== "param")) throw myBad(stmt, "param expected")
    return ggnt
  }

  let frameKeyOfParameter = (param) => {
    if (param.children.length !== 1) {
      throw myBad("parameters should have one child")
    }
    let token = param.children[0]
    let copiedToken = uicp(token)
    let name = param.name
    if (name === undefined) throw myBad(undefined, "param missing name")
    return { name, token: copiedToken }
  }

  let isValue = (x) => {
    return x.category === "value"
  }

  let lookupAddressInExternal = (address) => {
    // only contains stdlib
    for (let i = 0; i < externalVars.rows.length; i++) {
      let v = externalVars.rows[i]
      if (v.value.address === address) return v
    }

    // check the modules
    let moduleNames = Object.keys(modules)
    for (let i = 0; i < moduleNames.length; i++) {
      let moduleName = moduleNames[i]
      let module = modules[moduleName]
      let variables = module.variables
      for (let j = 0; j < variables.length; j++) {
        let v = variables[j]
        if (v.value && v.value.address === address) {
          return v
        }
      }
    }

    // check the generated builtin functions
    for (let i = 0; i < generatedBuiltinFunctions.length; i++) {
      let v = generatedBuiltinFunctions[i]
      if (v.id === address) return v
    }
    return undefined
  }

  let evalFunctionCall = (originalE, callee, args, hole, methToken) => {
    if (callee.type !== "reference") {
      throw userError(callee, "Left hand side of parenthesis must be a reference")
    }

    if (callee.referenceType !== "function") {
      throw userError(callee, "Left hand side of parenthesis must be a reference to a function")
    }
    for (let i = 0; i < args.length; i++) {
      if (!isValue(args[i])) throw myBad(args[i], "expected value")
    }

    if (!callee.address) throw myBad(originalE, "missing address")

    let extV = lookupAddressInExternal(callee.address)
    if (extV) {
      // if (extV.value.referenceType !== "function") {

      //     throw userError(originalE, "Expected a function reference")
      // }

      // external address

      let func = extV.func

      let ret = func(args, originalE)
      if (ret === undefined) throw myBad(undefined, "what")
      replace(originalE, ret)
      snap(instr.EXTERNAL_FUNC_FINISHED)
      if (originalE.category !== "value") debugger
      return
    }

    if (!hole.fromConstructor && !hasIsolatedExpresionInCurrentFrame()) {
      // check if already focused
      //let copiedTop = uicp(exprInSource)

      markCurrentLine()
      resetExpression()
      //replace(exprInSource, originalExprInSource) // reset expression

      snap(instr.DETACH_EXPR)
    }

    //let hole = originalE
    hole.isHole = true
    hole.children = [mkToken("⚫", { blackBorder: true })]
    snap(instr.MAKE_HOLE)

    let funcInHeap = heap.find((x) => x.id === callee.address)

    if (methToken) {
      setHighlightedElements([methToken, collectTokens(originalE)[0]])
      snap(instr.HIGHLIGHT_METHOD_CALL)
      setHighlightedElements([])
    }
    if (!funcInHeap) debugger
    setHighlightedElements([funcInHeap.idToken, collectTokens(callee)[0]])
    snap(instr.HIGHLIGHT_FUNCTION_CALL)
    setHighlightedElements([])

    if (["function"].includes(callee.referenceType)) {
      let head = callee
      let addr = head.address
      let func = heap.find((x) => x.id === addr)
      let fdmarkerId = func.funcdefMarkerId

      let fdmarkerToken = func.funcdefMarker
      let fdmarkerInSource = endoflineTokens.find((x) => x.id === fdmarkerId)

      let fdmarkerInSourceToken = fdmarkerInSource.token
      setHighlightedElements([fdmarkerToken, fdmarkerInSourceToken])
      snap(instr.HIGHLIGHT_FUNCALL_LINE)
      setHighlightedElements([])
    }

    let functionInHeap = retrieveFromHeap(callee.address)

    let defASTNode = functionInHeap.defASTNode
    if (defASTNode.type !== "funcdef") {
      throw myBad(callee, "funcref does not point to func def")
    }

    let parameters = getParametersFromFuncDef(defASTNode) //token list

    if (parameters.length !== args.length) {
      throw userError(originalE, "Wrong number of arguments. Actual: " + args.length + ". Expected: " + parameters.length)
    }
    let closure = functionInHeap.closure

    if (closure === undefined) {
      throw myBad(callee, "No closure for this function")
    }

    let closureToken = uicp(functionInHeap.closureToken)

    let originalCtx = findFrameById(closure)

    addToHoverAssociations(closureToken.id, originalCtx.idToken.id)

    let frameKeys = parameters.map(frameKeyOfParameter)
    let copiedArgs = args.map((a) => uicp(a)) // args will populate the frame

    let frame = newFrame(closure, closureToken) // current frame will be added to the closure. no snap.

    for (let i = 0; i < frameKeys.length; i++) {
      addToFrame(frame, frameKeys[i], copiedArgs[i])
    }
    let context = newContext(frame)
    pushToContexts(context, callee)
    snap(instr.PUSHED_CONTEXT)

    // let getBodyFromFuncDefNode = node => {
    //     return
    // }
    let getBodyFromFuncDefNode = (stmt) => {
      let nt = nontokens(stmt.children)
      if (nt.length !== 3) throw myBad("assumed funcdefs have 2 nonchildren nodeds")
      return nt[2]
    }
    let block = getBodyFromFuncDefNode(defASTNode)

    let result = interpretBlock(block, hole)

    if (hole.fromConstructor && result.hitReturn) {
      throw myBad(hole, "return was hit even though it's a constructor call")
    }

    if (!hole.fromConstructor && hole.category !== "value" && result.hitReturn) {
      throw myBad(hole, "block returned yet return value not in hole")
    }
    if (!hole.fromConstructor && hole.category === "value" && !result.hitReturn) {
      throw myBad(hole, "block ran til end yet hole is filled")
    }

    if (hole.category !== "value") {
      replace(hole, makeNoneValue())
      // hole.filled = true
      // hole.children = [makeNoneValue()]
      snap(instr.NO_RETURN)
    }

    let getTopFrame = () => {
      return contexts[contexts.length - 1].frame
    }
    let topFrame = getTopFrame()

    let inSomeClosure = (frame) => {
      for (let i = 0; i < contexts.length; i++) {
        if (contexts[i].frame.closure === frame.id) return true
      }
      for (let i = 0; i < heap.length; i++) {
        if (heap[i].closure === frame.id) return true
      }
    }
    if (inSomeClosure(topFrame)) {
      frameHeaven.push(getTopFrame())
      snap(instr.ADD_TO_FRAMEHEAVEN)
      popContext()
      snap(instr.POPPED_CONTEXT)
    } else {
      popContext()
      snap(instr.POPPED_CONTEXT_NO_HEAVEN)
    }
  }

  // let makeNewValue = v => {
  //     return {
  //         category: "value",
  //         value: v,
  //         children: [mkToken("" + v)]
  //     }
  // }

  let makeBoolValue = (b, ch) => {
    if (typeof b !== "boolean") throw myBad(undefined, "expected boolean")
    let tokStr = b ? "True" : "False"
    return {
      category: "value",
      children: ch ? ch : [mkToken(tokStr)],
      primitiveType: "bool",
      value: b,
    }
  }

  let makeIntValue = (n, ch) => {
    if (typeof n !== "number") throw myBad(undefined, "expected number")
    return {
      category: "value",
      children: ch ? ch : [mkToken("" + n)],
      primitiveType: "int",
      value: n,
    }
  }

  let makeStringValue = (str, ch) => {
    if (typeof str !== "string") throw myBad(undefined, "expected string")
    return {
      category: "value",
      children: ch ? ch : [mkToken(str)],
      primitiveType: "string",
      value: str,
    }
  }

  let convertableToNumber = (t) => {
    return ["int", "bool"].includes(t.primitiveType)
  }

  let coerceToNumber = (t) => {
    if (!["int", "bool"].includes(t.primitiveType)) throw myBad(undefined, "expected coercable type")
    if (t.primitiveType === "int") return t.value
    return t.value ? 1 : 0
  }

  let isStringValue = (x) => {
    return x.primitiveType === "string" && x.category === "value"
  }

  let evalBinary = (opname, left, right, parent) => {
    if (typeof opname !== "string") throw myBad(parent, "what")

    let lazyops = ["and", "or"]
    if (!lazyops.includes(opname)) {
      exp(left)
      exp(right)

      if (left.category !== "value") {
        throw myBad(parent, "Expected left to be value")
      }
      if (right.category !== "value") {
        throw myBad(parent, "Expected right to be value")
      }
    }

    switch (opname) {
      case "+": {
        if (isStringValue(left)) {
          if (!isStringValue(right)) {
            throw userError(right, "You can only add a string to a string")
          }
          let newv = makeStringValue(left.value + right.value)
          replace(parent, newv)

          return
        }
        let leftValue = coerceToNumber(left)
        let rightValue = coerceToNumber(right)
        let newv = makeIntValue(leftValue + rightValue)
        replace(parent, newv)
        snap(instr.BINARY_DONE, { operator: "+" })
        return
      }
      case "-": {
        let leftValue = coerceToNumber(left)
        let rightValue = coerceToNumber(right)
        let newv = makeIntValue(leftValue - rightValue)
        replace(parent, newv)
        snap(instr.BINARY_DONE, { operator: "-" })
        return
      }
      case "*": {
        let leftValue = coerceToNumber(left)
        let rightValue = coerceToNumber(right)
        let newv = makeIntValue(leftValue * rightValue)
        replace(parent, newv)
        snap(instr.BINARY_DONE, { operator: "*" })
        return
      }
      case "%": {
        let leftValue = coerceToNumber(left)
        let rightValue = coerceToNumber(right)
        let newv = makeIntValue(leftValue % rightValue)
        replace(parent, newv)
        snap(instr.BINARY_DONE, { operator: "%" })
        return
      }
      case "==": {
        if (isReference(left) && isReference(right)) {
          if (!left.address) throw myBad(parent, "left missing address")
          if (!right.address) throw myBad(parent, "right missing address")
          return makeBoolValue(left.address === right.address)
        }

        let newv = makeBoolValue(!isReference(left) && !isReference(right) && left.value === right.value)
        replace(parent, newv)
        if (parent.category !== "value") debugger
        snap(instr.BINARY_DONE, { operator: "==" })
        return
      }
      case "!=": {
        if (isReference(left) && isReference(right)) {
          if (!left.address) throw myBad(parent, "left missing address")
          if (!right.address) throw myBad(parent, "right missing address")
          return makeBoolValue(left.address !== right.address)
        }

        let newv = makeBoolValue(!isReference(left) && !isReference(right) && left.value !== right.value)
        replace(parent, newv)
        if (parent.category !== "value") debugger
        snap(instr.BINARY_DONE, { operator: "!=" })

        return
      }
      case "<": {
        let newv = undefined
        if (convertableToNumber(left) && convertableToNumber(right)) {
          let leftValue = coerceToNumber(left)
          let rightValue = coerceToNumber(right)
          newv = makeBoolValue(leftValue < rightValue)
        } else if (isStringValue(left) && isStringValue(right)) {
          newv = makeBoolValue(left.value < right.value)
          replace(parent, newv)
          snap(instr.BINARY_DONE, { operator: "<" })
          return
        } else {
          newv = makeBoolValue(true)
        }
        replace(parent, newv)
        snap(instr.BINARY_DONE, { operator: "<" })
        return
      }
      case "<=": {
        let newv = undefined
        if (convertableToNumber(left) && convertableToNumber(right)) {
          let leftValue = coerceToNumber(left)
          let rightValue = coerceToNumber(right)
          newv = makeBoolValue(leftValue <= rightValue)
        } else if (isStringValue(left) && isStringValue(right)) {
          newv = makeBoolValue(left.value <= right.value)
        } else {
          newv = makeBoolValue(true)
        }
        replace(parent, newv)
        snap(instr.BINARY_DONE, { operator: "<=" })
        return
      }

      case ">": {
        let newv = undefined

        if (convertableToNumber(left) && convertableToNumber(right)) {
          let leftValue = coerceToNumber(left)
          let rightValue = coerceToNumber(right)
          newv = makeBoolValue(leftValue > rightValue)
        } else if (isStringValue(left) && isStringValue(right)) {
          newv = makeBoolValue(left.value > right.value)
        } else {
          newv = makeBoolValue(false)
        }
        replace(parent, newv)
        snap(instr.BINARY_DONE, { operator: ">" })
        return
      }
      case ">=": {
        let newv = undefined
        if (convertableToNumber(left) && convertableToNumber(right)) {
          let leftValue = coerceToNumber(left)
          let rightValue = coerceToNumber(right)
          newv = makeBoolValue(leftValue >= rightValue)
        } else if (isStringValue(left) && isStringValue(right)) {
          newv = makeBoolValue(left.value >= right.value)
        } else {
          newv = makeBoolValue(false)
        }
        replace(parent, newv)
        snap(instr.BINARY_DONE, { operator: ">=" })
        return
      }
      case "or": {
        exp(left)
        if (!isFalsyValue(left)) {
          replace(parent, left)
          snap(instr.BINARY_DONE, { operator: "or" })
          return
        }
        replace(parent, right)
        snap(instr.OR_REDUCTION)
        exp(parent)

        return
      }
      case "and": {
        exp(left)
        if (isFalsyValue(left)) {
          replace(parent, left)
          snap(instr.BINARY_DONE, { operator: "and" })
          return
        }
        replace(parent, right)
        snap(instr.AND_REDUCTION)
        exp(parent)

        return
      }
      default: {
        throw userError(left, "operation not supported: " + opname)
      }
    }
  }

  let getValue = (e) => {
    if (e.category !== "value") throw myBad("supposed to be a value")
    return e.value
  }

  let retrieveFromHeap = (addr) => {
    for (let i = 0; i < heap.length; i++) {
      let e = heap[i]
      if (e.id === addr) return e
    }
    throw myBad(undefined, "Unable to find the following in the heap: " + addr)
  }

  let equalAsValues = (a, b) => {
    if (a.category !== "value") throw myBad(undefined, "expected value")
    if (b.category !== "value") throw myBad(undefined, "expected value")

    return a.primitiveType === b.primitiveType && a.value === b.value
  }

  let lookupVarInFrame = (varname, frame) => {
    for (let i = 0; i < frame.rows.length; i++) {
      let row = frame.rows[i]

      if (row.left.name === varname) {
        return row
      }
    }
  }

  let findFrameWithId = (id) => {
    let frames = contexts.map((c) => c.frame)
    let next = frames.find((frame) => frame.id === id)
    if (next) return next
    let otherCandidate = frameHeaven.find((frame) => frame.id === id)
    if (!otherCandidate) throw myBad(undefined, "unable to find frame " + id)
    return otherCandidate
  }

  let nonlocalrowTransitive = (varname, frame) => {
    for (let i = 0; i < frame.rows.length; i++) {
      let row = frame.rows[i]
      if (row.left.name === varname) {
        return row
      }
    }
    let closure = frame.closure
    if (closure === null || closure === "global-frame") return undefined
    if (!closure) throw myBad(undefined, "missing closure")
    let next = findFrameWithId(frame.closure)
    return nonlocalrowTransitive(varname, next)
  }
  let nonlocalrow = (varname) => {
    let frame = contexts[contexts.length - 1].frame
    return nonlocalrowTransitive(varname, frame)
  }

  let globalrow = (varname) => {
    let frame = contexts[0].frame
    for (let i = 0; i < frame.rows.length; i++) {
      let row = frame.rows[i]
      if (row.left.name === varname) {
        return row
      }
    }
    return undefined
  }

  let lookupVarInFrameTransitive = (varname, frame) => {
    for (let i = 0; i < frame.rows.length; i++) {
      let row = frame.rows[i]
      if (row.left.name === varname) {
        return row
      }
    }

    let closure = frame.closure
    if (closure === null) return undefined
    if (!closure) throw myBad(undefined, "missing closure")
    let next = findFrameWithId(closure)
    setHighlightedElements([frame.closureToken, next.idToken])
    snap(instr.LOOKUP_PARENT_FRAME)
    setHighlightedElements([])
    return lookupVarInFrameTransitive(varname, next)
  }
  let lookupVarInAllFrames = (varname, varexp) => {
    let currentFrame = contexts[contexts.length - 1].frame

    setHighlightedElements([varexp])
    snap(instr.LOOKUP_VAR_INTRO)
    setHighlightedElements([currentFrame.idToken, varexp])
    snap(instr.LOOKUP_CURRENT_FRAME)
    setHighlightedElements([])
    return lookupVarInFrameTransitive(varname, currentFrame)
  }

  let lookupVarInExternal = (varname) => {
    for (let i = 0; i < externalVars.rows.length; i++) {
      let ev = externalVars.rows[i]
      if (ev.name === varname) return ev
    }
    return undefined
  }

  let pushToHeap = (e, sourceNode) => {
    //if (!["heap-function", "heap-dict", "heap-array"].includes(e.type)) throw myBad("can only add functions, dicts and arrays to heap")
    if (heap.length >= 16) {
      throw userError(sourceNode, "This interpreter only supports up to 16 objects in the heap.")
    }
    heap.push(e)
  }
  let pushToContexts = (c, sourceNode) => {
    //if (!["heap-function", "heap-dict", "heap-array"].includes(e.type)) throw myBad("can only add functions, dicts and arrays to heap")
    if (contexts.length >= 10) {
      throw userError(sourceNode, "This interpreter only supports up to 10 contexts.")
    }
    contexts.push(c)
  }

  let checkValues = (arr) => {
    for (let i = 0; i < arr.length; i++) {
      if (arr[i].category !== "value") throw myBad(arr[i], "expected value")
    }
  }

  let isNumberValue = (x) => {
    return x.category === "value" && x.primitiveType === "int"
  }

  let exp = (e) => {
    if (!e.kind === "expr") throw myBad(e, "can only evaluate expressions")
    if (e.type === "binary") {
      let nt = nontokens(e.children)

      if (nt.length !== 3) throw myBad(e, "expected binary ops to have 3 nontoken children")
      let [left, op, right] = nt
      let opname = op.name
      if (!opname) throw myBad("expecting op to have a name")
      evalBinary(opname, left, right, e)
      return
    } else if (e.type === "funcall") {
      evalChildren(e.children) // evaluates the head and args
      let nt = nontokens(e.children)
      // if (nt.length !== 2) throw myBad(e, "should have 2 nontoken elements")
      let head = nt[0]
      let args = nt.length === 1 ? [] : nontokens(nt[1].children)
      //debugger
      for (let i = 0; i < args.length; i++) {
        exp(args[i])
      }

      if (!["function", "class", "method"].includes(head.referenceType)) {
        throw userError(head, "can only call function or class")
      }

      let hole = e
      if (head.referenceType == "class") {
        let builtinClass = externalVars.rows.find((x) => x.value && x.value.address === head.address)
        if (builtinClass) {
          let objid = genObjectHeapId()
          let objidToken = mkToken(objid, { ref: true })

          let instanceOfToken = uicp(head.children[0])

          let objectInHeap = {
            type: "heap-object",
            label: mkToken("instance"),
            instanceOf: {
              titleToken: mkToken("instance of"),
              valueToken: instanceOfToken,
            },
            id: objid,
            idToken: objidToken,
            attributes: [],
          }
          pushToHeap(objectInHeap, head)
          snap(instr.INSTANCE_CREATED)
          let objectref = {
            id: genObjectrefId(),
            category: "value",
            type: "reference",
            referenceType: "object",
            address: objid,
            children: [cpTokenHover(objidToken)],
          }
          replace(e, objectref)
          snap(instr.OBJECT_REF)
          return
        }

        let classInHeap = heap.find((x) => x.id === head.address)

        if (!classInHeap) throw myBad(e, "can't find class in heap")

        let collectAttributes = (classInHeap, sofar, keysSofar) => {
          classInHeap.attributes.forEach((row) => {
            if (keysSofar.has(row.key)) return
            keysSofar.add(row.key)
            let left = row.keyToken
            let right = row.value // must be a value

            sofar.push({
              id: genInstanceAttributeRowId(),
              key: row.key,
              keyToken: uicp(left),
              value: uicp(right),
            })
          })
          snap(instr.POPULATE_ONE_LEVEL)
          classInHeap.superclasses.forEach((v) => {
            if (v.referenceType !== "class") throw myBad(e, "baseclass should be a class")
            let nclassInHeap = heap.find((x) => x.id === v.address)
            if (!nclassInHeap) throw myBad(e, " could not find class in heap")
            collectAttributes(nclassInHeap, sofar, keysSofar)
          })
        }

        let instanceAttributes = []

        let objid = genObjectHeapId()
        let objidToken = mkToken(objid, { ref: true })

        let valueToken = uicp(head.children[0])
        let cl = heap.find((h) => h.id === head.address)
        if (cl) {
          addToHoverAssociations(valueToken.id, cl.idToken.id)
        }

        let objectInHeap = {
          type: "heap-object",
          label: mkToken("instance"),
          id: objid,
          idToken: objidToken,
          attributes: instanceAttributes,
          instanceOf: {
            titleToken: mkToken("instance of"),
            valueToken,
          },
        }
        pushToHeap(objectInHeap, head)
        collectAttributes(classInHeap, instanceAttributes, new Set())

        //snap(instr."populate-object")
        let toPopulate = []

        for (let i = 0; i < instanceAttributes.length; i++) {
          let instanceAttribute = instanceAttributes[i]
          let value = instanceAttribute.value
          if (value.referenceType !== "function") continue
          // let's create a method
          let id = genMethodHeapId()
          let idToken = mkToken(id, { ref: true })
          let boundToValue = {
            id: genObjectrefId(),
            category: "value",
            type: "reference",
            referenceType: "object",
            address: objid,
            children: [cpTokenHover(objidToken)],
          }
          let methodInHeap = {
            type: "heap-method",
            label: mkToken("method"),
            boundToValue,
            boundToToken: mkToken("bound to"),
            id,
            idToken,
            funcAddress: uicp(value),
            funcToken: mkToken("function"),
          }
          instanceAttribute.value = undefined
          pushToHeap(methodInHeap, head)
          toPopulate.push({ instanceAttribute, id, idToken })
        }
        snap(instr.METHODS_CREATED)

        for (let i = 0; i < toPopulate.length; i++) {
          let { instanceAttribute, id, idToken } = toPopulate[i]
          let methodref = {
            id: genMethodrefId(),
            category: "value",
            type: "reference",
            referenceType: "method",
            address: id,
            children: [cpTokenHover(idToken)],
          }
          instanceAttribute.value = methodref
        }
        snap(instr.METHODS_REFS_CREATED)

        let objectref = {
          id: genObjectrefId(),
          category: "value",
          type: "reference",
          referenceType: "object",
          address: objid,
          children: [cpTokenHover(objidToken)],
        }

        replace(e, objectref)
        snap(instr.OBJECT_REF)
        let co = objectInHeap.attributes.find((attr) => attr.key === "__init__")
        if (!co) return
        hole = { fromConstructor: true }
        head = co.value
      }

      let methToken = undefined

      if (head.referenceType === "method") {
        let meth = heap.find((x) => x.id === head.address)
        if (!meth) throw myBad(e, "could not find method in heap")
        methToken = meth.idToken

        let v = meth.boundToValue
        args.unshift(v)
        head = meth.funcAddress
        if (head.referenceType !== "function") throw myBad("expected a function")
      }

      evalFunctionCall(e, head, args, hole, methToken)
      if (e.category !== "value") {
        debugger
        throw myBad(undefined, "expected value after function call")
      }
      return
    } else if (e.isPrimitiveLiteral) {
      if (e.type === "number-literal") {
        let v = makeIntValue(e.number, e.children)
        replace(e, v)
        return
      } else if (e.type === "string-literal") {
        let v = makeStringValue(e.stringContents, e.children)
        replace(e, v)
        return
      } else if (e.type === "bool-literal") {
        let v = makeBoolValue(e.bool, e.children)
        replace(e, v)
        return
      } else if (e.type === "none-literal") {
        let v = makeNoneValue(e.children)
        replace(e, v)
        return
      } else {
        throw myBad(e, "unrecognized primitive literal")
      }
    } else if (e.type === "array-literal") {
      evalChildren(e.children)
      let nt = nontokens(e.children)
      let arrayId = masterGener()
      let values = nt.map(uicp)

      let arrayInHeap = makeArrayInHeap(arrayId, values, e)

      pushToHeap(arrayInHeap)
      snap(instr.NEW_LIST)

      let newValue = {
        id: genArrayRefValueId(),
        category: "value",
        type: "reference",
        referenceType: "array",
        address: arrayId,
        children: [cpTokenHover(arrayInHeap.idToken)],
      }
      replace(e, newValue)
      snap(instr.LIST_REF)
    } else if (e.type === "dict-literal") {
      let ntpapa = nontokens(e.children)
      if (ntpapa.length !== 1) throw myBad("assumed only one nonterminal child")
      let keyvaluepairs = ntpapa[0]
      evalChildren(keyvaluepairs.children)
      let nt = nontokens(keyvaluepairs.children)
      if (nt.length % 2 !== 0) throw myBad(e, "Expected even number of children")
      let dictRows = []
      for (let i = 0; i < nt.length; i += 2) {
        let key = nt[i]
        if (key.type === "reference") throw userError(key, "A reference cannot be the key of a dict")
        dictRows.push({
          id: genDictRowId(),
          key: uicp(key),
          value: uicp(nt[i + 1]),
        })
      }
      let dictid = masterGener()

      let d = {
        label: mkToken("dictionary"),
        idToken: mkToken(dictid, { ref: true }),
        id: dictid,
        type: "heap-dict",
        rows: dictRows,
      }
      pushToHeap(d)
      snap(instr.NEW_DICT)
      let newValue = {
        id: genDictRefValueId(),
        category: "value",
        type: "reference",
        referenceType: "dict",
        address: dictid,
        children: [cpTokenHover(d.idToken)],
      }
      replace(e, newValue)
      snap(instr.DICT_REF)
    } else if (e.type === "bracket") {
      let getValueAtArrayIndex = (arrayIdRef, indexValue) => {
        let arrayId = arrayIdRef.address
        let index = indexValue.value
        let arr = heap.find((x) => x.id === arrayId)
        if (!arr) throw myBad(e, "could not find array at " + arrayId)

        setHighlightedElements([arrayIdRef, arr.idToken])
        snap(instr.HIGHLIGHT_LIST_LOOKUP)

        if (index >= arr.rows.length) {
          throw userError(indexValue, "Array index out of bounds")
        }

        if (index >= arr.rows.length) {
          throw userError(indexValue, "Array index out of bounds")
        }
        if (index < 0) {
          throw userError(indexValue, "Python supports negative indices but this tool doesn't")
        }

        let row = arr.rows[index]

        setHighlightedElements([indexValue, row.indexToken])
        snap(instr.HIGHLIGHT_LIST_LOOKUP_INNER)
        setHighlightedElements([])

        if (!row) throw myBad(e, "Index out of bound: " + index)
        return row.value
      }

      let lookupDict = (dict, key, sourceNode) => {
        let rows = dict.rows
        for (let i = 0; i < rows.length; i++) {
          if (equalAsValues(rows[i].key, key)) {
            return rows[i]
          }
        }
        throw userError(sourceNode, "Unable to find the key in the dict: " + key.value)
      }

      evalChildren(e.children)
      let nt = nontokens(e.children)
      if (nt.length !== 2) throw myBad(e, "Expected bracket expression to have 2 children")
      if (nt[0].type !== "reference") throw userError(e, "left hand side of bracket should be a reference")
      if (nt[0].referenceType === "array") {
        if (!isNumberValue(nt[1]))
          throw userError(e, "If the left hand side of a bracket expression is an array, the inside of the bracket should be a number")
        let indexValue = nt[1]
        let arrayId = nt[0]
        if (!arrayId) throw myBad("empty array address")
        let v = getValueAtArrayIndex(arrayId, indexValue)
        let final = uicp(v)
        replace(e, final)
        snap(instr.BRACKET_LOOKUP_DONE)
        return
      } else if (nt[0].referenceType === "dict") {
        // if (!isStringValue(nt[1]))
        //   throw userError(
        //     e,
        //     "If the left hand side of a bracket expression is a dict, the inside of the bracket should be a string"
        //   )

        let dictInHeap = retrieveFromHeap(nt[0].address)

        setHighlightedElements([nt[0], dictInHeap.idToken])
        snap(instr.HIGHLIGHT_DICT_LOOKUP)

        let v = lookupDict(dictInHeap, nt[1], e)

        setHighlightedElements([collectTokens(nt[1])[0], collectTokens(v.key)[0]])
        snap(instr.HIGHLIGHT_DICT_LOOKUP_INNER)
        setHighlightedElements([])

        let final = uicp(v.value)
        replace(e, final)
        snap(instr.DICT_BRACKET_LOOKUP_DONE)
        return
      } else {
        throw userError(e, "left hand side of bracket should be an array or a dict")
      }
    } else if (e.type === "dot") {
      let getAttributeValue = (objectIdRef, name, nameToken) => {
        let objectId = objectIdRef.address
        let obj = heap.find((x) => x.id === objectId)
        if (!obj) throw myBad(e, "could not find object at " + objectId)

        setHighlightedElements([objectIdRef, obj.idToken])
        snap(instr.HIGHLIGHT_ATTRIBUTE_LOOKUP)

        let attributes = obj.attributes
        if (!attributes) {
          throw myBad(e, "could not find attribute ")
        }
        let attribute = attributes.find((a) => a.key === name)

        if (!attribute) throw myBad(e, "missing attribute with name: " + name)
        setHighlightedElements([nameToken, attribute.keyToken])
        snap(instr.HIGHLIGHT_ATTRIBUTE_LOOKUP_NAME)
        setHighlightedElements([])

        if (!attribute) throw userError(e, "Attribute not not found: " + name)
        return attribute.value
      }

      evalChildren(e.children)
      let nt = nontokens(e.children)
      if (nt.length !== 2) throw myBad(e, "Expected dot expression to have 2 children")
      if (nt[0].type !== "reference") throw userError(e, "left hand side of dot should be a reference")
      if (!["class", "object", "array"].includes(nt[0].referenceType)) {
        throw userError(e, "left hand side of dot should be a class reference or an object reference")
      }
      //if (!isNumberValue(nt[1])) throw userError(e, "If the left hand side of a dot expression is an array, the inside of the bracket should be a number")

      let name = nt[1].name
      let objectIdRef = nt[0]
      if (!objectIdRef) throw myBad("empty object address")
      let v = getAttributeValue(objectIdRef, name, nt[1].children[0])
      let final = uicp(v)
      replace(e, final)
      snap(instr.ATTRIBUTE_LOOKUP_DONE)
      return
    } else if (e.type === "ternary") {
      let nt = nontokens(e.children)
      if (nt.length !== 3) throw myBad(e, "Expected ternary to have 3 non token children")
      let cond = nt[1]
      exp(cond)
      snap(instr.TERNARY_COND)
      if (boolOfValue(cond)) {
        replace(e, nt[0])
        snap(instr.TERNARY_TRUE)
        return exp(e)
      } else {
        replace(e, nt[2])
        snap(instr.TERNARY_FALSE)
        return exp(e)
      }
    } else if (e.type === "var") {
      let varname = e.name
      if (contexts[contexts.length - 1].currentlyDefining.includes(varname))
        throw userError(e, "Cannot use variable that is being currently defined: " + varname)
      if (varname === undefined) throw myBad(e, "varname without name")
      let row = lookupVarInAllFrames(varname, e)
      if (row === undefined) {
        throw userError(e, "Could not find variable")
      }

      if (row.right === undefined) throw myBad(e, "no value for row")

      if (row.isExternal && !interpreterOpts.alwaysShowExternal) {
        externalVarsShown = [row.id]
        //snap({ name: "CHANGE SOON", text: "CHANGE SOON" })
      }

      setHighlightedElements([e, row.left.token])
      snap(instr.VAR_LOOKUP_HIGHLIGHT)
      setHighlightedElements([])

      replace(e, uicp(row.right))
      if (e.category !== "value") debugger

      if (row.isExternal && !interpreterOpts.alwaysShowExternal) {
        externalVarsShown = []
        //snap({ name: "CHANGE SOON", text: "CHANGE SOON" })
      }
      snap(instr.VAR_LOOKUP)
      return
    } else if (e.type === "paren") {
      let nt = nontokens(e.children)
      if (nt.length !== 1) {
        throw myBad(e, "parenthesis should have one nonterminal child")
      }
      exp(nt[0])
      replace(e, nt[0])
      snap(instr.PAREN)
    } else if (e.type === "not") {
      let nt = nontokens(e.children)
      if (nt.length !== 1) {
        throw myBad(e, "parenthesis should have one nonterminal child")
      }
      exp(nt[0])
      replace(e, makeBoolValue(isFalsyValue(nt[0])))
      snap(instr.NOT_EXPR)
    } else {
      throw myBad(e, "unexpected expression type: " + e.type)
    }
  }

  let infoOfError = (errorId) => {
    if (errorId === "runtime-error") {
      return instr.RUNTIME_ERROR
    }
    if (errorId === "drew-too-much") {
      return instr.DREW_TOO_MUCH
    }
    if (errorId === "drew-too-little") {
      return instr.DREW_TOO_LITTLE
    }
    if (errorId === "draw-mismatch") {
      return instr.INCORRECT_DRAWING
    }
    if (errorId === "dev-error") {
      return instr.MY_BAD
    }
  }

  try {
    interpretBlock(topAST, undefined)
    if (expectedLines && expectedLines.length !== 0) {
      throw drewTooLittleExc(undefined, "Your drawing in incomplete")
    }
  } catch (err) {
    if (!err.lastFrameInstructions) throw err
    lastFrameInstructions = err.lastFrameInstructions
    if (lastFrameInstructions.node) propagateUserError(lastFrameInstructions.node)
    snap(infoOfError(lastFrameInstructions.type))
    swimmingly = false
  }
  if (swimmingly && overToken) {
    swimmingly = false
    lastFrameInstructions = {
      type: "over-token-quota",
      maximum: overToken.maximum,
      actual: overToken.actual,
      userTokens: overToken.userTokens,
    }
    snap(instr.TOKEN_QUOTA)
  }

  if (swimmingly && restrictionViolated) {
    swimmingly = false
    lastFrameInstructions = {
      type: "restriction-violated",
      msg: restrictionViolated.desc,
    }
    snap(instr.RESTRICTION_VIOLATED)
  }

  console.log("copy snap time", copysnaptime / 1000)
  console.log("check dup time", totalCheckDups / 1000)
  console.log("cloning source token time", cloningSourceTokenTime / 1000)

  // try {
  //     interpretBlock(topAST, undefined)
  // } catch (err) {
  //     if (!err.userError) {
  //         debugger
  //     }
  // }

  return {
    animations: animationFrames,
    refLookupHighlights: hoverAssociations,
    swimmingly,
  }
}

export default interpretProgram
