import React, { useEffect, useState, useRef } from "react"
import produce from "immer"
import { animRef } from "./anim.js"
import { useSelector } from "react-redux"
import { dispatch, isPermitted, store } from "./store"
import { codeFont } from "./globals.js"
import { colorEditCode, colorError, colorBreakpoint, colorStartingCode } from "./colors"
import { populateDecorationsRef } from "./populate-decorations-ref"
import { tutorialOnClick } from "./TutorialButton"

import { useColorBackground } from "./colors"
let tutorialAttr = (idInInterface, onClick) => {
  return {
    ref: (r) => populateDecorationsRef(idInInterface, r),
    onClick: tutorialOnClick(idInInterface, onClick),
  }
}
let spanOfErrorToken = (t) => {
  return (
    <span key={t.id} ref={(r) => animRef(r, t.id, { origin: t.origin, copyText: true })}>
      {t.text}
    </span>
  )
}

let userTextOfSection = (x) => {
  let padWithNewlines = (str, n) => {
    let current = str.split("\n").length
    while (current < n) {
      str += "\n"
      current++
    }
    return str
  }
  return x.type === "read" ? x.text : padWithNewlines(x.userTyped || "", x.text.split("\n").length)
}

function HighlightLineNumber({ parseError, sourceCode, identifier, adminMode }) {
  const [state, setState] = useState({ shown: false })
  useEffect(() => {
    if (parseError === undefined) {
      setState((state) =>
        produce(state, (state) => {
          state.shown = false
        })
      )
      return
    }
    setState((state) =>
      produce(state, (state) => {
        state.shown = true
        state.parseError = parseError
      })
    )
  }, [parseError])

  if (!state.parseError) return null

  let opacity = state.shown ? 1 : 0
  let textSections = sourceCode.map((x) => {
    if (adminMode) return x.text
    else return userTextOfSection(x)
  })
  let lines = textSections.join("\n").split("\n")

  let toDisplay = lines.map((txt, ind) => {
    if (ind + 1 === state.parseError.lineNum)
      return (
        <>
          <span class="born-animation" key={identifier + "span"} style={{ backgroundColor: colorError }}>
            {txt}
          </span>
          {spanOfErrorToken(state.parseError.errorMsgToken)}
          {"\n"}
        </>
      )
    console.log(txt)
    return txt + "\n"
  })

  return (
    <pre
      key={identifier}
      style={{
        ...preStyle,
        ...codeFont,
        animationDuration: "1s",
        transition: "opacity 1s",
        opacity: opacity,
        zIndex: 50,
        position: "absolute",
        left: "0px",
        top: "0px",
      }}
    >
      {toDisplay}
    </pre>
  )
}

function MyTextArea(props) {
  let ref = useRef(undefined)
  useEffect(() => {
    let el = ref.current //document.getElementById(id);
    el.onkeydown = function (e) {
      if (e.keyCode === 9) {
        // tab was pressed

        // get caret position/selection
        var val = this.value,
          start = this.selectionStart,
          end = this.selectionEnd

        // set textarea value to: text before caret + tab + text after caret
        this.value = val.substring(0, start) + "\t" + val.substring(end)

        // put caret at right position again
        this.selectionStart = this.selectionEnd = start + 1

        // prevent the focus lose
        return false
      }
    }
  }, [])
  let numCols = undefined
  if (props.value) {
    let lines = props.value.split("\n")
    let maxLine = Math.max(...lines.map((x) => x.length))
    numCols = Math.max(10, maxLine)
  }
  return (
    <textarea
      cols={numCols}
      autocomplete="off"
      autocorrect="off"
      autocapitalize="off"
      spellcheck="false"
      ref={ref}
      {...props}
    />
  )
}

let textAreaStyle = {
  height: "auto",
  whiteSpace: "nowrap",
  padding: "0px",
  margin: "0px",
  border: "0px",
  overflow: "hidden",
  resize: "none",
  overflowY: "hidden",
  ...codeFont,
}
let preStyle = { padding: "0px", margin: "0px", ...codeFont }

let range = (n) => {
  let ret = []
  for (let i = 0; i < n; i++) {
    ret.push(i)
  }
  return ret
}

export default function MultiSection({
  adminMode,
  parseError,
  animationView,
  identifier,
  exerciseId,
  breakpoints,
}) {
  let colorBackground = useColorBackground()
  let currentExercise = useSelector((s) => s.content.exercises[exerciseId])
  let isTutorialExercise = useSelector(
    (s) => s.currentView?.exercise && s.content.tutorialDescriptions?.[s.currentView.exercise]
  )

  let sections = currentExercise.textSections
  let setBreakpoints = (s) => {
    dispatch({ type: "set-breakpoints", breakpoints: s })
  }

  let setSections = (s) => {
    dispatch({
      type: "set-text-sections-of-exercise",
      exerciseId,
      sections: s,
    })
  }

  let numLines = 0
  for (let i = 0; i < sections.length; i++) {
    let section = sections[i]
    if (section.type === "read") {
      let lines = section.text.split("\n")
      numLines += Math.max(1, lines.length)
    }
    if (section.type === "write") {
      let lines = section.text.split("\n")
      numLines += Math.max(1, lines.length)
    }
  }
  let breakpointGutterElements = range(numLines).map((index) => {
    let style = {}
    if (breakpoints[index]) {
      style.backgroundColor = colorBreakpoint
    }
    return (
      <div style={{ whiteSpace: "nowrap" }}>
        <pre
          style={{ ...preStyle, cursor: "pointer", userSelect: "none" }}
          {...tutorialAttr("breakpoint-" + index, () => {
            setBreakpoints(
              produce(breakpoints, (breakpoints) => {
                breakpoints[index] = !breakpoints[index]
              })
            )
          })}
          // onClick={}
        >
          {index + 1}
          <span style={{ ...style, ...codeFont, borderRadius: "400px" }}> </span>
        </pre>
      </div>
    )
  })

  let buttonStyle = { height: "14px", fontSize: "8px", margin: "0px" }

  let createBeforeButton = (index) => (
    <button
      style={buttonStyle}
      onClick={() => {
        setSections(
          produce(sections, (draft) => {
            draft.splice(index, 0, { type: "read", text: "" })
          })
        )
      }}
    >
      {" "}
      +{" "}
    </button>
  )

  let createAfterButton = (index) => (
    <button
      style={buttonStyle}
      onClick={() => {
        setSections(
          produce(sections, (draft) => {
            draft.splice(index + 1, 0, { type: "read", text: "" })
          })
        )
      }}
    >
      {" "}
      +{" "}
    </button>
  )

  let switchTypeButton = (index) => (
    <button
      style={buttonStyle}
      onClick={() => {
        setSections(
          produce(sections, (draft) => {
            draft[index].type = draft[index].type === "read" ? "write" : "read"
          })
        )
      }}
    >
      {" "}
      switch{" "}
    </button>
  )

  let deleteButton = (index) => (
    <button
      style={buttonStyle}
      onClick={() => {
        setSections(
          produce(sections, (draft) => {
            draft.splice(index, 1)
          })
        )
      }}
    >
      {" "}
      del{" "}
    </button>
  )

  let innerView = (
    <div>
      {sections.map((section, index) => {
        let buttonControls = [switchTypeButton(index), createAfterButton(index), deleteButton(index)]

        let expected = section.text
        let expectedLines = expected.split("\n")
        let numRows = Math.max(1, expectedLines.length)
        if (section.type === "read") {
          return (
            <div style={{ display: "flex" }}>
              <MyTextArea
                readonly={adminMode ? false : true}
                style={{ ...textAreaStyle, backgroundColor: colorBackground }}
                value={expected}
                rows={numRows}
                onChange={(evt) => {
                  if (!adminMode) return
                  if (!isPermitted(store.getState())) return
                  let newText = evt.target.value
                  let newSections = produce(sections, (draft) => {
                    draft[index].text = newText
                  })
                  setSections(newSections)
                }}
              />
              {adminMode && <div style={{ whiteSpace: "nowrap" }}>{buttonControls}</div>}
            </div>
          )
        }

        let starting = section.startingText

        if (section.type === "write") {
          let inner = undefined
          if (!adminMode) {
            let contents = section.userTyped || ""
            inner = (
              <MyTextArea
                style={{
                  ...textAreaStyle,
                  backgroundColor: colorEditCode,
                }}
                value={isTutorialExercise ? starting : contents}
                rows={numRows}
                onChange={(evt) => {
                  if (isTutorialExercise) return
                  let newText = evt.target.value

                  let lines = newText.split("\n")
                  let expectedLines = section.text.split("\n")
                  lines = lines.slice(0, expectedLines.length)

                  let newSections = produce(sections, (draft) => {
                    draft[index].userTyped = lines.join("\n")
                  })

                  setSections(newSections)
                }}
              />
            )
          } else {
            inner = (
              <>
                <MyTextArea
                  style={{
                    ...textAreaStyle,
                    backgroundColor: colorEditCode,
                  }}
                  value={expected}
                  rows={numRows}
                  onChange={(evt) => {
                    if (!isPermitted(store.getState())) return

                    let newText = evt.target.value
                    let newSections = produce(sections, (draft) => {
                      draft[index].text = newText
                    })

                    setSections(newSections)
                  }}
                />
                <MyTextArea
                  style={{
                    ...textAreaStyle,
                    backgroundColor: colorStartingCode,
                  }}
                  value={starting}
                  rows={numRows}
                  onChange={(evt) => {
                    if (!isPermitted(store.getState())) return
                    let newText = evt.target.value
                    let newSections = produce(sections, (draft) => {
                      draft[index].startingText = newText
                    })
                    setSections(newSections)
                  }}
                />
                <div style={{ whiteSpace: "nowrap" }}>{buttonControls}</div>
              </>
            )
          }

          return <div style={{ display: "flex" }}>{inner}</div>
        }
        return <div>Invalid section, contact Jacques to tell him.</div>
      })}
    </div>
  )
  let hasParseError = parseError !== undefined
  return (
    <div key={identifier}>
      {adminMode && <div>{createBeforeButton(0)}</div>}
      <div style={{ fontSize: "14px", display: "flex" }}>
        <div>{breakpointGutterElements}</div>
        <div key={identifier + "-innerviewcontainer"} style={{ position: "relative", left: "3px" }}>
          <div
            style={{
              position: "absolute",
              top: "0px",
              left: "0px",
              pointerEvents: "none",
            }}
          >
            <HighlightLineNumber
              adminMode={adminMode}
              key={"highli"}
              identifier={"highlightnum"}
              sourceCode={sections}
              parseError={parseError}
            />
          </div>
          <div
            key={identifier + "-innerview"}
            style={{
              opacity: animationView || hasParseError ? 0 : 1,
              transition: "opacity 1s",
              position: "absolute",
              left: "0px",
              top: "0px",
            }}
          >
            {innerView}
          </div>
          {animationView && (
            <div style={{ position: "absolute", left: "0px", top: "0px" }}>{animationView}</div>
          )}
        </div>
      </div>
    </div>
  )
}
