import { getDecorationsForID, populateDecorationsRef } from "./populate-decorations-ref"

import { useSelector } from "react-redux"
import { useActiveSegments } from "./useActiveSegments"
import React from "react"
import { tutorialFocusInfo } from "./useTutorialFocus"
import { dispatch } from "./store"
import { tutorialLoopIsPlaying } from "./loops.js"
import { store } from "./store"
let isAdmin = () => {
  return store.getState().adminMode
}

export let tutorialOnClick = (idInInterface, onClick, alwaysClickable) => {
  return () => {
    let { focusedEventDetails, focusedSegmentDetails, segmentTool, elementId } = tutorialFocusInfo()
    if (segmentTool && idInInterface) {
      console.log("toggling")
      dispatch({
        type: "toggle-decoration-to-segment",
        idInInterface,
        segmentId: elementId,
        tool: segmentTool,
      })
      return
    }

    if (focusedEventDetails?.type === "click") {
      dispatch({
        type: "click-event-target",
        idInInterface,
      })
      return
    }
    if (onClick && tutorialLoopIsPlaying() && !isAdmin() && !alwaysClickable) {
      alert("You must pause the tutorial before you can interact with this page.")
      return
    }
    if (onClick) onClick()
  }
}

export function TutorialButton({ onClick, children, idInInterface, alwaysClickable, size, ...otherProps }) {
  if (!idInInterface) throw new Error("what")
  let activeSegments = useActiveSegments()
  let mouseOverClick = useSelector((s) => s.mouseOverClick)
  let mouseOverNoclick = useSelector((s) => s.mouseOverNoclick)

  let decorations = getDecorationsForID(idInInterface, activeSegments, mouseOverClick, mouseOverNoclick)
  return (
    <button
      ref={(r) => {
        populateDecorationsRef(idInInterface, r, decorations)
      }}
      style={{ fontSize: size }}
      {...otherProps}
      onClick={tutorialOnClick(idInInterface, onClick, alwaysClickable)}
    >
      {children}
    </button>
  )
}
