import { useSelector } from "react-redux"
export let useActiveSegments = () => {
  let focusedSegmentDetails = useSelector(
    (s) =>
      s.currentView?.exercise &&
      s.content.tutorialDescriptions?.[s.currentView.exercise] &&
      s.tutorialFocus?.type === "segment" &&
      s.content.segmentDescriptions[s.tutorialFocus.id]
  )
  let stateActiveSegments = useSelector((s) => s.activeSegments) || []
  return [...stateActiveSegments, ...(focusedSegmentDetails ? [focusedSegmentDetails] : [])]
}
