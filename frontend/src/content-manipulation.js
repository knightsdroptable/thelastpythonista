import { v4 as uuidv4 } from "uuid"
import * as uniqid from "uniqid"
import produce from "immer"
import { Shift_exprContext } from "python-ast"

let swap = (arr, i, j) => {
  let t = arr[i]
  arr[i] = arr[j]
  arr[j] = t
}

let genExerciseId = () => {
  return uniqid()
}

let genChapterId = () => {
  return uniqid()
}

export let findExercise = (content, exerciseId) => {
  let ret = content.exercises[exerciseId]
  if (!ret) throw new Error("could not find exercise")
}

export let moveExerciseUp = (content, exerciseId) => {
  let chapterIds = content.chapterOrder
  return produce(content, (content) => {
    for (let i = 0; i < chapterIds.length; i++) {
      let chapterId = chapterIds[i]
      let chapter = content.chapters[chapterId]
      let exercises = chapter.exerciseOrder
      for (let j = 0; j < exercises.length; j++) {
        let ex = exercises[j]
        if (ex === exerciseId) {
          if (j !== 0) {
            swap(exercises, j, j - 1)
          } else {
            if (i === 0) return
            exercises.shift()
            let previousChapterId = chapterIds[i - 1]
            let previousChapter = content.chapters[previousChapterId]
            previousChapter.exerciseOrder.push(ex)
          }
          return
        }
      }
    }
  })
}

export let moveExerciseDown = (content, exerciseId) => {
  let chapterIds = content.chapterOrder
  let ret = produce(content, (content) => {
    for (let i = 0; i < chapterIds.length; i++) {
      let chapterId = chapterIds[i]
      let chapter = content.chapters[chapterId]
      let exercises = chapter.exerciseOrder
      for (let j = 0; j < exercises.length; j++) {
        let ex = exercises[j]
        if (ex === exerciseId) {
          if (j !== exercises.length - 1) {
            swap(exercises, j, j + 1)
          } else {
            if (i === content.chapterOrder.length - 1) return
            exercises.pop()
            let previousChapterId = chapterIds[i + 1]
            let previousChapter = content.chapters[previousChapterId]

            previousChapter.exerciseOrder.unshift(ex)
          }
          return
        }
      }
    }
  })
  debugger
  return ret
}

export let deleteExercise = (content, exerciseId) => {
  let chapterIds = Object.keys(content.chapters)
  return produce(content, (content) => {
    delete content.exercises[exerciseId]
    for (let i = 0; i < chapterIds.length; i++) {
      let chapterId = chapterIds[i]
      let chapter = content.chapters[chapterId]
      chapter.exerciseOrder = chapter.exerciseOrder.filter((x) => x !== exerciseId)
    }
  })
}

// mark mark
export let createNewExercise = () => {
  debugger
  return {
    name: "some exercise",
    id: genExerciseId(),
    textSections: [{ type: "read", text: "from turtle import *\nforward(100)" }],
    restrictions: ["token-quota"],
  }
}

export let createNewChapter = () => {
  return { name: "some chapter", id: genChapterId(), exerciseOrder: [] }
}

export let addExerciseAfter = (content, exerciseId) => {
  let chapterIds = Object.keys(content.chapters)
  let newExercise = createNewExercise()
  return produce(content, (content) => {
    content.exercises[newExercise.id] = newExercise
    for (let i = 0; i < chapterIds.length; i++) {
      let chapterId = chapterIds[i]
      let chapter = content.chapters[chapterId]
      let exercises = chapter.exerciseOrder
      for (let j = 0; j < exercises.length; j++) {
        let ex = exercises[j]
        if (ex === exerciseId) {
          exercises.splice(j + 1, 0, newExercise.id)
          return
        }
      }
    }
  })
}

export let setExerciseName = (content, exerciseId, name) => {
  return produce(content, (content) => {
    content.exercises[exerciseId].name = name
  })
}

export let setTextSectionsOfExercise = (content, exerciseId, textSections) => {
  if (!content.exercises[exerciseId]) debugger
  return produce(content, (content) => {
    content.exercises[exerciseId].textSections = textSections
  })
}

export let setExerciseData = (content, exerciseId, exerciseData) => {
  return produce(content, (content) => {
    content.exercises[exerciseId].textSections = exerciseData.textSections
    content.exercises[exerciseId].restrictions = exerciseData.restrictions
  })
}

export let moveChapterUp = (content, chapterId) => {
  return produce(content, (content) => {
    let chapterOrder = content.chapterOrder
    for (let i = 1; i < chapterOrder.length; i++) {
      let cid = chapterOrder[i]
      if (cid === chapterId) {
        swap(chapterOrder, i, i - 1)
        return
      }
    }
  })
}

export let moveChapterDown = (content, chapterId) => {
  return produce(content, (content) => {
    let chapterOrder = content.chapterOrder
    for (let i = 0; i < chapterOrder.length - 1; i++) {
      let cid = chapterOrder[i]
      if (cid === chapterId) {
        swap(chapterOrder, i, i + 1)
        return
      }
    }
  })
}

export let deleteChapter = (content, chapterId) => {
  return produce(content, (content) => {
    delete content.chapters[chapterId]
    content.chapterOrder = content.chapterOrder.filter((x) => x !== chapterId)
  })
}

export let addChapterAfter = (content, chapterId) => {
  let newchapter = createNewChapter()
  return produce(content, (content) => {
    content.chapters[newchapter.id] = newchapter
    let chapterOrder = content.chapterOrder
    for (let i = 0; i < chapterOrder.length; i++) {
      let cid = chapterOrder[i]
      if (cid === chapterId) {
        chapterOrder.splice(i + 1, 0, newchapter.id)
        return
      }
    }
  })
}

export let addChapterAtTop = (content) => {
  let newchapter = createNewChapter()
  return produce(content, (content) => {
    content.chapters[newchapter.id] = newchapter
    content.chapterOrder.unshift(newchapter.id)
  })
}

export let setChapterName = (content, chapterId, name) => {
  return produce(content, (content) => {
    content.chapters[chapterId].name = name
  })
}

export let addExerciseAtTop = (content, chapterId) => {
  let newExercise = createNewExercise()
  return produce(content, (content) => {
    content.exercises[newExercise.id] = newExercise
    let chapter = content.chapters[chapterId]
    chapter.exerciseOrder.unshift(newExercise.id)
  })
}
