import React, { useState, useRef, useEffect } from "react"
import produce from "immer"
export default function Grower({ chs, shown }) {
  const [state, setState] = useState({
    calculatedHeight: 0,
    calculatedOnce: false,
  })
  let innerRef = useRef(undefined)
  useEffect(() => {
    let height = innerRef.current.offsetHeight
    setState((state) =>
      produce(state, (state) => {
        state.calculatedHeight = height
        state.calculatedOnce = true
      })
    )
  }, [])
  return (
    <div
      style={{
        overflowY: "hidden",
        overflowX: "hidden",
        height: !state.calculatedOnce ? undefined : shown ? state.calculatedHeight : 0,
        transition: `height ${shown ? 0.8 : 0.5}s`,
        boxSizing: "border-box",
      }}
    >
      <div ref={innerRef}>{chs}</div>
    </div>
  )
}
