function isIterator(obj) {
  if (!obj) {
    return false
  }
  return typeof obj[Symbol.iterator] === "function" && "next" in obj
}

export let drive = (it, cb, current) => {
  current = current || { canceled: false }
  let done = false
  let cancel = () => {
    current.canceled = true
  }

  let driver = async () => {
    let r = undefined
    while (!current.canceled) {
      let y = it.next(r)
      if (y.done || current.canceled) {
        // it was canceled or the iterator has finished
        done = true
        if (cb) cb()
        return
      }
      let v = y.value
      if (v instanceof Promise) {
        r = await v
      } else if (isIterator(v)) {
        await new Promise((res) => {
          drive(v, res, current)
        })
        r = undefined
      } else {
        r = v
      }
    }
    // it was canceled
    done = true
    if (cb) cb()
  }

  driver()
  return { cancel, isDone: () => done, wasCanceled: () => current.canceled }
}
