import React, { useState, useRef, useEffect } from "react"
import produce from "immer"

export default function SwitchBetween({ a, b, tog, someid }) {
  const [state, setState] = useState({
    mode: tog ? "current-a" : "current-b",
    opacity: 1,
  })

  const timeoutRef = useRef(undefined)

  useEffect(() => {
    if (timeoutRef.current) {
      clearTimeout(timeoutRef.current)
      timeoutRef.current = undefined
    }
    if (
      (tog && state.mode === "current-a") ||
      (!tog && state.mode === "current-b")
    ) {
      // nothing to change
      setState((state) =>
        produce(state, (state) => {
          state.opacity = 1
        })
      )
    } else {
      setState((state) =>
        produce(state, (state) => {
          state.opacity = 0
        })
      )
      timeoutRef.current = setTimeout(() => {
        setState((state) =>
          produce(state, (state) => {
            state.opacity = 1
            state.mode = tog ? "current-a" : "current-b"
          })
        )
      }, 250)
    }
    return () => {
      if (timeoutRef.current) {
        clearTimeout(timeoutRef.current)
        timeoutRef.current = undefined
      }
    }
  }, [tog])
  return (
    <div
      style={{
        opacity: state.opacity,
        transition: "opacity 0.25s",
        display: "flex",
        alignItems: "stretch",
      }}
    >
      {state.mode === "current-a" ? a : b}
    </div>
  )
}
