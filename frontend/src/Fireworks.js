import React, { useState, useEffect } from "react"
import produce from "immer"

export default function Fireworks({ duration }) {
  const [state, setState] = useState({
    shown: true,
  })
  useEffect(() => {
    setInterval(() => {
      setState((state) =>
        produce(state, (state) => {
          state.shown = false
        })
      )
    }, 6000)
  }, [])
  if (!state.shown) return null
  return (
    <div
      style={{
        pointerEvents: "none",
        position: "fixed",
        left: "0px",
        top: "0px",
        right: "0px",
        bottom: "0px",
        overflow: "hidden",
        zIndex: 1000000,
      }}
    >
      <div className={"__pyro-before"}></div>
      <div className={"__pyro-after"}></div>
    </div>
  )
}
