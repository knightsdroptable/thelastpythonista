import React, { useEffect } from "react"
import { dispatch } from "./store"
import tooltipAttributes from "./tooltip-attributes"
import { useColorBackground } from "./colors"
import { TutorialButton } from "./TutorialButton"
import { store } from "./store"

export default function FadeButton({
  tooltipText,
  shown,
  minOpacity,
  onClick,
  child,
  idInInterface,
  alwaysClickable,
  size,
}) {
  let colorBackground = useColorBackground()
  minOpacity = minOpacity === undefined ? 0.2 : minOpacity
  let style = {
    cursor: "pointer",
    border: "none",
    outline: "none",
    cursor: shown ? "pointer" : "none",
    opacity: 1,
    backgroundColor: colorBackground,
  }
  if (!shown) {
    style.opacity = minOpacity
    style.pointerEvents = "none"
  }

  useEffect(() => {
    if (store.getState().adminMode) return

    return () => dispatch({ type: "__set-tooltip", tooltip: undefined })
  }, [tooltipText])

  return (
    <TutorialButton
      alwaysClickable={alwaysClickable}
      idInInterface={idInInterface}
      {...tooltipAttributes(tooltipText)}
      class="__26px-when-hover"
      style={style}
      onClick={onClick}
    >
      <div style={{ display: "flex", alignItems: "center", fontSize: size }}>{child}</div>
    </TutorialButton>
  )
}
