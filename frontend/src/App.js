import { wakeup } from "./hibernate.js"
import { startAudioRecording, playVoiceAudio } from "./tutorial-events"
import { overturemp3 } from "./globals"
import gotonextexercisemp3 from "./gotonextexercise.mp3"
import tooltipAttributes from "./tooltip-attributes"
import { ImArrowUp2 } from "react-icons/im"
import { stopLoop, setRunLoop, runLoopIsPlaying, tutorialLoopIsPlaying } from "./loops"
import { collectTokens } from "./collect-tokens"
import { getMultiplierOfSpeed } from "./speed.js"
import * as R from "ramda"
import * as flatten from "flatten"
import TutorialDescription from "./TutorialDescription"
import { getAudio, playAudio, stopAudio, playAudioUntilFinished } from "./playaudio.js"
import { useColorBackground, colorModalBlanket, colorBlack, colorError, colorDarkened } from "./colors"
import { drive } from "./drive"
import testAudioPath from "./misc-sounds/TURTLE_FORWARD.mp3"
import { pathPrefix, sessionId, mouseMoveDefaultTime, cheeraudio } from "./globals"
import { GiTurtle } from "react-icons/gi"
import SwitchBetweenMany from "./SwitchBetweenMany"
import SwitchHighlight from "./SwitchHighlight"
import IncrementNumCompleted from "./IncrementNumCompleted"
import GrowableText from "./GrowableText"
import FadeButton from "./FadeButton"
import Fireworks from "./Fireworks"
import VideoHistory from "./VideoHistory"
import { codeFont, hardcodedVideos } from "./globals.js"

import mouseOverNoclickImage from "./mouse-over-noclick.png"
import mouseOverClickImage from "./mouse-over-clicked.png"
import {
  getAllDecorations,
  getDecorationsForID,
  populateDecorationsRef,
  getDomNodeFromIdInInterface,
  assocRedraw,
  clearIdInInterfaceDomNodeAssociation,
} from "./populate-decorations-ref"
import { userPlayTutorialButton, userTutorialFastForward, userTutorialRewind } from "./tutorial-events"
import {
  BiChevronsRight,
  BiChevronsLeft,
  BiVolumeFull,
  BiVolumeMute,
  BiPlay,
  BiStop,
  BiRewind,
  BiFastForward,
  BiSkipNext,
  BiSkipNextCircle,
  BiPause,
  BiCaptions,
} from "react-icons/bi"

// import { SiBbciplayer } from "react-icons/si"
// import { FiSkipForward } from "react-icons/fi"
import { GrRun } from "react-icons/gr"
// import { IoPlayBackOutline } from "react-icons/io5"
// import { IoPlayForwardOutline } from "react-icons/io5"
// import { MdPauseCircleOutline } from "react-icons/md"
// import { FaRegStopCircle } from "react-icons/fa"
import YoutubePopup from "./YoutubePopup"
import copyToClipboard from "copy-to-clipboard"
import "./App.css"
import React, { useEffect, useState, useRef, createRef } from "react"
import { animRef, animStyle, refresh, DrawLines, getTimeDelta, abruptStop, GrayLines, setNodeSpeeds } from "./anim.js"
import lineMarkerPNG from "./whitearrow.png"
import curvedArrowTopLeft from "./curved-arrow-orange-top-left.png"
import curvedArrowBottomLeft from "./curved-arrow-orange-bottom-left.png"
import curvedArrowBottomRight from "./curved-arrow-orange-bottom-right.png"
import curvedArrowTopRight from "./curved-arrow-orange-top-right.png"
import produce from "immer"
import MultiSection from "./Multisection"
import { useSelector } from "react-redux"
import { dispatch, possibleRestrictions, store } from "./store"
import ContentNavigationMenu from "./ContentNavigationMenu"
import { getResourceURL } from "./resources"
import { tutorialOnClick } from "./TutorialButton"

let isPunctuation = (c) => {
  return [".", "?", "!", ","].includes(c)
}

let punctualize = (str) => {
  if (!str) return str
  if (!str.trim) debugger
  str = str.trim()
  if (str.length === 0) return str
  let lastChar = str[str.length - 1]
  if (isPunctuation(lastChar)) return str
  return str + "."
}

const isAscii = (str) => /^[\u0000-\u007f]*$/.test(str)

let increaseSpeed = () => {
  dispatch({ type: "increase-speed" })
  let currentSpeed = store.getState().currentSpeed
  setNodeSpeeds()
  window.localStorage.setItem("currentSpeed", JSON.stringify(currentSpeed))
}

let decreaseSpeed = () => {
  dispatch({ type: "decrease-speed" })
  let currentSpeed = store.getState().currentSpeed
  setNodeSpeeds()
  window.localStorage.setItem("currentSpeed", JSON.stringify(currentSpeed))
}

let getUserData = (label) => {
  try {
    return JSON.parse(window.localStorage.getItem(`${sessionId}-userdata-${label}`))
  } catch (err) {
    return null
  }
}

let setUserData = (label, data) => {
  window.localStorage.setItem(`${sessionId}-userdata-${label}`, JSON.stringify(data))
}

let hardcodedVideoButton = (id, text) => {
  if (!hardcodedVideos[id]) return null
  return (
    <button
      onClick={() => {
        dispatch({ type: "show-hardcoded-video", id: id })
      }}
      className="flatbutton"
    >
      {text}
    </button>
  )
}

function Decorations() {
  //let decorations = useSelector((s) => s.pointDecorations)
  let stateCurrentSpeed = useSelector((s) => s.currentSpeed)
  let stateActiveSegments = useSelector((s) => s.activeSegments) || []
  let focusedSegmentDetails = useSelector(
    (s) =>
      s.currentView?.exercise &&
      s.content.tutorialDescriptions?.[s.currentView.exercise] &&
      s.tutorialFocus?.type === "segment" &&
      s.content.segmentDescriptions[s.tutorialFocus.id]
  )
  let activeSegments = [...stateActiveSegments, ...(focusedSegmentDetails ? [focusedSegmentDetails] : [])]
  let mouseOverClick = useSelector((s) => s.mouseOverClick)
  let mouseOverNoclick = useSelector((s) => s.mouseOverNoclick)
  useSelector((s) => s.assocCounter) // don't delete this
  //let assoc = useSelector((s) => s.idInInterfaceDomnodeAssociation)

  let decorations = getAllDecorations(activeSegments, mouseOverClick, mouseOverNoclick)

  let animationFromQuadrant = (x, y) => {
    if (isNaN(x) || isNaN(y)) throw new Error("huh")
    if (x < 300 && y < 300) return "move-from-bottom-right"
    if (x > 300 && y > 300) return "move-from-top-left"
    if (x < 300 && y > 300) return "move-from-top-right"
    return "move-from-bottom-left"
  }
  let seen = new Set()
  decorations.forEach((d) => {
    let idInInterface = d.idInInterface
    let decorationType = d.type
    if (seen.has(idInInterface + decorationType)) debugger
    seen.add(idInInterface + decorationType)
  })
  //if (decorations.length > 0) debugger
  return (
    <>
      {decorations.map((d) => {
        //if (!assoc) throw new Error("missing assoc")
        //if (!assoc) return null
        let idInInterface = d.idInInterface
        if (!idInInterface) throw new Error("missing id")
        let domNode = getDomNodeFromIdInInterface(idInInterface)
        // if (!domNode) throw new Error("No DOM node association")
        if (!domNode) return null
        if (!document.contains(domNode)) new Error("decorated DOM node not in document")
        let decorationType = d.type

        let boundingRect = domNode.getBoundingClientRect()
        // top and left are relative to the viewport

        // x and y are relative to the top left of the page

        if (!idInInterface || !decorationType) debugger
        let createImg = (w, h, x, y, src, keyframes, ang, animationDuration) => {
          let getTr = () => {
            if (ang !== undefined) return `rotate(${ang}deg)`
            return undefined
          }

          return (
            <div
              key={idInInterface + decorationType}
              style={{
                position: "absolute",
                top: "0px",
                left: "0px",
                overflow: "hidden",
                width: `${x + 200}px`,
                height: `${y + 200}px`,
                overflow: "hidden",
                // height: "100px",
                // right: "0px",
                // bottom: "0px",
                // minHeight: "100%",
                // minWidth: "100%",
                pointerEvents: "none",
                boxSizing: "border-box",
                zIndex: 129308,
              }}
            >
              <div
                style={{
                  zIndex: 999999,
                  position: "absolute",
                  left: x + "px",
                  top: y + "px",
                  width: "0px",
                  // transition: "left 1s, top 1s",
                }}
              >
                <img
                  src={src}
                  style={{
                    animationName: keyframes,
                    //transform: flipVertically && "scaleY(-1)",
                    height: h + "px",
                    width: w + "px",
                    transform: getTr(),
                    position: "relative",
                    animationDuration: `${animationDuration}s`,
                  }}
                />
              </div>
            </div>
          )
        }
        let topY = boundingRect.top + window.scrollY
        let leftX = boundingRect.left + window.scrollX
        let botY = boundingRect.bottom + window.scrollY
        let rightX = boundingRect.right + window.scrollX
        let elementWidth = boundingRect.right - boundingRect.left
        let elementHeight = boundingRect.bottom - boundingRect.top
        if (decorationType === "point") {
          let getParams = () => {
            let lx = leftX - 51
            let rx = rightX + 10
            let ty = topY - 33
            let by = botY
            if (topY < 300 && leftX < 300) {
              return { x: rx - elementWidth / 2, y: by, src: curvedArrowBottomRight }
            }
            if (topY < 300 && leftX > 300) {
              return { x: lx + elementWidth / 2 + 13, y: by, src: curvedArrowBottomLeft }
            }
            if (topY > 300 && leftX < 300) {
              return { x: rx, y: ty + elementHeight / 2, src: curvedArrowTopRight }
            }

            return { x: lx, y: ty + elementHeight / 2, src: curvedArrowTopLeft }
          }

          let params = getParams()
          console.log(params)
          return createImg(50, 50, params.x, params.y, params.src, "shake", 0, 0.6 / getMultiplierOfSpeed(stateCurrentSpeed))
        }

        let sf = 6
        let mousewidth = 512 / sf
        let mouseheight = 320 / sf
        let mousedx = -mousewidth / 2 + elementWidth / 2
        let mousedy = -mouseheight / 6 + elementHeight / 2
        if (decorationType === "mouse-over-click") {
          return createImg(
            mousewidth,
            mouseheight,
            leftX + mousedx,
            topY + mousedy,
            mouseOverClickImage,
            undefined,
            15,
            mouseMoveDefaultTime / 1000 / getMultiplierOfSpeed(stateCurrentSpeed)
          )
        } else if (decorationType === "mouse-over-noclick") {
          console.log("keyframes", animationFromQuadrant(leftX, topY))
          return createImg(
            mousewidth,
            mouseheight,
            leftX + mousedx,
            topY + mousedy,
            mouseOverNoclickImage,
            animationFromQuadrant(leftX, topY),
            15,
            mouseMoveDefaultTime / 1000 / getMultiplierOfSpeed(stateCurrentSpeed)
          )
        } else if (["fill-orange", "fill-white"].includes(decorationType)) {
        } else {
          console.log("*****************unrecognized decoration type: " + decorationType)
        }
      })}
    </>
  )
}

function ChapterVideo({ chapterId, clickNextElement }) {
  let stateNavigateContent = useSelector((s) => s.navigateContent)
  let content = useSelector((s) => s.content)
  let adminMode = useSelector((s) => s.adminMode)

  let chapter = content.chapters[chapterId]
  if (!chapter) debugger
  let youtubeID = chapter.youtubeID
  let adminView = undefined
  if (adminMode) {
    adminView = (
      <>
        <div>
          <span>Video url </span>
          <input
            value={youtubeID || ""}
            onChange={(evt) => {
              dispatch({ type: "set-chapter-video-id", id: evt.target.value })
            }}
            type="text"
          />
        </div>
      </>
    )
  }

  return (
    <>
      {stateNavigateContent && <ContentNavigationMenu />}
      {adminView}

      <YoutubePopup
        noIdChild={"The video for this chapter is not ready yet"}
        youtubeID={youtubeID}
        cb={clickNextElement}
        below={"Let's go!"}
        above={"Watch this video and then click on the button below the video."}
      />
    </>
  )
}

function SaveProgress() {
  let colorBackground = useColorBackground()

  return (
    <div
      style={{
        position: "fixed",
        top: "0px",
        left: "0px",
        right: "0px",
        bottom: "0px",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        // backgroundColor: colorModalBlanket,
        zIndex: 29308,
      }}
    >
      <div
        style={{
          padding: "30px",
          backgroundColor: colorBackground,
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          width: "600px",
          boxShadow: "0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23)",
          position: "relative",
        }}
      >
        <div
          style={{ top: "5px", right: "8px", position: "absolute", cursor: "pointer" }}
          {...tutorialAttr(false, "close-save-progress", () => {
            debugger
            dispatch({ type: "close-save-progress-window" })
          })}
        >
          X
        </div>
        <div style={{ fontSize: "18px", letterSpacing: "1px" }}>Save Progress</div>
        <p>
          This platform associates your progress with the following unique and permanent link that you can use to restore your progress:
        </p>
        <div style={{ textAlign: "center" }}>
          {pathPrefix}?session={sessionId}
        </div>

        <button
          style={{ marginLeft: "5px", marginTop: "5px", width: "100px" }}
          onClick={() => {
            copyToClipboard(`${pathPrefix}?session=${sessionId}`)
          }}
        >
          copy link
        </button>

        {/* <p>You can also just bookmark this page.</p> */}
        {/* <p>Do you want to receive an email with that link?</p>
        <div>
          <input
            value={state.email}
            type="text"
            onChange={(evt) => {
              let text = evt.target.value
              setState((state) =>
                produce(state, (state) => {
                  state.email = text
                })
              )
            }}
          />
          <button
            onClick={() => {
              fetch("https://wakata.io/apiv2/email-user-persistent-link", {
                method: "POST",
                body: JSON.stringify({
                  link: `${pathPrefix}/session/${sessionId}`,
                  email: state.email,
                }),
              })
              alert("Email sent")
            }}
          >
            Send me that link
          </button>
        </div> */}
        {/* <p>Your email is not stored anywhere and you will never be contacted.</p> */}
      </div>
    </div>
  )
}

let tutorialAttr = (alwaysClickable, idInInterface, onClick) => {
  return {
    ref: (r) => populateDecorationsRef(idInInterface, r),
    onClick: tutorialOnClick(idInInterface, onClick, alwaysClickable),
  }
}

function BugReport({ onClose }) {
  let colorBackground = useColorBackground()
  const [state, setState] = useState({
    email: "",
    description: "",
  })

  let currentExercise = useSelector((s) => s.currentView && s.currentView.exercise && s.content.exercises[s.currentView.exercise])

  let view = useSelector((s) => s.currentView)
  let onSubmit = (evt) => {
    fetch("https://wakata.io/apiv2/python-foundations-bug-report", {
      method: "POST",
      body: JSON.stringify({
        uid: sessionId,
        email: state.email,
        description: state.description,
        textSections: currentExercise && currentExercise.textSections,
        view,
      }),
    })
    evt.preventDefault()
    dispatch({
      type: "close-bug-report-window",
    })
  }
  return (
    <div
      style={{
        position: "fixed",
        top: "0px",
        left: "0px",
        right: "0px",
        bottom: "0px",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: colorModalBlanket,
        zIndex: 2930874501923847,
      }}
    >
      <div
        style={{
          padding: "30px",
          backgroundColor: colorBackground,
          position: "relative",
          boxShadow: "0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23)",
        }}
      >
        <button
          style={{
            position: "absolute",
            top: "5px",
            right: "5px",
            backgroundColor: "rgba(0,0,0,0)",
            border: "none",
            fontSize: "19px",
          }}
          onClick={onClose}
        >
          X
        </button>

        <form onSubmit={onSubmit}>
          <div>
            Your email (optional):
            <input
              value={state.email}
              onChange={(evt) => {
                let text = evt.target.value
                setState((state) =>
                  produce(state, (state) => {
                    state.email = text
                  })
                )
              }}
              type="text"
            />
          </div>
          <div>Description</div>
          <div>
            <textarea
              style={{ border: "1px solid black", padding: "2px" }}
              rows={20}
              cols={40}
              value={state.description}
              onChange={(evt) => {
                let text = evt.target.value
                setState((state) =>
                  produce(state, (state) => {
                    state.description = text
                  })
                )
              }}
            />
          </div>
          <button>Send bug report</button>
        </form>
      </div>
    </div>
  )
}

function SlowlyChangeText({ idInInterface, text }) {
  const [state, setState] = useState({
    opacity: 0,
    textShown: "",
  })
  useEffect(() => {
    if (state.opacity !== 0) {
      setState((state) =>
        produce(state, (state) => {
          state.opacity = 0
        })
      )
      setTimeout(() => {
        setState((state) =>
          produce(state, (state) => {
            state.opacity = 1
            state.textShown = text
          })
        )
      }, 500)
    } else {
      setState((state) =>
        produce(state, (state) => {
          state.opacity = 1
          state.textShown = text
        })
      )
    }
  }, [text])
  return (
    <span {...tutorialAttr(false, idInInterface)} style={{ transition: "opacity 0.4s", opacity: state.opacity }}>
      {state.textShown}
    </span>
  )
}

function Tooltip({ data }) {
  let colorBackground = useColorBackground()
  const [state, setState] = useState({
    opacity: 0,
    shownData: { x: 0, y: 0, text: "" },
  })
  useEffect(() => {
    if (!data && state.opacity !== 0) {
      setState((state) =>
        produce(state, (state) => {
          state.opacity = 0
        })
      )
      return
    }
    if (data) {
      setState((state) =>
        produce(state, (state) => {
          state.opacity = 1
          state.shownData = data
        })
      )
    }
  }, [data])

  let inner = state.shownData.text
  if (!Array.isArray(inner)) inner = [inner]
  let height = 14 + inner.length * 14
  let innerView = inner.map((x) => <div>{x}</div>)
  return (
    <div
      //className="fadeInTooltip"
      //className={comcss["fadeInTooltip"]}
      style={{
        opacity: state.opacity,
        transition: "opacity 0.5s, left 0.2s, top 0.2s",
        backgroundColor: colorBackground,
        padding: "7px",
        border: "1px solid black",
        position: "fixed",
        left: state.shownData.x + 5 + "px",
        top: state.shownData.y - height + "px",
        zIndex: 1000000,
        pointerEvents: "none",
        fontSize: "10px",
      }}
    >
      {innerView}
    </div>
  )
}

let annotateTokens = (userTokens) => {
  let sortedUserTokens = [...userTokens]
  sortedUserTokens.sort((a, b) => {
    return a.lineNumber - b.lineNumber
  })
  let annotatedTokens = ""
  let currentLine = sortedUserTokens[0].lineNumber
  let tokenNum = 1
  for (let i = 0; i < sortedUserTokens.length; i++) {
    let token = sortedUserTokens[i]
    while (currentLine < token.lineNumber) {
      annotatedTokens += "\n"
      currentLine++
    }
    annotatedTokens += `[${tokenNum}]${token.text}`
    tokenNum++
  }
  return annotatedTokens
}

let annotateTokensSuperscript = (userTokens) => {
  let sortedUserTokens = [...userTokens]
  sortedUserTokens.sort((a, b) => {
    return a.lineNumber - b.lineNumber
  })
  let annotatedTokens = []
  let currentLine = sortedUserTokens[0].lineNumber
  let tokenNum = 1
  for (let i = 0; i < sortedUserTokens.length; i++) {
    let token = sortedUserTokens[i]
    while (currentLine < token.lineNumber) {
      annotatedTokens.push("\n")
      currentLine++
    }
    if (token.text.trim().length === 0) {
      annotatedTokens.push(token.text)
    } else {
      annotatedTokens.push(<sup style={{ fontSize: "10px" }}>{tokenNum}</sup>)
      annotatedTokens.push(token.text + " ")
      tokenNum++
    }
  }
  return annotatedTokens
}

let stopSymbol = <BiStop />
//<em style={{ fontSize: "18px", paddingLeft: "8px", opacity: 0.9 }}>c</em>
let playSymbol = <BiPlay />

function LabeledCheckbox({ onChange, checked, label }) {
  return (
    <div>
      <input
        checked={checked}
        onChange={(evt) => {
          onChange(evt.target.checked)
        }}
        type="checkbox"
      />
      <span> {label}</span>
    </div>
  )
}

let tableStyle = {
  borderSpacing: "0px",
  borderCollapse: "collapse",
  ...animStyle,
}

//let totalNumberExercises = calculateTotalNumberExercises(originalContent)

let videoSeenData = []

let wasVideoSeen = (id) => {
  return videoSeenData.includes(id)
}
let addToSeenVideos = (id) => {
  if (!videoSeenData.includes(id)) videoSeenData.push(id)
}

let preStyle = { margin: "0px" }

let transpose = (m) => m[0].map((x, i) => m.map((x) => x[i]))

let intersperse = (arr, elems) => {
  let ret = []
  for (let i = 0; i < arr.length; i++) {
    ret.push(arr[i])
    if (i !== arr.length - 1) ret.push(elems[i])
  }
  return ret
}

let startingTextSections = [
  {
    type: "read",
    text: `from simpleturtle import *`,
  },
  {
    type: "write",
    text: `circle(30)
penup()
forward(20)
right(90)
pendown()
forward(20)
left(90)
forward(20)
penup()
circle(20)

`,
  },
]

let YoutubeExerciseVideoInput = ({ txt, propname }) => {
  let content = useSelector((s) => s.content)
  let currentView = useSelector((s) => s.currentView)
  let youtubeID = content.exercises[currentView.exercise][propname] || ""
  return (
    <div>
      <span>{txt} </span>
      <input
        value={youtubeID}
        onChange={(evt) => {
          dispatch({
            type: "set-exercise-video",
            interaction: propname,
            youtubeID: evt.target.value,
          })
        }}
        type="text"
      />
    </div>
  )
}

// let testTokenCounter = () => {
//   let testSections = [
//     {
//       type: "read",
//       text: "from turtle import *",
//     },
//     {
//       type: "write",
//       text: "forward(123512512)",
//       userTyped: "forward(50)\nx = 10\ny=20\nz=30",
//     },
//     {
//       type: "write",
//       text: "forward(234512351)",
//       userTyped: "forward(50)\nx = 10\ny=20\nz=30",
//     },
//     {
//       type: "read",
//       text: "from turtle import *",
//     },
//   ]
//   let solutionSource = textOfSections(testSections)
//   let solutionParseResult = parsePython(solutionSource)
//   let userSource = userTextOfSections(testSections)
//   let userParseResult = parsePython(userSource)
//   let utokens = userParseResult.tokens
//   let userWrittenLines = userInputtedLines(testSections)
//   let userSpecificTokens = extractUserTokens(userWrittenLines, utokens)
//   let annotated = annotateTokens(userSpecificTokens)
//   console.log("annotated", annotated)
// }
// testTokenCounter()

//`import turtle`

// let sourceCode = `a = 10
// b = 10 + a
// c = a + b * 2 + a`

//animationFrames = animationFrames.slice(15, 17)
//console.log(unparse(parsePython("x = 3")))

let arr = ["a", "b", "c", "d"]
let shift = (arr, num) => {
  arr = [...arr]
  for (let i = 0; i < num; i++) {
    arr.push(arr.shift())
  }
  return arr
}

let range = (x) => {
  let ret = []
  for (let i = 0; i < x; i++) {
    ret.push(i)
  }
  return ret
}

let numRows = 2
let numCols = 2

let world = []
for (let i = 0; i < numRows; i++) {
  for (let j = 0; j < numCols; j++) {
    world.push("" + (j + i * numCols))
  }
}

function shuffle(array) {
  var currentIndex = array.length,
    temporaryValue,
    randomIndex

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {
    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex)
    currentIndex -= 1

    // And swap it with the current element.
    temporaryValue = array[currentIndex]
    array[currentIndex] = array[randomIndex]
    array[randomIndex] = temporaryValue
  }

  return array
}

// let mkFuncdefMarker = fdm => {

//   return <span
//     style={{ border: "1px solid black", ...animStyle }}
//     class="flintstones"
//     key={fdm.token.id}
//     ref={r => animRef(r, fdm.token.id, { copyText: true, relative: fdm.relativeTo, left: -80 })} >{fdm.token.text}</span>
// }

let mkBox = (id, child, extraCSS) => {
  return (
    <div style={{ border: "1px solid black", ...extraCSS, ...animStyle }} key={id} ref={(r) => animRef(r, id)}>
      {child}
    </div>
  )
}

let mkDashedBox = (id, child, labelView) => {
  return (
    <>
      <div
        style={{
          border: "1px dashed black",
          padding: "5px",
          borderRadius: "4px",
          ...animStyle,
        }}
        key={id}
        ref={(r) => animRef(r, id)}
      >
        {labelView}
        {child}
      </div>
    </>
  )
}

let mkMonotd = (id, child, extraCSS, span) => {
  return (
    <td
      colSpan={span}
      style={{
        padding: "2px",
        whiteSpace: "nowrap",
        border: "0.5px solid #444",
        ...codeFont,
        ...extraCSS,
        ...animStyle,
      }}
      key={id}
      ref={(r) => animRef(r, id)}
    >
      {child}
    </td>
  )
}

let mkLineMarker = (id, relativeTo) => {
  return (
    <img
      src={lineMarkerPNG}
      style={{
        position: "absolute",
        height: "20px",
        width: "30px",
        ...animStyle,
      }}
      key={id}
      // onClick={tutorialOnClick("line-marker-arrow")}
      ref={(r) => {
        animRef(r, id, { relative: relativeTo, left: -30, top: -3 })
        // populateDecorationsRef("line-marker-arrow", r)
      }}
    />
  )
}

let lineMarkerTarget = undefined

function LineMarkerView() {
  let c = useSelector((s) => s.lineMarkerCounter) // don't delete this
  const [state, setState] = useState({
    lastX: 0,
    lastY: 0,
  })
  useEffect(() => {
    if (!lineMarkerTarget) {
      setState({
        opacity: 0,
      })
      return
    }
    let boundingRect = lineMarkerTarget.getBoundingClientRect()
    let x = boundingRect.left + window.scrollX - 30
    let y = boundingRect.top + window.scrollY
    setState((state) =>
      produce(state, (state) => {
        state.lastX = x
        state.lastY = y
        state.opacity = 1
      })
    )
  }, [c])

  return (
    <img
      src={lineMarkerPNG}
      {...tutorialAttr(false, "line-marker")}
      style={{
        position: "absolute",
        pointerEvents: !store.getState().adminMode && "none",
        height: "20px",
        width: "30px",
        transition: "left 0.5s, top 0.5s, opacity 0.2s",
        left: state.lastX - 3 + "px",
        top: state.lastY + "px",
        opacity: state.opacity,
      }}
      key="line-marker"
    />
  )
}

let tdstyle = { margin: "0px", padding: "0px" }

// let localhost =
//   window.location.hostname === "localhost" ||
//   window.location.hostname === "127.0.0.1"

let delay = (t) => new Promise((res) => setTimeout(res, t))

// let getAudioDuration = (audio) => {
//   return new Promise((res) => {
//     audio.addEventListener("loadedmetadata", (evt) => {
//       res(audio.duration)
//     })
//   })
// }

function* startPlay() {
  dispatch({ type: "clear-highlights" })
  let frameIndex = store.getState().frameIndex
  let aframes = store.getState().animationFrames
  if (!aframes) {
    return
  }

  for (; frameIndex < aframes.length; frameIndex++) {
    let animationFrame = aframes[frameIndex]
    let description = animationFrame.description
    dispatch({
      type: "set-frame-index",
      index: frameIndex,
    })
    let isNarrated = store.getState().narrationVoice
    if (isNarrated) {
      let sndFile = description.sndFile
      let audio = yield getAudio(sndFile)
      dispatch({
        type: "set-narration-text-index",
        index: frameIndex,
      })
      yield playAudioUntilFinished(audio)
      //yield delay(audio.duration * 1000 + 50)
      //stopAudio()
    }

    let delayTime = getTimeDelta() / 2
    yield delay(delayTime)
  }
}

let startPlayButtonAction = () => {
  stopLoop()
  setRunLoop(drive(startPlay()))
}

let soundInstructionsShown = false

let getSoundInstructionsShown = () => {
  return soundInstructionsShown
}

let setSoundInstructionsShown = (v) => {
  soundInstructionsShown = v
}

let useAppear = (b, dt) => {
  const [state, setState] = useState({ mode: "hidden" })
  let r = useRef(undefined)
  let cancelTimeout = () => {
    if (r.current) {
      clearTimeout(r.current)
    }
  }
  useEffect(() => {
    if (b && ["hidden", "death"].includes(state.mode)) {
      cancelTimeout()
      setState((state) =>
        produce(state, (state) => {
          state.mode = "birth"
        })
      )
      r.current = setTimeout(() => {
        setState((state) =>
          produce(state, (state) => {
            state.mode = "shown"
          })
        )
      }, dt)
    } else if (!b && ["birth", "shown"].includes(state.mode)) {
      cancelTimeout()
      setState((state) =>
        produce(state, (state) => {
          state.mode = "death"
        })
      )
      r.current = setTimeout(() => {
        setState((state) =>
          produce(state, (state) => {
            state.mode = "hidden"
          })
        )
      }, dt)
    }
  }, [b])
  return state.mode
}

function TurtleMoveList({ moves, currentIndex, shown }) {
  let mode = useAppear(shown, 1000)
  let focused = useRef(undefined)
  useEffect(() => {
    if (focused.current) {
      try {
        focused.current.scrollIntoView({ behavior: "smooth", block: "center" })
      } catch (err) {}
    }
  }, [currentIndex])
  console.log("mode", mode)
  if (mode === "hidden") return null
  return (
    <div
      className={mode === "birth" ? "width-expand-animation-to-250" : mode === "death" ? "width-retract-animation-from-250" : "width-250"}
      style={{
        overflowX: "scroll",
        height: "400px",
        borderTop: "1px solid",
        borderRight: "1px solid",
        borderBottom: "1px solid",
      }}
    >
      {moves.map((m, i) => {
        return (
          <div
            ref={(r) => {
              if (currentIndex === i) {
                focused.current = r
              }
            }}
            style={{
              whiteSpace: "nowrap",
              overflow: "hidden",
              padding: "2px 0px 2px 15px",
              borderBottom: "1px solid",
              backgroundColor: currentIndex === i && "orange",
              fontSize: "13px",
            }}
          >
            {("" + i).padStart(2, " ")}.{m}
          </div>
        )
      })}
    </div>
  )
}

function App() {
  // app useselector
  // const  = useSelector((s) => s.changeVoice)
  let stateInitialDrawEndInstructions = useSelector((s) => s.initialDrawEndInstructions)
  let stateFinishedExerciseIds = useSelector((s) => s.finishedExerciseIds)
  let stateLoopType = useSelector((s) => s.loopType)
  let statePlayTutorialIndex = useSelector((s) => s.playTutorialIndex)
  let statePlaytutorialLength = useSelector((s) => s.playTutorialLength)
  let stateTutorialPressedOnce = useSelector((s) => s.tutorialPressedOnce)
  let stateTurtleActionListEnabled = useSelector((s) => s.turtleActionListEnabled)
  let stateCurrentSpeed = useSelector((s) => s.currentSpeed)
  let mouseOverClick = useSelector((s) => s.mouseOverClick)
  let mouseOverNoclick = useSelector((s) => s.mouseOverNoclick)
  const stateFakePlayButtonPressed = useSelector((s) => s.fakePressedPlay)
  let colorBackground = useColorBackground()
  let stateActiveSegments = useSelector((s) => s.activeSegments) || []

  let stateTutorialFocus = useSelector((s) => s.tutorialFocus)

  let isTutorialExercise = useSelector((s) => s.currentView?.exercise && s.content.tutorialDescriptions?.[s.currentView.exercise])

  let stateTurtleAnimationFrames = useSelector((s) => s.turtleAnimationFrames)

  let stateEventScript = useSelector((s) => s.voiceScript)
  let stateScriptDefinitions = useSelector((s) => s.scriptDefinitions)
  let focusedSegmentDetails = useSelector(
    (s) =>
      s.currentView?.exercise &&
      s.content.tutorialDescriptions?.[s.currentView.exercise] &&
      s.tutorialFocus?.type === "segment" &&
      s.content.segmentDescriptions[s.tutorialFocus.id]
  )
  let stateNarrationTextIndex = useSelector((s) => s.narrationTextIndex)
  let stateNarrationTextOn = useSelector((s) => s.narrationText)
  let stateNarrationVoiceOn = useSelector((s) => s.narrationVoice)
  let stateBugReportWindow = useSelector((s) => s.bugReportWindow)
  let stateSaveProgress = useSelector((s) => s.saveProgressWindow)
  let stateTooltip = useSelector((s) => s.tooltip)
  let stateInitialDrawDone = useSelector((s) => s.initialDrawDone)
  let stateFrameIndex = useSelector((s) => s.frameIndex)
  let stateExerciseRestrictions = useSelector(
    (s) => s.currentView && s.currentView.exercise && s.content.exercises[s.currentView.exercise].restrictions
  )

  let stateAlwaysShowExternalVars = useSelector(
    (s) => s.currentView && s.currentView.exercise && s.content.exercises[s.currentView.exercise].alwaysShowExternalVars
  )
  let stateFinishedThisExercise = useSelector((s) => s.finishedThisExercise)
  let stateShowInitialDrawInstructions = useSelector((s) => s.showInitialDrawInstructions)
  let stateCountdown = useSelector((s) => s.timerCountdown)

  let stateBreakpoints = useSelector((s) => s.breakpoints)
  let stateTurtleGoal = useSelector((s) => s.turtleGoal)
  let stateCompleted = useSelector((s) => s.completed)
  let stateInitialDrawMode = useSelector((s) => s.initialDrawMode)
  let stateTokenQuotaView = useSelector((s) => s.tokenQuotaView)
  let stateIncrementNumCompleted = useSelector((s) => s.incrementNumCompleted)
  let stateWatchYoutube = useSelector((s) => s.watchYoutube)
  let stateSolutionNumTokens = useSelector((s) => s.solutionNumTokens)
  let stateAdminMode = useSelector((s) => s.adminMode)
  let stateAnimationFrames = useSelector((s) => s.animationFrames)
  let stateIsLastExercise = useSelector((s) => s.isLastExercise)
  let stateFinishedAllExercises = useSelector((s) => s.finishedAllExercises)
  let stateCurrentView = useSelector((s) => s.currentView)
  let stateVideoHistory = useSelector((s) => s.videoHistory)
  let stateViolatedRestrictions = useSelector((s) => s.violatedRestrictions)

  let stateSyntaxError = useSelector((s) => s.syntaxError)
  let stateRefLookupHighlights = useSelector((s) => s.refLookupHighlights)
  let stateLookedupByUser = useSelector((s) => s.lookedupByUser)
  let stateLoadingFinishedExercises = useSelector((s) => s.loadingFinishedExercises)

  let activeSegments = [...stateActiveSegments, ...(focusedSegmentDetails ? [focusedSegmentDetails] : [])]

  // redraw
  useEffect(() => {
    dispatch({ type: "increment-counter", name: "lineMarkerCounter" })
  })

  useEffect(() => {
    assocRedraw()
  })

  let stopPlay = () => {
    stopLoop()
  }

  let stopButton = () => {
    stopPlay()
    dispatch({ type: "stop-button" })
  }

  let clickNextElement = () => {
    stopPlay()
    dispatch({ type: "navigate-next-element" })
  }

  let clickNextExercise = () => {
    dispatch({ type: "navigate-next-exercise" })
  }
  let clickPrevExercise = () => {
    dispatch({ type: "navigate-previous-exercise" })
  }

  useEffect(() => {
    let listener = (evt) => {
      if (store.getState().changeVoice) {
        if (evt.key === "Escape") {
          evt.preventDefault()
          evt.stopPropagation()
          dispatch({ type: "set-state-prop", name: "changeVoice", value: false })
          return
        }

        if (evt.key === "F1") {
          evt.preventDefault()
          evt.stopPropagation()
          if (store.getState().audioRecording) {
            wakeup("stop-recording")
            return
          }
          startAudioRecording()
          // record or stop
        }
        if (evt.key === "F2") {
          evt.preventDefault()
          evt.stopPropagation()
          playVoiceAudio()
          // play
        }
        if (evt.key === "F3") {
          evt.preventDefault()
          evt.stopPropagation()
          store.dispatch({ type: "prev-voice-event" })

          // prev
        }
        if (evt.key === "F4") {
          evt.preventDefault()
          evt.stopPropagation()
          store.dispatch({ type: "next-voice-event" })

          // next
        }
        return
      }
      if (evt.key === "Escape") {
        dispatch({ type: "unset-tutorial-focus" })
        return
      }
      if (!evt.ctrlKey) return
      if (evt.key.toLowerCase() === "j") {
        // case sensitive
        if (!stateAdminMode) return
        evt.preventDefault()
        evt.stopPropagation()
        addEx()
      } else if (evt.key.toLowerCase() === "k") {
        // case sensitive
        alert("copied")
        evt.preventDefault()
        evt.stopPropagation()
        copyCurrentExercise()
      } else if (evt.key.toLowerCase() === "l") {
        // case sensitive
        if (!stateAdminMode) return
        evt.preventDefault()
        evt.stopPropagation()
        pasteCurrentExercise()
      } else if (evt.key.toLowerCase() === "g") {
        // case sensitive
        evt.preventDefault()
        evt.stopPropagation()
        downloadContent()
      } else if (evt.key.toLowerCase() === "a") {
        // case sensitive
        evt.preventDefault()
        evt.stopPropagation()
        dispatch({ type: "toggle-admin-mode" })
      } else if (evt.key.toLowerCase() === "2") {
        evt.preventDefault()
        evt.stopPropagation()
        dispatch({ type: "toggle-hide-tutorial-editor" })
        //dispatch({ type: "add-event-before", row: i })
      } else if (evt.key.toLowerCase() === "3") {
        evt.preventDefault()
        evt.stopPropagation()
        let s = store.getState()
        const tdesc = s.currentView?.exercise && s.content.tutorialDescriptions?.[s.currentView.exercise]
        const focus = s.tutorialFocus
        if (!focus) return
        let row = tdesc.eventOrder.indexOf(focus.id)
        if (row === -1) alert("unknown row")
        dispatch({ type: "add-event-before", row })
      } else if (evt.key === "End") {
        // case sensitive
        evt.preventDefault()
        evt.stopPropagation()
        lastFrameButton()
      } else if (evt.key === "ArrowRight") {
        evt.preventDefault()
        evt.stopPropagation()
        nextFrameButton()
      } else if (evt.key === "Enter") {
        evt.preventDefault()
        evt.stopPropagation()
        if (stateAnimationFrames) {
          stopButton()
        } else {
          startButton()
          //initialDrawButton()
        }
      } else if (evt.key === "\\") {
        evt.preventDefault()
        evt.stopPropagation()
        if (stateAnimationFrames) {
          stopButton()
        } else {
          //startButton()
          initialDrawButton()
        }
      } else if (evt.key === "ArrowLeft") {
        evt.preventDefault()
        evt.stopPropagation()
        prevFrameButton()
      } else if (evt.key === "ArrowDown") {
        evt.preventDefault()
        evt.stopPropagation()
        if (evt.shiftKey) {
          mdown()
          return
        }
        clickNextElement()
      } else if (evt.key === "ArrowUp") {
        evt.preventDefault()
        evt.stopPropagation()
        if (evt.shiftKey) {
          mup()
          return
        }

        dispatch({ type: "goto-previous-content-element" })
      } else if (evt.key === "p") {
        if (!stateAdminMode) return
        evt.preventDefault()
        evt.stopPropagation()
        if (stateCurrentView.exercise) {
          dispatch({ type: "delete-exercise" })
        } else if (stateCurrentView.chapter) {
          dispatch({ type: "delete-chapter" })
        }
      } else if (evt.key === "m") {
        if (!stateAdminMode) return
        evt.preventDefault()
        evt.stopPropagation()
        dispatch({ type: "name-current" })
      }
      return true
    }
    document.addEventListener("keydown", listener)
    let dragEndListener = (evt) => {
      dispatch({ type: "drag-ended" })
    }
    document.addEventListener("dragend", dragEndListener)
    return () => {
      document.removeEventListener("keydown", listener)
      document.removeEventListener("dragend", dragEndListener)
    }
  })

  useEffect(() => {
    let f = function (e) {
      refresh()
    }
    document.addEventListener("scroll", f)
    return () => document.removeEventListener("scroll", f)
  }, [])

  ///const playLoopRef = React.useRef(undefined);
  useEffect(refresh)
  //clearIdInInterfaceDomNodeAssociation()
  if (stateLoadingFinishedExercises) {
    return "loading..."
  }

  let addEx = (eid) => {
    dispatch({ type: "add-exercise", eid: eid })
  }

  let mup = () => {
    dispatch({ type: "move-up" })
  }

  let mdown = () => {
    dispatch({ type: "move-down" })
  }

  let downloadContent = () => {
    dispatch({ type: "download-content" })
  }

  let copyCurrentExercise = () => {
    dispatch({ type: "copy-current-exercise" })
  }

  let pasteCurrentExercise = () => {
    dispatch({ type: "paste-current-exercise" })
  }

  if (stateVideoHistory) {
    return (
      <VideoHistory
        closeCB={() => {
          dispatch({ type: "video-history-viewable", viewable: false })
        }}
      />
    )
  }

  if (stateCurrentView.chapter) {
    return <ChapterVideo clickNextElement={clickNextElement} chapterId={stateCurrentView.chapter} />

    // viewing a chapter
  }

  // let currentExercise =
  //   stateCurrentView &&
  //   stateCurrentView.exercise &&
  //   state.content.exercises[stateCurrentView.exercise]

  //if (!stateCurrentExerciseId) return chooseExerciseMenu

  //if (!currentExercise) return chooseExerciseMenu

  let tableOfFrame = (frame, isGlobalFrame, frameIndex) => {
    let label = cellOfToken(`frame-loc-label-${frameIndex}`, frame.classFrame ? frame.classFrameLabel : frame.label, { border: "none" })
    let frameId = cellOfToken(`frame-loc-id-${frameIndex}`, frame.idToken, {
      border: "none",
      textAlign: "right",
    })
    let closureLine = null

    if (frame.closure) {
      let closureLabel = cellOfToken(`frame-loc-closure-label-${frameIndex}`, frame.closureLabel)
      let closure = cellOfToken(`frame-loc-closure-value-${frameIndex}`, frame.closureToken)
      closureLine = (
        <tr>
          {closureLabel}
          {closure}
        </tr>
      )
    }

    let frameRows = frame.rows
    if (isGlobalFrame && externalVarsShown) {
      frameRows = frameRows.filter((x) => !x.isExternal || externalVarsShown.includes(x.id))
    }

    return (
      <table style={{ ...tableStyle }}>
        <tr>
          {label}
          {frameId}
        </tr>
        {closureLine}
        <tr>
          {cellOfToken(
            `frame-loc-variable-name-label-${frameIndex}`,
            { id: frame.id + "-varname-label", text: "Variable name" },
            { textAlign: "center", minWidth: "150px" }
          )}
          {cellOfToken(
            `frame-loc-variable-value-label-${frameIndex}`,
            { id: frame.id + "-value-label", text: "Value" },
            { textAlign: "center", minWidth: "100px" }
          )}
        </tr>
        {isGlobalFrame && externalVarsShown && externalVarsShown.length === 0 ? (
          <tr>{cellOfToken(`frame-loc-external-indicator-${frameIndex}`, frame.externalLabelToken, undefined, 2)}</tr>
        ) : null}
        {frameRows.map((row, rowIndex) => {
          // if (seen[`frame-loc-cell-left-${frameIndex}-${rowIndex}`]) throw new Error("already seen")
          // seen[`frame-loc-cell-left-${frameIndex}-${rowIndex}`] = true

          let leftView = cellOfToken(`frame-loc-cell-left-${frameIndex}-${rowIndex}`, row.left.token)
          let rightView = cellOfToken(`frame-loc-cell-right-${frameIndex}-${rowIndex}`, collectTokens(row.right)[0])
          return (
            <tr>
              {leftView}
              {rightView}
            </tr>
          )
        })}
      </table>
    )
  }

  let viewOfRestrictions = (restrictions) => {
    let elementOfRestriction = (restriction) => {
      if (restriction === "token-quota") {
        return <div>You can use a maximum of {stateSolutionNumTokens} tokens.</div>
      }
      return <div>{possibleRestrictions.find((x) => x.id === restriction).instructions}</div>
    }
    if (restrictions.length === 1) return elementOfRestriction(restrictions[0])
    return (
      <ul>
        {restrictions.map((x) => (
          <li>{elementOfRestriction(x)}</li>
        ))}
      </ul>
    )
  }
  let renderUI = (
    parseError,
    animationView,
    animationFrameDescription,
    lastFrameInstructions,
    lineView,
    turtle,
    turtleGoal,
    exerciseRestrictions,
    showMulti,
    animationTokens
  ) => {
    let showTextNarration = !stateInitialDrawMode && stateNarrationTextOn && animationFrameDescription

    let buttonHelp = (
      <>
        <div>Press the {stopSymbol} button to modify your code so you can fix the error.</div>
      </>
    )

    let topLeftOptions = {
      parseError: () => (
        <>
          <div>You have a syntax error and it is highlighted in red.</div>
          <div>Press the {stopSymbol} button to be able to fix your code.</div>
          {hardcodedVideoButton("syntax-error-video", "What does this mean?")}
        </>
      ),
      lastFrameInstructions: (errDeets) => {
        if (!errDeets) debugger
        if (errDeets.type === "runtime-error") {
          return (
            <>
              <div>There was an error: {punctualize(errDeets.msg)} </div>
              {hardcodedVideoButton("runtime-error", "What does this mean?")}
              {buttonHelp}
            </>
          )
        }
        //  let vidButton = errDeets.type && hardcodedVideoButton(errDeets.type, "What does this mean?")
        if (errDeets.type === "over-token-quota") {
          let actualNumber = errDeets.actual
          let maximum = errDeets.maximum
          let userTokens = errDeets.userTokens
          return (
            <>
              <div>
                You wrote {userTokens.length} tokens which is more than the maximum of {maximum}
              </div>
              {/* <div>{vidButton}</div> */}
              {buttonHelp}
              <div>
                <button
                  className="flatbutton"
                  onClick={() => {
                    dispatch({ type: "popup-token-quota", actualNumber, userTokens, maximum })
                  }}
                >
                  Details
                </button>
              </div>
            </>
          )
        }

        return (
          <>
            <div>{errDeets.msg}</div>
            {/* <div>{vidButton}</div> */}
            {buttonHelp}
          </>
        )
      },
      initialDrawMode: () => (
        <>
          <div>An image will soon be drawn. You will need to reproduce that image using code.</div>
          <div>A {stopSymbol} button will soon appear. Press it to start coding.</div>
        </>
      ),
      turtleGoal: () => {
        return (
          <>
            <div>
              Press the <GrRun /> button to run the code on the right. Once it's finished running, you will have a better idea of what you
              need to modify to solve the exercise.
            </div>
            {exerciseRestrictions && exerciseRestrictions.length > 0 && viewOfRestrictions(exerciseRestrictions)}
          </>
        )
      },
      intro: () => (
        <>
          Click the <GiTurtle /> and an image will be drawn. Your task will be to reproduce that image.
        </>
      ),
      soundInstructions: () => {
        return (
          <>
            <div>To get audio instructions, test your speakers and enable audio.</div>
            <div>
              You can disable sound at any time with the <BiVolumeFull /> button
            </div>

            <button
              className="flatbutton"
              onClick={() => {
                let run = async () => {
                  let audio = await getAudio(testAudioPath)
                  playAudio(audio)
                }
                run()
              }}
            >
              Test audio
            </button>

            <button
              className="flatbutton"
              onClick={() => {
                dispatch({ type: "set-voice-narration", value: true, notIgnored: true })

                //wakeup("user-selects-speaker-options")
              }}
            >
              Enable audio
            </button>

            <button
              className="flatbutton"
              onClick={() => {
                dispatch({ type: "set-voice-narration", value: false, notIgnored: true })
                // dispatch({ type: "set-show-sound-instructions", shown: false })
                //wakeup("user-selects-speaker-options")
              }}
            >
              Disable audio
            </button>
          </>
        )
      },
    }
    //let oldAnimationFrameDescription = animationFrameDescription

    let switchOptions = [
      [lastFrameInstructions, topLeftOptions.lastFrameInstructions, true],
      [
        runLoopIsPlaying() && animationFrameDescription && showTextNarration,
        () => {
          return animationFrameDescription.stepNarrationText
        },
        false,
      ],

      [parseError, topLeftOptions.parseError, true],

      [stateInitialDrawMode && stateInitialDrawEndInstructions, () => <>Press the {stopSymbol} button below to start coding.</>, true],
      [
        // [stateInitialDrawMode || stateShowInitialDrawInstructions, topLeftOptions.intro, true],
        !stateCompleted && turtleGoal,
        topLeftOptions.turtleGoal,
        true,
      ],
      [stateFinishedThisExercise, () => "", true],
      [true, topLeftOptions.intro, true],
    ]

    // top left

    let viewOfDefinition = (d) => {
      return (
        <div style={{ width: "100%", padding: "3px 10px 3px 10px" }}>
          <div style={{ fontWeight: "bold" }}>{d.header}</div>
          <div>{d.body}</div>
        </div>
      )
    }

    let getFinalTopLeftText = () => {
      if (isTutorialExercise) {
        if (stateNarrationTextOn) {
          return (
            <>
              <div style={{ padding: "3px 10px 3px 10px" }}>{punctualize(stateEventScript)}</div>
              {stateScriptDefinitions?.map(viewOfDefinition)}
            </>
          )
        }
        return (
          <div style={{ padding: "10px" }}>
            Press the {captionsOff} button for closed captioning.
            {stateScriptDefinitions?.map(viewOfDefinition)}
          </div>
        )
      }
      if (stateFinishedAllExercises)
        return (
          <>
            You completed <em>every single exercise</em>!
          </>
        )
      if (stateCompleted) {
        if (stateIsLastExercise) {
          return <>Use the navigation menu to select which exercise to do next</>
        }

        return (
          <>
            Success! You can go to the next stage
            <button className="flatbutton" onClick={clickNextElement}>
              Next Exercise
            </button>
          </>
        )
      }

      return (
        <SwitchHighlight
          countdown={stateCountdown}
          style={{
            lineHeight: "1.5",
            height: "100%",
            width: "100%",
            padding: "19px",
          }}
          options={switchOptions}
        />
      )
    }

    let topLeftText = getFinalTopLeftText()

    let tokenQuotaView = undefined
    if (stateTokenQuotaView) {
      let maximum = stateTokenQuotaView.maximum
      let userTokens = stateTokenQuotaView.userTokens
      let boxInner = undefined
      if (userTokens.length === 0) boxInner = "strange error"
      else {
        let annotated = annotateTokensSuperscript(userTokens)
        boxInner = (
          <>
            <div>
              You used {userTokens.length} tokens but you were only allowed {maximum} tokens.{" "}
            </div>
            <div>Here is a list of the tokens you used</div>
            <pre style={{ fontSize: "19px" }}>{annotated}</pre>
          </>
        )
      }
      tokenQuotaView = (
        <div
          style={{
            display: "flex",
            position: "fixed",
            height: "100vh",
            width: "100vw",
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: colorModalBlanket,
            zIndex: 1000000,
          }}
        >
          <div style={{ border: "1px solid", padding: "20px" }}>
            <div style={{ padding: "10px" }}>{boxInner}</div>
            <button
              className="flatbutton"
              onClick={() => {
                dispatch({ type: "close-token-quota" })
              }}
            >
              close
            </button>
          </div>
        </div>
      )
    }

    let showLefthandControls = stateAnimationFrames !== undefined
    // if (stateTurtleAnimationFrames?.length > 0) debugger
    let getTurtleMovesListView = () => {
      if (!stateTurtleAnimationFrames) return null
      let turtles = stateTurtleAnimationFrames.map((x) => x.animationFrame.turtle)

      let tokens = turtles.map((x) => x.lineDescriptionToken)
      let turtleTexts = tokens.map((x) => x.text)
      return (
        <div>
          <TurtleMoveList
            shown={stateTurtleActionListEnabled}
            currentIndex={stateInitialDrawMode ? stateFrameIndex : undefined}
            moves={turtleTexts}
          />
        </div>
      )
    }
    let turtleMovesListView = getTurtleMovesListView()
    let rightOfTurtleBox = (
      <div
        style={{
          marginLeft: "8px",
          pointerEvents: !showLefthandControls && "none",
          opacity: stateInitialDrawMode ? 1 : 0,
          boxShadow: "0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)",
        }}
      >
        <div>
          <FadeButton
            tooltipText="start coding"
            idInInterface={"left-hand-control-stop"}
            key={"lhc-stop"}
            shown={showLefthandControls}
            onClick={() => {
              stopButton()
            }}
            child={stopSymbol}
          />
        </div>
        <div>
          <FadeButton
            tooltipText="rewind"
            idInInterface={"left-hand-control-rewind-forward"}
            key={"lhc-rw"}
            shown={showLefthandControls && stateFrameIndex > 0}
            onClick={prevFrameButton}
            child={<BiRewind />}
          />
        </div>
        <div>
          <SwitchBetweenMany
            options={[
              ["a", !runLoopIsPlaying(), playButtonTurtleRight],
              ["b", true, pauseButtonTurtleRight],
            ]}
          />
        </div>
        <div>
          <FadeButton
            tooltipText="fast forward"
            idInInterface={"left-hand-control-fast-forward"}
            key={"lhc-ff"}
            shown={showLefthandControls && stateFrameIndex < stateAnimationFrames.length - 1}
            onClick={nextFrameButton}
            child={<BiFastForward />}
          />
        </div>

        <div>
          <FadeButton
            tooltipText="skip to end"
            idInInterface={"left-hand-control-skip"}
            key={"lhc-last"}
            minOpacity={0}
            shown={showLefthandControls && stateFrameIndex < stateAnimationFrames.length - 1}
            onClick={() => {
              lastFrameButton()
              dispatch({ type: "set-state-prop", name: "initialDrawEndInstructions", value: "true" })
            }}
            child={<BiSkipNext />}
          />
        </div>
      </div>
    )
    let whatTurtleIsDoing = (
      <div
        style={{
          display: "flex",
          justifyContent: "flex-start",
          alignItems: "stretch",
          //pointerEvents: !showLefthandControls && "none",
          padding: "10px 10px 10px 20px",
          margin: "10px 0px 10px 0px",
          height: "40px",
          transition: "opacity 0.5s",
          // opacity: stateInitialDrawMode ? 1 : 0,
          boxShadow: "0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)",
        }}
      >
        {!stateTurtleActionListEnabled && turtle && turtle.lineDescriptionToken && (
          <SlowlyChangeText idInInterface={"turtle-is-doing-text"} text={turtle.lineDescriptionToken.text} />
        )}
        <div
          onClick={() => {
            dispatch({ type: "toggle-action-list-enabled" })
          }}
          style={{ display: "flex", justifyContent: "flex-end", flex: 1 }}
        >
          {stateTurtleAnimationFrames && (
            <span style={{ cursor: "pointer" }} {...tooltipAttributes("details")}>
              {stateTurtleActionListEnabled ? <BiChevronsLeft /> : <BiChevronsRight />}
            </span>
          )}
        </div>
      </div>
    )
    let seekButtons = (
      <div
        style={{
          display: "flex",
          alignItems: "stretch",
          marginBottom: "15px",
        }}
      >
        <SwitchBetweenMany
          options={[
            ["a", stateNarrationVoiceOn, turnNarrationVoiceOff],
            ["b", true, turnNarrationVoiceOn],
          ]}
        />
        <SwitchBetweenMany
          options={[
            ["a", stateNarrationTextOn, turnNarrationTextOff],
            ["b", true, turnNarrationTextOn],
          ]}
        />

        <div
          style={{
            display: "flex",
            alignItems: "flex-start",
            alignItems: "center",
          }}
        >
          <FadeButton
            alwaysClickable={true}
            tooltipText="decrease speed"
            idInInterface={"top-controls-decrease-speed"}
            key={"b8"}
            minOpacity={minOpac}
            shown={true}
            onClick={() => {
              decreaseSpeed()
            }}
            child={"-"}
          />
          <div style={{ display: "flex", justifyContent: "center", width: "40px" }}>
            <div
              style={{
                display: "flex",
                alignItems: "center",
                transition: "opacity 0.2s",
                opacity: 1,
                textAlign: "center",
              }}
            >
              {getMultiplierOfSpeed(stateCurrentSpeed)}x
            </div>
          </div>
          <FadeButton
            alwaysClickable={true}
            tooltipText="increase speed"
            idInInterface={"top-controls-increase-speed"}
            minOpacity={minOpac}
            key={"b9"}
            shown={true}
            onClick={() => {
              increaseSpeed()
            }}
            child={"+"}
          />
        </div>

        <FadeButton
          tooltipText="run"
          idInInterface={"top-controls-run"}
          key={"fcb2"}
          shown={turtleGoal && !stateSyntaxError && !stateAnimationFrames}
          onClick={() => startButton()}
          size={"19px"}
          child={<GrRun />}
        />
        <FadeButton
          tooltipText="rewind"
          idInInterface={"top-controls-rewind"}
          key={"b2"}
          minOpacity={minOpac}
          shown={stateAnimationFrames && !stateInitialDrawMode && stateFrameIndex >= 1}
          onClick={prevFrameButton}
          child={<BiRewind />}
        />
        <SwitchBetweenMany
          options={[
            ["a", (!stateTurtleGoal || !runLoopIsPlaying() || stateInitialDrawMode) && !stateFakePlayButtonPressed, seekPlayButton],
            ["b", true, seekPauseButton],
          ]}
        />

        <FadeButton
          tooltipText="fast forward"
          idInInterface={"top-controls-fast-forward"}
          minOpacity={minOpac}
          key={"b4"}
          shown={stateAnimationFrames && stateFrameIndex < stateAnimationFrames.length - 1 && !stateInitialDrawMode}
          onClick={nextFrameButton}
          child={<BiFastForward />}
        />
        <FadeButton
          tooltipText="skip to end"
          idInInterface={"top-controls-skip-to-end"}
          minOpacity={minOpac}
          key={"b7"}
          shown={stateAnimationFrames && !stateInitialDrawMode}
          onClick={lastFrameButton}
          child={<BiSkipNext />}
        />

        <FadeButton
          tooltipText="start coding"
          idInInterface={"top-controls-stop"}
          key={"b6"}
          minOpacity={minOpac}
          shown={stateSyntaxError || (stateAnimationFrames && !stateInitialDrawMode)}
          onClick={stopButton}
          child={stopSymbol}
        />
        <FadeButton
          tooltipText="skip to breakpoint"
          idInInterface={"top-controls-skip-to-breakpoint"}
          key={"b5"}
          minOpacity={minOpac}
          shown={stateAnimationFrames && !stateInitialDrawMode}
          onClick={nextBreakpointButton}
          child={<BiSkipNextCircle />}
        />
      </div>
    )

    // ret

    let tutorialButtonStyle = { fontSize: "13px", width: "100px", padding: "5px", margin: "0px 5px 0px 5px" }

    let progressBarWidth = 600

    let lastPartOftutorial = statePlaytutorialLength !== undefined && statePlayTutorialIndex === statePlaytutorialLength - 1

    let playedsofarWidth =
      statePlaytutorialLength === undefined ? 0 : (progressBarWidth * statePlayTutorialIndex) / Math.max(1, statePlaytutorialLength - 1)
    let remainingToPlayWidth =
      statePlaytutorialLength === undefined
        ? progressBarWidth
        : progressBarWidth - (progressBarWidth * statePlayTutorialIndex) / Math.max(1, statePlaytutorialLength - 1)
    let tutorialPart = isTutorialExercise && (
      <>
        <div>
          <div
            key={"tutorial-part"}
            style={{
              display: "flex",
              justifyContent: "center",
              fontSize: "30px",
              paddingTop: "5px",
              // minHeight: !(stateTutorialPressedOnce || stateAdminMode) && "100vh",
            }}
          >
            <div>
              <button
                style={tutorialButtonStyle}
                {...tutorialAttr(true, "rewind-tutorial", () => {
                  let isPlaying = tutorialLoopIsPlaying()
                  userTutorialRewind()

                  if (isPlaying) userPlayTutorialButton()
                })}
              >
                rewind
              </button>
            </div>
            <div>
              <button
                style={tutorialButtonStyle}
                {...tutorialAttr(true, "playTutorial", () => {
                  if (stateLoopType === "tutorial") {
                    stopLoop()
                    return
                  }
                  dispatch({ type: "set-attr", name: "tutorialPressedOnce", value: true })
                  userPlayTutorialButton()
                })}
              >
                {stateLoopType === "tutorial" ? "pause" : "play"}
              </button>
              <div
                key="transition-part-arrow"
                style={{
                  opacity: stateTutorialPressedOnce || stateAdminMode ? "0" : "1",
                  pointerEvents: (stateTutorialPressedOnce || stateAdminMode) && "none",
                  transition: "opacity 0.3s",
                  fontSize: "100px",
                }}
              >
                <div style={{ textAlign: "center", fontSize: "80px", marginTop: "20px" }}>
                  <ImArrowUp2 />
                </div>
              </div>
            </div>
            <div>
              <button
                style={tutorialButtonStyle}
                {...tutorialAttr(true, "ff-tutorial", () => {
                  let isPlaying = tutorialLoopIsPlaying()
                  userTutorialFastForward()
                  if (isPlaying) userPlayTutorialButton()
                })}
              >
                fast forward
              </button>
            </div>
            <div>
              <button
                {...tutorialAttr(true, "skip-tutorial-button", () => {
                  stopLoop()
                  dispatch({ type: "navigate-next-exercise" })
                })}
                style={tutorialButtonStyle}
              >
                {lastPartOftutorial ? "finish tutorial" : "skip tutorial"}
              </button>
            </div>
          </div>
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              opacity: !stateTutorialPressedOnce && 0,
            }}
          >
            <div
              onClick={(e) => {
                if (statePlaytutorialLength === undefined) return

                let rect = e.target.getBoundingClientRect()
                let x = e.clientX - rect.left //x position within the element.
                let perc = x / progressBarWidth
                let index = Math.floor(perc * statePlaytutorialLength)
                if (index < 0) index = 0
                if (index >= statePlaytutorialLength) index = statePlaytutorialLength - 1
                let isPlaying = tutorialLoopIsPlaying()

                stopLoop()
                dispatch({ type: "set-play-tutorial-index", index })
                if (isPlaying) userPlayTutorialButton()
              }}
              style={{
                paddingTop: "15px",
                paddingBottom: "5px",
                width: `${progressBarWidth}px`,
                display: "flex",
                cursor: "pointer",
              }}
            >
              <div
                style={{
                  backgroundColor: "#fa8072",
                  height: "3px",
                  width: `${playedsofarWidth}px`,
                  transition: "width 0.5s",
                }}
              ></div>
              <div
                style={{
                  backgroundColor: "#bbb",
                  height: "3px",
                  width: `${remainingToPlayWidth}px`,
                  transition: "width 0.5s",
                }}
              ></div>
            </div>
          </div>
        </div>
        {!stateTutorialPressedOnce && !stateAdminMode && (
          <div
            style={{
              textAlign: "center",
              fontSize: "15px",

              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              position: "fixed",
              top: "0px",
              left: "0px",
              right: "0px",
              bottom: "0px",
              fontSize: "18px",
              pointerEvents: "none",
            }}
          >
            <div style={{ pointerEvents: "all" }}>
              <div style={{ marginTop: "30px" }}>Before we begin, make sure your speakers are working.</div>
              <button
                onClick={() => {
                  playAudio(cheeraudio)
                }}
                style={{ marginTop: "20px", marginBottom: "20px" }}
              >
                Test audio
              </button>
              <div>Then click the play button to start the tutorial. </div>
            </div>
          </div>
        )}
      </>
    )
    // if (isTutorialExercise && !stateTutorialPressedOnce)
    //   return <div style={{ minHeight: "100vh", backgroundColor: colorBackground }}>{tutorialPart}</div>
    //return <>{tutorialPart}</>

    let interfaceWithoutTutorialControls = (
      <>
        <TutorialDescription />

        <Tooltip data={stateTooltip} />
        {stateSaveProgress && <SaveProgress />}
        {stateBugReportWindow && (
          <BugReport
            onClose={() => {
              dispatch({ type: "close-bug-report-window" })
            }}
          />
        )}
        {tokenQuotaView}
        {stateCompleted && <Fireworks />}
        {stateIncrementNumCompleted && (
          <IncrementNumCompleted
            num={stateIncrementNumCompleted.num}
            cb={() => {
              dispatch({ type: "close-increment-num-completed" })
              stateIncrementNumCompleted && stateIncrementNumCompleted.cb && stateIncrementNumCompleted.cb()
            }}
          />
        )}
        {/* <IncrementNumCompleted num={5} /> */}
        {stateWatchYoutube && (
          <YoutubePopup
            coverEverything={true}
            below={stateWatchYoutube.text}
            youtubeID={stateWatchYoutube.youtubeID}
            cb={() => {
              let cb = stateWatchYoutube.cb
              dispatch({ type: "close-youtube" })
              if (cb) cb()
            }}
          />
        )}
        <div
          style={{
            display: "flex",
          }}
        >
          <div key="multi-papa" style={{ marginLeft: "40px", marginRight: "10px" }}>
            <div>
              <div
                {...tutorialAttr(false, "top-left-box")}
                style={{
                  margin: "40px 0px 0px 0px",
                  boxShadow: "0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)",
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  justifyContent: "center",
                  overflowY: "auto",
                  width: "400px",
                  height: "200px",
                  borderRadius: "2px",
                }}
              >
                {topLeftText}
              </div>
              <div
                key={"contbuttons"}
                style={{
                  width: "400px",
                  height: "50px",
                }}
              >
                {whatTurtleIsDoing}
              </div>
              {/* big turtle button */}
              {
                <div style={{ display: "flex" }}>
                  <DrawLines onClick={stateAnimationFrames ? undefined : initialDrawButton} turtle={turtle} turtleGoal={turtleGoal} />
                  {turtleMovesListView}
                  <div>{rightOfTurtleBox}</div>
                </div>
              }
            </div>
          </div>

          <div style={{ margin: "40px 0px 0px 20px" }}>
            {seekButtons}
            {/* <div style={{ display: "flex" }}>
              {showTextNarration && (
                <div style={{ border: "1px solid", height: "120px", width: "400px", padding: "10px" }}>
                  {animationFrameData.description.stepNarrationText}
                </div>
              )}
            </div> */}

            {(stateAdminMode || showMulti) && (
              <MultiSection
                adminMode={stateAdminMode}
                parseError={parseError}
                animationView={animationView}
                key="center-piece"
                identifier="multi-section"
                breakpoints={stateBreakpoints}
                exerciseId={stateCurrentView && stateCurrentView.exercise}
              />
            )}

            {stateAdminMode && (
              <div style={{ marginTop: "20px" }}>
                <div>
                  <LabeledCheckbox
                    label={"Always show external vars"}
                    onChange={(v) => {
                      dispatch({
                        type: "toggle-show-external-vars",
                        v,
                      })
                    }}
                    checked={stateAlwaysShowExternalVars}
                  />
                </div>
                <div>
                  {possibleRestrictions.map((restriction) => {
                    return (
                      <LabeledCheckbox
                        label={restriction.label}
                        onChange={(v) => {
                          dispatch({
                            type: "toggle-restriction",
                            restrictionId: restriction.id,
                            v,
                          })
                        }}
                        checked={stateExerciseRestrictions.includes(restriction.id)}
                      />
                    )
                  })}
                </div>
                <div>
                  <YoutubeExerciseVideoInput txt="intro" propname="videoIntro" />
                  <YoutubeExerciseVideoInput txt="first stop" propname="videoFirstStop" />
                  <YoutubeExerciseVideoInput txt="success" propname="videoSuccess" />
                </div>
              </div>
            )}
          </div>
        </div>
        <div>{stateAdminMode && animationFrameDescription && animationFrameDescription.title}</div>
        {lineView}
      </>
    )

    let showNonTutorial = !isTutorialExercise || stateTutorialPressedOnce || stateAdminMode
    let bottomSt = showNonTutorial
      ? { opacity: 1, transition: "opacity 0.5s" }
      : { opacity: 0, pointerEvents: "none", transition: "opacity 0.5s" }

    return (
      <div style={{ minHeight: "100vh", backgroundColor: colorBackground, boxSizing: "border-box" }}>
        <Decorations />
        <LineMarkerView />
        {tokenQuotaView}
        <div
          style={{
            height: isTutorialExercise && !stateTutorialPressedOnce && "100vh",
            boxSizing: "border-box",

            // overflowY: isTutorialExercise && !stateTutorialPressedOnce && "hidden",
          }}
        >
          {isTutorialExercise && tutorialPart}
          <ContentNavigationMenu key="cnm" />

          <div key="without-tutorial-controls" style={bottomSt}>
            {interfaceWithoutTutorialControls}
          </div>
          {activeSegments?.map((a) => {
            if (a.resourceLocation) {
              return (
                <div
                  style={{
                    position: "fixed",
                    pointerEvents: "none",
                    overflowY: "hidden",
                    top: "50px",
                    left: "400px",
                    right: "0px",
                    bottom: "0px",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    zIndex: 7000,
                  }}
                >
                  <div
                    style={{
                      // padding: "20px",
                      border: "1px solid #555",
                      borderRadius: "5px",
                      backgroundColor: "rgba(255,255,255,0.5)",
                    }}
                  >
                    <img
                      style={{
                        maxHeight: "calc(100vh - 100px)",
                        maxWidth: "calc(100vw - 450px)",
                        pointerEvents: "none",
                      }}
                      src={getResourceURL(a.resourceLocation)}
                    />
                  </div>
                </div>
              )
            }
            return null
          })}
        </div>
      </div>
    )
  }
  // if (!getSoundInstructionsShown()) {
  //   setSoundInstructionsShown(true)

  //   dispatch({ type: "set-show-sound-instructions", shown: true, notIgnored: true })
  //   yield hibernate("user-selects-speaker-options")
  // }
  // dispatch({ type: "set-show-sound-instructions", shown: false, notIgnored: true })
  let initialDrawButton = () => {
    if (!stateInitialDrawDone) {
      // dispatch({ type: "ignore-all" })
      function* gen() {
        dispatch({ type: "big-turtle-dissapear", notIgnored: true })
        if (!stateAdminMode) yield delay(800)

        // if (!getUserData("seen-initial-draw-instructions")) {
        //   setUserData("seen-initial-draw-instructions", true)
        //   dispatch({ type: "show-countdown", duration: 6, notIgnored: true }) // also shows instructions
        //   dispatch({ type: "show-initial-draw-instructions", notIgnored: true }) // also shows instructions
        //   if (!stateAdminMode) yield delay(6000)
        //   dispatch({ type: "hide-countdown", duration: 2, notIgnored: true }) // also shows instructions
        // }
        dispatch({ type: "show-small-turtle", notIgnored: true }) // also shows instructions
        if (!stateAdminMode) yield delay(500)
        dispatch({ type: "initial-draw", notIgnored: true })
        // dispatch({ type: "stop-ignore-all", notIgnored: true })
        yield startPlay()
        yield delay(1000)
        dispatch({ type: "set-state-prop", name: "initialDrawEndInstructions", value: "true" })
      }
      setRunLoop(drive(gen()))
    } else {
      dispatch({ type: "initial-draw" })
      startPlayButtonAction()
    }
  }

  let startButton = () => {
    let alreadyFinished = stateFinishedExerciseIds.includes(stateCurrentView.exercise)
    stopLoop()
    dispatch({ type: "run-button" })
    if (store.getState().syntaxError) return
    function* run() {
      if (store.getState().completed && !isTutorialExercise && !alreadyFinished) {
        dispatch({ type: "set-state-prop", name: "finishedThisExercise", value: true })
        dispatch({ type: "open-increment-num-completed" })
        // dispatch({ type: "ignore-all" })
        // debugger
        yield playAudioUntilFinished(overturemp3)
        yield delay(1500)
        // dispatch({ type: "stop-ignore-all", notIgnored: true })
        dispatch({ type: "close-increment-num-completed" })
        let gotonext = yield getAudio(gotonextexercisemp3)
        yield playAudioUntilFinished(gotonext, true)
        return
      }
      yield startPlay()
    }
    setRunLoop(drive(run()))
  }

  let pausePlay = () => {
    dispatch({ type: "clear-highlights" })
    stopLoop()
  }

  let nextBreakpointButton = () => {
    if (stateFrameIndex === undefined) return
    if (stateAnimationFrames === undefined) return
    pausePlay()
    for (let i = stateFrameIndex + 1; i < stateAnimationFrames.length; i++) {
      let aframe = stateAnimationFrames[i]
      let nln = aframe.animationFrame.newLineNum
      if (nln !== undefined && stateBreakpoints[nln - 1]) {
        dispatch({ type: "set-frame-index", index: i })
        return
      }
    }
  }

  let prevFrameButton = () => {
    stopLoop()
    dispatch({ type: "prev-frame" })
  }

  let nextFrameButton = () => {
    stopLoop()
    dispatch({ type: "next-frame" })
  }

  let lastFrameButton = () => {
    stopLoop()
    dispatch({ type: "last-frame" })
  }
  // let restartPlay = () => {
  //   dispatch({ type: "restart-play" })
  // }

  // let freshControlButtons = (
  //   <div
  //     style={{
  //       display: "flex",
  //       alignItems: "stretch",
  //       pointerEvents: stateAnimationFrames && "none",
  //     }}
  //   >

  //   </div>
  // )
  let minOpac = stateInitialDrawMode ? 0 : stateTurtleGoal ? 0.2 : 0

  let turnNarrationVoiceOn = (
    <FadeButton
      alwaysClickable={true}
      idInInterface={"top-controls-toggle-voice"}
      tooltipText="enable voice narration"
      minOpacity={minOpac}
      key={"narrOn"}
      shown={true}
      size={"19px"}
      child={<BiVolumeMute />}
      onClick={() => {
        dispatch({ type: "set-voice-narration", value: true })
      }}
    />
  )

  let turnNarrationVoiceOff = (
    <FadeButton
      alwaysClickable={true}
      tooltipText="disable voice narration"
      idInInterface={"top-controls-toggle-voice"}
      minOpacity={minOpac}
      key={"narrOff"}
      shown={true}
      size={"19px"}
      child={<BiVolumeFull />}
      onClick={() => {
        let isPlaying = tutorialLoopIsPlaying()
        stopAudio()
        stopLoop()
        dispatch({ type: "set-voice-narration", value: false })
        if (isPlaying) userPlayTutorialButton()
      }}
    />
  )

  let captionsOn = (
    <div style={{ display: "flex", alignItems: "center" }}>
      <BiCaptions />
    </div>
  )

  let captionsOff = (
    <div style={{ position: "relative", display: "flex", alignItems: "center" }}>
      <BiCaptions />

      <div style={{ position: "absolute", top: "0px", left: "0px" }}>
        <svg style={{ width: "18px", height: "18px" }}>
          <line x1="0" y1="0" x2="18" y2="18" style={{ stroke: "#333", strokeWidth: "2px" }} />
        </svg>
      </div>
    </div>
  )

  let turnNarrationTextOn = (
    <FadeButton
      size={"19px"}
      alwaysClickable={true}
      tooltipText="enable text narration"
      idInInterface={"top-controls-toggle-text-narration"}
      minOpacity={minOpac}
      key={"narrTextOn"}
      shown={true}
      child={captionsOff}
      onClick={() => {
        dispatch({ type: "set-text-narration", value: true })
      }}
    />
  )

  let turnNarrationTextOff = (
    <FadeButton
      size={"19px"}
      alwaysClickable={true}
      tooltipText="disable text narration"
      idInInterface={"top-controls-toggle-text-narration"}
      minOpacity={minOpac}
      key={"narrTextOff"}
      shown={true}
      child={captionsOn}
      onClick={() => {
        dispatch({ type: "set-text-narration", value: false })
      }}
    />
  )

  let seekPlayButton = (
    <FadeButton
      tooltipText="play"
      idInInterface={"top-controls-play-pause"}
      minOpacity={minOpac}
      key={"splayb"}
      shown={stateTurtleGoal && stateAnimationFrames && !stateInitialDrawMode && stateFrameIndex < stateAnimationFrames.length - 1}
      child={playSymbol}
      onClick={() => {
        // setCurrentSpeed(stateCurrentSpeed || 3)
        startPlayButtonAction()
        //dispatch({ type: "start-play" })
        //startPlay(stateAnimationFrames, true)
      }}
    />
  )

  let playButtonTurtleRight = (
    <FadeButton
      tooltipText="play"
      idInInterface={"turtle-right-play-pause"}
      minOpacity={minOpac}
      key={"splayb-turtleright"}
      shown={true}
      child={playSymbol}
      onClick={() => {
        startPlayButtonAction()
      }}
    />
  )

  let seekPauseButton = (
    <FadeButton
      tooltipText="pause"
      idInInterface={"top-controls-play-pause"}
      minOpacity={minOpac}
      key={"spauseb"}
      shown={stateTurtleGoal && runLoopIsPlaying() && !stateInitialDrawMode}
      child={<BiPause />}
      onClick={() => {
        stopLoop()
      }}
    />
  )

  let pauseButtonTurtleRight = (
    <FadeButton
      tooltipText="pause"
      idInInterface={"turtle-right-play-pause"}
      minOpacity={minOpac}
      key={"spauseb-turtleright"}
      shown={true}
      child={<BiPause />}
      onClick={() => {
        stopLoop()
      }}
    />
  )

  // let controlButtons = (
  //   <div style={{ margin: "10px 0px 10px 0px" }}>{lefthandControls}</div>
  // )

  if (stateSyntaxError) {
    return renderUI(stateSyntaxError, undefined, undefined, undefined, undefined, undefined, stateTurtleGoal, undefined, true)
  }

  if (!stateAnimationFrames || stateFrameIndex === undefined) {
    return renderUI(
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      stateTurtleGoal,
      stateCurrentView.exercise && stateExerciseRestrictions,
      stateTurtleGoal !== undefined
    )
  }

  if (stateFrameIndex >= stateAnimationFrames.length) {
    return "went over animation length. This is a bug. Contact Jacques."
  }
  // let animationFrames =

  let animationFrameTopLeftDescriptionData =
    stateNarrationTextIndex === undefined ? undefined : stateAnimationFrames[stateNarrationTextIndex]

  let animationFrameData = stateAnimationFrames[Math.min(stateFrameIndex, stateAnimationFrames.length - 1)]
  if (!animationFrameData) debugger
  let animationFrame = animationFrameData.animationFrame
  let lastFrameInstructions = animationFrame.lastFrameInstructions

  let animationTokens = animationFrame.sourceTokens
  let contexts = animationFrame.contexts

  let frameHeaven = animationFrame.frameHeaven

  let turtle = animationFrame.turtle
  let heap = animationFrame.heap

  lineMarkerTarget = undefined

  let lineMarker = animationFrame.lineMarker
  // let lineView = undefined
  // if (lineMarker) {

  //   lineView = mkLineMarker("line-marker-id", lineMarker)
  // }

  let onelineStyle = { whiteSpace: "nowrap" }

  let externalVarsShown = animationFrame.externalVarsShown
  //let funcdeflines = Math.max(...funcdefmarkers.map(x => x.lineNumber))

  let highlightedElements = animationFrame.highlightedElements

  let refLookupHighlights = stateRefLookupHighlights

  // let decorationsNeeded = {
  //   "frame-loc-cell-left-0-0": [{ type: "arrow" }],
  //   "frame-loc-cell-left-0-1": [{ type: "arrow" }],
  //   "frame-loc-cell-right-0-3": [{ type: "arrow" }],
  // }

  let spanOfToken = (idInInterface, t, notclickable, extraCSS) => {
    if (Array.isArray(extraCSS)) debugger
    if (!t) debugger
    let blackened = stateLookedupByUser.includes(t.id) || highlightedElements.includes(t.id)
    let refStyle = blackened ? { color: "white", backgroundColor: colorBlack, ...extraCSS } : { ...extraCSS }

    if (t.options) {
      if (t.options.ref && isAscii(t.text)) {
        refStyle = { ...refStyle, fontStyle: "italic", fontWeight: "bold" }
      }
      if (t.options.userError) {
        refStyle = { ...refStyle, backgroundColor: colorError, color: "black" }
      }
      if (t.options.blackBorder) {
        refStyle = { ...refStyle, border: "1px solid black", margin: "-1px" }
      }
      if (t.options.lineMarker) {
        refStyle = {
          ...refStyle,
          fontStyle: "italic",
          //fontFamily: "'Dancing Script', cursive",
          // display: "inline-block",
          // height: "0px",
          //fontSize: "12px",
        }
      }
    }
    // let getDecorations = () => {
    //   return flatten(
    //     activeSegments.filter((x) => x.idInInterface === idInInterface).map((x) => x.decorations)
    //   )
    //   // if (stateTutorialFocus?.type !== "segment") return []
    //   // let segmentId = stateTutorialFocus.id
    //   // return stateTutorialDesc.segmentDetails?.[segmentId]?.decorations?.[idInInterface] || []
    // }

    let decorations = getDecorationsForID(idInInterface, activeSegments, mouseOverClick, mouseOverNoclick)
    // let decorations = flatten(
    //   activeSegments.filter((x) => x.decorations[] === idInInterface).map((x) => x.decorations)
    // )

    if (decorations.find((x) => x.type === "fill-orange")) {
      refStyle = { ...refStyle, backgroundColor: "orange" }
    }
    // if (decorations.length > 0) debugger
    if (decorations.find((x) => x.type === "fill-white")) {
      refStyle = { ...refStyle, backgroundColor: colorDarkened }
    }

    let getEnterLeave = () => {
      if (t?.options?.tooltip) {
        return tooltipAttributes(t.options.tooltip)
      }
      if (refLookupHighlights && refLookupHighlights[t.id]) {
        return {
          onMouseEnter: () => {
            dispatch({ type: "entered-highlight", tid: t.id })
          },
          onMouseLeave: () => {
            dispatch({ type: "leave-highlight", tid: t.id })
          },
        }
      }
    }

    if (!t.text) debugger
    let style = {
      cursor: !notclickable && "pointer",
      whiteSpace: t.text.trim().length > 0 ? "nowrap" : "pre",
      ...refStyle,
      ...animStyle,
    }

    return (
      <span
        {...getEnterLeave()}
        onClick={tutorialOnClick(
          idInInterface,
          notclickable
            ? undefined
            : () => {
                stopLoop()
                dispatch({ type: "jump-to-before", tid: t.id })
                startPlay()
              }
        )}
        style={style}
        key={t.id}
        ref={(r) => {
          if (t.id === lineMarker) lineMarkerTarget = r
          populateDecorationsRef(idInInterface, r, decorations)
          animRef(r, t.id, { origin: t.origin, copyText: true })
        }}
      >
        {t.text}
      </span>
    )
  }

  let cellOfToken = (idInInterface, tok, opts, span) => {
    let s = spanOfToken(idInInterface, tok)
    return mkMonotd(tok.id + "-wrapping", s, opts, span)
  }

  // let externalVarsContents = undefined
  // if (externalVars.shown.length === 0) {
  //   externalVarsContents = externalVars.hiddenTokens.map((x) => spanOfToken(x))
  // } else {
  //   externalVarsContents = (
  //     <table style={tableStyle}>
  //       {externalVars.shown.map((row) => {
  //         let leftView = cellOfToken(row.leftToken)
  //         let rightView = cellOfToken(collectTokens(row.value)[0])
  //         return (
  //           <tr>
  //             {leftView}
  //             {rightView}
  //           </tr>
  //         )
  //       })}
  //     </table>
  //   )
  // }
  // let externalVarsView = mkBox(
  //   "external-vars",
  //   <div style={{ margin: "10px", whiteSpace: "nowrap" }}>{externalVarsContents}</div>,
  //   { marginBottom: "10px" }
  // )
  let endoflineTokens = animationFrame.endoflineTokens

  let tokenContainsNewline = (t) => {
    return t.text.includes("\n")
  }

  let countNewlines = (str) => {
    return (str.match(/\n/g) || []).length
  }

  let inlineEndoflines = (currentTokens, toBeAdded) => {
    toBeAdded = [...toBeAdded]
    toBeAdded.sort((a, b) => a.lineNumber - b.lineNumber)
    let currentLine = 1
    let ret = []
    for (let i = 0; i < currentTokens.length; i++) {
      let currentToken = currentTokens[i]
      if (tokenContainsNewline(currentToken)) {
        while (toBeAdded.length > 0 && currentLine >= toBeAdded[0].lineNumber) {
          ret.push(toBeAdded[0].whitespaceToken)
          ret.push(toBeAdded[0].token)
          toBeAdded.shift()
        }
      }

      ret.push(currentToken)
      currentLine += countNewlines(currentToken.text)
    }

    while (toBeAdded.length > 0) {
      let n = toBeAdded.pop()
      ret.push(n.whitespaceToken)
      ret.push(n.token)
    }
    return ret
  }

  let finalAnimationTokens = inlineEndoflines(animationTokens, endoflineTokens)

  let animationViewExt = (
    <div style={{ display: "flex" }}>
      <div style={{ display: "flex", marginRight: "40px" }}>
        {/* {leftGutter.length > 0 && <pre style={{ ...preStyle, marginRight: "30px" }}>{leftGutter}</pre>} */}
        <pre style={preStyle}>{finalAnimationTokens.map((tok, ind) => spanOfToken(`source-${ind}`, tok, true, undefined))}</pre>
        {/* {rightGutter.length > 0 && <pre style={{ ...preStyle, marginLeft: "5px" }}>{rightGutter}</pre>} */}
      </div>

      <div>
        {/* {externalVarsView} */}

        {contexts.map((context, contextIndex) => {
          let isolated = undefined
          if (context.isolatedExpression) {
            let tokensOfIsolatedExpression = collectTokens(context.isolatedExpression)

            //if (!context.gobackMarker) debugger
            isolated = (
              <div
                style={{
                  ...onelineStyle,
                  ...codeFont,
                  marginLeft: "20px",
                  marginTop: "20px",
                }}
              >
                <span style={{ marginRight: "20px" }}>
                  {tokensOfIsolatedExpression.map((x, ind) => spanOfToken(`isolated-${contextIndex}-${ind}`, x))}
                </span>
                {context.gobackMarker && spanOfToken(`goback-${contextIndex}`, context.gobackMarker.token)}
              </div>
            )
          }
          let frame = context.frame

          let loops = context.loops
          let loopViews = loops.map((loop, loopIndex) => {
            if (loop.type === "while-loop") {
              let lt = loop.token
              let wkt = loop.whileKeywordToken
              return (
                <div>
                  {spanOfToken(`while-keyword-tok-${contextIndex}-${loopIndex}`, wkt)}
                  {spanOfToken(`while-tok-${contextIndex}-${loopIndex}`, lt)}{" "}
                </div>
              )
            }
            if (loop.type === "except") {
              let lt = loop.token
              let wkt = loop.exceptKeywordToken
              return (
                <div>
                  {spanOfToken(`except-keyword-tok${contextIndex}-${loopIndex}`, wkt)}
                  {spanOfToken(`except-tok-${contextIndex}-${loopIndex}`, lt)}{" "}
                </div>
              )
            }
            if (loop.type !== "for-loop") throw new Error("unrecognized loop " + loop.type)
            let columns = []
            let lb = spanOfToken(`loop-left-bracket-${contextIndex}-${loopIndex}`, loop.leftBracket)
            let rb = spanOfToken(`loop-right-bracket-${contextIndex}-${loopIndex}`, loop.rightBracket)
            let iteratingVariable = loop.iteratingVariable
            let iteratingTokens = collectTokens(iteratingVariable)
            let iteratingVariableView = spanOfToken(`loop-variable-${contextIndex}-${loopIndex}`, iteratingTokens[0])
            columns.push([null, lb])
            for (let i = 0; i < loop.elements.length; i++) {
              let e = loop.elements[i]
              let top = i === loop.index ? iteratingVariableView : null
              let tokens = collectTokens(e)
              let view = spanOfToken(`loop-element-${contextIndex}-${loopIndex}-${i}`, tokens[0])
              let cell = view
              columns.push([top, cell])
              if (i !== loop.elements.length - 1) {
                let commaView = spanOfToken(`loop-comma-${contextIndex}-${loopIndex}-${i}`, loop.commas[i])
                columns.push([null, commaView])
              }
            }
            columns.push([null, rb])

            let rows = transpose(columns)
            return (
              <table style={tableStyle}>
                {rows.map((row) => {
                  return (
                    <tr>
                      {row.map((cell) => {
                        return (
                          <td>
                            <code style={onelineStyle}>{cell}</code>
                          </td>
                        )
                      })}
                    </tr>
                  )
                })}
              </table>
            )
          })

          let labelView = (
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                height: "0px",
              }}
            >
              <div
                style={{
                  position: "relative",
                  bottom: "14px",
                  zIndex: 10,
                  whiteSpace: "pre",
                }}
              >
                {spanOfToken(`context-label-${contextIndex}`, context.label, false, {
                  backgroundColor: colorBackground,
                  zIndex: 10,
                  whiteSpace: "pre",
                })}
              </div>
            </div>
          )

          let enumVars = (vars, kind) => {
            let ret = []
            for (let i = 0; i < vars.length; i++) {
              ret.push(spanOfToken(`outside-var-${contextIndex}-${kind}-${i}`, vars[i].nameToken))
              if (i !== vars.length - 1) ret.push(spanOfToken(vars[i].commaToken))
            }
            return ret
          }

          let nonlocalView = undefined
          if (context.nonlocals.length > 0) {
            nonlocalView = (
              <div>
                {spanOfToken(`nonlocal-label-${contextIndex}`, context.nonlocalToken)} {enumVars(context.nonlocals, "nonlocal")}
              </div>
            )
          }

          let globalVarsView = undefined
          if (context.globals.length > 0) {
            globalVarsView = (
              <div>
                {spanOfToken(`global-label-${contextIndex}`, context.globalToken)} {enumVars(context.globals, "global")}
              </div>
            )
          }

          let contextInnerView = (
            <div style={{ display: "flex", marginTop: "15px" }}>
              <div>
                {nonlocalView}
                {globalVarsView}
                {tableOfFrame(frame, contextIndex === 0, contextIndex)}
                {loopViews}
              </div>
              {isolated}
            </div>
          )

          return <div style={{ padding: "5px" }}>{mkDashedBox(context.id + "-outer-box", contextInnerView, labelView)}</div>
        })}
        {frameHeaven.length > 0 ? (
          <div style={{ display: "flex", alignItems: "center" }}>
            <div
              key={"frame-heaven-left"}
              ref={(r) => animRef(r, "frame-heaven-left")}
              style={{
                flex: 1,
                borderTop: "1px solid",
                height: "1px",
                ...animStyle,
              }}
            />
            <div key={"frame-heaven-center"} ref={(r) => animRef(r, "frame-heaven-center", { copyText: true })} style={{ ...animStyle }}>
              frame heaven
            </div>
            <div style={{ flex: 1 }}>
              <div
                key={"frame-heaven-right"}
                ref={(r) => animRef(r, "frame-heaven-right")}
                style={{
                  flex: 1,
                  borderTop: "1px solid",
                  height: "1px",
                  ...animStyle,
                }}
              />
            </div>
          </div>
        ) : null}
        {frameHeaven.map(tableOfFrame)}
      </div>
      <div style={{ marginLeft: "30px" }}>
        {heap.map((h, heapIndex) => {
          if (h.type === "heap-dict") {
            let label = cellOfToken(`heap-loc-${heapIndex}-heap-label`, h.label, {
              border: "none",
            })
            let address = cellOfToken(`heap-loc-${heapIndex}-heap-address`, h.idToken, {
              border: "none",
              textAlign: "right",
            })

            return (
              <table style={{ ...tableStyle, marginTop: "10px" }}>
                <tr>
                  {label}
                  {address}
                </tr>
                {h.rows.map((row, rowIndex) => {
                  let leftBox = cellOfToken(`heap-loc-${heapIndex}-cell-${rowIndex}-left`, row.key.children[0])
                  let rightBox = cellOfToken(`heap-loc-${heapIndex}-cell-${rowIndex}-right`, row.value.children[0])

                  return (
                    <tr>
                      {leftBox}
                      {rightBox}
                    </tr>
                  )
                })}
              </table>
            )
          } else if (h.type === "heap-array") {
            let label = cellOfToken(`heap-loc-${heapIndex}-label`, h.label, {
              border: "none",
            })
            let address = cellOfToken(`heap-loc-${heapIndex}-heap-address`, h.idToken, {
              border: "none",
              textAlign: "right",
            })
            let rows = h.rows
            let attributes = h.attributes
            let attributeLabel = cellOfToken(`heap-loc-${heapIndex}-attribute-label`, h.attributeLabelToken)
            return (
              <table style={{ ...tableStyle, marginTop: "10px" }}>
                <tr>
                  {label}
                  {address}
                </tr>
                {rows.map((row, rowIndex) => {
                  let leftView = cellOfToken(`heap-loc-${heapIndex}-element-cell-${rowIndex}-left`, row.indexToken)

                  let rightView = cellOfToken(`heap-loc-${heapIndex}-element-cell-${rowIndex}-right`, row.value.children[0])

                  return (
                    <tr>
                      {leftView}
                      {rightView}
                    </tr>
                  )
                })}
                <tr>{attributeLabel}</tr>
                {attributes.map((row, rowIndex) => {
                  let leftView = cellOfToken(`heap-loc-${heapIndex}-attribute-cell-${rowIndex}-left`, row.keyToken)

                  let rightView = cellOfToken(`heap-loc-${heapIndex}-attribute-cell-${rowIndex}-right`, row.value.children[0])

                  return (
                    <tr>
                      {leftView}
                      {rightView}
                    </tr>
                  )
                })}
              </table>
            )
          } else if (h.type === "heap-object") {
            let address = cellOfToken(`heap-loc-${heapIndex}-address`, h.idToken, {
              border: "none",
              textAlign: "right",
            })
            let rows = h.attributes
            let instofTitle = cellOfToken(`heap-loc-${heapIndex}-instanceof-label`, h.instanceOf.titleToken)
            let instofV = cellOfToken(`heap-loc-${heapIndex}-instanceof-value`, h.instanceOf.valueToken)
            let label = cellOfToken(`heap-loc-${heapIndex}-label`, h.label, { border: "none" })
            return (
              <table style={{ ...tableStyle, marginTop: "10px" }}>
                <tr>
                  {label}
                  {address}
                </tr>
                <tr>
                  {instofTitle}
                  {instofV}
                </tr>
                {rows.map((row, rowIndex) => {
                  let leftView = cellOfToken(`heap-loc-${heapIndex}-instance-cell-${rowIndex}-left`, row.keyToken)
                  let rightView = null
                  if (row.value) {
                    rightView = cellOfToken(`heap-loc-${heapIndex}-instance-cell-${rowIndex}-right`, collectTokens(row.value)[0])
                  }
                  return (
                    <tr>
                      {leftView}
                      {rightView}
                    </tr>
                  )
                })}
              </table>
            )
          } else if (h.type === "heap-method") {
            let label = cellOfToken(`heap-loc-${heapIndex}-heap-label`, h.label, {
              border: "none",
            })
            let address = cellOfToken(`heap-loc-${heapIndex}heap-address`, h.idToken, {
              border: "none",
              textAlign: "right",
            })
            let leftView = cellOfToken(`heap-loc-${heapIndex}-func-label`, h.funcToken)
            let rightView = cellOfToken(`heap-loc-${heapIndex}-func-address`, collectTokens(h.funcAddress)[0])
            let boundleftView = cellOfToken(`heap-loc-${heapIndex}-bound-label`, h.boundToToken)
            let boundrightView = cellOfToken(`heap-loc-${heapIndex}-bound-value`, collectTokens(h.boundToValue)[0])

            return (
              <table style={{ ...tableStyle, marginTop: "10px" }}>
                <tr>
                  {label}
                  {address}
                </tr>
                <tr>
                  {leftView}
                  {rightView}
                </tr>
                <tr>
                  {boundleftView}
                  {boundrightView}
                </tr>
              </table>
            )
          } else if (h.type === "heap-function") {
            let label = cellOfToken(`heap-loc-${heapIndex}-heap-label`, h.label, {
              border: "none",
            })
            let address = cellOfToken(`heap-loc-${heapIndex}-heap-address`, h.idToken, {
              border: "none",
              textAlign: "right",
            })

            let funcdefmarker = cellOfToken(`heap-loc-${heapIndex}-funcdef-marker-line`, h.funcdefMarker)

            let closureLabel = cellOfToken(`heap-loc-${heapIndex}-closure-label`, h.closureLabel)
            let closure = cellOfToken(`heap-loc-${heapIndex}-closure-address`, h.closureToken)

            return (
              <table style={{ ...tableStyle, marginTop: "10px" }}>
                <tr>
                  {label}
                  {address}
                </tr>

                <tr>
                  {cellOfToken(`heap-loc-${heapIndex}-funcdef-marker-label`, h.funcdefMarkerLabel)}
                  {funcdefmarker}
                </tr>

                <tr>
                  {closureLabel}
                  {closure}
                </tr>
              </table>
            )
          } else if (h.type === "heap-class") {
            let address = cellOfToken(`heap-loc-${heapIndex}-heap-label`, h.idToken, {
              border: "none",
              textAlign: "right",
            })
            let label = cellOfToken(`heap-loc-${heapIndex}-heap-address`, h.label, {
              border: "none",
            })

            let rows = h.attributes
            let superclassLine = undefined
            if (h.superclasses.length > 0) {
              let superclassesLeft = cellOfToken(`heap-loc-${heapIndex}-superclasses-label`, h.superclassesToken)
              let superclassesRightParts = h.superclasses.map((e, baseind) =>
                collectTokens(e).map((x, ind) => spanOfToken(`heap-loc-${heapIndex}-superclass-ref-${ind}-${baseind}`, x))
              )
              let superclassesRight = intersperse(
                superclassesRightParts,
                h.superclassesCommas.map((x, ind) => spanOfToken(`superclass-${heapIndex}-${ind}`, x))
              )
              let superclassesRightBox = mkMonotd(h.id + "-superclasses-right", superclassesRight)
              superclassLine = (
                <tr>
                  {superclassesLeft}
                  {superclassesRightBox}
                </tr>
              )
            }

            return (
              <table style={{ ...tableStyle, marginTop: "10px" }}>
                <tr>
                  {label}
                  {address}
                </tr>
                {superclassLine}

                {rows.map((row, rowInd) => {
                  let leftView = cellOfToken(`heap-loc-${heapIndex}-attribute-label-${rowInd}`, collectTokens(row.keyToken)[0])
                  let rightView = cellOfToken(`heap-loc-${heapIndex}-attribute-value-${rowInd}`, collectTokens(row.value)[0])
                  return (
                    <tr>
                      {leftView}
                      {rightView}
                    </tr>
                  )
                })}
              </table>
            )
          }
        })}
      </div>
    </div>
  )

  if (stateInitialDrawMode) {
    return renderUI(
      undefined,
      undefined,
      animationFrameData.description.title,
      undefined, // last frame message
      undefined,
      turtle,
      undefined,
      undefined,
      stateTurtleGoal !== undefined
    )
  }

  return renderUI(
    undefined,
    animationViewExt,
    animationFrameTopLeftDescriptionData && animationFrameTopLeftDescriptionData.description,
    animationFrame.lastFrameInstructions, // last frame msg
    undefined,
    turtle,
    stateTurtleGoal,
    undefined,
    true,
    animationTokens
  )
}

export default App
