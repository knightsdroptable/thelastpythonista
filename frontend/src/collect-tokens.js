let collectTokensHelper = (tree, ret) => {
  if (tree.category === "token") {
    if (tree.text.length > 0) ret.push(tree)
    return
  }
  if (!tree.children) {
    console.log("wrong tree", tree)
  }
  tree.children.forEach((c) => {
    collectTokensHelper(c, ret)
  })
}

export let collectTokens = (tree) => {
  let ret = []
  collectTokensHelper(tree, ret)
  return ret
}

// let collectTokensHelper = (tree, ret) => {
//   if (tree.category === "token") {
//     if (tree.text.length > 0) ret.push(tree)
//     return
//   }
//   if (!tree.children) {
//     console.log("wrong tree", tree)
//     debugger
//   }
//   tree.children.forEach((c) => {
//     collectTokensHelper(c, ret)
//   })
// }

// let collectTokens = (tree) => {
//   let ret = []
//   collectTokensHelper(tree, ret)
//   return ret
// }
