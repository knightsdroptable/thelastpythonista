import { dispatch } from "./store"
export let getDecorationsForID = (idInInterface, activeSegments, mouseOverClick, mouseOverNoclick) => {
  let decorations = []

  activeSegments.forEach((x) => {
    let candidate = x?.decorations?.[idInInterface]
    if (candidate) candidate.forEach((x) => decorations.push(x))
  })
  if (mouseOverClick === idInInterface)
    decorations.push({ type: "mouse-over-click", idInInterface: mouseOverClick })
  if (mouseOverNoclick === idInInterface)
    decorations.push({ type: "mouse-over-noclick", idInInterface: mouseOverNoclick })
  return decorations
}

export let getAllDecorations = (activeSegments, mouseOverClick, mouseOverNoclick) => {
  let decorations = []

  activeSegments.forEach((x) => {
    //let candidate = x?.decorations?.[idInInterface]
    let candidate = x?.decorations
    if (!candidate) return
    let vs = Object.values(candidate)
    vs.forEach((arr) => arr.forEach((x) => decorations.push(x)))
  })
  if (mouseOverClick) {
    decorations.push({ type: "mouse-over-click", idInInterface: mouseOverClick })
  }
  if (mouseOverNoclick) {
    decorations.push({ type: "mouse-over-noclick", idInInterface: mouseOverNoclick })
  }

  return decorations
}

let idInInterfaceDomNodeAssociation = {}
export let clearIdInInterfaceDomNodeAssociation = () => {
  idInInterfaceDomNodeAssociation = {}
}
let assocWasUpdated = false

export let getDomNodeFromIdInInterface = (id) => {
  return idInInterfaceDomNodeAssociation[id]
}

export let assocRedraw = () => {
  if (assocWasUpdated) dispatch({ type: "idininterface-assoc-redraw-needed" })
  assocWasUpdated = false
}

export let populateDecorationsRef = (idInInterface, domNode) => {
  if (idInInterfaceDomNodeAssociation[idInInterface] !== domNode) {
    idInInterfaceDomNodeAssociation[idInInterface] = domNode
    assocWasUpdated = true
  }
  // if (decorations.length > 0) dispatch({ type: "idInInterface-domNode-association", domNode, idInInterface })
}
