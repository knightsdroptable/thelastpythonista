import * as randomstring from "randomstring"
import cheermp3 from "./valkyrie.mp3"
import overture1812 from "./overture1812.mp3"

export const codeFont = {
  fontSize: "17px",
  fontFamily: "'Courier', monospace",
}
export const isLocal =
  window.location.port !== "5000" &&
  (window.location.hostname === "localhost" || window.location.hostname === "127.0.0.1" || window.location.hostname.includes("192.168"))

export const pathPrefix =
  window.location.protocol +
  "//" +
  window.location.hostname +
  (window.location.port ? ":" + window.location.port : "") +
  (isLocal ? "" : "")

let getSessionFromAddressBar = () => {
  const urlParams = new URLSearchParams(window.location.search)
  const myParam = urlParams.get("session")

  return myParam
}

let getExerciseIdFromAddressBar = () => {
  const urlParams = new URLSearchParams(window.location.search)
  const myParam = urlParams.get("exerciseid")

  return myParam

  // let path = window.location.pathname
  // let parts = path.split("/")
  // let ind = parts.findIndex((x) => x === "exerciseid")
  // if (ind === -1) return undefined
  // let candidate = parts[ind + 1]

  // return candidate
}

export let sessionId =
  getSessionFromAddressBar() ||
  randomstring.generate({
    length: 32,
    charset: "alphanumeric",
  })

export let explicitExId = getExerciseIdFromAddressBar()

export let loadedFromLink = getSessionFromAddressBar() !== undefined

export let currentViewLocalStorageId = isLocal ? "__currentView" : "__currentView_" + sessionId

export let hardcodedVideos = {
  "syntax-error-video": {
    youtubeID: "rnv4j9gRBEo",
    desc: "What is a syntax error?",
  },
  "draw-mismatch": {
    youtubeID: "4PZFyeXsE20",
    desc: "What is a draw mismatch?",
  },
  "drew-too-little": { youtubeID: "XcsvDhEznrA", desc: "What does it mean to draw too little?" },
  "drew-too-much": { youtubeID: "ka2LRhknlzE", desc: "What does it mean to draw too much?" },
  "runtime-error": { youtubeID: "8TueLx7mrY4", desc: "What is a runtime error?" },
  "over-token-quota": { youtubeID: "mofw404xErc", desc: "You used more tokens than allowed." },
}

export let mouseMoveDefaultTime = 2000

export let cheeraudio = new Audio(cheermp3)
cheeraudio.volume = 0.6

export let overturemp3 = new Audio(overture1812)
overturemp3.volume = 0.4
