let waitingEmissions = {}
export let wakeup = (str) => {
  let lst = waitingEmissions[str]
  if (!lst) return
  for (let i = 0; i < lst.length; i++) {
    lst[i]()
  }
  waitingEmissions[str] = []
}
export let hibernate = (str) => {
  return new Promise((res) => {
    if (!waitingEmissions[str]) {
      waitingEmissions[str] = []
    }
    waitingEmissions[str].push(res)
  })
}
