import { useSelector } from "react-redux"
import { seenVideoHistory } from "./store"
import React, { useState } from "react"
import produce from "immer"
export default function VideoHistory({ closeCB }) {
  let content = useSelector((c) => c.content)
  const [state, setState] = useState({
    selected: undefined,
    desc: undefined,
  })
  return (
    <div
      style={{
        display: "flex",
        position: "fixed",
        height: "100vh",
        width: "100vw",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <div style={{ border: "1px solid", height: "600", position: "relative" }}>
        <div
          onClick={closeCB}
          style={{
            position: "absolute",
            top: "5px",
            right: "5px",
            cursor: "pointer",
          }}
        >
          X
        </div>
        <div
          style={{
            height: "50px",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <div>{state.desc ? state.desc : null}</div>
        </div>
        <div style={{ display: "flex", height: "400px" }}>
          <div
            style={{
              width: "650px",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            {state.selected && (
              <iframe
                style={{ marginBottom: "30px" }}
                width="560"
                height="315"
                src={"https://www.youtube.com/embed/" + state.selected}
                frameborder="0"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowFullScreen
              ></iframe>
            )}
          </div>
          <div style={{ padding: "20px 20px 0px 0px", maxWidth: "300px" }}>
            {seenVideoHistory.map((e) => {
              return (
                <div
                  onClick={() => {
                    setState((state) =>
                      produce(state, (state) => {
                        state.selected = e.youtubeID
                        state.desc = e.descObj.description
                      })
                    )
                  }}
                  style={{
                    cursor: "pointer",
                    border: "1px solid",
                    margin: "2px",
                    textOverflow: "ellipsis",
                    whiteSpace: "nowrap",
                    overflow: "hidden",
                  }}
                >
                  {e.descObj.description}
                </div>
              )
            })}
          </div>
        </div>
      </div>
    </div>
  )
}
