import produce from "immer"

import * as uniqid from "uniqid"
let eventIdCounter = 0
let genEventId = () => {
  return uniqid()
}

let genSegmentId = () => {
  return uniqid()
}

export let addTutorialTrack = (tdesc) => {
  return produce(tdesc, (tdesc) => {
    tdesc.numTracks = tdesc.numTracks + 1
  })
}

export let toggleDecorationToSegment = (content, segmentId, idInInterface, decorationType) => {
  let currentDetails = content.segmentDescriptions[segmentId] || {}
  let decorations = currentDetails.decorations || {}
  let specificDecorations = decorations[idInInterface] || []
  if (specificDecorations.includes(idInInterface)) {
    if (content.segmentDescriptions[segmentId].decorations[idInInterface].includes(decorationType)) {
      let newSpecifics = content.segmentDescriptions[segmentId].decorations[idInInterface].filter(
        (z) => z !== decorationType
      )
      return produce(content, (content) => {
        content.segmentDescriptions[segmentId].decorations[idInInterface] = newSpecifics
      })
    }
  }

  return produce(content, (content) => {
    if (!content.segmentDescriptions[segmentId]) content.segmentDescriptions[segmentId] = {}
    if (!content.segmentDescriptions[segmentId].decorations)
      content.segmentDescriptions[segmentId].decorations = {}
    if (!content.segmentDescriptions[segmentId].decorations[idInInterface])
      content.segmentDescriptions[segmentId].decorations[idInInterface] = []
    if (
      content.segmentDescriptions[segmentId].decorations[idInInterface].some((x) => x.type === decorationType)
    )
      return
    content.segmentDescriptions[segmentId].decorations[idInInterface].push({
      idInInterface,
      type: decorationType,
    })
  })
}

export let trackSegmentIntersect = (from1, to1, from2, to2) => {
  // check if from1 is between from2 and to2
  let between = (x, a, b) => {
    return x >= a && x <= b
  }
  return between(from1, from2, to2) || between(to1, from2, to2)
}

export let intersectsWithSomething = (seg, segments) => {
  for (let i = 0; i < segments.length; i++) {
    let trackStart = segments[i].from
    let trackEnd = segments[i].to
    if (
      segments[i].trackIndex === seg.trackIndex &&
      trackSegmentIntersect(trackStart, trackEnd, seg.from, seg.to)
    )
      return true
  }
  return false
}

export let delSegment = (tdesc, segmentId) => {
  return produce(tdesc, (tdesc) => {
    delete tdesc.trackSegments[segmentId]
  })
}

// export let setSegmentDarken = (tdesc, segmentId, value) => {
//   let previousValue = tdesc.segmentDetails[segmentId] || {}
//   return produce(tdesc, (tdesc) => {
//     tdesc.segmentDetails[segmentId] = { ...previousValue, darken: value }
//   })
// }

// export let clearDecorations = (tdesc, segmentId) => {
//   return produce(tdesc, (tdesc) => {
//     let previousValue = tdesc.segmentDetails[segmentId] || {}
//     return produce(tdesc, (tdesc) => {
//       tdesc.segmentDetails[segmentId] = { ...previousValue, decorations: [] }
//     })
//   })
// }

export let rowIsDeletable = (tdesc, rowIndex) => {
  let segmentIds = Object.keys(tdesc.trackSegments)
  return segmentIds.every((s) => {
    let seg = tdesc.trackSegments[s]
    return seg.from !== rowIndex && seg.to !== rowIndex
  })
}

export let delEvent = (tdesc, rowIndex) => {
  let event = tdesc.eventOrder[rowIndex]
  if (!event) throw new Error("huh")
  let segmentIds = Object.keys(tdesc.trackSegments)
  return produce(tdesc, (tdesc) => {
    tdesc.eventOrder.splice(rowIndex, 1)

    segmentIds.forEach((s) => {
      let seg = tdesc.trackSegments[s]
      if (seg.from > rowIndex) seg.from--
      if (seg.to > rowIndex) seg.to--
    })
    tdesc.numRows--
  })
}

export let changeEventType = (content, eventId, newType) => {
  return produce(content, (content) => {
    content.eventDescriptions[eventId].type = newType
  })
}

export let trackCellClicked = (tdesc, rowIndex, trackIndex) => {
  let newSegment = { from: rowIndex, to: rowIndex, trackIndex }
  if (intersectsWithSomething(newSegment, Object.values(tdesc.trackSegments))) return { id: undefined, tdesc }
  let id = genSegmentId()
  return {
    id,
    tdesc: produce(tdesc, (tdesc) => {
      tdesc.trackSegments[id] = { id, ...newSegment }
    }),
  }
}

export let resizeSegmentDragStart = (tdesc, segmentId, segmentPart) => {
  return produce(tdesc, (tdesc) => {
    tdesc.dragStart = {
      segmentId: segmentId,
      type: "resize-segment",
      segmentPart,
    }
  })
}

export let moveSegmentDragStart = (tdesc, segmentId, segmentPart) => {
  return produce(tdesc, (tdesc) => {
    tdesc.dragStart = {
      segmentId: segmentId,
      type: "move-segment",
      segmentPart,
    }
  })
}

export let moveEventDragStart = (tdesc, eventId) => {
  return produce(tdesc, (tdesc) => {
    tdesc.dragStart = {
      eventId,
      type: "move-event",
    }
  })
}

export let segmentDrop = (td, rowIndex, trackIndex) => {
  let dragStart = td.dragStart
  td = produce(td, (td) => {
    td.dragStart = undefined
  })
  if (!dragStart || !["move-segment", "resize-segment"].includes(dragStart.type)) return td
  let segment = td.trackSegments[dragStart.segmentId]
  if (!segment) throw new Error("huh")
  if (dragStart.type === "resize-segment" && segment.trackIndex !== trackIndex) return td

  if (dragStart.type === "resize-segment") {
    if (dragStart.segmentPart === "from" && rowIndex > segment.to) return td
    if (dragStart.segmentPart === "to" && rowIndex < segment.from) return td

    let createNewSegment = (segment) => {
      return dragStart.segmentPart === "from" ? { ...segment, from: rowIndex } : { ...segment, to: rowIndex }
    }

    let newSegment = createNewSegment(segment)
    if (
      intersectsWithSomething(
        newSegment,
        Object.values(td.trackSegments).filter((x) => x.id !== segment.id)
      )
    )
      return td
    return produce(td, (td) => {
      td.trackSegments[segment.id] = newSegment
    })
  }
  if (dragStart.type === "move-segment") {
    let newSegment = {
      ...segment,
      trackIndex,
      from: rowIndex,
      to: rowIndex + (segment.to - segment.from),
    }
    if (
      intersectsWithSomething(
        newSegment,
        Object.values(td.trackSegments).filter((x) => x.id !== segment.id)
      )
    )
      return td
    return produce(td, (td) => {
      td.trackSegments[segment.id] = newSegment
    })
  }
}

export let eventDrop = (tdesc, rowIndex) => {
  let dragStart = tdesc.dragStart
  tdesc = produce(tdesc, (td) => {
    td.dragStart = undefined
  })
  if (!dragStart || dragStart.type !== "move-event") return tdesc
  let indexOfOrigin = tdesc.eventOrder.findIndex((x) => x === dragStart.eventId)
  if (indexOfOrigin === rowIndex) return tdesc
  return produce(tdesc, (tdesc) => {
    tdesc.eventOrder.splice(indexOfOrigin, 1)
    tdesc.eventOrder.splice(rowIndex, 0, dragStart.eventId)
  })
}

export let segmentDel = (tdesc, rowIndex) => {
  return produce(tdesc, (tdesc) => {
    tdesc.eventOrder.splice(rowIndex, 1)
  })
}

export let addTutorialRow = (tdesc) => {
  return produce(tdesc, (tdesc) => {
    tdesc.numRows++
  })
}

export let removeRow = (tdesc) => {
  if (tdesc.numRows <= 1) return tdesc
  return produce(tdesc, (tdesc) => {
    tdesc.numRows--
  })
}

// export let setEventDetails = (tdesc, details) => {
//   return produce(tdesc, (tdesc) => {
//     tdesc.events[details.id] = details
//   })
// }
