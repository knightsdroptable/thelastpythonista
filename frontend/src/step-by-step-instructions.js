export const LIST_APPEND = {
  name: "LIST_APPEND",
  text: "We append an element to a list object and thereby increase the list's size.",
}

export const LIST_POP_NOARG = {
  name: "LIST_POP_NOARG",
  text: "We pop the last element of the list.",
}

export const LIST_POP_ARG = {
  name: "LIST_POP_ARG",
  text: "We pop an element at a specific index of the list.",
}

export const RANGE_LIST_OBJECT_CREATED = {
  name: "RANGE_LIST_OBJECT_CREATED",
  text: "range creates a list and returns a reference to that list.",
}

export const RUNTIME_ERROR = {
  name: "RUNTIME_ERROR",
  text: "A runtime error has occurred. It is displayed on the top right corner of the screen.",
}

export const DREW_TOO_MUCH = {
  name: "DREW_TOO_MUCH",
  text: "Too much was drawn.",
}

export const DREW_TOO_LITTLE = {
  name: "DREW_TOO_LITTLE",
  text: "You're missing elements from your drawing.",
}

export const INCORRECT_DRAWING = {
  name: "INCORRECT_DRAWING",
  text: "Your drawing is incorrect.",
}

export const MY_BAD = {
  name: "MY_BAD",
  text: "Jacques made a mistake. Please contact Jacques to tell him.",
}

export const TURTLE_FORWARD = {
  name: "TURTLE_FORWARD",
  text: "The turtle moves forward.",
}

export const TURTLE_RIGHT = {
  name: "TURTLE_RIGHT",
  text: "The turtle turns right.",
}

export const TURTLE_LEFT = {
  name: "TURTLE_LEFT",
  text: "The turtle turns left.",
}

export const PENUP = {
  name: "PENUP",
  text: "The pen is lifted. The turtle is no longer drawing.",
}

export const PENDOWN = {
  name: "PENDOWN",
  text: "The pen is placed on the paper. The turtle is drawing again.",
}

export const TURTLE_CIRCLE = {
  name: "TURTLE_CIRCLE",
  text: "The turtle draws an arc",
}

export const GOBACK_MARKER_GUTTER = {
  name: "GOBACK_MARKER_GUTTER",
  text:
    "We mark the line that we are isolating with an identifier so that we can jump back to it once the expression is evaluated.",
}

export const HIGHLIGHT_RETURN = {
  name: "HIGHLIGHT_RETURN",
  text: "We look for the identifier that corresponds to our line.",
}

export const REINTEGRATE_EXPRESSION = {
  name: "REINTEGRATE_EXPRESSION",
  text: "We replace the line with the computed value of the expression.",
}

export const EMPHASIZE_CLASSFRAME = {
  name: "EMPHASIZE_CLASSFRAME",
  text:
    "Since the current frame is a class frame, it cannot be in the closure of a function. Therefore, we take the current frame's closure as the closure of the function.",
}

export const HIGHLIGHT_LIST_OBJECT = {
  name: "HIGHLIGHT_LIST_OBJECT",
  text: "We look in the heap for the object at the reference's address. It is a list object.",
}

export const HIGHLIGHT_LIST_INDEX = {
  name: "HIGHLIGHT_LIST_INDEX",
  text: "We look for the index indicated by the value inside the brackets.",
}

export const HIGHLIGHT_DICT_OBJECT = {
  name: "HIGHLIGHT_DICT_OBJECT",
  text: "We look in the heap for the object at the reference's address. It is a dictionary object.",
}

export const HIGHLIGHT_DICT_KEY = {
  name: "HIGHLIGHT_DICT_KEY",
  text: "We look for the key equal to the value inside the brackets.",
}

export const HIGHLIGHT_VAR_IN_FRAME = {
  name: "HIGHLIGHT_VAR_IN_FRAME",
  text: "We look for the variable in the frame.",
}

export const REPLACE_VAR = {
  name: "REPLACE_VAR",
  text:
    "We replace the value of the variable in the frame with the value on the right hand side of the equal sign in our statement.",
}

export const PLACE_VAR = {
  name: "PLACE_VAR",
  text: "We add the variable to the current frame.",
}

export const NEXT_LINE = {
  name: "NEXT_LINE",
  text: "The next line is interpreted",
}

export const FILL_HOLE_NONE = {
  name: "FILL_HOLE",
  text:
    "Since the return statement does not have an expression, it fills the hole that was previously created with the value None.",
}

export const FILL_HOLE_EXP = {
  name: "FILL_HOLE_EXP",
  text: "The hole that was previously created is replaced with the value to the right of the return keyword.",
}

export const HIGHLIGHT_NONLOCAL = {
  name: "HIGHLIGHT_NONLOCAL",
  text: "It is a nonlocal variable, so we look for it in the closure of the current frame.",
}

export const HIGHLIGHT_NONLOCAL_ROW = {
  name: "HIGHLIGHT_NONLOCAL_ROW",
  text: "We find the variable.",
}

export const UPDATE_NONLOCAL_ROW = {
  name: "UPDATE_NONLOCAL_ROW",
  text: "We update the value of the variable with the value on the right hand side of the equal symbol.",
}

export const HIGHLIGHT_GLOBAL = {
  name: "HIGHLIGHT_GLOBAL",
  text: "It is a global variable, so we look for it in the global frame.",
}

export const HIGHLIGHT_GLOBAL_ROW = {
  name: "HIGHLIGHT_GLOBAL_ROW",
  text: "We find the variable.",
}

export const UPDATE_GLOBAL_ROW = {
  name: "UPDATE_GLOBAL_ROW",
  text: "We update the value of the variable with the value on the right hand side of the equal symbol.",
}

export const LIST_UPDATED = {
  name: "LIST_UPDATED",
  text: "The list has been updated.",
}

export const RESET_STATEMENT = {
  name: "RESET_STATEMENT",
  text: "The statement is reset to its original state.",
}

export const SUPERCLASS_LIST_RESET = {
  name: "SUPERCLASS_LIST_RESET",
  text: "The superclass expressions are reset to their original states.",
}

export const ISOLATED_SO_RESET_EXPR = {
  name: "ISOLATED_SO_RESET_EXPR",
  text: "The expression was isolated so it is reset in the sourcecode.",
}
export const RESET_FOR_LOOP = {
  name: "RESET_FOR_LOOP",
  text: "The expression before the colon is reset to its original state.",
}

export const RESET_ELIF = {
  name: "RESET_ELIF",
  text: "The condition expression is reset to its original state.",
}

export const ATTRIBUTE_UPDATED = {
  name: "ATTRIBUTE_UPDATED",
  text: "The value of the attribute is updated.",
}

export const POPULATE_EXTERNAL = {
  name: "POPULATE_EXTERNAL",
  text: "We add the variables of the package to the external variable section of our interface.",
}

export const HIDE_EXTERNAL = {
  name: "HIDE_EXTERNAL",
  text: "We hide the external variable section of our interface to save space.",
}

export const CREATE_CLASSFRAME = {
  name: "CREATE_CLASSFRAME",
  text:
    "We create a frame. In the future, the variables in this frame will become attributes of a class object in the heap.",
}

export const CLASS_IN_HEAP_CREATED = {
  name: "CLASS_IN_HEAP_CREATED",
  text: "We create a class object in the heap.",
}

export const CLASS_CONTEXT_POPPED = {
  name: "CLASS_CONTEXT_POPPED",
  text: "The class context is no longer needed, so it is destroyed.",
}

export const CLASSDEF_REF_FRAME = {
  name: "CLASSDEF_REF_FRAME",
  text:
    "A variable is added to the current frame. The value of that variable is a reference to the class object.",
}

export const ADD_FUNCDEF_MARKER = {
  name: "ADD_FUNCDEF_MARKER",
  text:
    "A marker is added to the function definition line. That marker will be used to associate function objects with the function definition.",
}

export const CREATE_FUNCTION = {
  name: "CREATE_FUNCTION",
  text: "A function object is created.",
}

export const FUNCDEF_POPULATE_FRAME = {
  name: "FUNCDEF_POPULATE_FRAME",
  text:
    "A variable with the funtion's name is added to the current frame. The value of that variable is a reference to the function object.",
}

export const FOR_ITERATOR_CONTEXT = {
  name: "FOR_ITERATOR_CONTEXT",
  text:
    "The list and the variable are copied into the current context to keep track of the value of the variable.",
}

export const FOR_NEXT_ELEM = {
  name: "FOR_NEXT_ELEM",
  text: "The variable moves on to the next element in the list.",
}

export const FOR_LOOP_POP = {
  name: "FOR_LOOP_POP",
  text: "The for loop is done so we no longer need the list in the context.",
}

export const WHILE_COND_RESET = {
  name: "WHILE_COND_RESET",
  text: "The condition expression is reset to its original state.",
}

export const LINEMARKER_ELIF = {
  name: "LINEMARKER_ELIF",
  text: "We move the line marker.",
}

export const RESTORED_ORIGINAL_STATE_EXCEPT = {
  name: "RESTORED_ORIGINAL_STATE_EXCEPT",
  text: "We undo all the work we did until the try block.",
}

export const EXTERNAL_FUNC_FINISHED = {
  name: "EXTERNAL_FUNC_FINISHED",
  text: "We replace the function call with the return value.",
}

export const DETACH_EXPR = {
  name: "DETACH_EXPR",
  text: "We will move the expression to another part of our interface so that we can focus on it.",
}

export const HIGHLIGHT_METHOD_CALL = {
  name: "HIGHLIGHT_METHOD_CALL",
  text: "We're calling a method, so there is an implicit first argument to the function call.",
}

export const HIGHLIGHT_FUNCTION_CALL = {
  name: "HIGHLIGHT_FUNCTION_CALL",
  text:
    "We find the function object because it contains two important pieces of information: the function definition line and the closure.",
}

export const HIGHLIGHT_FUNCALL_LINE = {
  name: "HIGHLIGHT_FUNCALL_LINE",
  text:
    "According to our function object, this is the function definition line, which determines the parameters and the statements to be interpreted.",
}

export const MAKE_HOLE = {
  name: "MAKE_HOLE",
  text: "This newly created hole symbol will be replaced with the function call return value.",
}

export const PUSHED_CONTEXT = {
  name: "PUSHED_CONTEXT",
  text: "We add a new context which contains a frame to store our local variables and their values.",
}

export const NO_RETURN = {
  name: "NO_RETURN",
  text:
    "The function did not return, so the return value is None. The hole symbol is replaced with this value.",
}

export const ADD_TO_FRAMEHEAVEN = {
  name: "ADD_TO_FRAMEHEAVEN",
  text:
    "The frame is in the closure of some function. When that function gets called, it may refer to variables in that frame, so we place the frame in frame heaven in case that happens.",
}

export const POPPED_CONTEXT = {
  name: "POPPED_CONTEXT",
  text: "The body of the function has finished evaluating, so we remove the context.",
}

export const POPPED_CONTEXT_NO_HEAVEN = {
  name: "POPPED_CONTEXT_NO_HEAVEN",
  text:
    "The body of the function has finished evaluating so the local variables are no longer accessible so we destroy the current context.",
}

export const BINARY_DONE = {
  name: "BINARY_DONE",
  text: "The binary expression is evaluated",
}

export const OR_REDUCTION = {
  name: "OR_REDUCTION",
  text:
    "The expression on the left of the or operator evaluated to falsy, so the expression is replaced with what is on the right hand side of the or expression",
}

export const AND_REDUCTION = {
  name: "AND_REDUCTION",
  text:
    "The expression on the left of the or operator evaluated to truthy, so the expression is replaced with what is on the right hand side of the or expression",
}

export const INSTANCE_CREATED = {
  name: "INSTANCE_CREATED",
  text: "An instance object is created in the heap.",
}

export const OBJECT_REF = {
  name: "OBJECT_REF",
  text: "The expression is replaced by a reference to the object in the heap.",
}

export const POPULATE_ONE_LEVEL = {
  name: "POPULATE_ONE_LEVEL",
  text: "The superclass attributes are copied to the new object.",
}

export const METHODS_CREATED = {
  name: "METHODS_CREATED",
  text: "For every attribute that contains a function reference, we create a method object.",
}

export const METHODS_REFS_CREATED = {
  name: "METHODS_REFS_CREATED",
  text:
    "For every attribute that contains a function reference, we replace its value with the corresponding method reference.",
}

export const NEW_LIST = {
  name: "NEW_LIST",
  text: "A list object is created in the heap.",
}
export const LIST_REF = {
  name: "LIST_REF",
  text: "The list literal expression is replaced with a reference to the list in the heap.",
}

export const NEW_DICT = {
  name: "NEW_DICT",
  text: "A new dictionary is created in the heap.",
}

export const DICT_REF = {
  name: "DICT_REF",
  text: "The dictionary literal is replaced with a reference to the dictionary object in the heap.",
}

export const HIGHLIGHT_LIST_LOOKUP = {
  name: "HIGHLIGHT_LIST_LOOKUP",
  text: "Using the reference, we find the object in the heap. It is a list",
}
export const HIGHLIGHT_LIST_LOOKUP_INNER = {
  name: "HIGHLIGHT_LIST_LOOKUP_INNER",
  text: "Using the index inside the brackets, we locate the element of the list that we're retrieving.",
}

export const BRACKET_LOOKUP_DONE = {
  name: "BRACKET_LOOKUP_DONE",
  text: "We replace the bracket expression with the value in the list object.",
}

export const HIGHLIGHT_DICT_LOOKUP = {
  name: "HIGHLIGHT_DICT_LOOKUP",
  text: "Using the reference, we find the object in the heap. It is a dict",
}
export const HIGHLIGHT_DICT_LOOKUP_INNER = {
  name: "HIGHLIGHT_DICT_LOOKUP_INNER",
  text: "Using the value inside the brackets, we locate the element of the dict that we're retrieving.",
}

export const DICT_BRACKET_LOOKUP_DONE = {
  name: "DICT_BRACKET_LOOKUP_DONE",
  text: "We replace the bracket expression with the value in the dict object.",
}

export const HIGHLIGHT_ATTRIBUTE_LOOKUP = {
  name: "HIGHLIGHT_ATTRIBUTE_LOOKUP",
  text: "Using the reference, we find the object in the heap.",
}

export const HIGHLIGHT_ATTRIBUTE_LOOKUP_NAME = {
  name: "HIGHLIGHT_ATTRIBUTE_LOOKUP_NAME",
  text: "Using the identifier beside the dot, we locate the attribute of the object that we're retrieving.",
}

export const ATTRIBUTE_LOOKUP_DONE = {
  name: "ATTRIBUTE_LOOKUP_DONE",
  text: "We replace the bracket expression with the value in the object.",
}

export const TERNARY_COND = {
  name: "TERNARY_COND",
  text: "We evaluate the condition expression of the ternary operator.",
}

export const TERNARY_TRUE = {
  name: "TERNARY_TRUE",
  text: "The condition expression evaluated to truthy, so we only evaluate the middle expression.",
}

export const TERNARY_FALSE = {
  name: "TERNARY_FALSE",
  text: "The condition expression evaluated to false, so we only evaluate the last expression.",
}

export const VAR_LOOKUP_HIGHLIGHT = {
  name: "VAR_LOOKUP_HIGHLIGHT",
  text: "We find the variable in the current frame or a closure of the current frame.",
}
export const VAR_LOOKUP = {
  name: "VAR_LOOKUP",
  text: "We replace the value with its associated value in the frame.",
}

export const VAR_LOOKUP_HIGHLIGHT_EXTERNAL = {
  name: "VAR_LOOKUP_HIGHLIGHT_EXTERNAL",
  text: "We find the variable in the external variables section of our interface.",
}
export const VAR_LOOKUP_EXTERNAL = {
  name: "VAR_LOOKUP_EXTERNAL",
  text: "We replace the variable with its associated value.",
}

export const PAREN = {
  name: "PAREN",
  text: "What's inside the parenthesis becomes the expression we're evaluating.",
}

export const NOT_EXPR = {
  name: "NOT_EXPR",
  text: "We apply the not operator, which evaluates falsy values to true and truthy values to false.",
}
