import AudioRecorder from "audio-recorder-polyfill"
import React, { useEffect, useState } from "react"
import produce from "immer"
import { useSelector } from "react-redux"
import { dispatch } from "./store"
window.MediaRecorder = AudioRecorder

export const recordAudio = (deviceId) => {
  if (!deviceId) throw new Error("must supply device id")
  return new Promise((resolve) => {
    let constraints = { audio: { deviceId: { exact: deviceId } } }

    navigator.mediaDevices.getUserMedia(constraints).then((stream) => {
      const mediaRecorder = new MediaRecorder(stream)
      const audioChunks = []

      mediaRecorder.addEventListener("dataavailable", (event) => {
        audioChunks.push(event.data)
      })

      const start = () => {
        mediaRecorder.start()
      }

      const stop = () => {
        return new Promise((resolve) => {
          mediaRecorder.addEventListener("stop", () => {
            const audioBlob = new Blob(audioChunks, { type: "audio/mpeg" })
            const audioUrl = URL.createObjectURL(audioBlob)
            const audio = new Audio(audioUrl)
            const play = () => {
              audio.play()
            }

            resolve({ audioBlob, audioUrl, play })
          })

          mediaRecorder.stop()
          stream
            .getTracks() // get all tracks from the MediaStream
            .forEach((track) => track.stop()) // stop each of them
        })
      }

      resolve({ start, stop })
    })
  })
}

// const stopRecording = async (state) => {
//   setState(
//     produce(state, (state) => {
//       state.uploading = true
//     })
//   )

//   let { audioBlob, audioUrl } = await state.recording.stop()
//   let idx = state.recording.idx
//   let resp = await post("/upload-wav", {
//     wav: audioBlob,
//   })
//   resp = JSON.parse(resp)
//   let soundId = resp.id
//   let newState = produce(state, (state) => {
//     state.uploading = false
//     state.data[idx].soundId = soundId
//     state.data[idx].soundURL = audioUrl
//     state.recording = undefined
//   })

//   setState(newState)
//   save(newState)
// }

let getMicChoices = () => {
  return new Promise((res, rej) => {
    function gotDevices(deviceInfos) {
      let devices = []
      for (let i = 0; i !== deviceInfos.length; ++i) {
        var deviceInfo = deviceInfos[i]
        if (deviceInfo.kind === "audioinput") {
          devices.push({ label: deviceInfo.label || "Microphone " + i, deviceId: deviceInfo.deviceId })
        }
        // if (deviceInfo.kind === 'audioinput') {
        //     console.log("device", deviceInfo.label || 'Microphone ' + i,
        //         deviceInfo.deviceId);
        // }
      }
      res(devices)
    }
    navigator.mediaDevices.getUserMedia({ audio: true }).then((stream) => {
      navigator.mediaDevices
        .enumerateDevices()
        .then(gotDevices)
        .then(() => {
          stream
            .getTracks() // get all tracks from the MediaStream
            .forEach((track) => track.stop()) // stop each of them
        })
    })
  })
  //.catch(errorCallback)
}

export function MicSelect({ onClose }) {
  const currentMic = useSelector((s) => s.mic)
  const [state, setState] = useState({})
  useEffect(() => {
    let run = async () => {
      let choices = await getMicChoices()
      setState((state) =>
        produce(state, (state) => {
          state.choices = choices
        })
      )
    }
    run()
  }, [])
  if (!state.choices) return null
  return (
    <div
      style={{
        position: "fixed",
        top: "0px",
        left: "0px",
        right: "0px",
        bottom: "0px",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "white",
      }}
    >
      <div style={{ width: "500px" }}>
        <div style={{ textalign: "center" }}>Select a microphone</div>
        {(state.choices || [])
          .map((choice, i) => {
            if (!choice.deviceId) return undefined
            return (
              <div>
                <input
                  type="radio"
                  onClick={() => {
                    window.localStorage.setItem("lastMicDeviceId", choice.deviceId)
                    window.localStorage.setItem("lastMicDeviceLabel", choice.label)
                    dispatch({ type: "set-mic", mic: choice })
                  }}
                  checked={currentMic?.deviceId === choice.deviceId}
                />
                {choice.label}
              </div>
            )
          })
          .filter((x) => x)}
        <div style={{ marginTop: "30px", display: "flex", justifyContent: "center" }}>
          <button
            onClick={() => {
              onClose()
            }}
          >
            close
          </button>
        </div>
      </div>
    </div>
  )
}
