import React, { useEffect, useState } from "react"
import produce from "immer"
import { totalNumberExercises } from "./store.js"
import { colorModalBlanket } from "./colors.js"
export default function IncrementNumCompleted({ num, cb }) {
  const [state, setState] = useState({
    mode: "start",
  })

  useEffect(() => {
    let initialCB = () => {
      setState((state) =>
        produce(state, (state) => {
          state.mode = "processing"
        })
      )
    }
    setTimeout(initialCB, 1500)

    let cb2 = () => {
      setState((state) =>
        produce(state, (state) => {
          state.mode = "done"
        })
      )
      if (cb) cb()
    }

    setTimeout(cb2, 3500)
  }, [])

  if (state.mode === "done") return null
  return (
    <div
      style={{
        display: "flex",
        position: "fixed",
        height: "100vh",
        width: "100vw",
        justifyContent: "center",
        alignItems: "center",
        zIndex: 500000,
        backgroundColor: colorModalBlanket,
      }}
    >
      <div style={{ fontSize: "35px" }}>
        <div style={{ textAlign: "center" }}>Exercises completed</div>
        <div
          style={{
            display: "flex",
            jusitfyContent: "center",
            marginTop: "15px",
          }}
        >
          <div style={{ position: "relative" }}>
            <div
              style={{
                position: "absolute",
                opacity: state.mode === "start" ? "0" : "1",
                left: state.mode === "start" ? "-300px" : "0px",
                transition: "left 2s, opacity 0.5s",
                transitionTimingFunction: "linear",
              }}
            >
              {num + 1}
            </div>
            <div
              style={{
                opacity: state.mode === "start" ? 1 : 0,
                transition: "opacity 1s",
              }}
            >
              {num}
            </div>
          </div>
          /{totalNumberExercises}
        </div>
      </div>
    </div>
  )
}
