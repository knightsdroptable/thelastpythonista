import React from "react"
import { addVideoPermanentHistory } from "./store"
import { colorModalBlanket } from "./colors"
import monaButton from "./buttonnotext.png"
//729 × 272

let centeredButton = (label, action) => {
  return (
    <div style={{ display: "flex", justifyContent: "center" }}>
      <button
        onClick={() => {
          if (action) action()
        }}
        class={"flatbutton"}
        style={{
          height: `65px`,
          width: `350px`,
          padding: "6px",
          cursor: "pointer",
          outline: "none",
          border: "none",
          // backgroundColor: colorBackground,

          fontSize: "22px",

          letterSpacing: "2px",
        }}
      >
        {label}
      </button>
    </div>
  )
}

export default function YoutubePopup({
  youtubeID,
  below,
  coverEverything,
  cb,
  id,
  descObj,
  noIdChild,
  above,
}) {
  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        height: "100vh",
        width: "100vw",
        position: "fixed",
        backgroundColor: colorModalBlanket,
        zIndex: coverEverything ? 1000 : 500,
        flexDirection: "column",
      }}
    >
      {youtubeID && (
        <div style={{ marginBottom: "20px", width: "400px", textAlign: "center", maxWidth: "100vw" }}>
          Watch this video and then click on the button below the video to move to the next step
        </div>
      )}
      <div
        className="youtube-iframe-container"
        style={{
          backgroundColor: colorModalBlanket,
          boxShadow: "0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23)",
          //boxShadow: "0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23)",

          borderRadius: "4px",
          marginBottom: "30px",
          maxWidth: "100%",
        }}
      >
        {youtubeID ? (
          <iframe
            className="youtube-iframe"
            src={"https://www.youtube.com/embed/" + youtubeID}
            frameborder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
          ></iframe>
        ) : (
          noIdChild
        )}
      </div>
      {centeredButton(below, () => {
        if (id) addVideoPermanentHistory(id, youtubeID, descObj)
        if (cb) cb()
      })}
    </div>
  )
}
