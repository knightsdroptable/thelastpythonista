import { pathPrefix } from "./globals"
import copyToClipboard from "copy-to-clipboard"
import { populateDecorationsRef } from "./populate-decorations-ref"

import React, { useState, useEffect } from "react"
import produce from "immer"
import { useSelector } from "react-redux"
import { dispatch } from "./store"
import Grower from "./Grower.js"
import { BiChevronsLeft } from "react-icons/bi"
import { BiChevronsRight } from "react-icons/bi"
import { stopLoop } from "./loops"
import { colorSelectedInMenu, useColorBackground } from "./colors"
import { isCompletedExercise, numberOfCompletedExercises, totalNumberExercises } from "./store"
import { tutorialOnClick } from "./TutorialButton"
let addEx = (eid) => {
  dispatch({ type: "add-exercise", eid: eid })
}
let uppercaseFirst = (str) => {
  return str[0].toUpperCase() + str.slice(1)
}

let tutorialAttr = (idInInterface, onClick) => {
  return {
    ref: (r) => populateDecorationsRef(idInInterface, r),
    onClick: tutorialOnClick(idInInterface, onClick),
  }
}

export default function ContentNavigationMenu() {
  const [state, setState] = useState({ needsFocus: false })

  let colorBackground = useColorBackground()
  let stateMenuExpanded = useSelector((s) => s.menuExpanded)

  let stateFinishedExerciseIds = useSelector((s) => s.finishedExerciseIds)
  let stateNavigateContent = useSelector((s) => s.navigateContent)
  let adminMode = useSelector((s) => s.adminMode)
  let stateCurrentView = useSelector((s) => s.currentView)
  let stateChapterExpanded = useSelector((s) => s.chapterExpanded)
  let stateAnimationFrames = useSelector((s) => s.animationFrames)
  let stateContent = useSelector((s) => s.content)
  const exerciseInMenuRef = React.useRef(undefined)
  let focusMenu = () => {
    if (exerciseInMenuRef.current) {
      exerciseInMenuRef.current.scrollIntoView({ block: "nearest" })
    }
  }
  let chapterOfCurrentExercise = (currentView, content) => {
    if (currentView.chapter) return currentView.chapter
    if (!currentView?.exercise) return undefined
    let chapters = content.chapterOrder

    let chap = chapters.find((c) => content.chapters[c].exerciseOrder.includes(currentView.exercise))

    return chap
  }

  let chapOfCurr = chapterOfCurrentExercise(stateCurrentView, stateContent)
  useEffect(() => {
    if (stateAnimationFrames === undefined && exerciseInMenuRef.current) {
      exerciseInMenuRef.current.scrollIntoView({ block: "nearest" })
    }
  }, [stateAnimationFrames])
  useEffect(() => {
    if (!state.needsFocus) return
    setState((state) =>
      produce(state, (state) => {
        state.needsFocus = false
      })
    )
    focusMenu()
  }, [state.needsFocus])

  let exercisesCompletedSet = new Set(stateFinishedExerciseIds)

  let contentRowForExercise = (exercise, exerciseIndex, chapterIndex) => {
    if (!exercise) debugger
    let currentlySelected = exercise.id === (stateCurrentView && stateCurrentView.exercise)

    let isTutorial = stateContent.tutorialDescriptions?.[exercise.id]
    return (
      <div
        ref={currentlySelected ? exerciseInMenuRef : undefined}
        style={{
          display: "flex",
          backgroundColor: currentlySelected && colorSelectedInMenu,
        }}
      >
        <div
          style={{ cursor: "pointer", fontSize: "14px", userSelect: "none" }}
          {...tutorialAttr("nav-ex-" + chapterIndex + "-" + exerciseIndex, () => {
            stopLoop()
            dispatch({ type: "jump-to-exercise", eid: exercise.id })
          })}
        >
          {isTutorial && "*Tutorial: "}
          {exercise.name.toLowerCase()}
          {exercisesCompletedSet.has(exercise.id) ? " ✔" : ""}
        </div>

        {adminMode && (
          <>
            <button onClick={() => addEx(exercise.id)}> +ex (J)</button>
            <button
              onClick={() =>
                dispatch({
                  type: "name-current",
                  opt: { exerciseId: exercise.id },
                })
              }
            >
              name (M)
            </button>
          </>
        )}
      </div>
    )
  }

  let contentRowForChapter = (chapter, chapterIndex) => {
    return (
      <div
        key={chapter.id}
        style={{
          display: "flex",
          marginLeft: "8px",
        }}
      >
        <span
          style={{ cursor: "pointer", fontWeight: "bold", color: "#444", userSelect: "none" }}
          {...tutorialAttr("ch-header-" + chapterIndex, () => {
            dispatch({
              type: "toggle-chapter-in-navigation",
              chapterId: chapter.id,
            })
          })}
        >
          {uppercaseFirst(chapter.name)}
        </span>
        {adminMode && (
          <>
            <button
              onClick={() => {
                dispatch({ type: "add-chapter-after", chapterId: chapter.id })
              }}
            >
              +c
            </button>
            <button
              onClick={() => {
                dispatch({ type: "add-exercise-at-top", chapterId: chapter.id })
              }}
            >
              +e
            </button>
            <button
              onClick={() =>
                dispatch({
                  type: "name-current",
                  opt: { chapterId: chapter.id },
                })
              }
            >
              name
            </button>
          </>
        )}
      </div>
    )
  }

  let contentRowChapterAndExercises = stateContent.chapterOrder.map((chapterId, chapterIndex) => {
    let chapter = stateContent.chapters[chapterId]
    let chr = contentRowForChapter(chapter, chapterIndex)

    let chapterOpen = (cid) => {
      // if (cid && stateCurrentView && stateCurrentView.chapter === cid) return true
      // if (stateCurrentView && stateCurrentView.exercise) {
      //   let exercises = stateContent.chapters[cid].exerciseOrder
      //   return exercises.includes(stateCurrentView.exercise)
      // }
      return (stateChapterExpanded || []).includes(cid) || cid === chapOfCurr
    }

    let exercisePart = (shown) => {
      let chapterDesc = stateContent.chapters[chapter.id].youtubeID && (
        <div
          style={{
            cursor: "pointer",
            backgroundColor: chapterId === stateCurrentView?.chapter && colorSelectedInMenu,
            fontSize: "14px",
          }}
          {...tutorialAttr("chapter-desc-" + chapterIndex, () => {
            stopLoop()
            dispatch({
              type: "set-current-view",
              payload: { chapter: chapter.id },
            })
          })}
        >
          chapter description
        </div>
      )
      let exerciseSelectors = chapter.exerciseOrder
        .map((exerciseId) => stateContent.exercises[exerciseId])
        .map((c, exerciseIndex) => contentRowForExercise(c, exerciseIndex, chapterIndex))
      return <Grower key={"grower-" + chapterId} shown={shown} chs={[chapterDesc, ...exerciseSelectors]} />
    }

    return (
      <div>
        <div>{chr}</div>
        {<div style={{ marginLeft: "20px" }}>{exercisePart(chapterOpen(chapterId))}</div>}
      </div>
    )
  })

  let chooseExerciseMenu = (
    <>
      {adminMode && (
        <>
          <div>
            <button
              onClick={() => {
                dispatch({ type: "download-content" })
              }}
            >
              Download content (G)
            </button>
          </div>
          <div>
            <button
              onClick={() => {
                stopLoop()
                dispatch({ type: "restore-content-from-file" })
              }}
            >
              Restore
            </button>
          </div>
          <div>
            <button
              onClick={() => {
                dispatch({ type: "copy-current-exercise" })
              }}
            >
              Copy current ex (K)
            </button>
          </div>
          <div>
            <button
              onClick={() => {
                stopLoop()
                dispatch({ type: "paste-current-exercise" })
              }}
            >
              Paste current ex (L)
            </button>
          </div>
          <div>
            <button
              onClick={() => {
                dispatch({ type: "add-chapter-top" })
              }}
            >
              Add chapter top
            </button>
          </div>
        </>
      )}
      {stateNavigateContent && (
        <button
          className={"flatbutton"}
          style={{ width: "172px" }}
          {...tutorialAttr("select-ex-viewable", () => {
            dispatch({ type: "navigation-viewable", viewable: false })
          })}
        >
          close navigation
        </button>
      )}
      {contentRowChapterAndExercises}
    </>
  )

  let exerciseContentMenu = (cid) => {
    let chapter = Object.values(stateContent.chapters).find((ch) => ch.exerciseOrder.includes(cid))
    let chapterName = (chapter && chapter.name) || "Unknown chapter"
    let exercise = stateContent.exercises[cid]
    let exerciseName = (exercise && exercise.name) || "Unknown exercise"
    let completedCheckmark = stateFinishedExerciseIds.includes(cid)
    // let videoButton = (
    //   <button
    //     className="flatbutton"
    //     onClick={() => {
    //       dispatch({ type: "video-history-viewable", viewable: true })
    //     }}
    //   >
    //     Videos
    //   </button>
    // )

    let saveProgressButton = (
      <button
        className="flatbutton width-170"
        {...tutorialAttr("save-progress", () => {
          dispatch({ type: "save-progress" })
        })}
      >
        save progress
      </button>
    )

    let copyCurrentExerciseId = stateCurrentView?.exercise && (
      <button
        className="flatbutton width-170"
        {...tutorialAttr("link-to-current-exercise", () => {
          let url = `${pathPrefix}?exerciseid=${stateCurrentView.exercise}`
          copyToClipboard(url)
          alert(`the link ${url} has been copied to your clipboard`)
        })}
      >
        copy link to exercise
      </button>
    )

    let bugReportButton = (
      <button
        className="flatbutton width-170"
        {...tutorialAttr("report-bug", () => {
          dispatch({ type: "bug-report" })
        })}
      >
        report bug
      </button>
    )

    let chooseExerciseButton = (
      <button
        className="flatbutton width-170"
        {...tutorialAttr("toggle-ex-selection", () => {
          dispatch({ type: "navigation-viewable", viewable: true })
        })}
      >
        navigate content
      </button>
    )

    let previousExerciseButton = (
      <button
        className="flatbutton width-170"
        {...tutorialAttr("nav-prev-ex", () => {
          stopLoop()
          setState((state) =>
            produce(state, (state) => {
              state.needsFocus = true
            })
          )
          dispatch({ type: "navigate-previous-exercise" })
        })}
      >
        previous exercise
      </button>
    )

    let nextExerciseButton = (
      <button
        className="flatbutton width-170"
        {...tutorialAttr("nav-next-ex", () => {
          stopLoop()
          setState((state) =>
            produce(state, (state) => {
              state.needsFocus = true
            })
          )
          dispatch({ type: "navigate-next-exercise" })
        })}
      >
        next exercise
      </button>
    )
    let firsttdstyle = { paddingRight: "20px" }
    let secondtdstyle = { textAlign: "left" }
    return (
      <div style={{ textAlign: "right" }}>
        <table style={{ marginBottom: "20px" }}>
          <tr>
            <td style={firsttdstyle}>status</td>
            <td style={secondtdstyle}>{completedCheckmark ? "completed" : "not completed"}</td>
          </tr>
          <tr>
            <td style={firsttdstyle}>chapter name</td>
            <td style={secondtdstyle}>{uppercaseFirst(chapterName)}</td>
          </tr>
          <tr>
            <td style={firsttdstyle}>exercise name</td>
            <td style={secondtdstyle}>{exerciseName.toLowerCase()}</td>
          </tr>
          <tr>
            <td style={firsttdstyle}>exercises completed</td>
            <td style={secondtdstyle}>
              {stateFinishedExerciseIds.length}/{totalNumberExercises}
            </td>
          </tr>
        </table>
        <div style={{ display: "flex", alignItems: "center", flexDirection: "column" }}>
          <div>{previousExerciseButton}</div>
          <div>{nextExerciseButton}</div>
          <div>{chooseExerciseButton}</div>
          {/* <div>{videoButton}</div> */}
          <div>{saveProgressButton}</div>
          <div>{copyCurrentExerciseId}</div>
          <div>{bugReportButton}</div>
        </div>
        {adminMode && (
          <div>
            <button
              onClick={() => {
                dispatch({ type: "check-restriction-consistency" })
              }}
            >
              Check violations
            </button>
          </div>
        )}
        {adminMode && (
          <div>
            <button
              onClick={() => {
                dispatch({ type: "check-solutions-interpret" })
              }}
            >
              Check solutions interpret
            </button>
          </div>
        )}
      </div>
    )
  }
  //boxShadow: "rgba(0,0,0,0.19) 0px 10px 20px inset,rgba(0,0,0,0.23) 0px 6px 6px inset",
  return (
    <div
      key={"rhs-menu"}
      style={{
        // overflowY: "scroll",
        height: "100vh",
        position: "fixed",
        right: "0px",
        top: "0px",
        zIndex: 6000,

        overflowX: "visible",
        display: "flex",
      }}
    >
      <div>
        <div
          style={{
            backgroundColor: "rgba(0,0,0,0)",
            fontSize: "20px",
            cursor: "pointer",
            color: "#020202",
            paddingRight: "40px",
            paddingTop: "40px",
          }}
          {...tutorialAttr("toggle-main-menu", () => {
            if (!stateMenuExpanded) dispatch({ type: "navigation-viewable", viewable: false })
            dispatch({ type: "set-menu-expanded", expanded: !stateMenuExpanded })
          })}
        >
          <div
            style={{
              position: "relative",
              cursor: "pointer",
              backgroundColor: colorBackground,
            }}
          >
            <div style={{ opacity: !stateMenuExpanded ? 1 : 0, transition: "opacity 0.5s" }}>MENU</div>
            <div
              style={{
                position: "absolute",
                opacity: !stateMenuExpanded ? 0 : 1,
                top: "0px",
                left: "0px",
                transition: "opacity 0.5s",
              }}
            >
              MENU
            </div>
          </div>
        </div>
      </div>
      <div
        style={{
          backgroundColor: colorBackground,
          position: "relative",
          width: !stateMenuExpanded ? "0px" : "400px",
          boxSizing: "border-box",
          overflowX: "hidden",
          transition: "width 1s, padding 0.5s",
          pading: !stateMenuExpanded ? "8px 0px 8px 0px" : "8px 8px 8px 8px",
          whiteSpace: "nowrap",
        }}
      >
        <div style={{ paddingRight: "40px", paddingTop: "40px" }}>
          {stateNavigateContent || !stateCurrentView
            ? chooseExerciseMenu
            : stateCurrentView.exercise
            ? exerciseContentMenu(stateCurrentView.exercise)
            : null}
        </div>
      </div>
    </div>
  )
}
