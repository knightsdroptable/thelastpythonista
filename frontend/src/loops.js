import { stopAudio } from "./playaudio"
import { dispatch } from "./store"
let loop = undefined
let loopType = undefined
export let stopLoop = () => {
  stopAudio()
  dispatch({ type: "playing-loop-type", loopType: undefined })
  if (loop) loop.cancel()
  loop = undefined
  loopType = undefined
}

let setLoop = (t, x) => {
  dispatch({ type: "playing-loop-type", loopType: t })
  loopType = t
  loop = x
}

export let setRunLoop = (x) => {
  setLoop("run", x)
}

export let setTutorialLoop = (x) => {
  setLoop("tutorial", x)
}

export let runLoopIsPlaying = () => {
  return loopType === "run"
}
export let tutorialLoopIsPlaying = () => {
  return loopType === "tutorial"
}
