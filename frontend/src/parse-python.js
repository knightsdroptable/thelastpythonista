//let jc = require('json-cycle');

//import { parse, createVisitor } from 'python-ast';

//let parse = require('python-ast').parse
//let walk = require('python-ast').walk

import { Single_inputContext } from "python-ast/dist/parser/Python3Parser"
import { File_inputContext } from "python-ast/dist/parser/Python3Parser"
import { Eval_inputContext } from "python-ast/dist/parser/Python3Parser"
import { DecoratorContext } from "python-ast/dist/parser/Python3Parser"
import { DecoratorsContext } from "python-ast/dist/parser/Python3Parser"
import { DecoratedContext } from "python-ast/dist/parser/Python3Parser"
import { Async_funcdefContext } from "python-ast/dist/parser/Python3Parser"
import { FuncdefContext } from "python-ast/dist/parser/Python3Parser"
import { ParametersContext } from "python-ast/dist/parser/Python3Parser"
import { TypedargslistContext } from "python-ast/dist/parser/Python3Parser"
import { TfpdefContext } from "python-ast/dist/parser/Python3Parser"
import { VarargslistContext } from "python-ast/dist/parser/Python3Parser"
import { VfpdefContext } from "python-ast/dist/parser/Python3Parser"
import { StmtContext } from "python-ast/dist/parser/Python3Parser"
import { Simple_stmtContext } from "python-ast/dist/parser/Python3Parser"
import { Small_stmtContext } from "python-ast/dist/parser/Python3Parser"
import { Expr_stmtContext } from "python-ast/dist/parser/Python3Parser"
import { AnnassignContext } from "python-ast/dist/parser/Python3Parser"
import { Testlist_star_exprContext } from "python-ast/dist/parser/Python3Parser"
import { AugassignContext } from "python-ast/dist/parser/Python3Parser"
import { Del_stmtContext } from "python-ast/dist/parser/Python3Parser"
import { Pass_stmtContext } from "python-ast/dist/parser/Python3Parser"
import { Flow_stmtContext } from "python-ast/dist/parser/Python3Parser"
import { Break_stmtContext } from "python-ast/dist/parser/Python3Parser"
import { Continue_stmtContext } from "python-ast/dist/parser/Python3Parser"
import { Return_stmtContext } from "python-ast/dist/parser/Python3Parser"
import { Yield_stmtContext } from "python-ast/dist/parser/Python3Parser"
import { Raise_stmtContext } from "python-ast/dist/parser/Python3Parser"
import { Import_stmtContext } from "python-ast/dist/parser/Python3Parser"
import { Import_nameContext } from "python-ast/dist/parser/Python3Parser"
import { Import_fromContext } from "python-ast/dist/parser/Python3Parser"
import { Import_as_nameContext } from "python-ast/dist/parser/Python3Parser"
import { Dotted_as_nameContext } from "python-ast/dist/parser/Python3Parser"
import { Import_as_namesContext } from "python-ast/dist/parser/Python3Parser"
import { Dotted_as_namesContext } from "python-ast/dist/parser/Python3Parser"
import { Dotted_nameContext } from "python-ast/dist/parser/Python3Parser"
import { Global_stmtContext } from "python-ast/dist/parser/Python3Parser"
import { Nonlocal_stmtContext } from "python-ast/dist/parser/Python3Parser"
import { Assert_stmtContext } from "python-ast/dist/parser/Python3Parser"
import { Compound_stmtContext } from "python-ast/dist/parser/Python3Parser"
import { Async_stmtContext } from "python-ast/dist/parser/Python3Parser"
import { If_stmtContext } from "python-ast/dist/parser/Python3Parser"
import { While_stmtContext } from "python-ast/dist/parser/Python3Parser"
import { For_stmtContext } from "python-ast/dist/parser/Python3Parser"
import { Try_stmtContext } from "python-ast/dist/parser/Python3Parser"
import { With_stmtContext } from "python-ast/dist/parser/Python3Parser"
import { With_itemContext } from "python-ast/dist/parser/Python3Parser"
import { Except_clauseContext } from "python-ast/dist/parser/Python3Parser"
import { SuiteContext } from "python-ast/dist/parser/Python3Parser"
import { TestContext } from "python-ast/dist/parser/Python3Parser"
import { Test_nocondContext } from "python-ast/dist/parser/Python3Parser"
import { LambdefContext } from "python-ast/dist/parser/Python3Parser"
import { Lambdef_nocondContext } from "python-ast/dist/parser/Python3Parser"
import { Or_testContext } from "python-ast/dist/parser/Python3Parser"
import { And_testContext } from "python-ast/dist/parser/Python3Parser"
import { Not_testContext } from "python-ast/dist/parser/Python3Parser"
import { ComparisonContext } from "python-ast/dist/parser/Python3Parser"
import { Comp_opContext } from "python-ast/dist/parser/Python3Parser"
import { Star_exprContext } from "python-ast/dist/parser/Python3Parser"
import { ExprContext } from "python-ast/dist/parser/Python3Parser"
import { Xor_exprContext } from "python-ast/dist/parser/Python3Parser"
import { And_exprContext } from "python-ast/dist/parser/Python3Parser"
import { Shift_exprContext } from "python-ast/dist/parser/Python3Parser"
import { Arith_exprContext } from "python-ast/dist/parser/Python3Parser"
import { TermContext } from "python-ast/dist/parser/Python3Parser"
import { FactorContext } from "python-ast/dist/parser/Python3Parser"
import { PowerContext } from "python-ast/dist/parser/Python3Parser"
import { Atom_exprContext } from "python-ast/dist/parser/Python3Parser"
import { AtomContext } from "python-ast/dist/parser/Python3Parser"
import { Testlist_compContext } from "python-ast/dist/parser/Python3Parser"
import { TrailerContext } from "python-ast/dist/parser/Python3Parser"
import { SubscriptlistContext } from "python-ast/dist/parser/Python3Parser"
import { SubscriptContext } from "python-ast/dist/parser/Python3Parser"
import { SliceopContext } from "python-ast/dist/parser/Python3Parser"
import { ExprlistContext } from "python-ast/dist/parser/Python3Parser"
import { TestlistContext } from "python-ast/dist/parser/Python3Parser"
import { DictorsetmakerContext } from "python-ast/dist/parser/Python3Parser"
import { ClassdefContext } from "python-ast/dist/parser/Python3Parser"
import { ArglistContext } from "python-ast/dist/parser/Python3Parser"
import { ArgumentContext } from "python-ast/dist/parser/Python3Parser"
import { Comp_iterContext } from "python-ast/dist/parser/Python3Parser"
import { Comp_forContext } from "python-ast/dist/parser/Python3Parser"
import { Comp_ifContext } from "python-ast/dist/parser/Python3Parser"
import { Encoding_declContext } from "python-ast/dist/parser/Python3Parser"
import { Yield_exprContext } from "python-ast/dist/parser/Python3Parser"
import { Yield_argContext } from "python-ast/dist/parser/Python3Parser"

import { TerminalNode } from "antlr4ts/tree/TerminalNode"

import * as antlr4ts_1 from "antlr4ts"
import * as Python3Lexer_1 from "python-ast/dist/parser/Python3Lexer"
import * as Python3Parser_1 from "python-ast/dist/parser/Python3Parser"

//genWhileLoopId

let genWhileLoopIdCounter = 0
let genWhileLoopId = () => {
  genWhileLoopIdCounter++
  return "wh-" + genWhileLoopIdCounter
}

let createVisitor = require("python-ast").createVisitor

class Listener {
  syntaxError(err) {
    console.log("WHAT")
  }
}

let errorToString = ({ recognizer, offendingSymbol, line, column, msg, err }) => {
  return `${offendingSymbol} line ${line}, col ${column}: ${msg}`
}

function parse(source) {
  const chars = new antlr4ts_1.ANTLRInputStream(source)
  const lexer = new Python3Lexer_1.Python3Lexer(chars)
  const tokens = new antlr4ts_1.CommonTokenStream(lexer)
  const parser = new Python3Parser_1.Python3Parser(tokens)
  let errors = []
  parser.removeErrorListeners()
  parser.addErrorListener({
    syntaxError: (recognizer, offendingSymbol, line, column, msg, err) => {
      //errors.push({ recognizer, offendingSymbol, line, column, msg, err });
      errors.push({ lineNum: line })
    },
  })
  let ret = parser.file_input()
  if (errors.length > 1) return { hasErrors: true, errors }
  return ret
}

// class Visitor {
//     visitChildren(ctx) {
//         if (!ctx) {
//             return;
//         }

//         if (ctx.children) {
//             return ctx.children.map(child => {
//                 if (child.children && child.children.length != 0) {
//                     return child.accept(this);
//                 } else {
//                     if (child && child.getText) console.log(child.getText());
//                 }
//             });
//         }
//     }
// }

// let format = (str, lineNum, start, end) => {
//     let lines = str.split("\n")
//     let line = lines[lineNum - 1]
//     console.log(line)
//     ret = " ".repeat(start)
//     ret += "=".repeat(end - start)
//     console.log(ret)
// }

let mergeStrings = (arr) => {
  let ret = []
  let sofar = ""
  for (let i = 0; i < arr.length; i++) {
    let e = arr[i]
    if (isString(e)) {
      sofar += e
    } else {
      ret.push(sofar)
      sofar = ""
      ret.push(e)
    }
  }
  if (!isString(ret[ret.length - 1])) {
    ret.push(sofar)
  }

  for (let i = 0; i < ret.length; i++) {
    let e = ret[i]
    if (i % 2 == 0 && typeof e !== "string") throw new Error("error 1 mergeStrings")
    if (i % 2 == 1 && typeof e === "string") throw new Error("error 2 mergeStrings")
  }

  return ret
}

let isASTNode = (t) => {
  return t.category === "ast-node"
}

let numAST = (arr) => arr.filter((x) => x.kind !== undefined).length

let mergeSpaces = (node) => {
  if (isString(node)) return node
  let children = node.children.map((ch) => {
    if (isString(ch)) return ch
    return mergeSpaces(ch)
  })
  return { ...node, children: mergeStrings(children) }
}

let tokenIdCounter = 0
let genTokenId = () => {
  tokenIdCounter++
  return "src-tok-" + tokenIdCounter
}

let tokenify = (tree) => {
  if (typeof tree === "string") {
    return {
      category: "token",
      id: genTokenId(),
      text: tree,
    }
  }
  // if (tree.constructor && tree.constructor.name === TerminalNode) {

  // }
  if (!isASTNode(tree)) {
    throw new Error("tokenify 1")
  }
  let children = tree.children.map(tokenify)
  return { ...tree, children }
}

// let emptinessBetweenNodes = (a, b) => {
//     let elemStart = actualStop(a)
//     let elemStop = actualStart(b)

//     emptiness(elemStart.line,
//         elemStop.charPosition,
//         elemStart.line,
//         elemStart.charPositionInLine)

// }

let isTerminal = (elem) => {
  return elem.constructor && elem instanceof TerminalNode
}

let isString = (e) => typeof e === "string"

let foobar = (elem) => {
  if (isTerminal(elem)) {
    return undefined
  }
  return {
    startLine: elem.start.line,
    startChar: elem.start.charPositionInLine,
    stopLine: elem.stop.line,
    stopChar: elem.stop.charPositionInLine + elem.stop.text.length,
  }
}

// odd indices have to be non terminals
// let intervalize = (arr, data) => {

//     if (arr.length === 0) return undefined
//     let start = actualStart(arr[0])

//     let line = data ? data.line : start.line
//     let charPosition = data ? data.charPositionInLine : start.charPositionInLine
//     let ret = []
//     if (line === undefined) debugger

//     //let zzz = arr.map(z => getLocations(z))

//     for (let i = 0; i < arr.length; i++) {
//         let elem = arr[i]
//         let elemStart = actualStart(elem)
//         let elemStop = actualStop(elem)

//         if (isEmptyTerminal(elem)) continue
//         // let isn = x => isTerminal(x) && x.trim().length === 0 && cntnew(elem.text) > 0
//         // let isNewlines = isn(elem)
//         // let [l, c] = isNewlines ? [line + cntnew(elem.text), 0] : [line, charPosition]

//         if (!elemStart) debugger

//         let spaces = emptiness(line,
//             charPosition,
//             elemStart.line,
//             elemStart.charPositionInLine)
//         if (elem.text.includes("while") && elem.text.includes("foobar")) debugger
//         line = elemStop.line
//         charPosition = elemStop.charPositionInLine
//         if (line === undefined) debugger
//         ret.push(spaces)

//         if (isTerminal(elem)) {
//             ret.push(elem.text)
//         } else {
//             ret.push(elem)
//         }
//     }
//     if (arr.length > 0) {
//         ret.push('')
//     }

//     let result = mergeStrings(ret)
//     return result
// }

// let visitor =
// {
//     visitSingle_input: inp => { },
//     visitFile_input: inp => { },
//     visitEval_input: inp => { },
//     visitDecorator: inp => { },
//     visitDecorators: inp => { },
//     visitDecorated: inp => { },
//     visitAsync_funcdef: inp => { },
//     visitFuncdef: inp => { },
//     visitParameters: inp => { },
//     visitTypedargslist: inp => { },
//     visitTfpdef: inp => { },
//     visitVarargslist: inp => { },
//     visitVfpdef: inp => { },
//     visitStmt: inp => { },
//     visitSimple_stmt: inp => { },
//     visitSmall_stmt: inp => { },
//     visitExpr_stmt: inp => { },
//     visitAnnassign: inp => { },
//     visitTestlist_star_expr: inp => { },
//     visitAugassign: inp => { },
//     visitDel_stmt: inp => { },
//     visitPass_stmt: inp => { },
//     visitFlow_stmt: inp => { },
//     visitBreak_stmt: inp => { },
//     visitContinue_stmt: inp => { },
//     visitReturn_stmt: inp => { },
//     visitYield_stmt: inp => { },
//     visitRaise_stmt: inp => { },
//     visitImport_stmt: inp => { },
//     visitImport_name: inp => { },
//     visitImport_from: inp => { },
//     visitImport_as_name: inp => { },
//     visitDotted_as_name: inp => { },
//     visitImport_as_names: inp => { },
//     visitDotted_as_names: inp => { },
//     visitDotted_name: inp => { },
//     visitGlobal_stmt: inp => { },
//     visitNonlocal_stmt: inp => { },
//     visitAssert_stmt: inp => { },
//     visitCompound_stmt: inp => { },
//     visitAsync_stmt: inp => { },
//     visitIf_stmt: inp => { },
//     visitWhile_stmt: inp => { },
//     visitFor_stmt: inp => { },
//     visitTry_stmt: inp => { },
//     visitWith_stmt: inp => { },
//     visitWith_item: inp => { },
//     visitExcept_clause: inp => { },
//     visitSuite: inp => { },
//     visitTest: inp => { },
//     visitTest_nocond: inp => { },
//     visitLambdef: inp => { },
//     visitLambdef_nocond: inp => { },
//     visitOr_test: inp => { },
//     visitAnd_test: inp => { },
//     visitNot_test: inp => { },
//     visitComparison: inp => { },
//     visitComp_op: inp => { },
//     visitStar_expr: inp => { },
//     visitExpr: inp => { },
//     visitXor_expr: inp => { },
//     visitAnd_expr: inp => { },
//     visitShift_expr: inp => { },
//     visitArith_expr: inp => { },
//     visitTerm: inp => { },
//     visitFactor: inp => { },
//     visitPower: inp => { },
//     visitAtom_expr: inp => { },
//     visitAtom: inp => { },
//     visitTestlist_comp: inp => { },
//     visitTrailer: inp => { },
//     visitSubscriptlist: inp => { },
//     visitSubscript: inp => { },
//     visitSliceop: inp => { },
//     visitExprlist: inp => { },
//     visitTestlist: inp => { },
//     visitDictorsetmaker: inp => { },
//     visitClassdef: inp => { },
//     visitArglist: inp => { },
//     visitArgument: inp => { },
//     visitComp_iter: inp => { },
//     visitComp_for: inp => { },
//     visitComp_if: inp => { },
//     visitEncoding_decl: inp => { },
//     visitYield_expr: inp => { },
//     visitYield_arg: inp => { },
// }

let unparse = (tree) => {
  if (tree.category == "token") {
    debugger
    return tree.text
  }
  let chs = tree.children

  let ret = ""
  for (let i = 0; i < chs.length; i++) {
    if (chs[i].category === "token") ret += chs[i].text
    else ret += unparse(chs[i])
  }
  return ret
}

let cntnew = (str) => {
  return str.split(/\r\n|\r|\n/).length - 1
}

let countNewlines = (tree) => {
  if (typeof tree == "string") return cntnew(tree)
  let ret = 0
  let chs = tree.children
  for (let i = 0; i < chs.length; i++) {
    if (i % 2 === 0) ret += cntnew(chs[i])
    else ret += countNewlines(chs[i])
  }
  return ret
}

let unsupported = (str, node) => {
  let err = new Error("Not supported: " + str)
  err.notSupported = true
  let start = actualStart(node)
  err.lineNum = start && start.line
  throw err
}

let unexpected = (str, node) => {
  let err = new Error("Not expected: " + str)
  err.notExpected = true
  let start = actualStart(node)
  err.lineNum = start && start.line
  throw err
}

let checkChildrenTypes = (inp, possible) => {
  let chs = inp.children
  for (let i = 0; i < chs.length; i++) {
    let child = chs[i]
    if (!possible.some((papa) => child instanceof papa)) {
      //throw new Error("must figure out what to do with error node")

      // if (child instanceof ErrorNode) {
      //   let parseError = new Error("parse error")
      //   parseError.errorType = "parse"
      //   //parseError.errorNode = chs[i]
      //   throw parseError
      // }

      throw new Error("err2")
    }
  }
  // if (chs.some(ch => !possible.includes(ch.constructor.name))) {
  //     debugger

  // }
}

let reduceExpressionArray = (chs, lineNumber) => {
  if (lineNumber === undefined) new Error("what?")
  if (chs.length % 2 !== 1) {
    debugger
    throw new Error("what?!? reduceExpressionArray")
  }
  let current = chs[0]
  for (let i = 1; i < chs.length; i += 2) {
    let children = [
      current, // sofar
      chs[i], // the op
      chs[i + 1], // rhs
    ]
    if (children.some((ch) => !isASTNode(ch) && !isString(ch))) {
      debugger
    }

    current = {
      lineNumber,
      category: "ast-node",
      kind: "expr",
      type: "binary",
      children,
    }
  }

  return current
}

function isNumber(str) {
  if (typeof str != "string") return false // we only process strings!
  return (
    !isNaN(str) && // use type coercion to parse the _entirety_ of the string (`parseFloat` alone does not do this)...
    !isNaN(parseFloat(str))
  ) // ...and ensure strings of whitespace fail
}

let actualStart = (tree) => {
  if (isTerminal(tree)) {
    if (isEmptyTerminal(tree)) {
      return undefined
    }
    let ret = {
      line: tree.payload.line,
      charPositionInLine: tree.payload.charPositionInLine,
    }
    Object.values(ret).forEach((x) => {
      if (x === null) throw new Error("actual start 1")
    })
    return ret
  }
  let chs = tree.children
  for (let i = 0; i < chs.length; i++) {
    let ret = actualStart(chs[i])
    if (ret) return ret
  }

  return undefined
}

let isEmptyTerminal = (t) => {
  return t.constructor && t instanceof TerminalNode && t.text.trim().length === 0
}
let actualStop = (tree) => {
  if (isTerminal(tree)) {
    if (isEmptyTerminal(tree)) return undefined
    let ret = {
      line: tree.payload.line,
      charPositionInLine: tree.payload.charPositionInLine + tree.text.length,
    }
    Object.values(ret).forEach((x) => {
      if (isNaN(x)) throw new Error("actual stop 1")
    })
    return ret
  }
  let chs = tree.children
  for (let i = chs.length - 1; i >= 0; i--) {
    let ret = actualStop(chs[i])
    if (ret) return ret
  }
  return undefined
}

// let getFirstStatement = (tree) => {
//     if (tree.kind !== "block") throw new Error("getFirstSattement 1")
//     let chs = tree.children
//     let nts = nonterminals(chs)
//     return nts[0]
// }

let getNextStatement = (inputTree, tid) => {
  let found = false
  let ret = undefined
  let doit = (tree) => {
    if (tree.type === "token") return
    if (found) {
      if (tree.kind === "stmt" && ret === undefined) {
        ret = tree.tid
      }
      return
    }
    if (tree.tid === tid) {
      found = true
    }
    tree.children.map(doit)
  }
  return ret
}

// let interpretProgram = inputTree => {

//     let view = {
//         tree: inputTree,
//         frames: [],
//         heap: []
//     }

//     let ret = []

//     let snapshot = () => {
//         ret.push(clone(view))
//     }

//     let statement = stmt => {
//         if (statement.type !== "stmt") throw new Error("interpret 1")
//         switch (statement.type) {
//             case 'assignment': {
//                 let subs = collect(statement.children)
//                 let rhs = subs[1]
//                 let v = evalExpr(rhs.tid, ctx)
//             }
//         }
//     }

//     let views = [{ tree: inputTree, ctx }]

//     let process = {
//         "block": tree => {
//             let subs = collect(tree)
//             for (let i = 0; i < subs.length; i++) {
//                 let views = interpret(subs[i], views[views.length - 1])

//             }
//             subs.forEach(x => {
//                 if (!(x.kind === "stmt" && x.type === "assignment")) throw new Error("step unsupported 1")
//                 if (nextStatement === undefined) {
//                     nextStatement = x.stmtId
//                 } else {
//                     if (nextStatement && nextStatement) {

//                     }
//                 }

//             })
//         }
//     }

// }

const parsePython = (source, userLines) => {
  let genExceptIdCounter = 0
  let genExceptId = () => {
    genExceptIdCounter++
    return "except-" + genExceptIdCounter
  }

  let genFuncdefIdCounter = 0
  let genFuncdefId = () => {
    genFuncdefIdCounter++
    return "df-" + genFuncdefIdCounter
  }

  let genClassdefIdCounter = 0
  let genClassdefId = () => {
    genClassdefIdCounter++
    return "c-" + genClassdefIdCounter
  }

  let presence = {}

  let visitNonTerminals = (chs) => {
    return chs.map((c) => {
      if (isTerminal(c)) return c
      return visitor.visit(c)
    })
  }

  let processNode = (name, inp) => {
    unsupported(name, inp.children[0])
    let subs = (inp.children || [])
      .map((ch) => {
        let sub = visitor.visit(ch)
        if (sub === undefined) return []
        if (!Array.isArray(sub)) debugger
        return sub.map((x) => "   " + x)
      })
      .flat()
    return [name, ...subs]
  }

  let visitor = createVisitor({
    /*single_input: NEWLINE | simple_stmt | compound_stmt NEWLINE;*/
    visitSingle_input: (inp) => {
      return processNode("    visitSingle_input", inp)
    },
    /* file_input: (NEWLINE | stmt)* EOF; */
    visitFile_input: (inp) => {
      if (inp.children.length === 0) return { category: "ast-node", kind: "block", children: [] }
      let nonTerminals = inp.children.filter((ch) => !(ch instanceof TerminalNode))
      if (nonTerminals.length === 0) return { category: "ast-node", kind: "block", children: [] }
      let chs = inp.children
      if (
        chs.some((ch) => {
          return ![StmtContext, TerminalNode].some((papa) => ch instanceof papa)
        })
      )
        throw new Error("huh")
      if (chs[chs.length - 1].text === "<EOF>") chs = chs.slice(0, chs.length - 1)

      return {
        category: "ast-node",
        kind: "block",
        children: visitNonTerminals(chs),
      }

      // let ret = z.filter(y => y.constructor.name === StmtContext)
      // return ret.map(x => visitor.visit(x))
      // // let ret = inp.children.map(ch => visitor.visit(ch))
      // // return ret.filte r(x => !isNewline(x))
      // return processNode("    visitFile_input", inp)
    },
    /*eval_input: testlist NEWLINE* EOF;*/
    visitEval_input: (inp) => {
      return processNode("    visitEval_input", inp)
    },
    /*decorator: '@' dotted_name ( '(' (arglist)? ')' )? NEWLINE;*/
    visitDecorator: (inp) => {
      return processNode("    visitDecorator", inp)
    },
    /*decorators: decorator+;*/
    visitDecorators: (inp) => {
      return processNode("    visitDecorators", inp)
    },
    /*decorated: decorators (classdef | funcdef | async_funcdef);*/
    visitDecorated: (inp) => {
      let possible = [ClassdefContext, FuncdefContext, Async_funcdefContext]
      checkChildrenTypes(inp, possible)
      let chs = inp.children
      if (chs.length !== 1) unsupported("decorated 1", chs[0])
      if (!(chs[0] instanceof FuncdefContext)) unsupported("decorated 2", chs[0])
      return visitor.visit(chs[0])

      return processNode("    visitDecorated", inp)
    },
    /*async_funcdef: ASYNC funcdef;*/
    visitAsync_funcdef: (inp) => {
      return processNode("    visitAsync_funcdef", inp)
    },
    /*funcdef: 'def' NAME parameters ('->' test)? ':' suite;*/
    visitFuncdef: (inp) => {
      let possible = [TerminalNode, ParametersContext, TestContext, SuiteContext]
      checkChildrenTypes(inp, possible)
      let chs = inp.children
      if (chs.length !== 5) unsupported("visitFuncdef 1", chs[0])
      return {
        category: "ast-node",
        id: genFuncdefId(),
        kind: "statement",
        type: "funcdef",
        lineNumber: actualStart(inp).line,
        children: [
          chs[0],
          {
            category: "ast-node",
            kind: "funcdef-var",
            name: chs[1].text.trim(),
            children: [chs[1]],
          },
          visitor.visit(chs[2]),
          chs[3], // colon
          visitor.visit(chs[4]),
        ],
      }
      return processNode("    visitFuncdef", inp)
    },
    /*parameters: '(' (typedargslist)? ')';*/
    visitParameters: (inp) => {
      let possible = [TerminalNode, TypedargslistContext]
      checkChildrenTypes(inp, possible)
      let children = visitNonTerminals(inp.children)

      return {
        category: "ast-node",
        kind: "parameters",
        children,
      }
      return processNode("    visitParameters", inp)
    },
    /*
        typedargslist: (tfpdef ('=' test)? (',' tfpdef ('=' test)?)* (',' (
            '*' (tfpdef)? (',' tfpdef ('=' test)?)* (',' ('**' tfpdef (',')?)?)?
          | '**' tfpdef (',')?)?)?
        | '*' (tfpdef)? (',' tfpdef ('=' test)?)* (',' ('**' tfpdef (',')?)?)?
        | '**' tfpdef (',')?);
        */
    visitTypedargslist: (inp) => {
      let possible = [TfpdefContext, TestContext, TerminalNode]
      checkChildrenTypes(inp, possible)
      return {
        category: "ast-node",
        kind: "paramlist",
        children: visitNonTerminals(inp.children),
      }
      return processNode("    visitTypedargslist", inp)
    },
    /* tfpdef: NAME (':' test)?; */
    visitTfpdef: (inp) => {
      let possible = [TestContext, TerminalNode]
      checkChildrenTypes(inp, possible)
      if (inp.children.length !== 1) unsupported("qualified parameters not supported", inp.children[0])
      let child = inp.children[0]
      if (!isTerminal(child)) unexpected("tfpdef 1", inp.children[0])
      return {
        category: "ast-node",
        kind: "param",
        name: child.text,
        children: [child],
      }
      return processNode("    visitTfpdef", inp)
    },
    /*
    varargslist: (vfpdef ('=' test)? (',' vfpdef ('=' test)?)* (',' (
            '*' (vfpdef)? (',' vfpdef ('=' test)?)* (',' ('**' vfpdef (',')?)?)?
          | '**' vfpdef (',')?)?)?
      | '*' (vfpdef)? (',' vfpdef ('=' test)?)* (',' ('**' vfpdef (',')?)?)?
      | '**' vfpdef (',')?
    );
        */
    visitVarargslist: (inp) => {
      return processNode("    visitVarargslist", inp)
    },
    /* vfpdef: NAME; */
    visitVfpdef: (inp) => {
      return processNode("    visitVfpdef", inp)
    },
    /* stmt: simple_stmt | compound_stmt; */
    visitStmt: (inp) => {
      let chs = inp.children
      if (inp.children.length !== 1) {
        debugger
        unexpected("err1 visitStmt", chs[0])
      }
      let ch = inp.children[0]
      if (![Simple_stmtContext, Compound_stmtContext].some((papa) => ch instanceof papa)) {
        debugger
        unexpected("err2 visitStmt", chs[0])
      }
      let ret = visitor.visit(ch)
      return ret
      return processNode("    visitStmt", inp)
    },
    /* simple_stmt: small_stmt (';' small_stmt)* (';')? NEWLINE; */
    visitSimple_stmt: (inp) => {
      let chs = inp.children

      let possible = [Small_stmtContext, TerminalNode]

      checkChildrenTypes(inp, possible)

      let nonTerminals = chs.filter((x) => !(x instanceof TerminalNode))
      if (nonTerminals.length > 1) {
        unsupported("visitSimple_stmt 1", chs[0])
      }

      return {
        category: "ast-node",
        kind: "padding",
        children: visitNonTerminals(chs),
      }
    },
    /*small_stmt: (expr_stmt | del_stmt | pass_stmt | flow_stmt |
                 import_stmt | global_stmt | nonlocal_stmt | assert_stmt);
                 */
    visitSmall_stmt: (inp) => {
      let possible = [
        Expr_stmtContext,
        Del_stmtContext,
        Pass_stmtContext,
        Flow_stmtContext,
        Import_stmtContext,
        Global_stmtContext,
        Nonlocal_stmtContext,
        Assert_stmtContext,
      ]
      let chs = inp.children

      checkChildrenTypes(inp, possible)

      if (chs.length !== 1) {
        unsupported("visitSmall_stmt 1", chs[0])
      }

      return visitor.visit(chs[0])
      return processNode("    visitSmall_stmt", inp)
    },
    /*
    expr_stmt: testlist_star_expr (annassign | augassign (yield_expr|testlist) | ('=' (yield_expr|testlist_star_expr))*);
                         
        */
    visitExpr_stmt: (inp) => {
      let possible = [
        Testlist_star_exprContext,
        AnnassignContext,
        AugassignContext,
        Yield_exprContext,
        TestlistContext,
        TerminalNode,
      ]
      checkChildrenTypes(inp, possible)

      let chs = inp.children
      if (chs.length === 1 && chs[0] instanceof Testlist_star_exprContext) {
        return {
          category: "ast-node",
          kind: "statement",
          lineNumber: actualStart(inp).line,
          type: "expr-statement",

          children: [visitor.visit(chs[0])],
        }
      } else if (
        chs.length === 3 &&
        chs[0] instanceof Testlist_star_exprContext &&
        chs[1] instanceof TerminalNode &&
        chs[1].text === "=" &&
        chs[2] instanceof Testlist_star_exprContext
      ) {
        return {
          kind: "statement",
          type: "assignment",
          category: "ast-node",
          lineNumber: actualStart(inp).line,
          children: visitNonTerminals(chs),
        }
      } else {
        debugger
        unsupported("visitExpr_stmt", chs[0])
      }
      return processNode("    visitExpr_stmt", inp)
    },
    /* annassign: ':' test ('=' test)?; */
    visitAnnassign: (inp) => {
      return processNode("    visitAnnassign", inp)
    },
    /* testlist_star_expr: (test|star_expr) (',' (test|star_expr))* (',')?; */
    visitTestlist_star_expr: (inp) => {
      let possible = [TestContext, TerminalNode, Star_exprContext]
      checkChildrenTypes(inp, possible)
      let chs = inp.children
      if (chs.length !== 1) {
        unsupported("visitTestlist_star_expr 1", chs[0])
      }
      if (!(chs[0] instanceof TestContext)) {
        unsupported("visitTestlist_star_expr 2", chs[0])
      }
      return visitor.visit(chs[0])
      return processNode("    visitTestlist_star_expr", inp)
    },
    /*
        augassign: ('+=' | '-=' | '*=' | '@=' | '/=' | '%=' | '&=' | '|=' | '^=' |
                '<<=' | '>>=' | '**=' | '//=');
                */
    visitAugassign: (inp) => {
      return processNode("    visitAugassign", inp)
    },
    /* del_stmt: 'del' exprlist; */
    visitDel_stmt: (inp) => {
      return processNode("    visitDel_stmt", inp)
    },
    /* pass_stmt: 'pass'; */
    visitPass_stmt: (inp) => {
      return processNode("    visitPass_stmt", inp)
    },
    /* flow_stmt: break_stmt | continue_stmt | return_stmt | raise_stmt | yield_stmt; */
    visitFlow_stmt: (inp) => {
      let possible = [
        Break_stmtContext,
        Continue_stmtContext,
        Return_stmtContext,
        Raise_stmtContext,
        Yield_stmtContext,
      ]
      checkChildrenTypes(inp, possible)
      let chs = inp.children
      if (chs.length !== 1) {
        unexpected("visitFlow_stmt 1", chs[0])
      }

      return visitor.visit(chs[0])
      return processNode("    visitFlow_stmt", inp)
    },
    /* break_stmt: 'break'; */
    visitBreak_stmt: (inp) => {
      return processNode("    visitBreak_stmt", inp)
    },
    /* continue_stmt: 'continue'; */
    visitContinue_stmt: (inp) => {
      return processNode("    visitContinue_stmt", inp)
    },
    /*return_stmt: 'return'(testlist) ?;*/
    visitReturn_stmt: (inp) => {
      let possible = [TestlistContext, TerminalNode]
      checkChildrenTypes(inp, possible)

      if (inp.children.length === 1) {
        return {
          category: "ast-node",
          kind: "statement",
          lineNumber: actualStart(inp).line,
          type: "return",
          children: inp.children,
        }
      }

      if (inp.children.length !== 2) unexpected("return assumption violated", inp)
      let testlistChild = inp.children[1]
      if (testlistChild.children.length !== 1) unsupported("Tuples not supported", inp)
      let e = visitor.visit(testlistChild.children[0])

      return {
        category: "ast-node",
        kind: "statement",
        lineNumber: actualStart(inp).line,
        type: "return",
        children: [inp.children[0], e], // 2 or 3 children
      }
    },
    /*yield_stmt: yield_expr;*/
    visitYield_stmt: (inp) => {
      return processNode("    visitYield_stmt", inp)
    },
    /*raise_stmt: 'raise' (test ('from' test)?)?;*/
    visitRaise_stmt: (inp) => {
      let possible = [TestContext, TerminalNode]
      checkChildrenTypes(inp, possible)

      let chs = inp.children
      if (chs.length === 1) unsupported("Raise without an argument is not supported", inp)
      if (chs.lengthh === 4) unsupported("from not supported", inp)
      if (chs.length !== 2) unexpected("Expected 2", inp)
      return {
        category: "ast-node",
        kind: "statement",
        lineNumber: actualStart(inp).line,
        type: "raise",
        children: visitNonTerminals(chs),
      }

      return processNode("    visitRaise_stmt", inp)
    },
    /* import_stmt: import_name | import_from;*/
    visitImport_stmt: (inp) => {
      let possible = [Import_nameContext, Import_fromContext]

      checkChildrenTypes(inp, possible)
      let chs = inp.children
      if (chs.length !== 1) unexpected("visitImport_stmt", chs[0])
      return visitor.visit(chs[0])
      return processNode("    visitImport_stmt", inp)
    },
    /* import_name: 'import' dotted_as_names; */
    visitImport_name: (inp) => {
      return processNode("    visitImport_name", inp)
    },
    /*import_from: ('from' (('.' | '...')* dotted_name | ('.' | '...')+)
                  'import' ('*' | '(' import_as_names ')' | import_as_names));
                  */
    visitImport_from: (inp) => {
      let possible = [Dotted_nameContext, Import_as_namesContext, TerminalNode]

      checkChildrenTypes(inp, possible)
      let chs = inp.children
      if (chs.length !== 4) unsupported("import should have 4 parts")
      if (chs[3].text !== "*") unsupported("last part of import should be *")
      if (chs[1].children.length !== 1) unsupported("No dots in import")
      let children = [chs[0], chs[1].children[0], chs[2], chs[3]]
      if (children.some((x) => !isTerminal(x))) unexpected("should all be terminals", chs[0])
      return {
        category: "ast-node",
        kind: "statement",
        lineNumber: actualStart(inp).line,
        type: "import",
        moduleName: chs[1].children[0].text.trim(),
        children,
      }

      return processNode("    visitImport_from", inp)
    },
    /*import_as_name: NAME ('as' NAME)?;*/
    visitImport_as_name: (inp) => {
      return processNode("    visitImport_as_name", inp)
    },
    /* dotted_as_name: dotted_name ('as' NAME)?; */
    visitDotted_as_name: (inp) => {
      return processNode("    visitDotted_as_name", inp)
    },
    /* import_as_names: import_as_name (',' import_as_name)* (',')?; */
    visitImport_as_names: (inp) => {
      return processNode("    visitImport_as_names", inp)
    },
    /* dotted_as_names: dotted_as_name (',' dotted_as_name)*; */
    visitDotted_as_names: (inp) => {
      return processNode("    visitDotted_as_names", inp)
    },
    /* dotted_name: NAME ('.' NAME)*; */
    visitDotted_name: (inp) => {
      return processNode("    visitDotted_name", inp)
    },
    /* global_stmt: 'global' NAME (',' NAME)*; */
    visitGlobal_stmt: (inp) => {
      let possible = [TerminalNode]
      checkChildrenTypes(inp, possible)
      let chs = inp.children
      return {
        category: "ast-node",
        kind: "statement",
        lineNumber: actualStart(inp).line,
        type: "global",
        name: chs[1].text.trim(),
        children: [
          chs[0],
          {
            category: "ast-node",
            kind: "nonlocal-name",
            children: [chs[1]],
          },
        ],
      }

      return processNode("    visitGlobal_stmt", inp)
    },
    /* nonlocal_stmt: 'nonlocal' NAME (',' NAME)*; */
    visitNonlocal_stmt: (inp) => {
      let possible = [TerminalNode]
      checkChildrenTypes(inp, possible)
      let chs = inp.children
      return {
        category: "ast-node",
        kind: "statement",
        lineNumber: actualStart(inp).line,
        type: "nonlocal",
        name: chs[1].text.trim(),
        children: [
          chs[0],
          {
            category: "ast-node",
            kind: "nonlocal-name",
            children: [chs[1]],
          },
        ],
      }
      return processNode("    visitNonlocal_stmt", inp)
    },
    /* assert_stmt: 'assert' test (',' test)?; */
    visitAssert_stmt: (inp) => {
      return processNode("    visitAssert_stmt", inp)
    },
    /* compound_stmt: if_stmt | while_stmt | for_stmt | try_stmt | with_stmt | funcdef | classdef | decorated | async_stmt; */
    visitCompound_stmt: (inp) => {
      let possible = [
        If_stmtContext,
        While_stmtContext,
        For_stmtContext,
        Try_stmtContext,
        With_stmtContext,
        FuncdefContext,
        ClassdefContext,
        DecoratedContext,
        Async_stmtContext,
      ]
      checkChildrenTypes(inp, possible)
      return visitor.visit(inp.children[0])

      //return processNode("    visitCompound_stmt", inp)
    },
    /* async_stmt: ASYNC (funcdef | with_stmt | for_stmt); */
    visitAsync_stmt: (inp) => {
      return processNode("    visitAsync_stmt", inp)
    },
    /* if_stmt: 'if' test ':' suite ('elif' test ':' suite)* ('else' ':' suite)?; */
    visitIf_stmt: (inp) => {
      let possible = [TestContext, SuiteContext, TerminalNode]
      checkChildrenTypes(inp, possible)
      if (userLines && userLines[actualStart(inp).line]) presence.ifstatement = true
      return {
        kind: "statement",
        lineNumber: actualStart(inp).line,
        type: "if",
        category: "ast-node",
        children: visitNonTerminals(inp.children),
      }
    },
    /* while_stmt: 'while' test ':' suite ('else' ':' suite)?; */
    visitWhile_stmt: (inp) => {
      let possible = [TestContext, SuiteContext, TerminalNode]
      checkChildrenTypes(inp, possible)
      presence.whilestatement = true
      return {
        id: genWhileLoopId(),

        category: "ast-node",
        kind: "statement",
        lineNumber: actualStart(inp).line,
        type: "while",
        children: visitNonTerminals(inp.children),
      }
    },
    /* for_stmt: 'for' exprlist 'in' testlist ':' suite ('else' ':' suite)?; */
    visitFor_stmt: (inp) => {
      let possible = [ExprlistContext, TestContext, SuiteContext, TerminalNode, TestlistContext]
      checkChildrenTypes(inp, possible)
      let chs = inp.children
      if (chs.length !== 6) {
        throw unsupported("for else clause is unsupported")
      }

      let exprlist = chs[1]
      if (exprlist.children.length !== 1) throw unsupported("can only iterate over 1 variable")

      let testlist = chs[3]
      if (testlist.children.length !== 1) throw unsupported("tuples are unsupported")
      if (userLines && userLines[actualStart(inp).line]) presence.forstatement = true
      return {
        kind: "statement",
        lineNumber: actualStart(inp).line,
        type: "for",
        category: "ast-node",
        children: [
          chs[0],
          visitor.visit(exprlist.children[0]),
          chs[2],
          visitor.visit(testlist.children[0]),
          chs[4],
          visitor.visit(chs[5]),
        ],
      }
    },
    /* try_stmt: ('try' ':' suite
               ((except_clause ':' suite)+
                ('else' ':' suite)?
                ('finally' ':' suite)? |
               'finally' ':' suite));
               */
    visitTry_stmt: (inp) => {
      let possible = [Except_clauseContext, SuiteContext, TerminalNode]
      checkChildrenTypes(inp, possible)
      let chs = inp.children
      if (chs.length % 3 !== 0) throw unexpected("assumed try children divisible by 3")
      let visited = visitNonTerminals(chs)
      let firstExceptData = undefined
      for (let i = 0; i < visited.length; i += 3) {
        let elem = visited[i]
        let nodeType = undefined
        if (elem.kind === "except") {
          nodeType = "except"
          let lineNumber = elem.lineNumber
          let id = elem.id
          if (!lineNumber) {
            debugger
            throw unexpected("expected line number")
          }
          if (!id) {
            debugger
            throw unexpected("expected id")
          }
          firstExceptData = {
            lineNumber,
            id,
          }
        } else if (elem instanceof TerminalNode) {
          if (!["try", "else", "finally"].includes(elem.text.trim()))
            unexpected("expected try, else or finally")
          nodeType = elem.text.trim()
        } else {
          unexpected("try exception violated")
        }
        visited[i + 2].tryPart = nodeType
      }
      if (!firstExceptData) {
        debugger

        throw unexpected("expected at least one except block")
      }
      return {
        kind: "statement",
        lineNumber: actualStart(inp).line,
        type: "try",
        category: "ast-node",
        children: visited,
        firstExceptData,
      }
    },
    /* with_stmt: 'with' with_item (',' with_item)*  ':' suite;  */
    visitWith_stmt: (inp) => {
      return processNode("    visitWith_stmt", inp)
    },
    /* with_item: test ('as' expr)?; */
    visitWith_item: (inp) => {
      return processNode("    visitWith_item", inp)
    },
    /* except_clause: 'except' (test ('as' NAME)?)?; */
    visitExcept_clause: (inp) => {
      let possible = [TestContext, TerminalNode]
      checkChildrenTypes(inp, possible)
      let chs = inp.children
      let visited = visitNonTerminals(chs)
      if (visited.length === 4) {
        if (!(visited[3] instanceof TerminalNode)) {
          unexpected("expected a  terminal at the end")
        }
        visited[3] = {
          kind: "except-name",
          category: "ast-node",
          children: [visited[3]],
          name: visited[3].text.trim(),
        }
      }
      // except mark
      return {
        id: genExceptId(),
        category: "ast-node",
        kind: "except",
        children: visited,
        lineNumber: actualStart(inp).line,
      }
    },
    /* suite: simple_stmt | NEWLINE INDENT stmt+ DEDENT; */
    visitSuite: (inp) => {
      let possible = [Simple_stmtContext, StmtContext, TerminalNode]

      checkChildrenTypes(inp, possible)
      let chs = inp.children
      return {
        category: "ast-node",
        kind: "block",
        children: visitNonTerminals(chs),
      }
    },
    /* test: or_test ('if' or_test 'else' test)? | lambdef; */
    visitTest: (inp) => {
      let possible = [Or_testContext, TestContext, LambdefContext, TerminalNode]
      let chs = inp.children
      if (chs.some((ch) => !possible.some((papa) => ch instanceof papa))) {
        debugger
        throw new Error("err2 visitTest")
      }

      if (chs[0] instanceof Or_testContext) {
        if (chs.length === 1) {
          return visitor.visit(chs[0])
        } else if (chs.length === 5) {
          return {
            lineNumber: actualStart(inp).line,
            category: "ast-node",
            kind: "expr",
            type: "ternary",
            children: visitNonTerminals(chs),
          }
        } else {
          unexpected("test should have 1 or 5 children", chs[0])
        }
      } else {
        unsupported("lambda is not supported", chs[0])
      }
      unexpected("this should not happen", chs[0])
    },
    /* test_nocond: or_test | lambdef_nocond; */
    visitTest_nocond: (inp) => {
      return processNode("    visitTest_nocond", inp)
    },
    /* lambdef: 'lambda' (varargslist)? ':' test; */
    visitLambdef: (inp) => {
      return processNode("    visitLambdef", inp)
    },
    /* lambdef_nocond: 'lambda' (varargslist)? ':' test_nocond; */
    visitLambdef_nocond: (inp) => {
      return processNode("    visitLambdef_nocond", inp)
    },
    /* or_test: and_test ('or' and_test)*; */
    visitOr_test: (inp) => {
      let possible = [And_testContext, TerminalNode]
      checkChildrenTypes(inp, possible)
      if (inp.children === 1) {
        return visitor.visit(inp.children[0])
      }
      let chs = inp.children.map((c, i) => {
        if (i % 2 === 1)
          return {
            category: "ast-node",
            kind: "binary-operator",
            name: c.text.trim(),
            children: [c],
          }
        return visitor.visit(c)
      })

      return reduceExpressionArray(chs, actualStart(inp).line)
    },
    /* and_test: not_test ('and' not_test)*; */
    visitAnd_test: (inp) => {
      checkChildrenTypes(inp, [Not_testContext, TerminalNode])

      if (inp.children === 1) {
        return visitor.visit(inp.children[0])
      }
      let chs = inp.children.map((c, i) => {
        if (i % 2 === 1)
          return {
            category: "ast-node",
            kind: "binary-operator",
            name: c.text.trim(),
            children: [c],
          }
        return visitor.visit(c)
      })

      return reduceExpressionArray(chs, actualStart(inp).line)
      unexpected("this shouldn't happen", chs[0])
    },
    /* not_test: 'not' not_test | comparison; */
    visitNot_test: (inp) => {
      checkChildrenTypes(inp, [Not_testContext, ComparisonContext, TerminalNode])
      let chs = inp.children
      if (chs.length === 2) {
        return {
          lineNumber: actualStart(inp).line,
          category: "ast-node",
          kind: "expr",
          type: "not",
          children: visitNonTerminals(inp.children),
        }
      }

      if (chs.length === 1) return visitor.visit(chs[0])
      return unexpected("this should not happen", chs[0])
    },
    /* comparison: expr (comp_op expr)*; */
    visitComparison: (inp) => {
      checkChildrenTypes(inp, [ExprContext, Comp_opContext])
      let chs = inp.children
      if (chs.length === 1) {
        return visitor.visit(chs[0])
      }

      if (chs.length === 3) {
        return {
          lineNumber: actualStart(inp).line,
          category: "ast-node",
          kind: "expr",
          type: "binary",
          children: visitNonTerminals(chs),
        }
      }
      unexpected("this shouldn't happen", chs[0])
      let processed = visitNonTerminals(inp.children)
      return reduceExpressionArray(processed, actualStart(inp).line)
    },
    /* comp_op: '<'|'>'|'=='|'>='|'<='|'<>'|'!='|'in'|'not' 'in'|'is'|'is' 'not'; */
    visitComp_op: (inp) => {
      let opname = inp.children.map((x) => x.text.trim()).join(" ")
      if (!["<", ">", "==", ">=", "<=", "<>", "!=", "in", "not in", "is", "is not"].includes(opname)) {
        throw new Error("what?")
      }
      return {
        category: "ast-node",
        kind: "binary-operator",
        name: opname,
        children: inp.children,
      }
    },
    /* star_expr: '*' expr; */
    visitStar_expr: (inp) => {
      return processNode("    visitStar_expr", inp)
    },
    /* expr: xor_expr ('|' xor_expr)*; */
    visitExpr: (inp) => {
      checkChildrenTypes(inp, [Xor_exprContext, TerminalNode])
      let chs = inp.children
      if (chs.length !== 1) unsupported("xor operator not supported", chs[0])
      let ret = visitor.visit(chs[0])
      return ret
    },
    /* xor_expr: and_expr ('^' and_expr)*; */
    visitXor_expr: (inp) => {
      checkChildrenTypes(inp, [And_exprContext, TerminalNode])
      let chs = inp.children
      if (chs.length !== 1) unsupported("visitXor_expr", chs[0])

      let ret = visitor.visit(chs[0])
      return ret
    },
    /* and_expr: shift_expr ('&' shift_expr)*  */
    visitAnd_expr: (inp) => {
      checkChildrenTypes(inp, [Shift_exprContext, TerminalNode])
      let chs = inp.children
      if (chs.length !== 1) unsupported("visitAnd_expr", chs[0])
      let ret = visitor.visit(chs[0])
      return ret
    },
    /* shift_expr: arith_expr (('<<'|'>>') arith_expr)*; */
    visitShift_expr: (inp) => {
      checkChildrenTypes(inp, [Arith_exprContext, TerminalNode])
      let chs = inp.children
      if (chs.length !== 1) unsupported("visitShift_expr", chs[0])
      let ret = visitor.visit(chs[0])
      return ret
    },
    /* arith_expr: term (('+'|'-') term)*; */
    visitArith_expr: (inp) => {
      checkChildrenTypes(inp, [TermContext, TerminalNode])
      let chs = inp.children.map((c, i) => {
        if (i % 2 === 1)
          return {
            category: "ast-node",
            kind: "binary-operator",
            name: c.text.trim(),
            children: [c],
          }
        return visitor.visit(c)
      })

      let ret = reduceExpressionArray(chs, actualStart(inp).line)
      return ret
    },
    /* term: factor (('*'|'@'|'/'|'%'|'//') factor)*; */
    visitTerm: (inp) => {
      checkChildrenTypes(inp, [FactorContext, TerminalNode])

      let hasModulo = inp.children.some((x) => {
        return x instanceof TerminalNode && x.text.trim() === "%"
      })
      if (userLines && userLines[actualStart(inp).line] && hasModulo) presence.modulo = true

      let chs = inp.children.map((c, i) => {
        if (i % 2 === 1)
          return {
            category: "ast-node",
            kind: "binary-operator",
            name: c.text.trim(),
            children: [c],
          }
        return visitor.visit(c)
      })

      let ret = reduceExpressionArray(chs, actualStart(inp).line)
      return ret
    },
    /* factor: ('+'|'-'|'~') factor | power; */
    visitFactor: (inp) => {
      checkChildrenTypes(inp, [FactorContext, PowerContext, TerminalNode])
      if (inp.children.length !== 1) {
        unsupported("visitFactor", inp.children[0])
      }
      let ret = visitor.visit(inp.children[0])
      return ret
    },
    /* power: atom_expr ('**' factor)?; */
    visitPower: (inp) => {
      checkChildrenTypes(inp, [Atom_exprContext, FactorContext, TerminalNode])

      if (inp.children.length !== 1) {
        unsupported("visitPower", inp.children[0])
      }
      let ret = visitor.visit(inp.children[0])
      return ret
    },
    /* atom_expr: (AWAIT)? atom trailer*; */
    visitAtom_expr: (inp) => {
      checkChildrenTypes(inp, [AtomContext, TrailerContext, TerminalNode])
      if (!(inp.children[0] instanceof AtomContext)) {
        unsupported("visitAtom_expr")
      }
      let chs = visitNonTerminals(inp.children)

      let head = chs[0]
      for (let i = 1; i < chs.length; i++) {
        let child = chs[i]
        if (!["funcall", "dot", "bracket"].includes(child.type)) {
          unexpected("visitAtom_expr 1", chs[0])
        }
        let children = [head, ...child.children]
        //if (child.type === "funcall") debugger
        let lineNumber = actualStart(inp).line
        head = {
          lineNumber,
          category: "ast-node",
          kind: "expr",
          type: child.type,
          children,
        }
      }
      return head
    },
    /*
        atom: ('(' (yield_expr|testlist_comp)? ')' |
           '[' (testlist_comp)? ']' |
           '{' (dictorsetmaker)? '}' |
           NAME | NUMBER | STRING+ | '...' | 'None' | 'True' | 'False');
        */
    visitAtom: (inp) => {
      if (![1, 2, 3].includes(inp.children.length)) {
        unsupported("visitAtom", inp.children[0])
      }
      let chs = inp.children
      let child = inp.children[0]
      let contents = child.text

      if (contents === "(") {
        if (chs.length !== 3) {
          unsupported("The empty tuple is not supported", chs[0])
        }
        return {
          lineNumber: actualStart(inp).line,
          category: "ast-node",
          kind: "expr",
          type: "paren",
          children: visitNonTerminals(inp.children),
        }
      }
      if (contents === "[") {
        if (chs.length === 2) {
          if (userLines && userLines[actualStart(inp).line]) presence.list = true
          return {
            lineNumber: actualStart(inp).line,
            category: "ast-node",
            kind: "expr",
            type: "array-literal",
            children: [chs[0], chs[1]],
          }
        }
        if (inp.children.length !== 3) unsupported("visitAtom 2", chs[0])
        let elements = visitNonTerminals(chs[1].children)
        if (userLines && userLines[actualStart(inp).line]) presence.list = true
        return {
          lineNumber: actualStart(inp).line,
          category: "ast-node",
          kind: "expr",
          type: "array-literal",
          children: [chs[0], ...elements, chs[2]],
        }
      }

      if (contents === "{") {
        if (chs.length === 2) {
          if (userLines && userLines[actualStart(inp).line]) presence.dict = true
          return {
            lineNumber: actualStart(inp).line,
            category: "ast-node",
            kind: "expr",
            type: "dict-literal",
            children: [
              chs[0],
              {
                category: "ast-node",
                kind: "dict-key-value-pairs",
                children: [],
              },
              chs[1],
            ],
          }
        }
        if (inp.children.length !== 3) unsupported("visitAtom 3", chs[0])
        let elements = visitNonTerminals(chs[1].children)
        let keyValuePairs = {
          category: "ast-node",
          kind: "dict-key-value-pairs",
          children: elements,
        }
        if (userLines && userLines[actualStart(inp).line]) presence.dict = true
        return {
          lineNumber: actualStart(inp).line,
          category: "ast-node",
          kind: "expr",
          type: "dict-literal",
          children: [chs[0], keyValuePairs, chs[2]],
        }
      }

      let first = contents[0]
      if (isNumber(first) || first === ".") {
        if (userLines && userLines[actualStart(inp).line]) presence.number = true
        let ret = {
          isPrimitiveLiteral: true,
          category: "ast-node",
          kind: "expr",
          type: "number-literal",
          number: parseFloat(contents),
          children: [child],
        }
        return ret
      }
      if (["'", '"'].includes(first)) {
        if (userLines && userLines[actualStart(inp).line]) presence.string = true
        if (first === "'") {
          unsupported("single quotes unsupported", chs[0])
        }
        let ret = {
          isPrimitiveLiteral: true,
          category: "ast-node",
          type: "string-literal",
          kind: "expr",
          stringContents: contents.slice(1, contents.length - 1),
          children: [child],
        }
        return ret
      }
      if (["True", "False"].includes(contents)) {
        if (userLines && userLines[actualStart(inp).line]) presence.bool = true
        let ret = {
          isPrimitiveLiteral: true,
          bool: contents === "True",
          category: "ast-node",
          kind: "expr",
          type: "bool-literal",
          name: contents,
          children: [child],
        }
        return ret
      }
      if (contents === "None") {
        if (userLines && userLines[actualStart(inp).line]) presence.none = true
        let ret = {
          isPrimitiveLiteral: true,
          value: null,
          category: "ast-node",
          kind: "expr",
          type: "none-literal",
          children: [child],
        }
        return ret
      }

      let ret = {
        category: "ast-node",
        kind: "expr",
        type: "var",
        name: contents,
        children: [child],
      }
      return ret
    },
    /* testlist_comp: (test|star_expr) ( comp_for | (',' (test|star_expr))* (',')? ); */
    visitTestlist_comp: (inp) => {
      checkChildrenTypes(inp, [TestContext, Star_exprContext, TerminalNode])
      let chs = inp.children
      if (chs.length !== 1) {
        unsupported("tuples not supported", chs[0])
      }
      return visitor.visit(chs[0])
    },
    /* trailer: '(' (arglist)? ')' | '[' subscriptlist ']' | '.' NAME; */
    visitTrailer: (inp) => {
      checkChildrenTypes(inp, [ArglistContext, SubscriptlistContext, TerminalNode])

      let types = {
        "(": "funcall",
        "[": "bracket",
        ".": "dot",
      }

      let chs = inp.children

      let type = types[chs[0].text.trim()]
      if (type === undefined) {
        unsupported("visitTrailer", chs[0])
      }
      if (type === "dot") {
        return {
          type,
          children: [
            chs[0],
            {
              category: "ast-node",
              type: "attribute-name",
              name: chs[1].text.trim(),
              children: [chs[1]],
            },
          ],
        }
      }
      return {
        type,
        children: visitNonTerminals(inp.children),
      }

      return processNode("    visitTrailer", inp)
    },
    /* subscriptlist: subscript (',' subscript)* (',')?; */
    visitSubscriptlist: (inp) => {
      checkChildrenTypes(inp, [SubscriptContext, TerminalNode])
      let chs = inp.children
      if (inp.children.length !== 1) {
        unsupported("visitSubscriptlist 1 ", chs[0])
      }
      if (!(inp.children[0] instanceof SubscriptContext)) {
        unsupported("visitSubscriptlist 2 ", chs[0])
      }
      return visitor.visit(inp.children[0])
      return processNode("    visitSubscriptlist", inp)
    },
    /* subscript: test | (test)? ':' (test)? (sliceop)?; */
    visitSubscript: (inp) => {
      checkChildrenTypes(inp, [TestContext, SliceopContext, TerminalNode])
      if (inp.children.length !== 1) {
        unsupported("visitSubscript 1 ", inp.children[0])
      }
      if (!(inp.children[0] instanceof TestContext)) {
        unsupported("visitSubscript 2 ", inp.children[0])
      }
      return visitor.visit(inp.children[0])

      return processNode("    visitSubscript", inp)
    },
    /* sliceop: ':' (test)?; */
    visitSliceop: (inp) => {
      return processNode("    visitSliceop", inp)
    },
    /* exprlist: (expr|star_expr) (',' (expr|star_expr))* (',')?; */
    visitExprlist: (inp) => {
      let possible = [ExprContext, Star_exprContext, TerminalNode]
      checkChildrenTypes(inp, possible)
      return {
        category: "ast-node",

        kind: "exprlist",
        children: visitNonTerminals(inp.children),
      }
    },
    /* testlist: test (',' test)* (',')?; */
    visitTestlist: (inp) => {
      let possible = [TestContext, TerminalNode]
      checkChildrenTypes(inp, possible)
      //debugger
      return {
        category: "ast-node",

        kind: "testlist",
        children: visitNonTerminals(inp.children),
      }

      return processNode("    visitTestlist", inp)
    },
    /* dictorsetmaker: ( ((test ':' test | '**' expr)
                       (comp_for | (',' (test ':' test | '**' expr))* (',')?)) |
                      ((test | star_expr)
                       (comp_for | (',' (test | star_expr))* (',')?)) );
     */
    visitDictorsetmaker: (inp) => {
      checkChildrenTypes(inp, [TestContext, TerminalNode, ExprContext, Star_exprContext])
      let chs = inp.children
      let ret = []
      for (let i = 0; i < chs.length; i += 4) {
        let key = chs[i]
        let colon = chs[i + 1]
        let v = chs[i + 2]
        if (colon.text !== ",") unsupported("expected colon", chs[0])
        let keyvaluechildren = []
        keyvaluechildren.push(visitor.visit(key))
        keyvaluechildren.push(colon)
        keyvaluechildren.push(visitor.visit(v))
        if (i + 3 < chs.length) keyvaluechildren.push(chs[i + 3])
        ret.push({
          kind: "dict-key-value",
          children: keyvaluechildren,
        })
      }
      return ret
    },
    /* classdef: 'class' NAME ('(' (arglist)? ')')? ':' suite; */
    visitClassdef: (inp) => {
      let possible = [TerminalNode, ArglistContext, SuiteContext]
      checkChildrenTypes(inp, possible)
      let chs = inp.children
      let middle = chs.slice(2, chs.length - 2)
      let ret = {
        category: "ast-node",
        id: genClassdefId(),
        kind: "statement",
        type: "classdef",
        lineNumber: actualStart(inp).line,
        children: [
          chs[0],
          {
            category: "ast-node",
            kind: "classdef-var",
            name: chs[1].text.trim(),
            children: [chs[1]],
          },
          ...visitNonTerminals(middle),
          chs[chs.length - 2], // colon
          visitor.visit(chs[chs.length - 1]),
        ],
      }

      return ret

      return processNode("    visitClassdef", inp)
    },
    /* arglist: argument (',' argument)*  (',')?; */
    visitArglist: (inp) => {
      checkChildrenTypes(inp, [TerminalNode, ArgumentContext])

      return {
        category: "ast-node",

        kind: "arglist",
        children: visitNonTerminals(inp.children),
      }
    },

    /*
    argument: ( test (comp_for)? |
                test '=' test |
                '**' test |
                '*' test );
        */

    visitArgument: (inp) => {
      checkChildrenTypes(inp, [TestContext, Comp_forContext, ArgumentContext])
      let chs = inp.children
      if (chs.length !== 1) {
        unsupported("visitArgument 1", chs[0])
      }
      if (!(chs[0] instanceof TestContext)) {
        unsupported("visitArgument 2", chs[0])
      }
      return visitor.visit(chs[0])
    },

    /*comp_iter: comp_for | comp_if;*/

    visitComp_iter: (inp) => {
      return processNode("list comprehensions not supported", inp)
    },
    /* comp_for: (ASYNC)? 'for' exprlist 'in' or_test (comp_iter)?;  */
    visitComp_for: (inp) => {
      return processNode("list comprehensions not supported", inp)
    },
    /* comp_if: 'if' test_nocond (comp_iter)?; */
    visitComp_if: (inp) => {
      return processNode("if comprehension not supported", inp)
    },
    /*  comp_if: 'if' test_nocond (comp_iter)?; */
    visitEncoding_decl: (inp) => {
      return processNode("if comprehension not supported", inp)
    },
    /* yield_expr: 'yield' (yield_arg)?; */
    visitYield_expr: (inp) => {
      return processNode("yield not supported", inp)
    },
    /* yield_arg: 'from' test | testlist; */
    visitYield_arg: (inp) => {
      return processNode("yield not supported", inp)
    },
    visitTerminalNode: (e) => {
      console.log(e.text)
      return e
    },
  })

  source = source.trimRight()
  let srcLines = source.split("\n").map((x) => (x += "\n"))

  let getIntervalFromSource = (initialLine, initialChar, endLine, endChar) => {
    initialLine--
    endLine--
    if (initialLine === endLine) {
      return srcLines[initialLine].slice(initialChar, endChar)
    }
    let ret = srcLines[initialLine].slice(initialChar)
    for (let i = initialLine + 1; i < endLine; i++) {
      ret += srcLines[i]
    }
    ret += srcLines[endLine].slice(0, endChar)
    return ret
  }

  let emptiness = (initialLine, initialChar, endLine, endChar) => {
    let r = getIntervalFromSource(initialLine, initialChar, endLine, endChar)

    return r
  }

  let addSpaces = (nonterminal, line, charPositionInLine) => {
    let chs = nonterminal.children

    if (chs.length === 0) {
      return []
      throw new Error("addSpaces 1")
    }
    let children = []

    for (let i = 0; i < chs.length; i++) {
      let c = chs[i]
      if (isEmptyTerminal(c)) continue

      let start = actualStart(c)
      if (!start) debugger
      actualStart(c)
      let stop = actualStop(c)
      let spaces = emptiness(line, charPositionInLine, start.line, start.charPositionInLine)
      if (spaces.length > 0) children.push(spaces)
      let cp = isTerminal(c) ? c.text : addSpaces(c, start.line, start.charPositionInLine)
      children.push(cp)
      line = stop.line
      charPositionInLine = stop.charPositionInLine

      // if (isTerminal(c)) {
      //     let elemStart = actualStart(c)
      //     let elemStop = actualStop(c)
      //     let spaces = emptiness(line, charPositionInLine, elemStart.line, elemStart.charPositionInLine)
      //     children.push(spaces)
      //     children.push(c.text)
      //     line = elemStop.line
      //     charPositionInLine = elemStop.charPositionInLine
      //     continue
      // }
      // if (!isASTNode(c)) throw new Error("What")
      // let childInfo = addSpaces(c, line, charPositionInLine)
      // children.push(childInfo.processed)
      // //if (numAST(children) !== numAST(chs)) debugger
      // line = childInfo.line
      // charPositionInLine = childInfo.charPositionInLine
    }
    for (let i = 0; i < children.length; i++) {
      if (isTerminal(children[i])) {
        throw new Error("addSpaces 1")
      }
      let ch = children[i]
      if (!isASTNode(ch) && !isString(ch)) {
        throw new Error("addSpaces 2")
      }
    }
    let previousChildren = nonterminal.children.filter((x) => x.kind !== undefined).length
    let newChildren = children.filter((x) => x.kind !== undefined).length
    // if (previousChildren !== newChildren) debugger
    // if (nonterminal.kind === "padding") debugger

    return { ...nonterminal, children: children }
  }

  let normalize = (tree) => {
    let withSpaces = addSpaces(tree, 1, 0)

    // let merged = mergeSpaces(withSpaces.processed)
    let withId = tokenify(withSpaces)
    //debugger
    return withId
  }

  let ast = parse(source)

  let mkToken = (text) => {
    return {
      category: "token",
      id: genTokenId(),
      text,
    }
  }

  if (ast.hasErrors) {
    let err = ast.errors[0]
    return {
      lineNum: err.lineNum,
      hasError: true,
      errorMsg: err.errorMsg,
      errorMsgToken: mkToken(err.errorMsg || ""),
    }
    // debugger
    // console.log("ERRORS FOUND")
    // ast.errors.forEach(e => {
    //     console.log(errorToString(e))
    // })
    // return
  }

  let getErrors = (cst) => {
    let errors = []
    let visitor = createVisitor({
      visitErrorNode: (e) => {
        errors.push({ lineNum: e.symbol.line })
      },
    })
    visitor.visit(cst)
    return errors
  }

  let errors = getErrors(ast)

  if (errors.length > 0) {
    return {
      lineNum: errors[0].lineNum,
      hasError: true,
      errorMsg: errors[0].errorMsg,
      errorMsgToken: mkToken(errors[0].errorMsg || ""),
    }
  }
  let result = undefined
  result = visitor.visit(ast)

  let collectTokens = (cst) => {
    let tokens = []
    let doit = (tree) => {
      if (tree instanceof TerminalNode) {
        if (tree.text.trim().length > 0 && tree.symbol.start < source.length) {
          let start = actualStart(tree)
          let topush = {
            charpos: start.charPositionInLine,
            lineNumber: start.line,
            text: tree.text.trim(),
          }
          tokens.push(topush)
        }
        return
      }
      tree.children.forEach(doit)
    }
    doit(cst)
    return tokens
  }

  let tree = normalize(result)
  let allTokens = collectTokens(ast)

  // try {
  //     result = visitor.visit(ast)
  // } catch (err) {
  //     let errstr = err.toString()
  //     // if (err.notSupported) {
  //     //     return {
  //     //         lineNum: err.lineNum,
  //     //         hasError: true,
  //     //         errorMsg: errstr,
  //     //         errorMsgToken: mkToken(errstr)
  //     //     }
  //     // }
  //     //if (err.notExpected) {
  //     // have to send an email to myself
  //     // debugger
  //     return {
  //         lineNum: err.lineNum,
  //         hasError: true,
  //         errorMsg: errstr,
  //         errorMsgToken: mkToken(errstr)
  //     }
  //     //}
  //     // have to send an email to myself
  // }

  return { ast: tree, tokens: allTokens, presence }
}

//console.log("length", src.length, src.split("\n")[0].length, src.split("\n")[1].length)

//parsePython(fullprog)
export { parsePython, unparse }
