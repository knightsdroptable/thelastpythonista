import * as R from "ramda"
//import { nextBreakpointButton } from "./nextbreakpointbutton"
import { mouseMoveDefaultTime } from "./globals.js"
import { playAudio, stopAudio, getAudio, playAudioUntilFinished } from "./playaudio"
import { dispatch, getTutorialPrecomputes, store } from "./store"
import CLICK_SOUND from "./click_sound.mp3"
import React, { useEffect, useState, useRef } from "react"
import { useSelector } from "react-redux"
import { recordAudio } from "./record-audio"
import { hibernate, wakeup } from "./hibernate"
import { drive } from "./drive"
import { getResourceURL } from "./resources"
import { getMultiplierOfSpeed } from "./speed.js"
import produce from "immer"
import { setTutorialLoop, stopLoop } from "./loops.js"
const post = async (url, body) => {
  url = "http://localhost:2777" + url
  let data = new FormData()
  for (let k in body) {
    data.append(k, body[k])
  }
  console.log("sending to", url)
  let resp = await fetch(url, {
    method: "POST",
    body: data,
  })
  return resp.text()
}

let tunedDelay = (t) => {
  let currentSpeed = store.getState().currentSpeed
  let m = getMultiplierOfSpeed(currentSpeed)
  t = t / m
  return new Promise((res) => setTimeout(res, t))
}

let delay = (t) => new Promise((res) => setTimeout(res, t))

let getDestinationIndex = (animationFrames, destination) => {
  if (!destination) {
    throw new Error("missing destination")
  }
  let ret = animationFrames.findIndex((x) => R.equals(x.stepDescriptor, destination))

  if (ret === -1) {
    debugger
    throw new Error("Unable to find index")
  }
  return ret
}

export function* playBuiltinAudioUntilFinished(path, opts) {
  stopAudio()
  let audio = yield getAudio(path)
  if (opts?.volume !== undefined) audio.volume = opts.volume
  yield playAudioUntilFinished(audio)
}
let pressButton = function* (idInInterface) {
  dispatch({ type: "mouse-over-noclick", value: idInInterface })
  yield tunedDelay(mouseMoveDefaultTime)
  dispatch({ type: "mouse-over-click", value: idInInterface })

  if (store.getState().narrationVoice) yield playBuiltinAudioUntilFinished(CLICK_SOUND, { volume: 0.1 })
  yield tunedDelay(300)

  dispatch({ type: "mouse-over-click", value: undefined })
  dispatch({ type: "mouse-over-noclick", value: undefined })
}

export let startAudioRecording = (evt) => {
  if (evt) evt.stopPropagation()
  function* run() {
    let { start, stop } = yield recordAudio(store.getState().mic.deviceId)
    yield start()
    dispatch({ type: "audio-recording", recording: true })
    yield hibernate("stop-recording")
    dispatch({ type: "audio-recording", recording: false })
    let { audioBlob, audioUrl } = yield stop()
    let resp = yield post("/upload-wav", {
      wav: audioBlob,
    })
    try {
      let parsed = JSON.parse(resp)
      if (!parsed.success) {
        alert("upload failed: " + parsed.reason)
        return
      }
      dispatch({ type: "resource-uploaded-to-event", resourceLocation: parsed.fileName })
    } catch (err) {
      alert("what? " + resp)
    }
  }
  drive(run())
}

export let playVoiceAudio = async (evt) => {
  if (evt) evt.stopPropagation()
  let eventId = store.getState().tutorialFocus.id
  let eventDetails = store.getState().content.eventDescriptions[eventId]
  stopAudio()
  let resourceLocation = eventDetails.resourceLocation

  let loc = getResourceURL(resourceLocation)
  let aud = await getAudio(loc)
  playAudio(aud)
}

function ChangeVoiceView({ eventDetails, onClose }) {
  //let eventDetails = tdesc.events[focus.id]
  const audioRecording = useSelector((s) => s.audioRecording)
  //const mic = useSelector((s) => s.mic)
  return (
    <div
      style={{
        position: "fixed",
        left: "0px",
        right: "0px",
        top: "0px",
        bottom: "0px",
        backgroundColor: "white",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <div style={{ width: "500px", border: "1px solid", overflow: "scroll" }}>
        <button
          disabled={audioRecording}
          onClick={(evt) => {
            evt.stopPropagation()
            onClose()
          }}
        >
          close
        </button>
        {audioRecording ? (
          <button
            onClick={(evt) => {
              evt.stopPropagation()
              wakeup("stop-recording")
            }}
          >
            stop(F1)
          </button>
        ) : (
          <button disabled={audioRecording} onClick={startAudioRecording}>
            record(F1)
          </button>
        )}

        {eventDetails.resourceLocation && (
          <button disabled={audioRecording} onClick={playVoiceAudio}>
            Play(F2)
          </button>
        )}
        <button
          onClick={(evt) => {
            evt.stopPropagation()
            store.dispatch({ type: "prev-voice-event" })
          }}
        >
          prev(F3)
        </button>
        <button
          onClick={(evt) => {
            evt.stopPropagation()
            store.dispatch({ type: "next-voice-event" })
          }}
        >
          next(F4)
        </button>

        <textarea
          value={eventDetails.script || ""}
          onClick={(evt) => evt.stopPropagation()}
          onChange={(evt) => {
            dispatch({
              type: "set-event-details",
              propName: "script",
              propValue: evt.target.value,
              eventId: eventDetails.id,
            })
          }}
          style={{ height: "350px", width: "450px", border: "1px solid black" }}
        />
        <button
          onClick={() => {
            dispatch({ type: "add-definition" })
          }}
        >
          add definition
        </button>
        {(eventDetails.definitions || []).map((def, index) => {
          return (
            <>
              <div>
                <input
                  type="text"
                  value={def.header}
                  onChange={(evt) => {
                    dispatch({ type: "set-definition-header", text: evt.target.value, index })
                  }}
                />
              </div>
              <div>
                <textarea
                  style={{ height: "100px", width: "400px", border: "1px solid black" }}
                  onChange={(evt) => {
                    dispatch({ type: "set-definition-body", text: evt.target.value, index })
                  }}
                  value={def.body}
                />
              </div>
              <button
                onClick={() => {
                  dispatch({ type: "del-definition", index })
                }}
              >
                del
              </button>
            </>
          )
        })}
      </div>
    </div>
  )
}

function VoiceView({ eventDetails }) {
  // let r = useRef(undefined)
  // useEffect(() => {
  //   let listener = (evt) => {
  //     if (!evt.ctrlKey) return
  //     console.log("control pressed", evt.key)
  //     let k = evt.key
  //     debugger
  //     if (evt.key.toLowerCase() === "j") {
  //     } else if (evt.key.toLowerCase() === "k") {
  //       // // case sensitive
  //       // evt.preventDefault()
  //       // evt.stopPropagation()
  //       // copyCurrentExercise()
  //     }
  //   }

  //   r.current.addEventListener("keydown", listener)
  //   return () => r.current.removeEventListener("keydown", listener)
  // }, [])

  const stateChangeVoice = useSelector((s) => s.changeVoice && s.tutorialFocus?.id === eventDetails.id)
  return (
    <>
      {stateChangeVoice && (
        <ChangeVoiceView
          onClose={() => {
            dispatch({ type: "set-state-prop", name: "changeVoice", value: false })
          }}
          eventDetails={eventDetails}
        />
      )}
      <div style={{ display: "flex" }}>
        <textarea
          value={eventDetails.script || ""}
          onChange={(evt) => {
            dispatch({
              type: "set-event-details",
              propName: "script",
              propValue: evt.target.value,
              eventId: eventDetails.id,
            })
          }}
          style={{ height: "22px", border: "1px solid #aaa", flex: 1 }}
        />
        <button
          onClick={(evt) => {
            dispatch({ type: "tutorial-focus-click", goal: { type: "event", id: eventDetails.id } })

            dispatch({ type: "set-state-prop", name: "changeVoice", value: true })
            evt.stopPropagation()
          }}
        >
          edit
        </button>
      </div>
    </>
  )
}

function PTDelay({ eventDetails }) {
  return (
    <div>
      <input
        type="text"
        value={eventDetails.time || "0"}
        onChange={(evt) => {
          let t = parseInt(evt.target.value)
          if (isNaN(t)) return
          dispatch({
            type: "set-event-details",
            propName: "time",
            propValue: t,
            eventId: eventDetails.id,
          })
        }}
      />
    </div>
  )
}

function PTBreakpoint({ eventDetails }) {
  return (
    <div>
      <input
        type="text"
        value={eventDetails.sourceFileLineNumber === undefined ? "" : eventDetails.sourceFileLineNumber}
        onChange={(evt) => {
          let t = parseInt(evt.target.value)
          if (isNaN(t)) t = undefined
          dispatch({
            type: "set-event-details",
            propName: "sourceFileLineNumber",
            propValue: t,
            eventId: eventDetails.id,
          })
        }}
      />
    </div>
  )
}

// function ChangePictureView() {
//   return (
//     <div
//       style={{
//         position: "fixed",
//         top: "0px",
//         left: "0px",
//         right: "0px",
//         bottom: "0px",
//         backgroundColor: "white",
//       }}
//     >
//       <form>
//         <input
//           type="file"
//           accept={"image/png, image/jpeg"}
//           onChange={async (evt) => {
//             let file = evt.target.files[0]
//             const data = new FormData()
//             data.append("file", file)
//             let response = await fetch("http://localhost:2777/upload-resource", {
//               method: "POST",
//               body: data,
//             })
//             let body = await response.text()
//             let parsed = JSON.parse(body)
//             if (!parsed.success) {
//               console.log("fail")
//               alert("fail to upload image " + parsed.reason)
//               return
//             }
//             if (!parsed.fileName) throw new Error("what?")
//             dispatch({ type: "resource-uploaded-to-segment", resourceLocation: parsed.fileName })
//           }}
//         />
//       </form>
//       <div>{focusedSegmentDetails.resourceLocation}</div>
//       <button
//         onClick={() => {
//           dispatch({ type: "picture-view-shown", shown: false })
//         }}
//       >
//         close
//       </button>
//     </div>
//   )
// }

// function PictureView({ eventDetails }) {
//   const [state, setState] = useState({ changePictureView: false })
//   return (
//     <>
//       {state.changePictureView && <ChangePictureView eventDetails={eventDetails} />}
//       <button
//         onClick={() => {
//           setState((state) =>
//             produce(state, (state) => {
//               changePictureView: true
//             })
//           )
//         }}
//       >
//         edit
//       </button>
//     </>
//   )
// }

let seekToInputView = ({ eventDetails, delta }) => {
  return (
    <div>
      <button
        onClick={() => {
          let stateAnimationFrames = store.getState().animationFrames
          if (!stateAnimationFrames) throw new Error("HUH")
          let stateFrameIndex = store.getState().frameIndex + delta
          if (stateFrameIndex === undefined) throw new Error("what")
          let stepData = stateAnimationFrames[stateFrameIndex]
          let stepDescriptor = stepData.stepDescriptor
          if (!stepDescriptor) {
            debugger
            throw new Error("expecting step descriptor")
          }
          dispatch({
            type: "set-event-details",
            propName: "interpretationStep",
            propValue: stepDescriptor,
            eventId: eventDetails.id,
          })
        }}
      >
        here
      </button>
      {eventDetails.interpretationStep && JSON.stringify(eventDetails.interpretationStep)}
    </div>
  )
}

let seekToCurrentAction = (delta) =>
  function* ({ eventDetails, animationFrames }) {
    let destination = eventDetails.interpretationStep
    let destinationIndex = getDestinationIndex(animationFrames, destination)
    if (destinationIndex === undefined) throw new Error("missing destination index")
    destinationIndex += delta
    let currentFrameIndex = store.getState().frameIndex
    if (currentFrameIndex === destinationIndex) return
    // if (destinationIndex < currentFrameIndex) {
    //   yield pressButton("top-controls-rewind")
    // } else {
    //   yield pressButton("top-controls-fast-forward")
    // }
    if (destinationIndex === undefined) throw new Error("invalid destination index")
    dispatch({ type: "set-frame-index", index: destinationIndex })
  }

let seekToSkipAction = ({ eventDetails, animationFrames, delta }) => {
  let destination = eventDetails.interpretationStep
  let destinationIndex = getDestinationIndex(animationFrames, destination)
  if (destinationIndex === undefined) throw new Error("invalid destination index")
  destinationIndex += delta
  dispatch({ type: "set-frame-index", index: destinationIndex })
}

let nextBreakpointButton = (stateFrameIndex, stateAnimationFrames, stateBreakpoints) => {
  for (let i = stateFrameIndex + 1; i < stateAnimationFrames.length; i++) {
    let aframe = stateAnimationFrames[i]
    let nln = aframe.animationFrame.newLineNum
    if (nln !== undefined && stateBreakpoints[nln - 1]) {
      dispatch({ type: "set-frame-index", index: i })
      return
    }
  }
}

export let tutorialEventsData = [
  {
    id: "empty",
    name: "empty",
    skipAction: () => {},
    currentAction: function* () {},
  },
  {
    id: "voice",
    name: "voice",
    descriptiveName: (eventDetails) => {
      return (
        <textarea
          value={eventDetails.script || ""}
          onChange={(evt) => {
            dispatch({
              type: "set-event-details",
              propName: "script",
              propValue: evt.target.value,
              eventId: eventDetails.id,
            })
          }}
          style={{ height: "22px", width: "100%", border: "1px solid #aaa" }}
        />
      )

      //return "v: " + eventDetails.script
    },
    inputView: (eventDetails) => {
      return <VoiceView key="voice-view" eventDetails={eventDetails} />
    },
    skipAction: ({ eventDetails }) => {
      dispatch({ type: "voice-script", script: eventDetails.script, definitions: eventDetails.definitions })
    },
    currentAction: function* ({ eventDetails }) {
      dispatch({ type: "voice-script", script: eventDetails.script, definitions: eventDetails.definitions })
      if (!eventDetails.resourceLocation) {
        yield tunedDelay(1000)
      } else {
        let url = getResourceURL(eventDetails.resourceLocation)
        let audio = yield getAudio(url)
        if (!store.getState().narrationVoice) {
          let t = 1000 * audio.duration
          yield tunedDelay(t)
        } else {
          yield playAudioUntilFinished(audio)
        }
      }
    },
  },
  {
    id: "pt-delay",
    name: "delay",
    inputView: (eventDetails) => {
      return <PTDelay eventDetails={eventDetails} />
    },
    skipAction: () => {},
    currentAction: function* ({ eventDetails }) {
      yield tunedDelay(eventDetails.time || 0)
    },
  },

  {
    id: "pt-big-turtle-button-press",
    name: "Press big turtle button",
    skipAction: ({ turtleAnimationFrames, turtleGoal }) => {
      dispatch({
        type: "pt-click-big-turtle-button",
        animationFrames: turtleAnimationFrames,
        turtleGoal: turtleGoal,
      })
    },

    currentAction: function* ({ turtleAnimationFrames, turtleGoal }) {
      yield pressButton("big-turtle-button")
      dispatch({ type: "pt-click-big-turtle-button", animationFrames: turtleAnimationFrames, turtleGoal })
    },
  },
  {
    id: "pt-initial-turtle-play-to-end",
    name: "turtle play to end",
    skipAction: ({ turtleAnimationFrames }) => {
      dispatch({ type: "set-frame-index", index: turtleAnimationFrames.length - 1 })
    }, // when you skip over
    currentAction: function* ({ animationFrames, turtleAnimationFrames }) {
      while (store.getState().frameIndex < turtleAnimationFrames.length - 1) {
        dispatch({ type: "next-frame" })
        yield tunedDelay(500)
      }
    },
  },
  {
    id: "pt-turtle-stop",
    name: "turtle stop",
    skipAction: () => {
      dispatch({ type: "stop-button" })
    },
    currentAction: function* () {
      yield pressButton("left-hand-control-stop")
      dispatch({ type: "stop-button" })
    },
  },
  {
    id: "pt-press-run",
    name: "press run",
    skipAction: ({ animationFrames }) => {
      dispatch({ type: "set-animation-frames", animationFrames })
      dispatch({ type: "set-frame-index", index: 0 })
      dispatch({ type: "pt-pressed-play" })
    }, // when you skip over
    currentAction: function* ({ animationFrames }) {
      let start = Date.now()
      yield pressButton("top-controls-run")
      let dt = Date.now() - start
      console.log("time took", dt)
      dispatch({ type: "set-animation-frames", animationFrames })
      dispatch({ type: "set-frame-index", index: 0 })
      dispatch({ type: "pt-pressed-play" })
    },
  },
  {
    id: "pt-press-play",
    name: "press play",
    skipAction: ({ animationFrames }) => {
      dispatch({ type: "pt-pressed-play" })
    }, // when you skip over
    currentAction: function* () {
      yield pressButton("top-controls-play-pause")
      dispatch({ type: "pt-pressed-play" })
    },
  },

  {
    id: "pt-press-stop",
    name: "press stop",
    skipAction: () => {
      dispatch({ type: "stop-button" })
    }, // when you skip over
    currentAction: function* () {
      yield pressButton("top-controls-stop")
      dispatch({ type: "stop-button" })
    },
  },
  {
    id: "pt-press-ff",
    name: "fast forward",
    skipAction: ({ animationFrames }) => {
      dispatch({ type: "next-frame" })
    }, // when you skip over
    currentAction: function* () {
      yield pressButton("top-controls-fast-forward")
      dispatch({ type: "next-frame" })
    },
  },
  {
    id: "pt-press-rewind",
    name: "rewind",
    skipAction: () => {
      stopLoop()
      dispatch({ type: "prev-frame" })
    }, // when you skip over
    currentAction: function* () {
      yield pressButton("top-controls-rewind")
      dispatch({ type: "next-frame" })
    },
  },
  {
    id: "seek-to-before",
    name: "seek to indexed by current",
    inputView: (eventDetails) => {
      return seekToInputView({ eventDetails, delta: 0 })
    },

    skipAction: ({ eventDetails, animationFrames }) => {
      return seekToSkipAction({ eventDetails, animationFrames, delta: 0 })
    },
    currentAction: seekToCurrentAction(0),
  },
  {
    id: "seek-to-after",
    name: "seek to indexed by previous",
    inputView: (eventDetails) => {
      return seekToInputView({ eventDetails, delta: -1 })
    },

    skipAction: ({ eventDetails, animationFrames }) => {
      return seekToSkipAction({ eventDetails, animationFrames, delta: 1 })
    },
    currentAction: seekToCurrentAction(1),
  },
  {
    id: "open-menu",
    name: "open menu",
    skipAction: () => {
      dispatch({ type: "navigation-viewable", viewable: false })
      dispatch({ type: "set-menu-expanded", expanded: true })
    },
    currentAction: function* () {
      yield pressButton("toggle-main-menu")
      dispatch({ type: "set-menu-expanded", expanded: true })
    },
  },
  {
    id: "close-menu",
    name: "close menu",
    skipAction: () => {
      dispatch({ type: "set-menu-expanded", expanded: false })
    },
    currentAction: function* () {
      yield pressButton("toggle-main-menu")
      dispatch({ type: "set-menu-expanded", expanded: false })
    },
  },
  {
    id: "toggle-ex-selection",
    name: "exercise selection",
    skipAction: () => {
      dispatch({ type: "navigation-viewable", viewable: true })
    },
    currentAction: function* () {
      yield pressButton("toggle-ex-selection")
      dispatch({ type: "navigation-viewable", viewable: true })
    },
  },
  {
    id: "open-save-progress",
    name: "open save progress",
    skipAction: () => {
      dispatch({ type: "save-progress", viewable: true })
    },
    currentAction: function* () {
      yield pressButton("save-progress")
      dispatch({ type: "save-progress", viewable: true })
    },
  },
  {
    id: "close-save-progress",
    name: "close save progress",
    skipAction: () => {
      dispatch({ type: "close-save-progress-window", viewable: true })
    },
    currentAction: function* () {
      yield pressButton("close-save-progress")
      dispatch({ type: "close-save-progress-window", viewable: true })
    },
  },
  {
    id: "pt-breakpoint",
    name: "breakpoint",
    inputView: (eventDetails) => {
      return <PTBreakpoint eventDetails={eventDetails} />
    },
    skipAction: ({ eventDetails }) => {
      if (eventDetails.sourceFileLineNumber) {
        dispatch({ type: "toggle-breakpoint", row: eventDetails.sourceFileLineNumber - 1 })
      }
    },
    currentAction: function* ({ eventDetails }) {
      if (!eventDetails.sourceFileLineNumber) return
      yield pressButton("breakpoint-" + eventDetails.sourceFileLineNumber)
      dispatch({ type: "toggle-breakpoint", row: eventDetails.sourceFileLineNumber - 1 })
    },
  },
  {
    id: "pt-press-skip-to-breakpoint",
    name: "next breakpoint",
    skipAction: ({ animationFrames, frameIndex, breakpoints }) => {
      nextBreakpointButton(frameIndex, animationFrames, breakpoints)
    }, // when you skip over
    currentAction: function* ({ animationFrames, frameIndex, breakpoints }) {
      yield pressButton("top-controls-skip-to-breakpoint")
      nextBreakpointButton(frameIndex, animationFrames, breakpoints)
    },
  },
]

let ticksOfTutorialDescription = (eventDescriptions, tdesc) => {
  let segmentDescriptions = store.getState().content.segmentDescriptions
  let ret = []
  let segments = Object.values(tdesc.trackSegments)
  for (let i = 0; i < segments.length; i++) {
    let segment = segments[i]
    let deets = segmentDescriptions[segment.id]
    if (!deets) continue
    for (let j = segment.from; j <= segment.to; j++) {
      if (!ret[j]) ret[j] = { segments: [] }
      ret[j].segments.push(deets)
    }
  }
  for (let i = 0; i < tdesc.eventOrder.length && i < tdesc.numRows; i++) {
    let eventId = tdesc.eventOrder[i]
    if (!eventId) continue
    ret[i] = ret[i] || { segments: [] }
    let deets = eventDescriptions[eventId]
    ret[i].event = deets
  }
  return ret
}

export let pauseTutorialButton = () => {
  dispatch({ type: "tutorial-stopped" })
  stopLoop()
}

let playTicks = (precomputes, ticks, startingIndex) => {
  console.log("starting playticks")
  if (!precomputes) throw new Error("what")
  let { turtleGoal, turtleAnimationFrames, animationFrames } = precomputes
  dispatch({ type: "set-active-segments", segments: [] }) // remove all highlights
  dispatch({ type: "voice-script", script: undefined, definitions: undefined })
  for (let i = 0; i < startingIndex; i++) {
    let eventDetails = ticks[i]?.event
    let eventId = eventDetails?.id
    if (!eventId) continue

    let d = tutorialEventsData.find((x) => x.id === eventDetails.type)
    if (!d.skipAction) debugger
    let action = d.skipAction
    if (action)
      action({
        turtleGoal,
        turtleAnimationFrames,
        animationFrames,
        eventDetails,
        frameIndex: store.getState().frameIndex,
        breakpoints: store.getState().breakpoints,
      })
  }
  function* run() {
    stopAudio()
    dispatch({ type: "set-play-tutorial-length", length: ticks.length })
    for (let i = startingIndex; i < ticks.length; i++) {
      if (!ticks[i]) debugger
      if (!ticks[i].event) continue
      dispatch({ type: "set-play-tutorial-index", index: i })

      let tickDetails = ticks[i]
      if (!tickDetails) continue

      let segmentDetails = tickDetails.segments
      dispatch({ type: "set-active-segments", segments: segmentDetails })
      let eventDetails = ticks[i]?.event

      let action = tutorialEventsData.find((x) => x.id === eventDetails.type).currentAction

      if (!action) debugger

      let startTime = Date.now()
      yield action({
        eventDetails,
        turtleAnimationFrames,
        turtleGoal,
        animationFrames,
        frameIndex: store.getState().frameIndex,
        breakpoints: store.getState().breakpoints,
      })
      console.log("time it took: ", Date.now() - startTime)

      let nextEventType = ticks?.[i]?.event.type

      //if (nextEventType === "voice") debugger
      if (nextEventType === "voice" && eventDetails.type === "voice") {
      } else {
        yield tunedDelay(800)
      }
      // let eventId = eventDetails?.id
      // if (!eventId) continue
      // let d = tutorialEventsData.find((x) => x.id === eventDetails.type)
      // if (!d) throw new Error("huh")
      // if (!eventDetails.type) debugger
      // console.log("event details MOO:", eventDetails)
      // yield d.currentAction({ turtleGoal, turtleAnimationFrames, animationFrames, eventDetails })
    }

    stopLoop()
  }
  stopLoop()
  setTutorialLoop(drive(run()))
}

let tutorialPlay = (precomputes) => {
  dispatch({ type: "navigation-viewable", viewable: false })
  dispatch({ type: "set-menu-expanded", expanded: false })
  let s = store.getState()
  const tdesc = s.currentView?.exercise && s.content.tutorialDescriptions?.[s.currentView.exercise]
  if (!tdesc) throw new Error("huh")
  let currentIndex = store.getState().playTutorialIndex || 0
  dispatch({ type: "unset-tutorial-focus" }) // this is when I'm testing it and im focused on a tutorial element
  dispatch({ type: "reset-exercise" })
  let ticks = ticksOfTutorialDescription(s.content.eventDescriptions, tdesc)
  playTicks(precomputes, ticks, currentIndex)
}

export let adminPlayTutorialFromBeginningButton = () => {
  dispatch({ type: "set-play-tutorial-index", index: 0 })
  let precomputes = getTutorialPrecomputes(store.getState().content, store.getState().currentView)
  tutorialPlay(precomputes)
}

let getRowOfSelectedEvent = () => {
  let s = store.getState()
  let focus = s.tutorialFocus

  if (focus?.type !== "event") return undefined
  const tdesc = s.currentView?.exercise && s.content.tutorialDescriptions?.[s.currentView.exercise]

  if (!tdesc) return undefined
  let ind = tdesc.eventOrder.indexOf(focus.id)
  return ind
}

export let adminPlayTutorialFromSelectedButton = () => {
  let rowIndex = getRowOfSelectedEvent()
  if (!rowIndex === undefined) {
    alert("no event selected")
    return
  }
  dispatch({ type: "set-play-tutorial-index", index: rowIndex })
  let precomputes = getTutorialPrecomputes(store.getState().content, store.getState().currentView)
  tutorialPlay(precomputes)
}

export let userPlayTutorialButton = () => {
  let precomputes = store.getState().tutorialPrecomputes

  tutorialPlay(precomputes)
}

export let stopTutorialButton = () => {
  stopLoop()
}

let seekToRow = (precomputes, rowIndex) => {
  let { turtleGoal, turtleAnimationFrames, animationFrames } = precomputes
  if (animationFrames.length === 0) debugger
  //playTutorialButtonAction(precomputes)
  dispatch({ type: "set-active-segments", segments: [] }) // remove all highlights
  dispatch({ type: "set-breakpoints", breakpoints: [] })

  let s = store.getState()
  const tdesc = s.currentView?.exercise && s.content.tutorialDescriptions?.[s.currentView.exercise]
  if (!tdesc) throw new Error("huh")
  let ticks = ticksOfTutorialDescription(s.content.eventDescriptions, tdesc)
  dispatch({ type: "stop-button" })
  dispatch({ type: "set-play-tutorial-length", length: ticks.length })

  for (let i = 0; i < rowIndex; i++) {
    let eventDetails = ticks[i]?.event
    let eventId = eventDetails?.id
    if (!eventId) continue
    let d = tutorialEventsData.find((x) => x.id === eventDetails.type)
    if (!d) debugger
    let action = d.skipAction
    if (action)
      action({
        turtleGoal,
        turtleAnimationFrames,
        animationFrames,
        eventDetails,
        frameIndex: store.getState().frameIndex,
        breakpoints: store.getState().breakpoints,
      })
  }
  dispatch({ type: "set-play-tutorial-index", index: rowIndex })
}

export let adminTutorialSeekToRow = (rowIndex) => {
  let precomputes = getTutorialPrecomputes(store.getState().content, store.getState().currentView)
  seekToRow(precomputes, rowIndex)
}

export let userTutorialSeekToRow = (rowIndex) => {
  let precomputes = store.getState().tutorialPrecomputes
  dispatch({ type: "reset-exercise" })

  seekToRow(precomputes, rowIndex)
}

export let userTutorialFastForward = () => {
  let s = store.getState()
  stopLoop()
  const tdesc = s.currentView?.exercise && s.content.tutorialDescriptions?.[s.currentView.exercise]
  let currentIndex = store.getState().playTutorialIndex || 0
  let ticks = ticksOfTutorialDescription(s.content.eventDescriptions, tdesc)
  let getNextRowOfInterest = (ind) => {
    for (let i = currentIndex + 1; i < ticks.length; i++) {
      if (ticks[i]?.event?.type === "voice") return i
    }
    return undefined
  }

  let row = getNextRowOfInterest(currentIndex)
  if (row === undefined) return
  userTutorialSeekToRow(row)
}

export let userTutorialRewind = () => {
  let s = store.getState()
  stopLoop()
  const tdesc = s.currentView?.exercise && s.content.tutorialDescriptions?.[s.currentView.exercise]
  let currentIndex = store.getState().playTutorialIndex || 0
  let ticks = ticksOfTutorialDescription(s.content.eventDescriptions, tdesc)
  let getPrev = (ind) => {
    for (let i = ind - 1; i >= 0; i--) {
      if (ticks[i]?.event?.type === "voice") return i
    }
    return undefined
  }
  let row = getPrev(currentIndex)

  if (row === undefined) return
  userTutorialSeekToRow(row)
}
