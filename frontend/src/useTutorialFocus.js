import { useSelector } from "react-redux"
import { store } from "./store"
// export function useTutorialFocus() {
//   const tdesc = useSelector(
//     (s) => s.currentView?.exercise && s.content.tutorialDescriptions?.[s.currentView.exercise]
//   )
//   const eventDescriptions = useSelector((s) => s.content.eventDescriptions)
//   const segmentDescriptions = useSelector((s) => s.content.segmentDescriptions)
//   const focus = useSelector((s) => s.tutorialFocus)
//   let segmentTool = useSelector((s) => s.segmentTool)
//   let focusedEventDetails = focus?.type === "event" && eventDescriptions[focus.id]
//   let focusedSegmentDetails = focus?.type === "segment" && segmentDescriptions[focus.id]

//   return { focusedEventDetails, focusedSegmentDetails, segmentTool, elementId: focus?.id }
// }

export let tutorialFocusInfo = () => {
  let s = store.getState()
  const tdesc = s.currentView?.exercise && s.content.tutorialDescriptions?.[s.currentView.exercise]

  const focus = s.tutorialFocus
  let segmentTool = s.segmentTool

  let focusedEventDetails = focus?.type === "event" && s.content.eventDescriptions
  let focusedSegmentDetails = focus?.type === "segment" && s.content.segmentDescriptions
  return { focusedEventDetails, focusedSegmentDetails, segmentTool, elementId: focus?.id }
}
