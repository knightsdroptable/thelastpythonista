import { store } from "./store.js"
import { getMultiplierOfSpeed } from "./speed.js"
let getCurrentSpeed = () => store.getState().currentSpeed
let audiomemo = {}
export let getAudio = (path) => {
  if (audiomemo[path]) return audiomemo[path]
  let ret = new Audio(path)
  return new Promise((res) => {
    ret.addEventListener("canplaythrough", () => {
      audiomemo[path] = ret
      res(ret)
    })
  })
  // audiomemo[path] = ret
  // return ret
}

export let stopAudio = () => {
  if (audioPlaying) {
    try {
      audioPlaying.pause()
      audioPlaying.currentTime = 0
    } catch (err) {}
    try {
      setEndedEventListener(audioPlaying, undefined)
    } catch (err) {}
  }
  audioPlaying = undefined
}

let audioPlaying = undefined
export let playAudio = (audio, normalSpeed) => {
  let rate = normalSpeed ? 1 : getMultiplierOfSpeed(getCurrentSpeed())

  stopAudio()
  audioPlaying = audio
  audio.currentTime = 0
  audio.playbackRate = rate
  return audio.play()
}

let endedListenerMap = new Map()
let setEndedEventListener = (audio, f) => {
  let previous = endedListenerMap.get(audio)
  if (previous) {
    endedListenerMap.delete(audio)
    audio.removeEventListener("ended", previous)
  }
  endedListenerMap.set(audio, f)
  audio.addEventListener("ended", f)
}

export let playAudioUntilFinished = (audio, normalSpeed) => {
  let rate = normalSpeed ? 1 : getMultiplierOfSpeed(getCurrentSpeed())
  console.log("audio playing ", audioPlaying)
  stopAudio()
  console.log("after stop ")
  return new Promise((res, rej) => {
    audio.currentTime = 0

    setEndedEventListener(audio, res)

    audio.playbackRate = rate
    audioPlaying = audio

    audio.play()
  })
}
