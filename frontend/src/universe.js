import { isPlainObject } from "immer/dist/internal"

let replace = (a, b) => {
  if (a === b) return
  let keys = Object.keys(a)
  for (let i = 0; i < keys.length; i++) {
    delete a[keys[i]]
  }
  Object.assign(a, b)
  notifyChanged(a)
}

let setNonToken = (arr, index, e) => {
  let current = 0
  for (let i = 0; i < arr.length; i++) {
    let candidate = arr[i]
    if (candidate.category !== "token") {
      if (current === index) {
        if (arr[i] === e) return
        arr[i] = e
        notifiedChanged(arr[i])
        return
      }
      current++
    }
  }
  throw myBad("index out of range" + index)
}

let notifiedChanged = (obj) => {
  obj.__changed = true
  if (obj.__parent) notifiedChanged(obj.__parent)
}

let popContext = (u) => {
  if (u.contexts.length === 1) throw myBad(undefined, "popping global context")
  u.contexts.pop()
  notifiedChanged(u.contexts)
}

let push = (arr, e) => {
  arr.push(e)
  notifiedChanged(arr)
}

let papafy = (input) => {
  let doit = (e) => {
    if (isObject(e)) doitObj(e)
    if (Array.isArray(e)) doitArr(e)
    return
  }
  let doitObj = (e) => {
    let keys = Object.keys(e)
    for (let i = 0; i < keys.length; i++) {
      let k = keys[i]
      let ch = e[k]
      ch.__parent = e
      doit(ch)
    }
  }
  let doitArr = (e) => {
    for (let i = 0; i < e.length; i++) {
      let ch = e[i]
      ch.__parent = e
      doit(ch)
    }
  }
}

let createUniverse = () => {
  return {
    userErrorMsg: undefined,
    turtle: {
      pendown: true,
      angle: 0,
      turtleX: 0,
      turtleY: 0,
      lines: [],
      processing: false,
    },
    heap: [],
    frameHeaven: [],
    contexts: [
      {
        label: mkToken(" context "),
        gobackMarker: undefined,
        id: "global-context",
        isolatedExpression: undefined,
        nonlocals: [],
        globals: [],
        loops: [],
        currentlyDefining: [],
        nonlocalToken: mkToken("nonlocal: "),
        globalToken: mkToken("global: "),

        frame: {
          emptyToken: mkToken("empty frame"),
          id: "global-frame",
          label: mkToken("frame"),
          idToken: mkToken("global-frame", { lineMarker: true }),
          closure: null,
          rows: [],
          closureLabel: mkToken("parent"),
        },
      },
    ],
    externalVars: {
      shown: [],
      rows: [
        {
          name: "Exception",
          value: {
            id: genValueId(),
            category: "value",
            type: "reference",
            referenceType: "class",
            address: "exc",
            children: [mkToken("exc", { ref: true })],
          },
          id: genExternalVarId(),
          leftToken: mkToken("Exception"),
        },
        {
          name: "len",
          func: (args, sourceNode) => {
            if (args.length !== 1) throw userError(sourceNode, "len takes one argument")
            let arg = args[0]
            if (arg.referenceType !== "array") {
              throw userError(sourceNode, "The argument to len should be a list reference")
            }
            let address = arg.address
            let arr = heap.find((x) => x.id === address)
            if (!arr) throw myBad(sourceNode, "could not find object in heap")
            return makeIntValue(arr.rows.length)
          },
          value: {
            id: genValueId(),
            category: "value",
            type: "reference",
            referenceType: "function",
            address: "ln",
            children: [mkToken("ln", { ref: true })],
          },
          id: genExternalVarId(),
          leftToken: mkToken("len"),
        },
        {
          name: "range",
          func: (args, sourceNode) => {
            if (args.length !== 1 && args.length !== 2)
              throw userError(sourceNode, "range takes one or two arguments")
            if (args.some((x) => x.primitiveType !== "int"))
              throw userError(sourceNode, "The arguments to range should be numbers")
            let start, finish
            if (args.length === 1) {
              start = 0
              finish = args[0].value
            } else if (args.length === 2) {
              start = args[0].value
              finish = args[1].value
            }
            let arrayId = genHeapArrayId()
            let values = []
            for (let i = start; i < finish; i++) {
              values.push(makeIntValue(i))
            }
            let arrayInHeap = makeArrayInHeap(arrayId, values, sourceNode)
            heap.push(arrayInHeap)
            snap("range-list-object-created")
            let newValue = {
              id: genArrayRefValueId(),
              category: "value",
              type: "reference",
              referenceType: "array",
              address: arrayId,
              children: [uicp(arrayInHeap.idToken)],
            }
            return newValue
          },
          value: {
            id: genValueId(),
            category: "value",
            type: "reference",
            referenceType: "function",
            address: "rnge",
            children: [mkToken("rnge", { ref: true })],
          },
          id: genExternalVarId(),
          leftToken: mkToken("range"),
        },
      ],
      hiddenTokens: ["stdlib", "\n", " & ", "imported"].map(mkToken),
    },
  }
}
