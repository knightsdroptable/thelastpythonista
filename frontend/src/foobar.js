let isOr = (x) => x && x.___type === "OR"
let makeOr = (lst) => ({
  ___type: "OR",
  children: lst,
})
export let drive = (it, current) => {
  current = current || { canceled: false }
  let done = false
  let cancel = () => {
    if (done) return
    done = true
    current.canceled = true
  }

  const p = new Promise((res, rej) => {
    let thunk = async () => {
      let r = undefined
      while (true) {
        let y = undefined
        try {
          y = it.next(r) // next iteration
        } catch (err) {
          rej(err)
          return
        }
        if (y.done || current.canceled) {
          // it was canceled or the iterator has finished
          done = true
          if (current.canceled) rej(new Error("canceled"))
          else res(undefined)
          return
        }
        r = y.value
        while (r instanceof Promise || isIterator(r) || isOr(r)) {
          if (current.canceled) {
            // it was canceled
            done = true
            rej(new Error("canceled"))
            return
          }
          if (r instanceof Promise) {
            try {
              r = await r
            } catch (err) {
              cancel()
              rej(err)
              return
            }
          } else if (isIterator(r)) {
            r = await drive(r, current).promise
          } else if (isOr(r)) {
            let ret = []
            for (let i = 0; i < r.children; i++) {
              ret.push(drive(r.children[i], current))
            }
            let promises = ret.map((d) => d.promise)
            try {
              r = await Promise.any(promises)
            } catch (err) {
              cancel()
              rej(err)
              return
            }
            ret.forEach((d) => d.cancel())
          }
        }
      }
    }

    thunk()
  })
  return { cancel, isDone: () => done, wasCanceled: () => current.canceled, promise: p }
}
