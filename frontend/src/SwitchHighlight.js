import { CountdownCircleTimer } from "react-countdown-circle-timer"
import React, { useState, useRef, useEffect } from "react"
import produce from "immer"

export default function SwitchHighlight({ prepend, countdown, options, ...other }) {
  let firstCorrect = options.findIndex((x) => !!x[0])
  const [state, setState] = useState({
    index: firstCorrect,
    highlightAnimation: false,
    opacity: 1,
  })

  useEffect(() => {
    let firstCorrect = options.findIndex((x) => !!x[0])
    if (firstCorrect !== state.index) {
      setState((state) =>
        produce(state, (state) => {
          state.highlightAnimation = true
          state.index = firstCorrect
        })
      )
      setTimeout(() => {
        setState((state) =>
          produce(state, (state) => {
            state.highlightAnimation = false
          })
        )
      }, 1000)
    }
  }, [options])
  if (state.index === -1 || state.index >= options.length) return null
  let arg = options[state.index][0]
  if (!arg) return null
  let f = options[state.index][1]
  let flash = options[state.index][2]
  let inner = f(arg)
  let style = {
    height: "100%",
    width: "100%",
    opacity: state.opacity,
    position: "relative",
    ...other.style,
  }
  return (
    <div {...other} className={flash && state.highlightAnimation && "__highlight-animation"} style={style}>
      {countdown !== undefined && (
        <div style={{ position: "absolute", bottom: "5px", right: "5px" }}>
          <CountdownCircleTimer
            size={30}
            strokeWidth={4}
            isPlaying
            duration={countdown}
            colors={[["#004777", 0.33], ["#F7B801", 0.33], ["#A30000"]]}
            onComplete={() => [true, 1000]}
          ></CountdownCircleTimer>
        </div>
      )}
      {prepend}
      {inner}
    </div>
  )
}
