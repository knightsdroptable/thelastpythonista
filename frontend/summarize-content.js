let fs = require("fs")
let content = JSON.parse(fs.readFileSync(`${__dirname}/src/content2.json`))

let processEventId = (evid) => {
  let ev = content.eventDescriptions[evid]
  if (ev.type !== "voice") return
  console.log(ev.id + "\t" + ev.script)
}
let processExerciseId = (exid) => {
  let tutdesc = content.tutorialDescriptions[exid]
  if (!tutdesc) return
  console.log("\n\n\n\n")
  console.log("https://foundationsofpython.com/?exerciseid=" + exid + "\n")
  let events = tutdesc.eventOrder
  events.forEach(processEventId)
}
let processChapterIdDeep = (chid) => {
  let chapter = content.chapters[chid]
  chapter.exerciseOrder.forEach(processExerciseId)
}

let processChapterShallow = (chid) => {
  let chapter = content.chapters[chid]
  chapter.exerciseOrder.forEach((exid) => {
    if (!content.tutorialDescriptions[exid]) return
    console.log("https://foundationsofpython.com/?exerciseid=" + exid)
  })
}

let chapters = content.chapterOrder
chapters.forEach(processChapterShallow)
console.log("\n\n\n\n\n")
chapters.forEach(processChapterIdDeep)
