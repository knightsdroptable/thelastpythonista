let fs = require("fs")
let content = JSON.parse(fs.readFileSync(`${__dirname}/src/content2.json`))
let allExercises = new Set()
content.chapterOrder.forEach((c) => content.chapters[c].exerciseOrder.forEach((e) => allExercises.add(e)))
let tdescKeys = Object.keys(content.tutorialDescriptions)
tdescKeys = tdescKeys.filter((x) => allExercises.has(x))
let tdescs = tdescKeys.map((x) => content.tutorialDescriptions[x])
let eventDescriptions = content.eventDescriptions
let resources = []
let counter = 0
let genName = () => {
  counter++
  return "_n" + counter
}
tdescs.forEach((tdesc) => {
  if (!tdesc.eventOrder) throw new Error("huh")
  if (tdesc.numRows === undefined) throw new Error("what")
  let eventKeys = tdesc.eventOrder.slice(0, tdesc.numRows)
  let events = eventKeys.map((x) => eventDescriptions[x])

  events.forEach((ev, ind) => {
    if (!ev) return
    if (ev.type === "voice" && ev.resourceLocation) {
      resources.push(ev.resourceLocation)
    }
  })
  let segments = Object.values(tdesc.trackSegments)
  let segmentIds = segments.map((x) => x.id)
  let segmentDetails = segmentIds.map((x) => content.segmentDescriptions[x])
  segmentDetails.forEach((ev) => {
    if (!ev) return
    if (ev.resourceLocation) {
      resources.push(ev.resourceLocation)
    }
  })
})
let fileExists = (path) => {
  return fs.existsSync(path)
}
let moveFile = (oldPath, newPath) => {
  return new Promise((res, rej) => {
    fs.rename(oldPath, newPath, function (err) {
      if (err) throw rej(err)
      else res()
    })
  })
}
let copyFile = (oldPath, newPath) => {
  return new Promise((res, rej) => {
    fs.copyFile(oldPath, newPath, (err) => {
      if (err) rej(err)
      res()
    })
  })
}

let run = async () => {
  for (let i = 0; i < resources.length; i++) {
    let resource = resources[i]
    let loc1 = `${__dirname}/src/media-resources/${resource}`
    let loc2 = `${__dirname}/public/temp-resources/${resource}`
    if (fileExists(`${__dirname}/src/media-resources/${resource}`)) continue
    if (!fileExists(`${__dirname}/public/temp-resources/${resource}`))
      throw new Error("missing resource " + resource)
    await copyFile(loc2, loc1)
  }

  let idOfResource = {}
  let resourceOfId = {}

  for (let i = 0; i < resources.length; i++) {
    let resource = resources[i]
    let id = genName()
    idOfResource[resource] = id
    resourceOfId[id] = resource
  }

  let generatedSource = ""
  for (let i = 0; i < resources.length; i++) {
    let resource = resources[i]
    generatedSource += `import ${idOfResource[resource]} from './media-resources/${resource}'`
    generatedSource += "\n"
  }

  let innerMap = ""
  for (let i = 0; i < resources.length; i++) {
    let resource = resources[i]
    innerMap += `"${resource}":${idOfResource[resource]},`
  }
  generatedSource += `let assoc = {${innerMap}}`
  generatedSource += `\n`
  generatedSource += `export let getResourceURL = str =>  assoc[str] || (process.env.PUBLIC_URL + "/temp-resources/" + str)`
  fs.writeFileSync(`${__dirname}/src/resources.js`, generatedSource)
}

run()
