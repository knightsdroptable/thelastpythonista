let fs = require("fs")
let content = JSON.parse(fs.readFileSync(`${__dirname}/src/content2.json`))

let processEventId = (evid) => {
  let ev = content.eventDescriptions[evid]
  if (ev.type !== "voice") return
  let rl = ev.resourceLocation
  if (!rl) console.log("missing", rl.id)
}
let processExerciseId = (exid) => {
  let tutdesc = content.tutorialDescriptions[exid]
  if (!tutdesc) return
  let events = tutdesc.eventOrder
  events.forEach(processEventId)
}
let processChapterId = (chid) => {
  let chapter = content.chapters[chid]
  chapter.exerciseOrder.forEach(processExerciseId)
}
let chapters = content.chapterOrder
chapters.forEach(processChapterId)
