const ffprobe = require("ffprobe")
let ffprobeStatic = require("ffprobe-static")
let ffmpeg = require("ffmpeg")
let cors = require("cors")
let express = require("express")
let app = express()
let multer = require("multer")
var fs = require("fs")
let util = require("util")
app.use(cors())
let mv = (oldPath, newPath) => {
  return new Promise((res, rej) => {
    fs.rename(oldPath, newPath, function (err) {
      if (err) throw rej(err)
      else res()
    })
  })
}

let upload = multer({
  dest: __dirname + "/uploads/",
})

var execute = function (command, callback) {
  require("child_process").exec(command, { maxBuffer: 1024 * 500 }, function (error, stdout, stderr) {
    callback(error, stdout)
  })
}

const exec = util.promisify(execute)

let convertToMp3 = async (oldFilename) => {
  let root = oldFilename.split(".")[0]
  let newFilename = root + ".mp3"
  let audio = await new ffmpeg(oldFilename)
  await new Promise((res, rej) => {
    audio.fnExtractSoundToMP3(newFilename, (error, file) => {
      if (error) {
        console.log("error", error)
        rej(error)
      }
      res(newFilename)
    })
  })
  await exec(`mv -f ${oldFilename} ${newFilename}`)
  return newFilename
}

let audioFileDuration = async (path) => {
  let ret = await new Promise((res, rej) =>
    ffprobe(path, { path: ffprobeStatic.path }, function (err, info) {
      if (err) {
        rej(err)
        return
      }
      res(parseFloat(info.streams[0].duration))
    })
  )
  if (!ret || isNaN(ret)) {
    console.log("no duration")
    throw new Error("unexpected duration")
  }
  return ret
}

let applyCmd = async (fileName, f) => {
  let root = fileName.split(".")[0]
  let ext = fileName.split(".")[1]
  let tmp = root + "____tmp." + ext
  await exec(f(fileName, tmp))
  await exec(`mv -f ${tmp} ${fileName}`)
}

let durationToTimestamp = (duration) => {
  let minutes = 0
  while (duration > 60) {
    minutes++
    duration -= 60
  }
  minutes = ("" + minutes).padStart(2, "0")
  return `00:${minutes}:${duration.toFixed(3)}`
}

let processWav = async (fileName) => {
  let endTime = await audioFileDuration(fileName)
  if (endTime > 1) endTime -= 0.6
  let endTimestamp = durationToTimestamp(endTime)
  // await applyCmd(
  //   fileName,
  //   (pathIn, pathOut) => `ffmpeg -y -i ${pathIn} -ss 00:00:00 -to ${endTimestamp} -c:a copy ${pathOut}`
  // )
  await applyCmd(
    fileName,
    (pathIn, pathOut) => `ffmpeg -y -i ${pathIn} -af "lowpass=10000,highpass=50" ${pathOut}`
  )

  // await applyCmd(
  //   fileName,
  //   (pathIn, pathOut) => `sox ${pathIn} ${pathOut} noisered ${__dirname}/tmp/noise.prof 0.21`
  // )

  await applyCmd(
    fileName,
    (pathIn, pathOut) =>
      `ffmpeg -y -i ${pathIn} -af "compand=attacks=0:points=-60/-55|-18/-10|0/-5" ${pathOut}`
  )
}

app.post("/upload-resource", upload.single("file"), async (req, res) => {
  let file = req.file
  let extension = file.originalname.split(".").pop()
  if (!extension || extension.length < 2)
    res.send(JSON.stringify({ success: false, reason: "missing extension" }))
  let newFilename = file.filename + "." + extension

  await mv(
    `${__dirname}/uploads/${file.filename}`,
    `${__dirname}/../frontend/public/temp-resources/${newFilename}`
  )

  res.send(JSON.stringify({ success: true, fileName: newFilename }))
})

app.post("/upload-wav", upload.single("wav"), async (req, res) => {
  let file = req.file
  let extension = "wav"
  let base = file.filename
  let newFilename = base + "." + extension
  await mv(`${__dirname}/uploads/${file.filename}`, `${__dirname}/uploads/${newFilename}`)
  let fullpath = `${__dirname}/uploads/${newFilename}`
  await processWav(fullpath)
  let mp3 = await convertToMp3(fullpath)
  let mp3Filename = mp3.split("/").pop()
  console.log("fullpath", fullpath, mp3Filename)
  let src = `${__dirname}/uploads/${mp3Filename}`

  let dest = `${__dirname}/../frontend/public/temp-resources/${mp3Filename}`
  console.log("dest", dest)

  await mv(src, dest)
  res.send(JSON.stringify({ success: true, fileName: mp3Filename }))
})

app.listen(2777, () => {
  console.log("server started")
})
