let fs = require("fs")
let content = JSON.parse(fs.readFileSync(`${__dirname}/content.json`))
let keys = Object.keys(content)

console.log("keys", keys)
let chapterOrder = content.chapterOrder
let chapters = content.chapters
let exercises = content.exercises
let tutorialDescriptions = content.tutorialDescriptions
let tutorialKeys = Object.keys(tutorialDescriptions)
let allEventDescriptions = {}
let allSegmentDescriptions = {}
tutorialKeys.forEach((tk) => {
  let tdesc = tutorialDescriptions[tk]
  allEventDescriptions = { ...allEventDescriptions, ...tdesc.events }
  allSegmentDescriptions = { ...allSegmentDescriptions, ...tdesc.segmentDetails }
  delete tdesc.events
  delete tdesc.segmentDetails
})
content.eventDescriptions = allEventDescriptions
content.segmentDescriptions = allSegmentDescriptions
fs.writeFileSync(`${__dirname}/content2.json`, JSON.stringify(content, null, 4))
