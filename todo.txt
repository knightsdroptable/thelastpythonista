first tutorial
- This location in computer memory is represented by a number and it is called the address
- By convention, these numbers are written in a numeral system called hexadecimal that has 16 digits instead of 10
- You don't need to know hexadecimal to solve these exercises
- You just need to be able to determine if two numbers written in hexadecimal are the same or different, which you can do by just looking at them
- Unfortunately, there is one big obstacle in our quest to draw a straight line: that location in memory can change every time the interpreter is started
- So it's impossible for us to know the address when we write the code
- To solve this issue, we can write a statement that, when interpreted, gets the address and associates it with a name
- The association between a name and an address in memory is called a variable
- But in this chapter, that data is always an address
- When talking about memory locations, programmers often use the word reference instead of the word address, but they are the same thing
- So we have three ways to say the same thing: location in memory, address and reference

- The first statement will create a variable that contains a reference to the statements that draw a straight line  <-- Add a transition

second tutorial
- This variable contains a reference to a function object that makes the turtle turn right
- The function call [EXPRESSION] is partially evaluated, but now we have to continue the evaluation process


from konstantin:
- Tutorial: the TAB is indenting 8 spaces. It would be nice to be 4
- Tutorial: When a tutorial is finished, you say: "Move on by clicking on the indicated button", but actually no button is indicated.



